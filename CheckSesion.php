<?php
    session_start();
    include("include/include.php");
    include('DAO/DAOInicioSesion.php');
    $daoinicioSesion = new DAOInicioSesion;

    if (($_SESSION["USUA_usuario"] != ''))
    {
        echo $_SESSION["USUA_usuario"];
        header("Location: inicio_menu.php");
    }

    else
    {
        header("Location: index.php");
    }
    
    $temp=$_POST["temp"];
    
    if ($temp == '1')
    {
        $USUA_usuario = $_POST["txt_usuario"];
        $USUA_contrasena = $_POST["txt_contrasena"];
        
        if(($USUA_usuario != "") and ($USUA_contrasena != "") and ($BOD_idBodega != ""))
        {
            $objUsuario = new DTOUsuario
                            ($USUA_id,$BOD_idBodega,$ESTUSUA_idEstado,
                            $PER_id,$UN_id,$USUA_nombre,$USUA_apellidos, 
                            $USUA_usuario,$USUA_contrasena,$USUA_rut, 
                            $USUA_email,$USUA_fax);

            $numeroFilas = $daoinicioSesion->inicioSesion($objUsuario);
            if ($numeroFilas == 1) 
            {

                $arr_datos_usuario = $daoinicioSesion->datosUsuario($objUsuario);

                $_SESSION["USUA_usuario"] = $USUA_usuario;
                $_SESSION['USUA_nombre'] = $arr_datos_usuario['USUA_nombre'];
                header("Location: MenuUsuario.php");
            }

            else
            {
                echo '<script>alert("Los datos ingresados no son v�lidos);</script>';
                echo '<script>window.location="index.php";</script>';
            }
        }

        else
        {
            echo '<script>alert("Los datos ingresados no son v�lidos);</script>';
            echo '<script>window.location="index.php";</script>';
        }
    }
?>