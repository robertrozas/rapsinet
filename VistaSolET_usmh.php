<?php

 session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_SolicitudEvaluacion.php';

    $xajax = new xajax(); 
	   
	  
    $_SESSION['activo'] = "disabled = 'true'";
    $rechazos = array(2,4,6,8);
    $ver = $_GET['ver'];
    $estado = $_SESSION['arrDetsoleva']['estado_sol'];
     if (in_array($estado, $rechazos)&& $ver ==0)
      {
          $_SESSION['activo'] = "";
      }



        
        $xajax->registerFunction("confirmar_evaluacion");
        $xajax->registerFunction("confirmar_ET_primera_llamada");
        $xajax->registerFunction("confirmar_ET_segunda_llamada");
        $xajax->registerFunction("registrar_ET");
	$xajax->registerFunction("solicita");
	$xajax->registerFunction("provincias");
	$xajax->registerFunction("comunas");
	$xajax->registerFunction("hospital");
	$xajax->registerFunction("limpia");
	$xajax->registerFunction("texto");
	$xajax->registerFunction("numero");
	$xajax->registerFunction("validarut");
        $xajax->registerFunction("carga");
        $xajax->registerFunction("rechaza");
        $xajax->registerFunction("observa");
        $xajax->registerFunction("comentario");
        $xajax->registerFunction("lista_adjuntos");
        $xajax->registerFunction("ver_adjunto");
        $xajax->registerFunction("lista_comentarios");
        $xajax->registerFunction("lista_eventos");


	$xajax->processRequests(); 

    $lista = new DAO_SolicitudEvaluacion();

	$_SESSION['Regiones'] = $lista->regiones();


        
  function confirmar_ET_primera_llamada($idCabeceraSolicitud,$form)
    { 
        global $smarty; 
        $xaprobarsolicitud = new xajaxResponse();
        
        $opcion = $form['opcion'];
        
        $xaprobarsolicitud->addAlert($idCabeceraSolicitud);
        $xaprobarsolicitud->addAlert($opcion);
        
        
        $aprobar_solicitud = new DAO_SolicitudEvaluacion();
        $aprobarSolicitud = $aprobar_solicitud->confirmar_ET_primera_llamada($idCabeceraSolicitud, $opcion);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        $xaprobarsolicitud->addScript("opener.xajax_lista_primera_llamada();");
        $xaprobarsolicitud->addScript("opener.xajax_lista_segunda_llamada();");
        
        return $xaprobarsolicitud;  
       
         
       
    }      
    
    
    function confirmar_ET_segunda_llamada($idCabeceraSolicitud,$form)
    { 
        global $smarty; 
        $xaprobarsolicitud = new xajaxResponse();
        
        $opcion = $form['opcion'];
        
        $xaprobarsolicitud->addAlert($idCabeceraSolicitud);
        $xaprobarsolicitud->addAlert($opcion);
        
        
        $aprobar_solicitud = new DAO_SolicitudEvaluacion();
        $aprobarSolicitud = $aprobar_solicitud->confirmar_ET_segunda_llamada($idCabeceraSolicitud, $opcion);
        //$xaprobarsolicitud->addAlert($A);   
        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        $xaprobarsolicitud->addScript("opener.xajax_lista_segunda_llamada();");
        
        return $xaprobarsolicitud;  
       
    }
    
    
    function registrar_ET($idCabeceraSolicitud,$form)
    { 
        global $smarty; 
        $xaprobarsolicitud = new xajaxResponse();
        
        $opcion = $form['opcion'];
        
        $xaprobarsolicitud->addAlert($idCabeceraSolicitud);
        $xaprobarsolicitud->addAlert($opcion);
        
        $aprobar_solicitud = new DAO_SolicitudEvaluacion();
        $aprobarSolicitud = $aprobar_solicitud->registrar_ET($idCabeceraSolicitud, $opcion); 
        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        /*$xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        $xaprobarsolicitud->addScript("opener.xajax_lista_segunda_llamada();");*/
        
        return $xaprobarsolicitud;  
       
    }
        
        
  function lista_comentarios($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        
        $eventos = new DAO_SolicitudEvaluacion();    
        $_SESSION['arrLogSolicitudesComentarios']=$eventos->comentarios_solicitud($idCabeceraSolicitud);
        $tabla = $smarty->fetch('grilla_comentarios.tpl');
        $evento->addAssign("comentarios","innerHTML",$tabla);
        return $evento;
    }


  function lista_eventos($idCabeceraSolicitud)
    {   
       global $smarty;
        $evento = new xajaxResponse();
        
        $eventos = new DAO_SolicitudEvaluacion();    
        $_SESSION['arrLogSolicitudesEvento']=$eventos->eventos_solicitud($idCabeceraSolicitud,2);
        $tabla = $smarty->fetch('grilla_eventos.tpl');
        $evento->addAssign("eventos","innerHTML",$tabla);
        return $evento;

    }


   function ver_adjunto($id_adjunto)
    {
       $ver = new xajaxResponse();
       $ventana = $ver->redi_cierro('veradjunto_soleva.php?clave='.$id_adjunto);

       return $ver; 
    }


  function lista_adjuntos($id)
    {   global $smarty; 
        $adjunto = new xajaxResponse();
        
        $archivos = new DAO_SolicitudEvaluacion();
        $_SESSION['arrAdjuntos']  = $archivos->lista_archivos($id);
       
        $tabla = $smarty->fetch('grilla_adjuntos.tpl');
        $adjunto->addAssign("adjuntos","innerHTML",$tabla);
        return $adjunto;

    }
    
    function confirmar_evaluacion($idCabeceraSolicitud,$form)
    { 
        global $smarty; 
        $xaprobarsolicitud = new xajaxResponse();
        $opcion = $form['opcion'];
        $aprobar_solicitud = new DAO_SolicitudEvaluacion();
        $aprobarSolicitud = $aprobar_solicitud->confirmar_evaluacion($idCabeceraSolicitud, $opcion);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        $xaprobarsolicitud->addScript("opener.xajax_lista_primera_llamada();");
        
        return $xaprobarsolicitud;  

    }

  

  function comentario($cabecera,$comenta)
  {
    $comentario = new xajaxResponse();

    $comentando = new DAO_SolicitudEvaluacion();
    $comentando->agrega_comentario_soleva($cabecera,$comenta);

    $comentario->addAlert("Comentario agregado");
   

    return $comentario;
  }

  function observa($observacion)
  {
    $observa = new xajaxResponse();
    
    $cabecera = $_SESSION['arrDetsoleva']['cab_solevatrat'];
    $estado = $_SESSION['arrDetsoleva']['estado_sol'];
    
    if(empty($observacion)){
      $observa->addAlert("Debe ingresar una observacion!!");
      $observa->addScript("xajax_rechaza();");
    }
    else{  
    $rechazar = new DAO_SolicitudEvaluacion();
    
    $motivo = $_SESSION['observacion_soleva'];
    $observa->addAlert($rechazar->rechazo_soleva($cabecera,$estado,$observacion));
    $observa->addScript("opener.xajax_filtra_solevas('','','');");
    $observa->cierro(1);
     }
    return $observa;
  }

  function carga()
  {
    $carga = new xajaxResponse();
    $region = $_SESSION['arrDetsoleva']['region'];
    $provincia = $_SESSION['arrDetsoleva']['provincia'];
    $sexo = $_SESSION['arrDetsoleva']['sexo_pac'];
    $carga->addScript("$('#region option[value=".$region."]').attr('selected',true);");
    $carga->addScript("xajax_provincias('".$region."');");
    $carga->addScript("xajax_comunas('".$provincia."');");
    $carga->addScript("$('#sexo option[value=".$sexo."]').attr('selected',true);");
    
    return $carga;
  }

	function solicita($form)
	{
		$solicita = new xajaxResponse();

		$visar = new DAO_SolicitudEvaluacion();
		
    $cabecera = $_SESSION['arrDetsoleva']['cab_solevatrat'];
    $estado = $_SESSION['arrDetsoleva']['estado_sol'];
    
      if($_SESSION['activo']=="")//significa que los input estan enabled
      {
        //hago el update
        $solicita->addAlert($visar->actualiza_soleva($cabecera,$form));

      } 
       
       $solicita->addAlert($visar->visado_soleva($cabecera,$estado));
       $solicita->addScript("opener.xajax_filtra_solevas('','','');");
       $solicita->cierro(1);
    
		return $solicita;
	}

	function rechaza()
  {
    $rechaza = new xajaxResponse();
    $rechaza->addScript("nick=prompt('Ingrese Observacion','');
      document.getElementById('observacion').value = nick;document.getElementById('observacion').onchange();");
    return $rechaza;
  }

 function provincias($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_SolicitudEvaluacion(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincia').empty();");
      $provincias->addScript("$('#comuna').empty();");
      $provincias->CreaOpcion('provincia','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincia',$valor['nom_provincia'],$valor['id_provincias']);
      }  
      
      $provincia = $_SESSION['arrDetsoleva']['provincia'];    
      $provincias->addScript("$('#provincia option[value=".$provincia."]').attr('selected',true);");
    return $provincias;
  }


  function comunas($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_SolicitudEvaluacion(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna').empty();");
      $comuna->CreaOpcion('comuna','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      $comunita = $_SESSION['arrDetsoleva']['comuna'];
     
      $comuna->addScript("$('#comuna option[value=".$comunita."]').attr('selected',true);");

      $comuna->addScript("xajax_hospital('".$comunita."');");
      return $comuna;

  }

 function hospital($id_comuna)
 {
 	$hospital = new xajaxResponse();
 	$dao = new DAO_SolicitudEvaluacion();
 	$hospitales = $dao->hospital_comuna($id_comuna);

 	$hospital->addScript("$('#hospital').empty();");
    $hospital->CreaOpcion('hospital','Seleccione Establecimiento','0');
      foreach ($hospitales as $valor) 
      {
      $hospital->CreaOpcion('hospital',$valor['nombre'],$valor['codigo']);
      }  
      $hospitales = $_SESSION['arrDetsoleva']['hospital'];
      $hospital->addScript("$('#hospital option[value=".$hospitales."]').attr('selected',true);");


 	return $hospital;
 }


 function limpia()
 {
 	$limpia = new xajaxResponse();
 	$limpia->addScript("$('#provincia').empty();");
    $limpia->addScript("$('#comuna').empty();");
    $limpia->addScript("$('#hospital').empty();");
 	return $limpia;
 }

  function texto($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-ZáéíóúAÉÍÓÚñÑ\s]/g,'');});");
      return $texto;
    }

   function numero($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^0-9]/g,'');});");
      return $texto;
    }

    function validarut($rut,$nombre_input)
   { 
    $valida = new xajaxResponse();

    $pos = strripos($rut,"-");

    if($pos === false)
    {}
    else
     {
     $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];

    if($digito!=""){
     $s=1;
      for($m=0;$r!=0;$r/=10)
        $s=($s+$r%10*(9-$m++%6))%11;
      $dv = chr($s?$s+47:75);

    if($dv==$digito)
    { }
    else
    { $valida->addAlert("Rut incorrecto, ingrese nuevamente"); 
      $valida->addScript("document.formTrata.".$nombre_input.".focus();");}  }
    
      } 
    return $valida;
    }

      $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
      $smarty->assign('activo', $_SESSION['activo']); 
      $smarty->assign('ver', $ver);
      $smarty->assign('estados', array(2,4,6,8)); 
      $smarty->display('VistaSolET_usmh.tpl');
?>