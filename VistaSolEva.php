<?php
    
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_SolicitudEvaluacion.php';

    $xajax = new xajax(); 
	   
	  
    $_SESSION['activo'] = "disabled = 'true'";
    $ultimo = $_SESSION['arrDetsoleva']['ultimoTipoSolicitud'];
    
    $rechazos = array(2,4,6,8);
    $ver = $_GET['ver'];
    $estado = $_SESSION['arrDetsoleva']['estado_sol'];
    $lista = $_SESSION['arrDetsoleva']['det_opciones'];

    $arrOpciones = explode(',',$lista);
    $OP = "OP";
    $i = 1;
    foreach ($arrOpciones as $opcion)
    {
        switch ($opcion)
        {
            case "A":
            {
                $elige = "checked";
                break;
            }
            case "B":
            {
                $elige = "checked";
                break;
            }
            case "C":
            {
                $elige = "checked";
                break;
            }
            case "":
            {
                $elige = "";
                break;
            }
            
        }
        $smarty->assign($OP.$i,$elige);
        $i++;
    }   
    
     if (in_array($estado, $rechazos)&& $ver ==0 || $ultimo == 7)
      {
          $_SESSION['activo'] = "";
      }



	$xajax->registerFunction("solicita");
	$xajax->registerFunction("provincias");
  $xajax->registerFunction("provincia");
	$xajax->registerFunction("comunas");
  $xajax->registerFunction("comuna");
	$xajax->registerFunction("hospital");
  $xajax->registerFunction("hospital2");
	$xajax->registerFunction("limpia");
	$xajax->registerFunction("texto");
	$xajax->registerFunction("numero");
  $xajax->registerFunction("validarut");
  $xajax->registerFunction("carga");
  $xajax->registerFunction("rechaza");
  $xajax->registerFunction("observa");
  $xajax->registerFunction("comentario");
  $xajax->registerFunction("lista_adjuntos");
  $xajax->registerFunction("ver_adjunto");
  $xajax->registerFunction("lista_comentarios");
  $xajax->registerFunction("lista_eventos");


	$xajax->processRequests(); 

    $lista = new DAO_SolicitudEvaluacion();

	$_SESSION['Regiones'] = $lista->regiones();


  function lista_comentarios($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        
        $eventos = new DAO_SolicitudEvaluacion();    
        $_SESSION['arrLogSolicitudesComentarios']=$eventos->comentarios_solicitud($idCabeceraSolicitud);
        $tabla = $smarty->fetch('grilla_comentarios.tpl');
        $evento->addAssign("comentarios","innerHTML",$tabla);
        return $evento;
    }


  function lista_eventos($idCabeceraSolicitud)
    {   
       global $smarty;
        $evento = new xajaxResponse();
        
        $eventos = new DAO_SolicitudEvaluacion();    
        $_SESSION['arrLogSolicitudesEvento']=$eventos->eventos_solicitud($idCabeceraSolicitud,2);
        $tabla = $smarty->fetch('grilla_eventos.tpl');
        $evento->addAssign("eventos","innerHTML",$tabla);
        return $evento;

    }


   function ver_adjunto($id_adjunto)
    {
       $ver = new xajaxResponse();
       $ventana = $ver->redi_cierro('veradjunto_soleva.php?clave='.$id_adjunto);

       return $ver; 
    }


  function lista_adjuntos($id)
    {   global $smarty; 
        $adjunto = new xajaxResponse();
        
        $archivos = new DAO_SolicitudEvaluacion();
        $_SESSION['arrAdjuntos']  = $archivos->lista_archivos($id);
       
        $tabla = $smarty->fetch('grilla_adjuntos.tpl');
        $adjunto->addAssign("adjuntos","innerHTML",$tabla);
        return $adjunto;

    }

  

  function comentario($cabecera,$comenta)
  {
    $comentario = new xajaxResponse();

    $comentando = new DAO_SolicitudEvaluacion();
    $comentando->agrega_comentario_soleva($cabecera,$comenta);

    $comentario->addAlert("Comentario agregado");
   

    return $comentario;
  }

  function observa($observacion)
  {
    $observa = new xajaxResponse();
    
    $cabecera = $_SESSION['arrDetsoleva']['cab_solevatrat'];
    $estado = $_SESSION['arrDetsoleva']['estado_sol'];
    
    if(empty($observacion)){
      $observa->addAlert("Debe ingresar una observacion!!");
      $observa->addScript("xajax_rechaza();");
    }
    else{  
    $rechazar = new DAO_SolicitudEvaluacion();
    
    
    $observa->addAlert($rechazar->rechazo_soleva($cabecera,$estado,$observacion));
    $observa->addScript("opener.xajax_filtra_solevas('','','');");
    $observa->cierro(1);
     }
    return $observa;
  }

  function carga()
  {
    $carga = new xajaxResponse();
    $region = $_SESSION['arrDetsoleva']['region'];
    $provincia = $_SESSION['arrDetsoleva']['provincia'];
    $sexo = $_SESSION['arrDetsoleva']['sexo_pac'];
    $regiones = ($_SESSION['arrDetsoleva']['reg_sol']=="")? 0:$_SESSION['arrDetsoleva']['reg_sol'];
    $provincias = ($_SESSION['arrDetsoleva']['prov_sol']=="")? 0:$_SESSION['arrDetsoleva']['prov_sol'];
    $comunas = ($_SESSION['arrDetsoleva']['com_sol']);
    $sexo_sol = ($_SESSION['arrDetsoleva']['sexo_sol']);
    $vincula_sol = ($_SESSION['arrDetsoleva']['vincula_sol']);


    $carga->addScript("$('#region option[value=".$region."]').attr('selected',true);");
    $carga->addScript("xajax_provincias('".$region."');");
    $carga->addScript("xajax_comunas('".$provincia."');");
    $carga->addScript("$('#sexo option[value=".$sexo."]').attr('selected',true);");


    $carga->addScript("$('#regiones option[value=".$regiones."]').attr('selected',true);");
    $carga->addScript("xajax_provincia('".$regiones."');");
    $carga->addScript("xajax_comuna('".$provincias."');");
    $carga->addScript("$('#sexo_sol option[value=".$sexo_sol."]').attr('selected',true);");
    $carga->addScript("$('#vincula option[value=".$vincula_sol."]').attr('selected',true);");
    
    return $carga;
  }

	function solicita($form)
	{
		$solicita = new xajaxResponse();

		$visar = new DAO_SolicitudEvaluacion();
		
            $cabecera = $_SESSION['arrDetsoleva']['cab_solevatrat'];
            $estado = $_SESSION['arrDetsoleva']['estado_sol'];
    
      if($_SESSION['activo']=="")//significa que los input estan enabled
      {
        //hago el update
        $solicita->addAlert($visar->actualiza_soleva($cabecera,$form));

      } 
       
       $solicita->addAlert($visar->visado_soleva($cabecera,$estado));
       $solicita->addScript("opener.xajax_filtra_solevas('','','');");
       $solicita->cierro(1);
    
		return $solicita;
	}

    function rechaza()
    {
        $rechaza = new xajaxResponse();
        $rechaza->addScript("nick=prompt('Ingrese Observacion','');
        document.getElementById('observacion').value = nick;document.getElementById('observacion').onchange();");
        return $rechaza;
    }

  function provincias($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_SolicitudEvaluacion(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincia').empty();");
      $provincias->addScript("$('#comuna').empty();");
      $provincias->CreaOpcion('provincia','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincia',$valor['nom_provincia'],$valor['id_provincias']);
      }  
      
      $provincia = $_SESSION['arrDetsoleva']['provincia'];    
      $provincias->addScript("$('#provincia option[value=".$provincia."]').attr('selected',true);");
    return $provincias;
  }


  function comunas($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_SolicitudEvaluacion(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna').empty();");
      $comuna->CreaOpcion('comuna','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      $comunita = $_SESSION['arrDetsoleva']['comuna'];
     
      $comuna->addScript("$('#comuna option[value=".$comunita."]').attr('selected',true);");

      $comuna->addScript("xajax_hospital('".$comunita."');");
      return $comuna;

  }

 function provincia($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_SolicitudEvaluacion(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincias').empty();");
      $provincias->addScript("$('#comunas').empty();");
      $provincias->CreaOpcion('provincias','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincias',$valor['nom_provincia'],$valor['id_provincias']);
      }  
          
      $provincia = $_SESSION['arrDetsoleva']['prov_sol'];    
      $provincias->addScript("$('#provincias option[value=".$provincia."]').attr('selected',true);");
      
    return $provincias;
  }
function comuna($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_SolicitudEvaluacion(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comunas').empty();");
      $comuna->CreaOpcion('comunas','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comunas',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      $comunita = $_SESSION['arrDetsoleva']['com_sol'];
     
      $comuna->addScript("$('#comunas option[value=".$comunita."]').attr('selected',true);");
      return $comuna;

  }


 function hospital($id_comuna)
 {
 	$hospital = new xajaxResponse();
 	$dao = new DAO_SolicitudEvaluacion();
 	$hospitales = $dao->hospital_comuna($id_comuna);

 	$hospital->addScript("$('#hospital').empty();");
    $hospital->CreaOpcion('hospital','Seleccione Establecimiento','0');
      foreach ($hospitales as $valor) 
      {
      $hospital->CreaOpcion('hospital',$valor['nombre'],$valor['codigo']);
      }  
      $hospitales = $_SESSION['arrDetsoleva']['hospital'];
      $hospital->addScript("$('#hospital option[value=".$hospitales."]').attr('selected',true);");


 	return $hospital;
 }
 
 
 function hospital2($id_hospital)
 {
 	$hospital2 = new xajaxResponse();
        $hospital2->addAlert($id_hospital);
 	$dao = new DAO_SolicitudEvaluacion();
 	$hospitales2 = $dao->hospital_2();

 	//$hospital2->addScript("$('#hospital').empty();");
      $hospital2->CreaOpcion('hospital_psi','Seleccione Establecimiento','0');
      foreach ($hospitales2 as $valor) 
      {
      $hospital2->CreaOpcion('hospital_psi',$valor['nombre'],$valor['codigo']);
      }  
      //$hospitales = $_SESSION['arrDetsoleva']['hospital2'];
      $hospital2->addScript("$('#hospital_psi option[value=".$id_hospital."]').attr('selected',true);");


 	return $hospital2;
 }


 function limpia()
 {
 	$limpia = new xajaxResponse();
 	$limpia->addScript("$('#provincia').empty();");
    $limpia->addScript("$('#comuna').empty();");
    $limpia->addScript("$('#hospital').empty();");
 	return $limpia;
 }

  function texto($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-ZáéíóúAÉÍÓÚñÑ\s]/g,'');});");
      return $texto;
    }

   function numero($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^0-9]/g,'');});");
      return $texto;
    }

    function validarut($rut,$nombre_input)
   { 
    $valida = new xajaxResponse();

    $pos = strripos($rut,"-");

    if($pos === false)
    {}
    else
     {
     $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];

    if($digito!=""){
     $s=1;
      for($m=0;$r!=0;$r/=10)
        $s=($s+$r%10*(9-$m++%6))%11;
      $dv = chr($s?$s+47:75);

    if($dv==$digito)
    { }
    else
    { $valida->addAlert("Rut incorrecto, ingrese nuevamente"); 
      $valida->addScript("document.formTrata.".$nombre_input.".focus();");}  }
    
      } 
    return $valida;
    }


	  $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->assign('activo', $_SESSION['activo']);
    $smarty->assign('ver', $ver);
    $smarty->assign('estados', $rechazos);
    $smarty->display('VistaSolEva.tpl');

?>
