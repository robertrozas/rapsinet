<?php

require('fpdf.php');
session_start();
    

class PDF extends FPDF
{   
   function Header()
   {
    $this->SetFont('Arial','',10);
    $this->Cell(100);
    $this->Ln(20);
   }  
   
}
    $detalleReporte = $_SESSION['arrResSolET'];
    
    $lista = $detalleReporte[0]['opciones'];
    $arrOpciones = explode(',',$lista);
 
    $textoOpciones = "";
    $textoOpciones2 = "";
    $textoOpciones3 = "";

    $i = 1;
    foreach ($arrOpciones as $opciones)
    {
        switch ($opciones)
        {
            case "A":
            {
                $textoOpciones = "Necesidad de efectuar un diagnostico o evaluaci�n cl�nica que no puede realizarse en forma ambulatoria.";
                break;
            }
            case "B":
            {
                $textoOpciones2 = " Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.";
                break;
            }
            case "C":
            {
                $textoOpciones3 = " Que el estado o condici�n ps�quica o conductual de la persona representa un riesgo de da�o f�sico, ps�quico o psicosocial inminente, para s� mismo o para terceros.";
                break;
            }
            /*case "":
            {
                $textoOpciones = "";
                break;
            }*/
            
        }
        $i++;
    }

    
    $pdf=new PDF('P','mm','legal');
    $pdf->Footer();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");

    $pdf->SetY(35);       
    $pdf->SetX(125);
    $pdf->SetFont('Arial','B',10);
    $texto = "RESOLUCION N� ".$detalleReporte[0]['codigo']." \n";
    $texto .= "VALPARAISO,";
    $pdf->MultiCell(60, 4,$texto, 0, 'L');
    $pdf->SetFont('Arial','',10);
    $pdf->SetY(85);
    $pdf->SetX(41);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                         VISTOS:    "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(85);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto  = "                                                                       1.- La solicitud ".$detalleReporte[0]['codigo'].", por al que solicita la atenci�n psiqu�atrica para ".$detalleReporte[0]['nombrecompleto_pac']." de fecha ".$detalleReporte[0]['fecha_actual']."; 2.- Formulario para m�dicos psiquiatras (Solicitud de Internaci�n Administrativas) de Dr. ".$detalleReporte[0]['nombre_medico']." ";
    $texto  .= " de PAI Dr. Salvador Allende, Serpaj - Quillota, de fecha ".$detalleReporte[0]['fecha_evaluacion']."; 3.- Informe de Dr. ".$detalleReporte[0]['nombre_medico'].", Coordinadora de PAI Dr Salvador Allende, Serpaj - Quillota, de fecha ".$detalleReporte[0]['fecha_evaluacion']."; 4.- Antecedentes recibidos en el Departamento de Salud Publica de la Secretaria regional de Salud de la Regi�n de Valpara�so el ".$detalleReporte[0]['fecha_actual'].";";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(112);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        CONSIDERANDO:  "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(112);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                    Que el estado en que se encuentra ".$detalleReporte[0]['nombrecompleto_pac']." hace necesario su tratamiento para recuperar su salud y evitar conflictos con terceros, finalidad que persigue la internaci�n administrativa en un establecimiento asistencial; y ".$textoOpciones.", ".$textoOpciones2.", ".$textoOpciones3."";
    $texto .= " ";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(143);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        TENIENDO PRESENTE:"; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(143);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                               Lo dispuesto en los art�culos 5 y 130 y siguientes del C�digo Sanitario; en el DL N� 2763/79";
    $texto .= " modificado por la ley 19.937; en el Decreto Supremo N� 136/2004 que aprueba Reglamento Org�nico del Ministerio de";
    $texto .= " Salud; en los art�culos 13 y 14 del Decreto Supremo N� 570/98 del Ministro de Salud que aprueba el Reglamento para la internaci�n de las personas con enfermedades mentales y sobre los Establecimientos que las proporcionan y en uso de las facultades que me confiere el Decreto Supremo N�47 de 24 de marzo de 2010 del Ministario de Salud, dicto la siguiente:";

    
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(177);       
    $pdf->SetX(90);
    $pdf->SetFont('Arial','B',11); 
    $texto = "RESOLUCI�N";
    $pdf->MultiCell(30, 4,$texto, 0, 'J');

    $pdf->SetY(187);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10); 
    $texto = "1�. REMITASE LOS ANTECEDENTES AL SR. SECRETARIO REGIONAL MINISTARIAL DE SALUD DE LA REGION METROPOLITANA, a objeto que disponga la Evaluaci�n y tratamiento de ".$detalleReporte[0]['nombrecompleto_pac'].", con direcci�n en ".$detalleReporte[0]['dire_pac']." Limache en el ".$detalleReporte[0]['establecimiento_int']." debiendo cumplirse la medida de traslado por el establecimiento de salud que solicita la Internaci�n Administrativa.";

    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(207);       
    $pdf->SetX(25);
    $texto = "2�. AUTORICESE Y EFECTUESE EL TRASLADO ADMINISTRATIVO del paciente ".$detalleReporte[0]['nombrecompleto_pac']." hasta el destino precedentemente se�alado.";

    $pdf->MultiCell(170, 4,$texto, 0, 'J');
            
    $pdf->SetY(220);       
    $pdf->SetX(25);
    $texto = "3�. Lo dispuesto en el punto 1�, deber� materializarse dentro de los 180 d�as h�biles cointados desde la fecha de la presente resoluci�n, de lo contrario, deber� solicitarse una nueva orden a la Autoridad Sanitaria.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(245);       
    $pdf->SetX(70);
    $pdf->SetFont('Arial','B',10); 
    $texto = "AN�TESE, COMUN�QUESE Y C�MPLASE.";
    $pdf->MultiCell(73, 4,$texto, 0,
            'J');
    
    $pdf->SetY(275);       
    $pdf->SetX(95);
    
    $pdf->SetFont('Arial','B',10);
    
    $texto = "D. JAIME JAMMET ROJAS \n";
    $texto .= "SECRETARIO REGIONAL MINISTERIAL DE SALUD ";
    $texto .= "REGION VALPARAISO";
    $pdf->MultiCell(73, 4,$texto, 0, 'C');
    $pdf->SetFont('Arial','',7);
    $pdf->SetY(303);       
    $pdf->SetX(23);
    $texto = "DISTRIBUCI�N: \n";
    $texto .= "-    Solicitante/Interesado \n";
    $texto .= "-    SEREMI Salud Metropolitana \n";
    $texto .= "-    Hospital Sotero del R�o, Santiago \n";
    $texto .= "-    Comisi�n Regional de Protecci�n Personas Afectadas con Enfermedad Mental \n";   
    $texto .= "     OIRS SEREMI Salud Regi�n de Valpara�so \n";
    $texto .= "-    Salud Publica \n";
    $texto .= "-    Oficina de Partes";
    $pdf->MultiCell(150, 4,$texto, 0, 'L');
    $pdf->Output();

?>
