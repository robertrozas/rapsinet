<?php
require('fpdf.php');
session_start();


class PDF extends FPDF
{
//Cabecera de página
   function Header()
   {
    //Logo
    $this->Image("Logo.jpg" , 10 ,8, 35 , 38 , "JPG" ,"");
    //Arial bold 15
    $this->SetFont('Arial','B',8);
    //Movernos a la derecha
    $this->Cell(80);
    //Título
    $this->Cell(100,10,'Lista Solicitudes Internacion Administrativa sin Rakim Asignado',1,0,'C');
    //Salto de línea
    $this->Ln(20);
      
   }
   
   //Pie de página
   function Footer()
   {
    //Posición: a 1,5 cm del final
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',6);
    //Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
   }
   //Tabla simple
   function TablaSimple($header)
   {
    //Cabecera
    $this->Ln(20);
        $this->Cell(10,5,'ID',1);
        $this->Cell(100,5,'USUARIO',1);
        $this->Cell(30,5,'FECHA',1);
        $this->Cell(50,5,'TIPO',1);
        $this->Cell(35,5,'ESTADO',1);
        
       
        $this->Ln();
    foreach($header as $col)
    {
        $nombre = $col['nombres']." ".$col['apellidos'];
    	  $this->Cell(10,5,$col['codigo'],1);
        $this->Cell(100,5,$nombre,1);
        $this->Cell(30,5,$col['fechaEvaluacion'],1);
        $this->Cell(50,5,$col['tiposolicitud'],1);
        $this->Cell(35,5,$col['estadosolicitud'],1);
       
        $this->Ln();
    }  
     
     
   }
   
   //Tabla coloreada
function TablaColores($header)
{
//Colores, ancho de línea y fuente en negrita
$this->SetFillColor(255,0,0);
$this->SetTextColor(255);
$this->SetDrawColor(128,0,0);
$this->SetLineWidth(.3);
$this->SetFont('','B');
//Cabecera

for($i=0;$i<count($header);$i++)
$this->Cell(40,7,$header[$i],1,0,'C',1);
$this->Ln();
//Restauración de colores y fuentes
$this->SetFillColor(224,235,255);
$this->SetTextColor(0);
$this->SetFont('');
//Datos
   $fill=false;
$this->Cell(40,6,"hola",'LR',0,'L',$fill);
$this->Cell(40,6,"hola2",'LR',0,'L',$fill);
$this->Cell(40,6,"hola3",'LR',0,'R',$fill);
$this->Cell(40,6,"hola4",'LR',0,'R',$fill);
$this->Ln();
      $fill=!$fill;
      $this->Cell(40,6,"col",'LR',0,'L',$fill);
$this->Cell(40,6,"col2",'LR',0,'L',$fill);
$this->Cell(40,6,"col3",'LR',0,'R',$fill);
$this->Cell(40,6,"col4",'LR',0,'R',$fill);
$fill=true;
   $this->Ln();
   $this->Cell(160,0,'','T');
}

   
   
}

$pdf=new PDF('L');
//Títulos de las columnas
//$header=array('Columna 1','Columna 2','Columna 3','Columna 4');
$pdf->AliasNbPages();
//Primera página
$pdf->AddPage();
//$pdf->SetY(65);
//$pdf->AddPage();
$pdf->TablaSimple($_SESSION['arrSolicitudes']);
//Segunda página
//$pdf->AddPage();
//$pdf->SetY(65);
//$pdf->TablaColores($header);
$pdf->Output();


?>