<?php

require('fpdf.php');
session_start();
    

class PDF extends FPDF
{   
   function Header()
   {
    //Logo
    //$this->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");
    //Arial bold 15
    $this->SetFont('Arial','',10);
    //Movernos a la derecha
    $this->Cell(100);
    //Título
    /*$this->Cell(100,10,'Resolucion Internacion Administrativa',1,0,'C');*/
    //Salto de línea
    $this->Ln(20);
   }  
   
}
    $detalleReporte = $_SESSION['arrResSolINV'];
    
    $lista = $detalleReporte[0]['opciones'];
    $arrOpciones = explode(',',$lista);
 
    $textoOpciones = "";
    $textoOpciones2 = "";
    $textoOpciones3 = "";

    $i = 1;
    foreach ($arrOpciones as $opciones)
    {
        switch ($opciones)
        {
            case "A":
            {
                $textoOpciones = "AAAAAAAAAA";
                break;
            }
            case "B":
            {
                $textoOpciones2 = " BBBBBBBBBB";
                break;
            }
            case "C":
            {
                $textoOpciones3 = " CCCCCCCCCC";
                break;
            }
            /*case "":
            {
                $textoOpciones = "";
                break;
            }*/
            
        }
        $i++;
    }

    
    $pdf=new PDF('P','mm','legal');
    $pdf->Footer();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");

    $pdf->SetY(35);       
    $pdf->SetX(125);
    $pdf->SetFont('Arial','B',10);
    $texto = "RESOLUCION N� ".$detalleReporte[0]['codigo']. " \n";
    $texto .= "VALPARAISO,";
    $pdf->MultiCell(60, 4,$texto, 0, 'L');
    $pdf->SetFont('Arial','',10);
    $pdf->SetY(90);
    $pdf->SetX(41);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                         VISTOS:    "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(90);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto  = "                                                                       La solicitud de Internaci�n Administrativa N�00 del hospital del Salvador Valpara�so / Hospital Psiqui�trico Philippe Pinel de Putaendo,";
    $texto .= " presentada por M�dico Psiquiatra Dr. ".$detalleReporte[0]['nombre_medico'].", con fecha ".$detalleReporte[0]['fecha_actual'].", en la que solicita la internaci�n administrativa de ".$detalleReporte[0]['nombre_paciente'].", cedula de identidad ".$detalleReporte[0]['rut_paciente'].", actualmente hospitalizada / hospitalizado en ese establecimiento desde el xxxxx,";
    $texto .= " la cual es recepcionada v�a e-mail / fax en la Secretar�a Regional Ministerial de Salud de la Regi�n de Valpara�so el ".$detalleReporte[0]['fecha_actual'].";";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(120);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        CONSIDERANDO:  "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(120);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                      Que el estado en el que se encuentra ".$detalleReporte[0]['nombre_paciente'].", c�dula de identidad ".$detalleReporte[0]['rut_paciente'].", hace necesario su tratamiento";
    $texto .= " para recuperar su salud y evitar conflictos con terceros, finalidad que persigue la internaci�n administrativa en un establecimiento asistencial; y, que seg�n costa en los antecedentes que se adjuntan, especialmente el informe m�dico, aparece de manifiesto en el caso de ".$detalleReporte[0]['nombre_paciente']."., ".$textoOpciones.", ".$textoOpciones2.",".$textoOpciones3."";
    $texto .= " ";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(145);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        TENIENDO PRESENTE:"; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(145);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                               Lo dispuesto en los art�culos 5 y 130 y siguientes del C�digo Sanitario; en el DL N� 2763/79";
    $texto .= " modificado por la ley 19.937; en el Decreto Supremo N� 136/2004 que aprueba Reglamento Org�nico del Ministerio de";
    $texto .= " m�dico de xxxxx, de Consultorio xxxxx, de xxxxx 3.- El certificado de antecedentes de la especialidad de Dr. Ra�l C�rdenas, Director (S) del Hospital Del Salvador,";
    $texto .= " Salud; en los art�culos 13 y 14 del Decreto Supremo N� 570/98 del Ministro de Salud que aprueba el Reglamento para";
    $texto .= " las facultades que me confiere el Decreto Supremo N� 47 de 24 de Marzo 2010 del Ministerio de Salud, dicto la siguiente:";

    
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(180);       
    $pdf->SetX(90);
    $pdf->SetFont('Arial','B',10); 
    $texto = "RESOLUCI�N";
    $pdf->MultiCell(30, 4,$texto, 0, 'J');

    $pdf->SetY(190);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10); 
    $texto = "1�. Habi�ndose procedido con una internaci�n de urgencia no voluntaria, DISP�NGASE LA INTERNACI�N ADMINISTRATIVA, de ".$detalleReporte[0]['nombre_paciente'].", c�dula de identidad ".$detalleReporte[0]['rut_paciente'].", actualmente";
    $texto .= " hospitalizada / hozpitalizado para su tratamiento en el Hospital del Salvador Valpara�so / Hospital Psiqui�trico Philippe Pinel de Putaendo, especializado en atenci�n psiqui�trica.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(210);       
    $pdf->SetX(25);
    $texto = "2�. EL DIRECTOR DEL Hospital del Salvador Valpara�so / Hopital Psiqui�trico Philippe Pinel de Putaendo, queda por esta misma Resoluci�n autorizado para otorgar el alta en la fecha en que se complete el diagn�stico";
    $texto .= " y tratamiento del afectado / afectada, debiendo notificar, inmediatamente el egreso y plan de tratamiento post-alta a la Secretar�a Regional Ministerial de Salud Regi�n Valpara�so, para dictar la Resoluci�n Alta.";
    $texto .= " cumplimiento de esta resoluci�n.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(245);       
    $pdf->SetX(70);
    $pdf->SetFont('Arial','B',10); 
    $texto = "AN�TESE, COMUN�QUESE Y C�MPLASE.";
    $pdf->MultiCell(73, 4,$texto, 0, 'J');
    
    $pdf->SetY(285);       
    $pdf->SetX(95);
    
    $pdf->SetFont('Arial','B',10);
    
    $texto = "D. JAIME JAMMET ROJAS \n";
    $texto .= "SECRETARIO REGIONAL MINISTERIAL DE SALUD ";
    $texto .= "REGION VALPARAISO";
    $pdf->MultiCell(73, 4,$texto, 0, 'C');
    $pdf->SetFont('Arial','',7);
    $pdf->SetY(303);       
    $pdf->SetX(23);
    $texto = "DISTRIBUCI�N: \n";
    $texto .= "-    Solicitante/Interesado \n";
    $texto .= "-    Carabineros de Chile \n";
    $texto .= "-    Hospital del Salvador Valpara�so / Hospital Psiqui�trico Phillippe Pinel de Putaendo / \n";   
    $texto .= "     Hospital Naval Almirante Nef de Vi�a del Mar \n";
    $texto .= "-    Establecimiento: xxxx \n";
    $texto .= "-    Salud Publica \n";
    $texto .= "-    Oficina de Partes";
    $pdf->MultiCell(150, 4,$texto, 0, 'L');
    $pdf->Output();

?>
