<?php

require('fpdf.php');
session_start();
    

class PDF extends FPDF
{   
   function Header()
   {
    //Logo
    //$this->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");
    //Arial bold 15
    $this->SetFont('Arial','',10);
    //Movernos a la derecha
    $this->Cell(100);
    //Título
    /*$this->Cell(100,10,'Resolucion Internacion Administrativa',1,0,'C');*/
    //Salto de línea
    $this->Ln(20);
   }  
   
}
    $detalleReporte = $_SESSION['arrResSolAA'];
    
    $lista = $detalleReporte[0]['opciones'];
    $arrOpciones = explode(',',$lista);
 
    $textoOpciones = "";
    $textoOpciones2 = "";
    $textoOpciones3 = "";

    $i = 1;
    foreach ($arrOpciones as $opciones)
    {
        switch ($opciones)
        {
            case "A":
            {
                $textoOpciones = "AAAAAAAAAA";
                break;
            }
            case "B":
            {
                $textoOpciones2 = " BBBBBBBBBB";
                break;
            }
            case "C":
            {
                $textoOpciones3 = " CCCCCCCCCC";
                break;
            }
            /*case "":
            {
                $textoOpciones = "";
                break;
            }*/
            
        }
        $i++;
    }

    
    $pdf=new PDF('P','mm','legal');
    $pdf->Footer();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");

    $pdf->SetY(35);       
    $pdf->SetX(125);
    $pdf->SetFont('Arial','B',10);
    $texto = "RESOLUCION N� ".$detalleReporte[0]['codigo']." \n";
    $texto .= "VALPARAISO,";
    $pdf->MultiCell(60, 4,$texto, 0, 'L');
    $pdf->SetFont('Arial','',10);
    $pdf->SetY(90);
    $pdf->SetX(41);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                         VISTOS:    "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(90);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto  = "                                                                       1.- El Informe de Notificaci�n de Alta N� para ".$detalleReporte[0]['nombrecompleto_pac']. ", c�dula de identidad ".$detalleReporte[0]['rut_paciente'].", del m�dico tratante ".$detalleReporte[0]['medico_tratante'].", del hospital ".$detalleReporte[0]['centro_salud'].", de fecha ".$detalleReporte[0]['fecha_actual'].";";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(110);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        CONSIDERANDO:  "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(110);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                     Que las circuntancias que hicieron necesaria la interernaci�n psiqui�trica administrativa de ".$detalleReporte[0]['nombrecompleto_pac'].", cedula de identidad ".$detalleReporte[0]['rut_paciente'].", ha sido superadas";
    $texto .= " y que el/la paciente puede continuar su tratamiento en forma ambulatoria, seg�n consta en el Informe del M�dico psiquiatra tratante; y";
    $texto .= " ";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(135);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        TENIENDO PRESENTE:"; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(135);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                               Lo dispuesto en los art�culos 5 y 130 y siguientes del C�digo Sanitario; en el DL N� 2763/79";
    $texto .= " modificado por la ley 19.937; en el Decreto Supremo N� 136/2004 que aprueba Reglamento Org�nico del Ministerio de";
    $texto .= " Salud; en los art�culos 13 y 14 del Decreto Supremo N� 570/98 del Ministro de Salud que aprueba el Reglamento para la internaci�n de las personas con enfermedades mentales y sobre los Establecimientos que las proporcionan y en uso de las facultades que me confiere el Decreto Supremo N�47 de 24 de marzo de 2010 del Ministario de Salud, dicto la siguiente:";

    
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(170);       
    $pdf->SetX(90);
    $pdf->SetFont('Arial','B',11); 
    $texto = "RESOLUCI�N";
    $pdf->MultiCell(30, 4,$texto, 0, 'J');

    $pdf->SetY(180);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10); 
    $texto = "1�. DISP�NGASE el egreso de ".$detalleReporte[0]['nombrecompleto_pac'].", c�dula de identidad ".$detalleReporte[0]['rut_paciente'].", internado en Hospital Psiquiatrico ".$detalleReporte[0]['centro_salud'].", del Servicio de Salud.";

    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(200);       
    $pdf->SetX(25);
    $texto = "2�. D�JESE ESTABLECIDO, que ".$detalleReporte[0]['nombrecompleto_pac'].", c�dula de identidad ".$detalleReporte[0]['rut_paciente'].", deber� continuar su tratamiento en ".$detalleReporte[0]['centro_salud']." Del Hospital de ".$detalleReporte[0]['establecimiento'].", de acuerdo a las indicaciones de tu m�dico tratante.";

    $pdf->MultiCell(170, 4,$texto, 0, 'J');
            
    $pdf->SetY(220);       
    $pdf->SetX(25);
    $texto = "3�. P�NGASE la presente resoluci�n en conocimiento del Hospital Psiqui�trico/unidad de Salud Mental y Psiquiatr�a del Hospital ".$detalleReporte[0]['centro_salud'].", donde el paciente continuar� sus controles, incluida la documentaci�nque le sirve de repaldo.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(245);       
    $pdf->SetX(70);
    $pdf->SetFont('Arial','B',10); 
    $texto = "AN�TESE, COMUN�QUESE Y C�MPLASE.";
    $pdf->MultiCell(73, 4,$texto, 0,
            'J');
    
    $pdf->SetY(275);       
    $pdf->SetX(95);
    
    $pdf->SetFont('Arial','B',10);
    
    $texto = "D. JAIME JAMMET ROJAS \n";
    $texto .= "SECRETARIO REGIONAL MINISTERIAL DE SALUD ";
    $texto .= "REGION VALPARAISO";
    $pdf->MultiCell(73, 4,$texto, 0, 'C');
    $pdf->SetFont('Arial','',7);
    $pdf->SetY(303);       
    $pdf->SetX(23);
    $texto = "DISTRIBUCI�N: \n";
    $texto .= "-    Solicitante/Interesado \n";
    $texto .= "-    Carabineros de Chile \n";
    $texto .= "-    Hospital del Salvador Valpara�so / Hospital Psiqui�trico Phillippe Pinel de Putaendo / \n";   
    $texto .= "     Hospital Naval Almirante Nef de Vi�a del Mar \n";
    $texto .= "-    Establecimiento: xxxx \n";
    $texto .= "-    Salud Publica \n";
    $texto .= "-    Oficina de Partes";
    $pdf->MultiCell(150, 4,$texto, 0, 'L');
    $pdf->Output();

?>
