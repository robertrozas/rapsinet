<?php

require('fpdf.php');
session_start();
    

class PDF extends FPDF
{   
   function Header()
   {
    //Logo
    //$this->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");
    //Arial bold 15
    $this->SetFont('Arial','',10);
    //Movernos a la derecha
    $this->Cell(100);
    //Título
    /*$this->Cell(100,10,'Resolucion Internacion Administrativa',1,0,'C');*/
    //Salto de línea
    $this->Ln(20);
   }  
   
}
    $detalleReporte = $_SESSION['arrResSolInternacionAdmin'];
    
    $lista = $detalleReporte[0]['opciones'];
    $arrOpciones = explode(',',$lista);
 
    $textoOpciones = "";
    $textoOpciones2 = "";
    $textoOpciones3 = "";

    $i = 1;
    foreach ($arrOpciones as $opciones)
    {
        switch ($opciones)
        {
            case "A":
            {
                $textoOpciones = "Necesidad de efectuar un diagnostico o evaluaci�n cl�nica que no puede realizarse en forma ambulatoria.";
                break;
            }
            case "B":
            {
                $textoOpciones2 = " Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.";
                break;
            }
            case "C":
            {
                $textoOpciones3 = " Que el estado o condici�n ps�quica o conductual de la persona representa un riesgo de da�o f�sico, ps�quico o psicosocial inminente, para s� mismo o para terceros.";
                break;
            }
            /*case "":
            {
                $textoOpciones = "";
                break;
            }*/
            
        }
        $i++;
    }
    
     
    $pdf=new PDF('P','mm','legal');
    $pdf->Footer();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->Image("Logo.jpg" , 25 ,30, 34 , 34 , "JPG" ,"");

    $pdf->SetY(35);       
    $pdf->SetX(125);
    $pdf->SetFont('Arial','B',10);
    $texto = "RESOLUCION N� ".$detalleReporte[0]['codigo']. " \n";
    $texto .= "VALPARAISO,";
    $pdf->MultiCell(60, 4,$texto, 0, 'L');
    $pdf->SetFont('Arial','',10);
    $pdf->SetY(90);
    $pdf->SetX(41);
    $espacio = "     ";
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                          VISTOS: "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(90);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto  = "                                                                ".$espacio." 1.- La carta de ".$detalleReporte[0]['nombrecompleto_sol'].", con carnet de identidad ".$detalleReporte[0]['rut_sol'].", por la que solicita la";
    $texto .= " internaci�n/atenci�n psiqui�trica para su ".$detalleReporte[0]['vinculacion']." ".$detalleReporte[0]['nombrecompleto_pac'].", c�dula de identidad ".$detalleReporte[0]['rut_pac'].", fechada el " .$detalleReporte[0]['fecha_actual']. ". 2.- Informe";
    $texto .= " m�dico de Dr. ".$detalleReporte[0]['nombre_medico'].", de Consultorio ".$detalleReporte[0]['establecimiento_sol'].", de ".$detalleReporte[0]['fecha_evaluacion'].". 3.- El certificado de antecedentes de la especialidad de Dr. Ra�l C�rdenas, Director (S) del Hospital Del Salvador,";
    $texto .= " Valpara�so, con ".$detalleReporte[0]['fecha_actual']." 4.- El informe social extendido por Asistente Social ".$detalleReporte[0]['nombrecompleto_ac'].", de";
    $texto .= " De Consultorio ".$detalleReporte[0]['establecimiento_sol'].", de fecha ".$detalleReporte[0]['fecha_evaluacion']." 5.- Registro de Atencion de Usuarios de la SEREMI de Salud Regi�n";
    $texto .= " Valpara�so N� 500, de fecha ".$detalleReporte[0]['fecha_actual'].".";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(122);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        CONSIDERANDO:  "; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(122);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                      Que el estado en el que se encuentra ".$detalleReporte[0]['nombrecompleto_sol'].", c�dula de identidad hace necesario su tratamiento";
    $texto .= " para recuperar su salud y evitar conflictos con terceros, finalidad que persigue la internaci�n administrativa en un establecimiento asistencial; y ,".$textoOpciones.", ".$textoOpciones2.",".$textoOpciones3.";";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(156);       
    $pdf->SetX(25);
    $pdf->SetFont('Arial','B',10); 
    $texto = "                                                        TENIENDO PRESENTE:"; 
    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(156);
    $pdf->SetX(25);
    $pdf->SetFont('Arial','',10);
    $texto = "                                                                                             Lo dispuesto en los art�culos 5 y 130 y siguientes del C�digo Sanitario; en el DL N� 2763/79";
    $texto .= " modificado por la ley 19.937; en el Decreto Supremo N� 136/2004 que aprueba Reglamento Org�nico del Ministerio de";
    $texto .= " m�dico de ".$detalleReporte[0]['nombre_medico'].", de Consultorio ".$detalleReporte[0]['establecimiento_sol'].", de ".$detalleReporte[0]['fecha_evaluacion']." 3.- El certificado de antecedentes de la especialidad de Dr. Ra�l C�rdenas, Director (S) del Hospital Del Salvador,";
    $texto .= " Salud; en los art�culos 13 y 14 del Decreto Supremo N� 570/98 del Ministro de Salud que aprueba el Reglamento para";
    $texto .= " las facultades que me confiere el Decreto Supremo N� 47 de 24 de Marzo 2010 del Ministerio de Salud, dicto la siguiente:";

    $pdf->MultiCell(170, 4,$texto, 0, 'J');
    $pdf->SetY(191);       
    $pdf->SetX(90);
    $texto = "RESOLUCI�N";
    $pdf->MultiCell(30, 4,$texto, 0, 'J');

    $pdf->SetY(201);       
    $pdf->SetX(25);
    $texto = "1�. DISP�NGASE LA INTERNACI�N ADMINISTRATIVA para su tratamiento en ".$detalleReporte[0]['establecimiento_int'].", como centro especializado";
    $texto .= " en atenci�n psiqui�trica, de Cristian P�rez, Cedula de identidad 17.948.622-9, con domicilio Miraflores 155 Vi�a del mar,";
    $texto .= " debiendo cumplirse la medida de traslado por el establecimiento de salud m�s cercano a su residencia.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(221);       
    $pdf->SetX(25);
    $texto = "2�. REQUI�RASE en caso de oposicio�n de la afectada / del afectado, el auxilio de la fuerza p�blica, directamente de";
    $texto .= " la Unidad de Carabineros mas cercana, conforme a lo establecido en el Art�culo 8 del C�digo Sanitario, para el debido";
    $texto .= " cumplimiento de esta resoluci�n.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(237);       
    $pdf->SetX(25);
    $texto = "3�. Lo dispuesto en el punto 1�, deber� materializarse dentro de los 180 d�as h�biles contados desde la fecha";
    $texto .= " de la presente resoluci�n, de lo contrario, deber� solicitarse una nueva orden a esta Autoridad Sanitaria.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(253);       
    $pdf->SetX(25);
    $a = strtoupper($detalleReporte[0]['establecimiento_int']);
    
    $texto = "4� EL DIRECTOR DEL $a, queda por esta misma resoluci�n, autorizado para dar el alta en la fecha";
    $texto .= " en que se complete el diagn�stico y tratamiento de la afectada / del afectado, debiendo notificar inmediatamente el egreso y plan de";
    $texto .= " tratamiento post-alta a la Secretar�a regional Ministrarial de Salud Regi�n Valpara�so para dictar Resoluci�n de Alta.";
    $pdf->MultiCell(170, 4,$texto, 0, 'J');

    $pdf->SetY(276);       
    $pdf->SetX(70);
    $pdf->SetFont('Arial','B',10); 
    $texto = "AN�TESE, COMUN�QUESE Y C�MPLASE.";
    $pdf->MultiCell(73, 4,$texto, 0, 'J');
    
    $pdf->SetY(286);       
    $pdf->SetX(125);
    
    $pdf->SetFont('Arial','B',10);
    
    $texto = "JAIME JAMMET ROJAS \n";
    $texto .= "SECRETARIO REGIONAL MINISTERIAL DE SALUD ";
    $texto .= "REGION VALPARAISO";
    $pdf->MultiCell(73, 4,$texto, 0, 'C');
    $pdf->SetFont('Arial','',7);
    $pdf->SetY(303);       
    $pdf->SetX(23);
    $texto = "DISTRIBUCI�N: \n";
    $texto .= "-    Solicitante/Interesado \n";
    $texto .= "-    Carabineros de Chile \n";
    $texto .= "-    Hospital del Salvador Valpara�so / Hospital Psiqui�trico Phillippe Pinel de Putaendo / \n";   
    $texto .= "     Hospital Naval Almirante Nef de Vi�a del Mar \n";
    $texto .= "-    Establecimiento: ".$detalleReporte[0]['establecimiento_sol']." \n";
    $texto .= "-    Salud Publica \n";
    $texto .= "-    Oficina de Partes";
    $pdf->MultiCell(150, 4,$texto, 0, 'L');
    $pdf->Output();

?>
