<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoUSMH.php';
   
   	
    
    $xajax = new xajax(); 

    $xajax->registerFunction("filtra_solinv");
    $xajax->registerFunction("filtra_solalta");
    $xajax->registerFunction("rakim_solinv");
    $xajax->registerFunction("lista_solicitudes");
    $xajax->registerFunction("lista_primera_llamada");
    $xajax->registerFunction("lista_segunda_llamada");
    $xajax->registerFunction("rakim_solint");
    $xajax->registerFunction("rakim_solalta");
    $xajax->registerFunction("pdf_solinv");
    $xajax->registerFunction("pdf_solint");
    $xajax->registerFunction("visarsolET");
    
   
        
    $xajax->processRequests(); 

     function pdf_solinv() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solinv_secre.php');
        return $pdf;
     }
     
     function pdf_solint() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solint_secre.php');
        return $pdf;
     }
     
     
     
     
    function visarsolET($idCabeceraSolicitud,$ver)
    {
      $visarsolET = new xajaxResponse();
      $visarsolET->addAlert($idCabeceraSolicitud);
      $visarsolET->addAlert($ver);
      $visar = new DAODepartamentoUSMH();
      $_SESSION['arrDetsoleva']=$visar->visar_solET($idCabeceraSolicitud);
      
      $visarsolET->redi_blank('VistaSolET_usmh.php?ver='.$ver);
      return $visarsolET;
    } 
     
    
    function filtra_solinv($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolinvs']=$lista->lista_solinv($id_sol,$id_estado,$fecha,6);
      $smarty->assign('departamentito',6);
      $tabla = $smarty->fetch('grilla_solinv_secre.tpl');
      $solinv->addAssign("internaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesInternacionNoVoluntaria').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }
    
    function filtra_solalta($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      

       $id_sol = '';
       $id_estado = 'Seleccione';
       $fecha = '';
      
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolalta']=$lista->lista_solalta($id_sol,$id_estado,$fecha,6);
      $smarty->assign('departamentito',6);
      $tabla = $smarty->fetch('grilla_solalta_secre.tpl');
      $solinv->addAssign("alta","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesAltaAdministrativa').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }

   
   function rakim_solinv($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
   	 $rakim->addAlert($rakim_solinv->rakim_solinv($id_sol,$num));
   	 $rakim->addScript("xajax_filtra_solinv('','','');");
   	 }
   	 return $rakim;
   }
    
   function rakim_solint($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
   	 $rakim->addAlert($rakim_solinv->rakim_solint($id_sol,$num));
   	 $rakim->addScript("xajax_lista_solicitudes();");
   	 }
   	 return $rakim;
   }
   
   function rakim_solalta($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
             $rakim->addAlert($rakim_solinv->rakim_solalta($id_sol,$num));
             $rakim->addScript("xajax_filtra_solalta('','','');");
   	 }
   	 return $rakim;
   }


    function lista_solicitudes($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 7; 
       
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $idSolicitud = '';
       $idEstado = '';
       $fecha = '';
       $arr_solicitudes = new DAODepartamentoUSMH();
       $_SESSION['arrSolicitudesET'] = $arr_solicitudes->lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha);
       $smarty->assign('departamentito',7); 
       $tabla = $smarty->fetch('grilla_solevaluacionytratamiento_usmh.tpl');
       $xsolicitudes->addAssign("solicitudes","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesEvaluacionyTratamientos_usmh').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
       
       
       return $xsolicitudes;
        
       

    }
    
    
    function lista_primera_llamada($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 8; 
       $idSolicitud = '';
       $idEstado = '';
       $fecha = '';
       $est_id = $_SESSION['EST_id'];
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoUSMH();
       $_SESSION['arrSolicitudesPrimeraLlamada'] = $arr_solicitudes->lista_completa_primera_llamada($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id);
       $smarty->assign('departamentito',8);
       $tabla = $smarty->fetch('grilla_solET_usmh_primera_llamada.tpl');
       $xsolicitudes->addAssign("primera_llamada","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesET_usmh_primera_llamada').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
       return $xsolicitudes;
       

    }
    
    function lista_segunda_llamada($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 9; 
       $idSolicitud = '';
       $idEstado = '';
       $fecha = '';
       $est_id = $_SESSION['EST_id'];
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoUSMH();
       $_SESSION['arrSolicitudesSegundaLlamada'] = $arr_solicitudes->lista_completa_segunda_llamada($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id);
       $smarty->assign('departamentito',9);
       $tabla = $smarty->fetch('grilla_solET_usmh_segunda_llamada.tpl');
       $xsolicitudes->addAssign("segunda_llamada","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesET_usmh_segunda_llamada').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
       return $xsolicitudes;
       

    }

       

    if(isset($_SESSION['USUA_nombres']))
    { $smarty->assign('USUA_nombres', $_SESSION['USUA_nombres']);
      $smarty->assign('USUA_apellidos', $_SESSION['USUA_apellidos']);
      $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
      $smarty->display('AdminUSMH.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
