<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_Medicos.php';
   
   	
    
    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("muestra_modifica");
	$xajax->registerFunction("elimina");
	$xajax->registerFunction("lista_medicos");
	$xajax->registerFunction("agrega_medico");
	$xajax->registerFunction("limpia");
  $xajax->registerFunction("llena_establecimientos");
	$xajax->processRequests(); 


   
	function muestra_modifica($codigo)
	{
		$funcion = new xajaxResponse();
		$muestra = new DAO_Medicos();
		$arrMuestra = array();
		$arrMuestra = $muestra->lista_uno($codigo);

		$funcion->addAssign("rut","value",$arrMuestra['MED_rut']);
		$funcion->addAssign("nombres","value",$arrMuestra['MED_nombres']);
		$funcion->addAssign("apellidos","value",$arrMuestra['MED_apellidos']);
		$funcion->addAssign("direccion","value",$arrMuestra['MED_direccion']);
		$funcion->addAssign("celular","value",$arrMuestra['MED_celular']);
		$funcion->addAssign("fono","value",$arrMuestra['MED_fono']);
		$funcion->addAssign("correo","value",$arrMuestra['MED_correo']);
    $num = $arrMuestra['EST_id'];
    $funcion->addScript("xajax_llena_establecimientos(".$num.");");


		$_SESSION['opcion'] = "MOD";
		$_SESSION['clave'] = $codigo;
		return $funcion;
	}

	function elimina($codigo)
	{
	  $elimina = new xajaxResponse();
      $borra_usuario = new DAO_Medicos();
      $borra_usuario->elimina_medico($codigo);
      $elimina->addScript('xajax_lista_medicos();');
      $elimina->addAlert("Ha eliminado al Medico id: ".$codigo); 
	  return $elimina;
	}

	function lista_medicos()
	{

	   global $smarty;
	   $xusuarios = new xajaxResponse();
	   $arr_usuarios = new DAO_Medicos();
	   $_SESSION['arrUsuarios'] = $arr_usuarios->lista_completa();
	   $tabla = $smarty->fetch('grilla_medicos.tpl');
	   $xusuarios->addAssign("view1","innerHTML",$tabla);
	   $_SESSION['opcion'] = "ING";
	   return $xusuarios;

	}

    function agrega_medico($form)
    {
       $agrega = new xajaxResponse();
       
       $objUsado = new DTOMedico(
       	 $form['rut'], $form['nombres'], $form['apellidos'], $form['direccion'],$form['celular'],$form['fono'],$form['correo'],$form['establecimiento']);

       $SW = 1; //un elemento que este vacio en el formulario y no hago nada ;)

       foreach ($form as $elemento)//checkeo por cada elemento del formulario
       {   if(empty($elemento))
           {
           	  $SW = 0;
              break;
           }
       }	


     if ($SW == 1)
     {
       $accion = new DAO_Medicos();

       if ($_SESSION['opcion']=="ING") 
       {$agrega->addAlert("INGRESA");
       	$accion->ingresa($objUsado);
       }

       if ($_SESSION['opcion']=="MOD") 
        {$agrega->addAlert("MODIFICA");
         $accion->modifica($objUsado,$_SESSION['clave']);
         }

          $agrega->addScript('document.formUser.reset();');
          $agrega->addScript('xajax_lista_medicos();');

      }
      else
      {
      	$agrega->addAlert("Campos Vacíos en el formulario");
      }

  
       return $agrega;
    }

    function limpia()
    {
       $limpia = new xajaxResponse();
       $_SESSION['opcion'] = "ING";

       return $limpia;
    }

	   function llena_establecimientos($hospital) 
    {

      $llena = new xajaxResponse();

      $combo = new DAO_Medicos();

      $arreglo = $combo->llena_establecimientos();
      $llena->CreaOpcion('establecimiento','Seleccione Hospital...',0);

      if(empty($hospital)){
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento',utf8_encode($valor['nombre']),$valor['codigo']);
          
      }     
      
      }
      else
      {
        $llena->addScript("document.formUser.establecimiento.selectedIndex='".$hospital."';");

      }

      
      return $llena;
    } 


    
    if(isset($_SESSION['USUA_nombres']))
    {   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
        $smarty->display('MantenedorMedicos.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
