<?php
    session_start();
    include("include/include.php");
    include 'xajax/xajax.inc.php';
    include 'DAO/DAOSolicitudAltaAdministrativa.php';
    $xajax = new xajax(); 
    
    $xajax->registerFunction("llena_establecimientos");
    $xajax->registerFunction("llena_establecimientos2");
    $xajax->registerFunction("llena_medicos");
    $xajax->registerFunction("llena_medicos2");
    $xajax->registerFunction("ingresarSolicitud");
    
    
    $xajax->processRequests(); 
    
    function llena_establecimientos()
    {

      $llena = new xajaxResponse();
      $combo = new DAOSolicitudAltaAdministrativa(); 


      $arreglo = $combo->llena_establecimientos();
     
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento',$valor['nombre'],$valor['codigo']);
      }    

      return $llena;
     
    }
    
    function llena_establecimientos2()
    {

      $llena = new xajaxResponse();
      $combo = new DAOSolicitudAltaAdministrativa(); 


      $arreglo = $combo->llena_establecimientos();
     
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento2',$valor['nombre'],$valor['codigo']);
      }    

      return $llena;
     
    }
     
    function llena_medicos()
    {

        $llena = new xajaxResponse();
        $combo = new DAOSolicitudAltaAdministrativa(); 


        $arreglo = $combo->llena_medicos();

        foreach ($arreglo as $valor) 
        {
            $llena->CreaOpcion('medicos',$valor['nombre'],$valor['codigo']);
        }     

        return $llena;
    }
    
    function llena_medicos2()
    {

        $llena = new xajaxResponse();
        $combo = new DAOSolicitudAltaAdministrativa(); 


        $arreglo = $combo->llena_medicos();

        foreach ($arreglo as $valor) 
        {
            $llena->CreaOpcion('medicos2',$valor['nombre'],$valor['codigo']);
        }     

        return $llena;
    }
    
    function ingresarSolicitud($form)
    {
       $xingresarsolicitud = new xajaxResponse();
      
       $ingresar_solicitud = new DAOSolicitudAltaAdministrativa();
       $datosIngreso = $ingresar_solicitud->ingresa_solicitudaa($form);
       $xingresarsolicitud->addAlert($datosIngreso['@mensaje']);
       $xingresarsolicitud->addScript('document.formSol.reset();');
   
       return $xingresarsolicitud;
    }
    
    $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->display('SolicitudAltaHospitalizacion.tpl');
?>
