<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoSeremi.php';
    
    
    $xajax = new xajax(); 
    $xajax->registerFunction("lista_solicitudes");
    $xajax->registerFunction("lista_historico_usuarios");
    $xajax->registerFunction("lista_log_solicitudes");
    $xajax->registerFunction("visar_solicitudes");
    $xajax->registerFunction("test");
    $xajax->registerFunction("llena_estado");
    $xajax->registerFunction("llena_estado2");
    $xajax->registerFunction("llena_tipo");
    $xajax->registerFunction("filtra_estado");
    $xajax->registerFunction("filtra_estado2");
    $xajax->registerFunction("filtra_tipo");
    $xajax->registerFunction("filtra_fecha_a");
    $xajax->registerFunction("filtra_fecha_a2");
    $xajax->registerFunction("filtra_id");
    $xajax->registerFunction("filtra_id2");
    $xajax->registerFunction("filtra_solinv");
    $xajax->registerFunction("visarsolinv");
    $xajax->registerFunction("visarsolinternacionadmin");
    $xajax->registerFunction("detalle_solintadmin");
    $xajax->registerFunction("filtra_historico_usuarios");
    $xajax->registerFunction("filtra_solicitudInternacionAdmin");
    $xajax->registerFunction("pdf");
    $xajax->registerFunction("resolucion_SolicitudINV");
    $xajax->registerFunction("resolucion_SolicitudET");
    $xajax->registerFunction("resolucion_SolicitudAA");
    
    $xajax->registerFunction("resolucion_SolicitudInternacionAdministrativa");
    $xajax->registerFunction("filtra_solaa");
    $xajax->registerFunction("visarsoleva");
    $xajax->registerFunction("filtra_solevas");
    
    $xajax->registerFunction("visarSolicitudAA");
    
    $xajax->processRequests(); 

    function visarsoleva($id_cab,$ver)
    {
      $visarsoleva = new xajaxResponse();
      
      $visar = new DAODepartamentoSeremi();
      $_SESSION['arrDetsoleva']=$visar->visar_soleva($id_cab); 
      $visarsoleva->redi_blank('VistaSolEva.php?ver='.$ver);

      return $visarsoleva;
    }

    
    function filtra_solevas($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSeremi();
      $_SESSION['arrSolevas']=$lista->lista_solevas($id_sol,$id_estado,$fecha,3);
      $smarty->assign('departamentito',3);
      $tabla = $smarty->fetch('grilla_solevas.tpl');
      $solinv->addAssign("evaluaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesEvaluaciones').dataTable();");
      
      return $solinv;
    }
    
    function visarSolicitudAA($idCabeceraSolicitud,$ver)
    {
      $visarsolalta = new xajaxResponse();
      $visarsolalta->addAlert($idCabeceraSolicitud);

      $visar = new DAODepartamentoSeremi();
      $_SESSION['arrDetsolaa']=$visar->visar_solaa($idCabeceraSolicitud);
      
      
      $visarsolalta->redi_blank('VistaSolAltaHospitalizacion.php?ver='.$ver);
      return $visarsolalta;
    }
    
    
    function resolucion_SolicitudInternacionAdministrativa($idCabeceraSolicitud) 
     {  
        
        $pdf = new xajaxResponse();
        $pdf->AddAlert($idCabeceraSolicitud);
        $arr_res_sol_inv = new DAODepartamentoSeremi();

        $_SESSION['arrResSolInternacionAdmin'] = $arr_res_sol_inv->resolucion_sol_int_admin($idCabeceraSolicitud);
        $pdf->redi_blank('fpdf/pdf_resolucion_internacion_administrativa.php');
        return $pdf;
     }

     function resolucion_SolicitudINV($idCabeceraSolicitud) 
     {  
        
        $pdf = new xajaxResponse();
        //$pdf->AddAlert($idCabeceraSolicitud);
        $arr_res_sol_inv = new DAODepartamentoSeremi();
        $_SESSION['arrResSolINV'] = $arr_res_sol_inv->resolucion_sol_inv($idCabeceraSolicitud);
        $pdf->redi_blank('fpdf/pdf_resolucion_inv.php');
        return $pdf;
     }
     
     function resolucion_SolicitudAA($idCabeceraSolicitud) 
     {  
        
        $pdf = new xajaxResponse();
        $pdf->AddAlert($idCabeceraSolicitud);
        $arr_res_sol_AA = new DAODepartamentoSeremi();
        $_SESSION['arrResSolAA'] = $arr_res_sol_AA->resolucion_sol_AA($idCabeceraSolicitud);
        $pdf->redi_blank('fpdf/pdf_resolucion_AA.php');
        return $pdf;
     }
     
      function resolucion_SolicitudET($idCabeceraSolicitud) 
     {  
        
        $pdf = new xajaxResponse();
        $A = $_SESSION['arrSolevas'][0]['ES_TRATAMIENTO']; 
        $pdf->AddAlert($A);
        if ($A == 1) 
        {  
            
            $pdf->AddAlert("1");
            $arr_res_sol_et = new DAODepartamentoSeremi();
            $_SESSION['arrResSolET'] = $arr_res_sol_et->resolucion_sol_et($idCabeceraSolicitud);
            $pdf->redi_blank('fpdf/pdf_resolucion_traslado.php');
        }
        else
        {
            
            $pdf->AddAlert("2");
            $arr_res_sol_et = new DAODepartamentoSeremi();
            $_SESSION['arrResSolET'] = $arr_res_sol_et->resolucion_sol_et($idCabeceraSolicitud);
            $pdf->redi_blank('fpdf/pdf_resolucion_evaluacion_tratamiento.php');
        }
        return $pdf;
        
     } 
     
    function filtra_solaa($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      $solaa = new xajaxResponse();
      $lista = new DAODepartamentoSeremi();
      $_SESSION['arrSolaa']=$lista->lista_solaa($id_sol,$id_estado,$fecha,3);
      $smarty->assign('departamentito',3);
      $tabla = $smarty->fetch('grilla_solaltaadmin.tpl');
      $solaa->addAssign("hospitalizacion","innerHTML",$tabla);
      $solaa->addScript("$('#solicitudesAltaAdministrativa').dataTable();");
      
      return $solaa;
    }

     function visarsolinv($id_cab,$ver)
    {
      $visarsolinv = new xajaxResponse();
      $visarsolinv->addAlert($id_cab);

      $visar = new DAODepartamentoSeremi();
      $_SESSION['arrDetsolinv']=$visar->visar_solinv($id_cab); 
      $visarsolinv->redi_blank('VistaSolinvDSP.php?ver='.$ver);

      return $visarsolinv;
    }
    
    
     function filtra_solinv($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSeremi();
      $_SESSION['arrSolinvs']=$lista->lista_solinv($id_sol,$id_estado,$fecha,3);
      $smarty->assign('departamentito',3);
      $tabla = $smarty->fetch('grilla_solinvs.tpl');
      $solinv->addAssign("internaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesInternacionNoVoluntaria').dataTable();");
      
      return $solinv;
    }

    
    
    function pdf() //invoco al pdf que me refleja la grilla historial evaluaciones
    {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_historico_evaluaciones.php');
        return $pdf;
    }
    
    function visarsolinternacionadmin($idCabeceraSolicitud,$ver)
    {
      $visarsolinternacion = new xajaxResponse();
      $visarsolinternacion->addAlert($idCabeceraSolicitud);

      $visar = new DAODepartamentoSeremi();
      $_SESSION['arrDetsolinternacionadmin']=$visar->visar_solinternacionadmin($idCabeceraSolicitud);
      $visarsolinternacion->redi_blank('VistaSolInternacionAdmin.php?ver='.$ver); 

      return $visarsolinternacion;
    }

    function lista_solicitudes($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 3; 
       $idSolicitud = '';
       $idEstado = 'Seleccione';
       $fecha = '';
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoSeremi();
       $_SESSION['arrSolicitudes'] = $arr_solicitudes->lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha);
       $smarty->assign('departamentito',3);
       $tabla = $smarty->fetch('grilla_solicitudesInternacionAdmin.tpl');
       $xsolicitudes->addAssign("solicitudes","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesInternacionAdmin').dataTable();");
       return $xsolicitudes;
       

    }
    
    function llena_estado2()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSeremi(); 
      
      
      $arreglo = $combo->llena_estados2();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado2',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }
    
    function filtra_estado2($idSolicitud, $idEstado, $fecha) //filtro grilla historial evaluaciones por estado de la sol
    {
        
        global $smarty;
        $idDepartamento = 3;
        $test = new xajaxResponse();
        $lista = new DAODepartamentoSeremi();
        $_SESSION['arrSolicitudes'] = $lista->filtra_estado2($idDepartamento, $idSolicitud, $idEstado, $fecha);
        $tabla = $smarty->fetch('grilla_solicitudesInternacionAdmin.tpl');
        $test->addAssign("solicitudes","innerHTML",$tabla);
       
        //$test->addAlert($filtro);
        return $test;
    }
    
    function filtra_solicitudInternacionAdmin($idSolicitud, $idEstado, $fecha) //filtro grilla por tipo solicitud
    {
        
          global $smarty;
          $test = new xajaxResponse();
          $idDepartamento = '3';
          $lista = new DAODepartamentoSeremi();
          $_SESSION['arrSolicitudes'] = $lista->lista_completa_solicitudes($idDepartamento, $idSolicitud, $idEstado, $fecha);

          $tabla = $smarty->fetch('grilla_solicitudesInternacionAdmin.tpl');
          $test->addAssign("solicitudes","innerHTML",$tabla);
          return $test;
    }
    
    
     function llena_estado()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSeremi(); 
      
      
      $arreglo = $combo->llena_estados();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado',$valor['descripcion'],$valor['codigo']);
      }   
      return $llena;
    }
    
    function llena_tipo()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSeremi(); 
      
      
      $arreglo = $combo->llena_tipo();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('tipo',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }
    
     function lista_log_solicitudes()
    {
       global $smarty;
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSeremi();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_completa_log_solicitudes();
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       $xlogsolicitudes->addScript("$('#logsolicitudes').dataTable();");
       return $xlogsolicitudes;
    }
    
    
     function filtra_fecha_a($filtro,$tipo,$fecha)//filtro grilla historial evaluaciones por fecha asc o desc
     {

       global $smarty;
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSeremi();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_filtros_log($filtro,$tipo,$fecha);
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       $xlogsolicitudes->addScript("$('#logsolicitudes').dataTable();");
       return $xlogsolicitudes;
       
      }
      
    function filtra_estado($filtro,$tipo,$fecha) //filtro grilla historial evaluaciones por estado de la sol
    {
        
        global $smarty;
        $test = new xajaxResponse();
        $lista = new DAODepartamentoSeremi();
        $_SESSION['arrLogSolicitudes'] = $lista->lista_filtros_log($filtro,$tipo,$fecha);
        $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
        $test->addAssign("log_solicitudes","innerHTML",$tabla);
        $test->addScript("$('#logsolicitudes').dataTable();");
        //$test->addAlert($filtro);
        return $test;
    }
    
    function filtra_tipo($filtro,$tipo,$fecha) //filtro grilla por tipo solicitud
    {

        global $smarty;
        $test = new xajaxResponse();
        
        $test->addAlert($filtro);
        $test->addAlert($tipo);
        $test->addAlert($fecha);
        
        $lista = new DAODepartamentoSeremi();
        /*$opcion = 2;*/ 
        $_SESSION['arrLogSolicitudes'] = $lista->lista_filtros_log($filtro,$tipo,$fecha);

        $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
        $test->addAssign("log_solicitudes","innerHTML",$tabla);
        $test->addScript("$('#logsolicitudes').dataTable();");

        return $test;
    }
    
    
    function lista_historico_usuarios()
    {

       global $smarty;
       $xhistoricousuarios = new xajaxResponse();
       $arr_historico_usuarios = new DAODepartamentoSeremi();
       $_SESSION['arrHistoricoUsuarios'] = $arr_historico_usuarios->lista_historico_usuarios();
       $tabla = $smarty->fetch('grilla_historicoUsuarios.tpl');
       $xhistoricousuarios->addAssign("log_historico_usuarios","innerHTML",$tabla);
       $xhistoricousuarios->addScript("$('#loghistoricousuarios').dataTable();");
       return $xhistoricousuarios;
       

    }
    
    function visar_solicitudes($tipoSolicitud,$idSolicitud)
    {
       $xvisar_solicitud = new xajaxResponse();  
       $xvisar_solicitud->addAlert($idSolicitud);
       
       //Solicitud Internacion Administrativa
       
       if($tipoSolicitud == "Solicitud Internacion Administrativa")
       { 
          
          $arrSolicitudesPorVisar = new DAODepartamentoSeremi();
          $_SESSION['arrSolicitudesPorVisar'] = $arrSolicitudesPorVisar->solicitud_por_visar($idSolicitud);
          $xvisar_solicitud->addRedirect("VisacionInternacionAdministrativa.php"); 
         
       }    
       else
       {
           
       }   
        return $xvisar_solicitud;
    }
    
    
    function filtra_historico_usuarios($idSolicitud, $fecha)
    {

       global $smarty;
       $xhistoricousuarios = new xajaxResponse();
       $arr_historico_usuarios = new DAODepartamentoSeremi();
       $_SESSION['arrHistoricoUsuarios'] = $arr_historico_usuarios->filtro_historico_usuarios($idSolicitud, $fecha);
       $tabla = $smarty->fetch('grilla_historicoUsuarios.tpl');
       $xhistoricousuarios->addAssign("log_historico_usuarios","innerHTML",$tabla);
       $xhistoricousuarios->addScript("$('#loghistoricousuarios').dataTable();");
       return $xhistoricousuarios;
       

    }

   

     if(isset($_SESSION['USUA_nombres']))
    {   $smarty->assign('USUA_nombres', $_SESSION['USUA_nombres']);
        $smarty->assign('USUA_apellidos', $_SESSION['USUA_apellidos']);
        $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
        $smarty->display('AdminSeremi.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>

