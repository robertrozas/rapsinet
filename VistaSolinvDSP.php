<?php

    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoSaludPublica.php';

   
    $xajax = new xajax(); 
    $_SESSION['activo'] = "disabled='true'";
    $rechazos = array(2,4,6,8);
    $lista = $_SESSION['arrDetsolinv']['DETSOLINV_opcion'];
    $estado = $_SESSION['arrDetsolinv']['CABSOLINV_estadoSolicitud'];
    $arrOpciones = explode(',',$lista);
    $OP = "OP";
    $i = 1;
    foreach ($arrOpciones as $opcion)
    {
        switch ($opcion)
        {
            case "A":
            {
                $elige = "checked";
                break;
            }
            case "B":
            {
                $elige = "checked";
                break;
            }
            case "C":
            {
                $elige = "checked";
                break;
            }
            case "":
            {
                $elige = "";
                break;
            }
            
        }
        $smarty->assign($OP.$i,$elige);
        $i++;
    }   
 $ver = $_GET['ver'];   

      if (in_array($estado, $rechazos)&& $ver ==0)
      {
          $_SESSION['activo'] = "";
      }


    $xajax->registerFunction("visar");
    $xajax->registerFunction("rechaza");
    $xajax->registerFunction("observa");
    $xajax->registerFunction("refresco");
    $xajax->registerFunction("ver_adjunto");
    $xajax->registerFunction("llena_establecimientos");
    $xajax->registerFunction("llena_servicios");
    $xajax->registerFunction("llena_medicos");
    $xajax->registerFunction("llena_medicos");
    $xajax->registerFunction("comentario");
    $xajax->registerFunction("lista_adjuntos");
    $xajax->registerFunction("lista_comentarios");
    $xajax->registerFunction("lista_eventos");
    $xajax->processRequests(); 

    function refresco()
    {
      $refresco = new xajaxResponse();
      $refresco->addScript("opener.xajax_filtra_solinv('','','');");
      return $refresco;
    }

    
    function lista_comentarios($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesComentarios']=$eventos->comentarios_solicitud_inv($idCabeceraSolicitud);
        $tabla = $smarty->fetch('grilla_comentarios.tpl');
        $evento->addAssign("detalle_comentarios","innerHTML",$tabla);
        return $evento;
    }
    
    function lista_eventos($idCabeceraSolicitud)
    {   
       global $smarty;
        $evento = new xajaxResponse();
        
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesEvento']=$eventos->eventos_solicitud($idCabeceraSolicitud,1);
        $tabla = $smarty->fetch('grilla_eventos.tpl');
        $evento->addAssign("detalle","innerHTML",$tabla);
        return $evento;

    }
    
    function visar($id_cabecera,$id_estado,$form)
    {
       $vir = new xajaxResponse();
       $visar = new DAODepartamentoSaludPublica();
       

      if($_SESSION['activo']=="")//significa que los input estan enabled
      {
      //hago el update
      $visar->actualiza_solinv($id_cabecera,$form);

      } 

       $visar->visado_solinv($id_cabecera,$id_estado);

       $vir->addScript("opener.xajax_filtra_solinv('','','');");
       $vir->cierro(1);
       return $vir; 
    }
    
   function comentario($cabid,$texto)
   {
     $comenta = new xajaxResponse();
     
     $agregacom = new DAODepartamentoSaludPublica();
     $agregacom->agrega_comentario_inv($cabid, $texto);
     $comenta->addScript('xajax_lista_comentarios("'.$cabid.'");');
     //$comenta->addAlert($cabid.$texto);

     return $comenta;
   }

   
   
   function rechaza()
   {
    $rechaza = new xajaxResponse();
    $rechaza->addScript("nick=prompt('Ingrese Observacion','');
      document.getElementById('observacion').value = nick;document.getElementById('observacion').onchange();");
    return $rechaza;
   }

  function observa($observacion)
  {
    $observa = new xajaxResponse();
    
    $cabecera = $_SESSION['arrDetsolinv']['CABSOLINV_ID'];
    $estado = $_SESSION['arrDetsolinv']['CABSOLINV_estadoSolicitud'];
    
    if(empty($observacion)){
      $observa->addAlert("Debe ingresar una observacion!!");
      $observa->addScript("xajax_rechaza();");
    }
    else{  
    $rechazar = new DAODepartamentoSaludPublica();
    
    $motivo = $_SESSION['observacion_soleva'];
    $rechazar->rechazo_solinv($cabecera,$estado,$observacion);
    $observa->addScript("opener.xajax_filtra_solinv('','','');");
    $observa->cierro(1);
     }
    return $observa;
  }

     function ver_adjunto($id_adjunto)
    {
       $ver = new xajaxResponse();
       $ventana = $ver->redi_cierro('veradjunto_solint.php?clave='.$id_adjunto);

       return $ver; 
    }

    function lista_adjuntos($id)
    {   global $smarty; 
        $adjunto = new xajaxResponse();
        
        $archivos = new DAODepartamentoSaludPublica();
        $_SESSION['arrAdjuntos']  = $archivos->lista_archivos_solint($id);
       
        $tabla = $smarty->fetch('grilla_adjuntos.tpl');
        $adjunto->addAssign("detalle1","innerHTML",$tabla);
        return $adjunto;

    }
     
     function llena_establecimientos()
    {

      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      $hospital = $_SESSION['arrDetsolinv']['CABSOLINV_idEstablecimiento'];

      $arreglo = $combo->llena_establecimientos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento',($valor['nombre']),$valor['codigo']);
      }     


      $llena->addScript("$('#establecimiento option[value=".$hospital."]').attr('selected',true);");
      return $llena;
    } 
    
    
    function llena_servicios($servicios)
    {

      $llena = new xajaxResponse();
      //$llena->addAlert($servicios);
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->servicios();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('servicios',$valor['descripcion'],$valor['codigo']);
          if($valor['codigo']==$servicios)
          {
            $llena->addScript("document.formSol.servicios.selectedIndex='".$valor['codigo']."';");
          }

      }  

      return $llena;

    }
        
    function llena_medicos($medicos)
    {

      $llena = new xajaxResponse();

      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->medicos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('medicos',$valor['descripcion'],$valor['codigo']);
      }  

      $llena->addScript("$('#medicos option[value=".$medicos."]').attr('selected',true);");
      return $llena;

    }



      $smarty->assign('xajax_js', $xajax->getJavascript('xajax')); 
      $smarty->assign('ver', $ver); 
      $smarty->assign('activo', $_SESSION['activo']); 
      $smarty->assign('estados', array(2,4,6,8)); 
      $smarty->display('VistaSolinvDSP.tpl');
?>