<?php
    class DTOMedico
    {

        private $USUA_nombres;
        private $USUA_apellidos;
        private $USUA_rut;
        private $USUA_direccion;
        private $USUA_celular;
        private $USUA_fono;
        private $USUA_correo;
        private $ESTAB_id;
        //private $ESTAB_id;
        //private $DEP_id;

        function __construct
            ($USUA_rut,$USUA_nombres,$USUA_apellidos,
            $USUA_direccion,$USUA_celular,
            $USUA_fono,$USUA_correo,$ESTAB_id)
        {

            $this->USUA_nombres = $USUA_nombres;
            $this->USUA_apellidos = $USUA_apellidos;
            $this->USUA_rut = $USUA_rut;
            $this->USUA_direccion = $USUA_direccion;
            $this->USUA_celular = $USUA_celular;
            $this->USUA_fono = $USUA_fono;
            $this->USUA_correo = $USUA_correo;
            $this->ESTAB_id = $ESTAB_id;
            //$this->DEP_id = $DEP_id;
        }

       
        function set_USUA_nombres($USUA_nombres){
            $this->USUA_nombres = $USUA_nombres;
        }
        function get_USUA_nombres(){
            return $this->USUA_nombres;
        }
        

        function set_USUA_apellidos($USUA_apellidos){
            $this->USUA_apellidos = $USUA_apellidos;
        }
        function get_USUA_apellidos(){
            return $this->USUA_apellidos;
        }
        
        function set_USUA_rut($USUA_rut){
            $this->USUA_rut = $USUA_rut;
        }
        function get_USUA_rut(){
            return $this->USUA_rut;
        }
        
        function set_USUA_direccion($USUA_direccion){
            $this->USUA_direccion = $USUA_direccion;
        }
        function get_USUA_direccion(){
            return $this->USUA_direccion;
        }
        
        function set_USUA_celular($USUA_celular){
            $this->USUA_celular = $USUA_celular;
        }

         function get_USUA_celular(){
            return $this->USUA_celular;
        }

        function set_USUA_fono($USUA_fono){
            $this->USUA_fono = $USUA_fono;
        }
        function get_USUA_fono(){
            return $this->USUA_fono;
        }
        
        
        function set_USUA_correo($USUA_correo){
            $this->USUA_correo = $USUA_correo;
        }
        function get_USUA_correo(){
            return $this->USUA_correo;
        }
        
       
       function set_ESTAB_id($ESTAB_id){
            $this->ESTAB_id = $ESTAB_id;
        }
        function get_ESTAB_id(){
            return $this->ESTAB_id;
        }
        
        /*function set_DEP_id($DEP_id){
            $this->DEP_id = $DEP_id;
        }
        function get_DEP_id(){
            return $this->DEP_id;
        } */


    }
?>