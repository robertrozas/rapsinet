<?php
    class DTOHospital
    {

        private $EST_nombres;
        private $EST_direccion;
        private $EST_fono;
        private $EST_correo;
        private $EST_region;
        private $EST_provincia;
        private $EST_comuna;
        private $EST_condicion;
        private $SSALUD_id;
        private $ASSOL_id;
        
        function __construct
            ($EST_nombres,$EST_direccion,$EST_fono,
             $EST_correo,$EST_region,$EST_provincia,
             $EST_comuna,$EST_condicion,$SSALUD_id,$ASSOL_id)
        {

            $this->EST_nombres = $EST_nombres;
            $this->EST_direccion = $EST_direccion;
            $this->EST_fono = $EST_fono;
            $this->EST_correo = $EST_correo;
            $this->EST_region = $EST_region;
            $this->EST_provincia = $EST_provincia;
            $this->EST_comuna = $EST_comuna;
            $this->EST_condicion = $EST_condicion;
            $this->SSALUD_id = $SSALUD_id; 
            $this->ASSOL_id = $ASSOL_id;
        }

        function set_ASSOL_id($ASSOL_id){
            $this->ASSOL_id = $ASSOL_id;
        }
        function get_ASSOL_id(){
            return $this->ASSOL_id;
        }
       

        function set_SSALUD_id($SSALUD_id){
            $this->SSALUD_id = $SSALUD_id;
        }
        function get_SSALUD_id(){
            return $this->SSALUD_id;
        }
       
        function set_EST_nombres($EST_nombres){
            $this->EST_nombres = $EST_nombres;
        }
        function get_EST_nombres(){
            return $this->EST_nombres;
        }
        

        function set_EST_direccion($EST_direccion){
            $this->EST_direccion = $EST_direccion;
        }
        function get_EST_direccion(){
            return $this->EST_direccion;
        }
        
        function set_EST_fono($EST_fono){
            $this->EST_fono = $EST_fono;
        }
        function get_EST_fono(){
            return $this->EST_fono;
        }
        
        function set_EST_correo($EST_correo){
            $this->EST_correo = $EST_correo;
        }
        function get_EST_correo(){
            return $this->EST_correo;
        }
        
        function set_EST_region($EST_region){
            $this->EST_region = $EST_region;
        }

         function get_EST_region(){
            return $this->EST_region;
        }

        function set_EST_provincia($EST_provincia){
            $this->EST_provincia = $EST_provincia;
        }
        function get_EST_provincia(){
            return $this->EST_provincia;
        }
        
        
        function set_EST_comuna($EST_comuna){
            $this->USUA_correo = $EST_comuna;
        }
        function get_EST_comuna(){
            return $this->EST_comuna;
        }
        
       
       function set_EST_condicion($EST_condicion){
            $this->EST_condicion = $EST_condicion;
        }
        function get_EST_condicion(){
            return $this->EST_condicion;
        }
        
       
    }
?>