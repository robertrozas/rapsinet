<?php
    class DTOUsuario 
    {

        private $USUA_nombres;
        private $USUA_apellidos;
        private $USUA_rut;
        private $USUA_direccion;
        private $USUA_celular;
        private $USUA_fono;
        private $USUA_correo;
        private $USUA_usuario;
        private $USUA_contrasena;
        private $USUA_depto;
        private $USUA_perfil;
        private $USUA_establecimiento;
       

        function __construct
            ($USUA_rut,$USUA_nombres,$USUA_apellidos,
            $USUA_direccion,$USUA_celular,
            $USUA_fono,$USUA_correo,
            $USUA_usuario,$USUA_contrasena,$USUA_depto,$USUA_perfil,$USUA_establecimiento)
        {

            $this->USUA_nombres = $USUA_nombres;
            $this->USUA_apellidos = $USUA_apellidos;
            $this->USUA_rut = $USUA_rut;
            $this->USUA_direccion = $USUA_direccion;
            $this->USUA_celular = $USUA_celular;
            $this->USUA_fono = $USUA_fono;
            $this->USUA_correo = $USUA_correo;
            $this->USUA_usuario = $USUA_usuario;
            $this->USUA_contrasena = $USUA_contrasena;
            $this->USUA_depto = $USUA_depto;
            $this->USUA_perfil = $USUA_perfil;
            $this->USUA_establecimiento = $USUA_establecimiento;
        }

        function set_USUA_establecimiento($USUA_establecimiento){
            $this->USUA_establecimiento = $USUA_establecimiento;
        }
        function get_USUA_establecimiento(){
            return $this->USUA_establecimiento;
        }


        function set_USUA_perfil($USUA_perfil){
            $this->USUA_perfil = $USUA_perfil;
        }
        function get_USUA_perfil(){
            return $this->USUA_perfil;
        }


         function set_USUA_depto($USUA_depto){
            $this->USUA_depto = $USUA_depto;
        }
        function get_USUA_depto(){
            return $this->USUA_depto;
        }


        function set_USUA_nombres($USUA_nombres){
            $this->USUA_nombres = $USUA_nombres;
        }
        function get_USUA_nombres(){
            return $this->USUA_nombres;
        }
        

        function set_USUA_apellidos($USUA_apellidos){
            $this->USUA_apellidos = $USUA_apellidos;
        }
        function get_USUA_apellidos(){
            return $this->USUA_apellidos;
        }
        
        function set_USUA_rut($USUA_rut){
            $this->USUA_rut = $USUA_rut;
        }
        function get_USUA_rut(){
            return $this->USUA_rut;
        }
        
        function set_USUA_direccion($USUA_direccion){
            $this->USUA_direccion = $USUA_direccion;
        }
        function get_USUA_direccion(){
            return $this->USUA_direccion;
        }
        
        function set_USUA_celular($USUA_celular){
            $this->USUA_celular = $USUA_celular;
        }

         function get_USUA_celular(){
            return $this->USUA_celular;
        }

        function set_USUA_fono($USUA_fono){
            $this->USUA_fono = $USUA_fono;
        }
        function get_USUA_fono(){
            return $this->USUA_fono;
        }
        
        
        function set_USUA_correo($USUA_correo){
            $this->USUA_correo = $USUA_correo;
        }
        function get_USUA_correo(){
            return $this->USUA_correo;
        }
        
        function set_USUA_usuario($USUA_usuario){
            $this->USUA_usuario = $USUA_usuario;
        }
        function get_USUA_usuario(){
            return $this->USUA_usuario;
        }

        function set_USUA_contrasena($USUA_contrasena){
            $this->USUA_contrasena = $USUA_contrasena;
        }
        function get_USUA_contrasena(){
            return $this->USUA_contrasena;
        }
        
        /*function set_ESTAB_id($ESTAB_id){
            $this->USUA_contrasena = $ESTAB_id;
        }
        function get_ESTAB_id(){
            return $this->USUA_contrasena;
        }
        
        function set_DEP_id($DEP_id){
            $this->DEP_id = $DEP_id;
        }
        function get_DEP_id(){
            return $this->DEP_id;
        } */


    }
?>