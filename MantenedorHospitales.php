<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_Hospitales.php';
   
   	
    
    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("muestra_modifica");
	$xajax->registerFunction("elimina");
	$xajax->registerFunction("lista_hospitales");
	$xajax->registerFunction("agrega_hospital");
	$xajax->registerFunction("limpia");
  $xajax->registerFunction("provincias");
  $xajax->registerFunction("comunas");
  
	$xajax->processRequests(); 


  $combos = new DAO_Hospital();
  $_SESSION['Regiones'] = $combos->regiones();
  $_SESSION['SSalud'] = $combos->servicios(5); //Para extender la funcionalidad a otras regiones


   
	function muestra_modifica($codigo)
	{
		$funcion = new xajaxResponse();
		$muestra = new DAO_Hospital();
		$arrMuestra = array();

    
		$arrMuestra = $muestra->lista_uno($codigo);

    $funcion->addAssign("asistente","value",utf8_encode($arrMuestra['ASSOL_id']));
		$funcion->addAssign("nombre","value",utf8_encode($arrMuestra['EST_nombre']));
		$funcion->addAssign("dire","value",utf8_encode($arrMuestra['EST_ubicacion']));
		$funcion->addAssign("fono","value",utf8_encode($arrMuestra['EST_fono']));
		$funcion->addAssign("correo","value",$arrMuestra['EST_correo']);
		$region = utf8_encode($arrMuestra['EST_idRegion']);
    $_SESSION['provincia'] = utf8_encode($arrMuestra['EST_idProvincia']);
    $_SESSION['comuna'] = utf8_encode($arrMuestra['EST_idComuna']);
    $condicion = $arrMuestra['EST_psiquiatrico'];
		$servicio = $arrMuestra['SSALUD_id'];
    $funcion->addScript("xajax_provincias(".$region.")");
    $funcion->addScript("xajax_comunas(".$_SESSION['provincia'].")");
    

    $funcion->addScript("$('#region option[value=".$region."]').attr('selected',true);");
    $funcion->addScript("$('#sico option[value=".$condicion."]').attr('selected',true);");
    $funcion->addScript("$('#servicio option[value=".$servicio."]').attr('selected',true);");
   
		$_SESSION['opcion'] = "MOD";
		$_SESSION['clave'] = $codigo;
		return $funcion;
	}



  function provincias($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_Hospital(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincia').empty();");
      $provincias->CreaOpcion('provincia','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincia',$valor['nom_provincia'],$valor['id_provincias']);
      }  
          
      if($_SESSION['provincia'] <> "")
      {
        $provincias->addScript("$('#provincia option[value=".$_SESSION['provincia']."]').attr('selected',true);");
      }

    return $provincias;
  }


  function comunas($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_Hospital(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna').empty();");
      $comuna->CreaOpcion('comuna','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      if($_SESSION['comuna'] <> "")
      {
        $comuna->addScript("$('#comuna option[value=".$_SESSION['comuna']."]').attr('selected',true);");
      }

      return $comuna;

  }


	function elimina($codigo)
	{
	  $elimina = new xajaxResponse();
      $borra_usuario = new DAO_Hospital();
      $borra_usuario->elimina_hospital($codigo);
      $elimina->addScript('xajax_lista_hospitales();');
      $elimina->addAlert("Ha eliminado al Hospital id: ".$codigo); 
	  return $elimina;
	}

	function lista_hospitales()
	{

	   global $smarty;
	   $xusuarios = new xajaxResponse();
	   $arr_usuarios = new DAO_Hospital();
	   $_SESSION['arrHosp'] = $arr_usuarios->lista_completa();
	   $tabla = $smarty->fetch('grilla_hospitales.tpl');
	   $xusuarios->addAssign("view1","innerHTML",$tabla);
	   $_SESSION['opcion'] = "ING";
     $xusuarios->addScript("$('#grilla_hospitales').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 4, 'desc' ]]} );");
	   return $xusuarios;

	}

    function agrega_hospital($form)
    {
       $agrega = new xajaxResponse();
       
       $objUsado = new DTOHospital(
       	 utf8_decode($form['nombre']), utf8_decode($form['dire']), $form['fono'], $form['correo'],$form['region'],$form['provincia'],$form['comuna'],$form['sico'],$form['servicio'],$form['asistente']);



       $SW = 1; //un elemento que este vacio en el formulario y no hago nada ;)

       foreach ($form as $elemento)//checkeo por cada elemento del formulario
       {   if(empty($elemento))
           {
           	  $SW = 0;
              break;
           }
       }	


     if ($SW == 1)
     {
       $accion = new DAO_Hospital();

       if ($_SESSION['opcion']=="ING") 
       {$agrega->addAlert("INGRESA");
       	$accion->ingresa($objUsado);
       }

       if ($_SESSION['opcion']=="MOD") 
        {$agrega->addAlert("MODIFICA");
         $accion->modifica($objUsado,$_SESSION['clave']);
         }

          $agrega->addScript('document.formUser.reset();');
          $agrega->addScript('xajax_lista_hospitales();');

      }
      else
      {
      	$agrega->addAlert("Campos Vacíos en el formulario");
      }

  
       return $agrega;
    }

    function limpia()
    {
       $limpia = new xajaxResponse();
       $_SESSION['opcion'] = "ING";
       $limpia->addScript("$('#provincia').empty();");
       $limpia->addScript("$('#comuna').empty();");
       return $limpia;
    }

	  

    //Algo de seguridad al sitio, si las sesiones no existen entonces se envía a web de error XD
    if(isset($_SESSION['USUA_nombres']))
    {   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
        $smarty->display('MantenedorHospitales.tpl');
      }
    else
    {$smarty->display('404.tpl');}


?>
