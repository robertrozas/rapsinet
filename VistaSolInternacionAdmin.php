<?php

    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include ("PHPMailer/class.phpmailer.php");
    include 'DAO/DAODepartamentoSaludPublica.php';

    $xajax = new xajax(); 

    $xajax->registerFunction("visar");
    $xajax->registerFunction("rechazar");
    $xajax->registerFunction("ver_adjunto");
    $xajax->registerFunction("comentario");
    $xajax->registerFunction("lista_adjuntos");
    $xajax->registerFunction("refresco");
    $xajax->registerFunction("lista_comentarios");
    $xajax->registerFunction("lista_eventos");
    $xajax->registerFunction("llena_regiones");
    $xajax->registerFunction("llena_regiones2");
    $xajax->registerFunction("llena_provincias");
    $xajax->registerFunction("llena_provincias2");
    $xajax->registerFunction("llena_comunas");
    $xajax->registerFunction("llena_comunas2");
    $xajax->registerFunction("rechaza");
    $xajax->registerFunction("observa");
    $xajax->registerFunction("llena_establecimientos");
    $xajax->registerFunction("llena_establecimientos2");
    $xajax->registerFunction("llena_sexo");
    $xajax->registerFunction("llena_sexo2");
    $xajax->registerFunction("llena_vinculacion");
    
    
    $xajax->processRequests(); 
    
    $combo = new DAODepartamentoSaludPublica(); 
    $_SESSION['arrRegiones'] = $combo->regiones();


    $lista = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_opciones'];
    $arrOpciones = explode(',',$lista);
    $OP = "OP";
    $i = 1;
    foreach ($arrOpciones as $opcion)
    {
        switch ($opcion)
        {
            case "A":
            {
                $elige = "checked";
                break;
            }
            case "B":
            {
                $elige = "checked";
                break;
            }
            case "C":
            {
                $elige = "checked";
                break;
            }
            case "":
            {
                $elige = "";
                break;
            }
            
        }
        $smarty->assign($OP.$i,$elige);
        $i++;
    }   
    


    $_SESSION['activo'] = "disabled='true'";
    $rechazos = array(2,4,6,8);
    $estado = $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_idEstadoSolicitud'];
    $ultimo = $_SESSION['arrDetsolinternacionadmin']['ultimoTipoSolicitud'];
    function refresco()
    {
      $refresco = new xajaxResponse();
      $refresco->addScript("opener.xajax_lista_solicitudes();");
      return $refresco;
    }

    
   function observa($observacion)
  {
    $observa = new xajaxResponse();
    
    $cabecera = $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];
    $estado = $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_idEstadoSolicitud'];
    
    if(empty($observacion)){
      $observa->addAlert("Debe ingresar una observacion!!");
      $observa->addScript("xajax_rechaza();");
    }
    else{  
    $rechazar = new DAODepartamentoSaludPublica();
    
    
    $observa->addAlert($rechazar->rechazo_soladmin($cabecera,$estado,$observacion));
    $observa->addScript("opener.xajax_lista_solicitudes();");
    $observa->cierro(1);
     }
    return $observa;
  }

    function rechaza()
    {
        $rechaza = new xajaxResponse();
        $rechaza->addScript("nick=prompt('Ingrese Observacion','');
        document.getElementById('observacionesa').value = nick;document.getElementById('observacionesa').onchange();");
        return $rechaza;
    }


   function comentario($cabid,$texto)
   {
     $comenta = new xajaxResponse();
     $agregacom = new DAODepartamentoSaludPublica();
     $agregacom->agrega_comentario($cabid,$texto);
     $comenta->addScript('xajax_lista_comentarios("'.$cabid.'");');
     //$comenta->addAlert($cabid.$texto);

     return $comenta;
   }
   
   function lista_comentarios($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        //$evento->addAlert($idCabeceraSolicitud);
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesComentarios']=$eventos->comentarios_solicitud($idCabeceraSolicitud);
        $tabla = $smarty->fetch('grilla_comentarios.tpl');
        $evento->addAssign("detalle_comentarios","innerHTML",$tabla);
        return $evento;

    }
    
    
    function visar($idCabeceraSolicitud,$estadoDocumento)
    { 
        $xaprobarsolicitud = new xajaxResponse();
     
        $aprobar_solicitud = new DAODepartamentoSaludPublica();
        //$aprobarSolicitud = 

        $aprobar_solicitud->aprobar($idCabeceraSolicitud,$estadoDocumento);

        $xaprobarsolicitud->addAlert("Solicitud Aprobada");
        
        /*correo
        
          $destino="chikoperezbkn@gmail.com";
          		  
		  
	        $mail = new phpmailer();
          $mail->Mailer = "smtp";
          $mail->Host = "mail.ebelen.com";
          $mail->SMTPAuth = TRUE;
          $mail->Username = "cristian.perezebe"; 
          $mail->Password = "A1234z";
          $mail->From = "cristian.perez@ebelen.com";
          $mail->FromName = "TEST";
          $mail->Timeout=5;
          $mail->AddAddress($destino);
          $mail->Subject = "RAPSINET seremi";
          $mail->Body = "TEST";
          $exito = $mail->Send();

          $intentos=1; 
          while ((!$exito) && ($intentos < 5)) {
                sleep(5);
                //echo $mail->ErrorInfo;
                $exito = $mail->Send();
                $intentos=$intentos+1;	

           }
        
        */
        
        
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");

        $xaprobarsolicitud->cierro(1);

        return $xaprobarsolicitud;  
       
    }
    
    
    function rechazar($id_cabecera,$id_estado)
    {
       $rer = new xajaxResponse();
       //$rer->addAlert($id_cabecera.$id_estado);
       $rer->addScript("opener.xajax_lista_solicitudes();");

       return $rer; 
    }
   
    function ver_adjunto($id_adjunto)
    {
       $ver = new xajaxResponse();
       $ventana = $ver->redi_cierro('visualizar_adjunto.php?clave='.$id_adjunto);

       return $ver; 
    }

    function lista_adjuntos($id)
    {   global $smarty; 
        $adjunto = new xajaxResponse();
        
        $archivos = new DAODepartamentoSaludPublica();
        $_SESSION['arrAdjuntos']  = $archivos->lista_archivos($id);
       
        $tabla = $smarty->fetch('grilla_adjuntos.tpl');
        $adjunto->addAssign("detalle1","innerHTML",$tabla);
        return $adjunto;

    }
    
    function lista_eventos($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        //$evento->addAlert($idCabeceraSolicitud);
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesEvento']=$eventos->eventos_solicitud($idCabeceraSolicitud,3);
        $tabla = $smarty->fetch('grilla_eventos.tpl');
        $evento->addAssign("detalle_eventos","innerHTML",$tabla);
        return $evento;

    }
     
      $ver = $_GET['ver'];


      if (in_array($estado, $rechazos))
      {
          $_SESSION['activo'] = "";
      }
      
     
      if ($ultimo == 7)
      {
          $mensaje = new xajaxResponse();
          $_SESSION['activo'] = "";
          //$mensaje->addAlert("La Solicitud de Internación paso a ser Solicitud de Traslado, recuerde cambiar el establecimiento de Internación");
          echo "<script>alert('La Solicitud de Internación paso a ser Solicitud de Traslado, recuerde cambiar el establecimiento de Internación');</script>";
          //return $mensaje;
          
          
      }
      
    
    function llena_regiones()//lleno combo regiones con lo que hay en la bd
    {    
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      $_SESSION['arrRegiones'] = $combo->regiones();
      $region = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idRegionSol'];
      $region2 = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idRegionPac'];
      $llena->addScript("$('#region_sol option[value=".$region."]').attr('selected',true);");
      $llena->addScript("$('#region_pac option[value=".$region2."]').attr('selected',true);");
      return $llena;
    }

      
      function llena_regiones2($regiones)//lleno combo regiones con lo que hay en la bd
      {    
          $llena = new xajaxResponse();
          $combo = new DAODepartamentoSaludPublica(); 
          $llena->addAlert("REGION: ", $regiones);

          $arreglo = $combo->regiones();

          foreach ($arreglo as $valor) 
          {
             
              $llena->CreaOpcion('region_sol',$valor['nomregion'],$valor['id_region']);
              
          }   
          
          //$llena->addScript("$('#region_sol option[value=".$regiones."]').attr('selected',true);");
          $llena->addScript("$('#region_sol option[value=".$regiones."]').attr('selected',true);");
          return $llena;
          
      }
      
        function llena_establecimientos($est_sol)
        {
          
          $llena = new xajaxResponse();
          //$llena->addAlert($est_sol);
          $combo = new DAODepartamentoSaludPublica(); 


          $arreglo = $combo->llena_establecimientos();

          foreach ($arreglo as $valor) 
          {
              $llena->CreaOpcion('establecimiento_sol',$valor['nombre'],$valor['codigo']);
              if($valor['codigo']==$est_sol)
              {
                $llena->addScript("document.formId.establecimiento_sol.selectedIndex='".$valor['codigo']."';");
              }

          }    
          
          return $llena;
          
        }
        
        function llena_establecimientos2($est_sol)
        {
          
          $llena = new xajaxResponse();
          $combo = new DAODepartamentoSaludPublica(); 


          $arreglo = $combo->llena_establecimientos();

          foreach ($arreglo as $valor) 
          {
              $llena->CreaOpcion('establecimiento_int',$valor['nombre'],$valor['codigo']);
              if($valor['codigo']==$est_sol)
              {
               $llena->addScript("document.formId.establecimiento_int.selectedIndex='".$valor['codigo']."';");
              }

          }    
          
          $llena->addScript("$('#establecimiento_int option[value=".$est_sol."]').attr('selected',true);");
          
          return $llena;
          
        }
  
    function llena_provincias($idregion)
    {
        $provincias = new xajaxResponse();
        
        $proves = new DAODepartamentoSaludPublica(); 
        $listaprovs = $proves->provincias($idregion);

        $provincias->addScript("$('#ciudad_sol').empty();");
        $provincias->CreaOpcion('ciudad_sol','Seleccione Provincia','0');
        
        foreach ($listaprovs as $valor) 
        {
          $provincias->CreaOpcion('ciudad_sol',$valor['nom_provincia'],$valor['id_provincias']); 
                 
        } 
       
        $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idCiudadSol']; 
       
        $provincias->addScript("$('#ciudad_sol option[value=".$indice."]').attr('selected',true);");

        return $provincias;

    }

    function llena_provincias2($idregion)
    {
        $provincias = new xajaxResponse();
        
        
        $proves = new DAODepartamentoSaludPublica(); 
        $listaprovs = $proves->provincias($idregion);

        $provincias->addScript("$('#ciudad_pac').empty();");
        $provincias->CreaOpcion('ciudad_pac','Seleccione Provincia','0');
        
        foreach ($listaprovs as $valor) 
        {
          $provincias->CreaOpcion('ciudad_pac',$valor['nom_provincia'],$valor['id_provincias']); 
                 
        } 
       
        $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idCiudadPac']; 
       
       $provincias->addScript("$('#ciudad_pac option[value=".$indice."]').attr('selected',true);");

        return $provincias;
        
    }
    
    function llena_comunas2($idprovincia)
    {
        $comuna = new xajaxResponse();
        //$comuna->addAlert($idprovincia);
        $comus = new DAODepartamentoSaludPublica(); 
        $listacoms = $comus->comunas($idprovincia);

        $comuna->addScript("$('#comuna_pac').empty();");
        $comuna->CreaOpcion('comuna_pac','Seleccione Comuna','0');
        foreach ($listacoms as $valor) 
        {
            $comuna->CreaOpcion('comuna_pac',$valor['nom_comuna'],$valor['id_comuna']);
        }  

        
        $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idComunaPac']; 
       
        $comuna->addScript("$('#comuna_pac option[value=".$indice."]').attr('selected',true);");
     
        return $comuna;
    }

    function llena_comunas($idprovincia)
    {
        $comuna = new xajaxResponse();
        $comuna->addAlert($idprovincia);
        $comus = new DAODepartamentoSaludPublica(); 
        $listacoms = $comus->comunas($idprovincia);

        $comuna->addScript("$('#comuna_sol').empty();");
        $comuna->CreaOpcion('comuna_sol','Seleccione Comuna','0');
        foreach ($listacoms as $valor) 
        {
            $comuna->CreaOpcion('comuna_sol',$valor['nom_comuna'],$valor['id_comuna']);
        }  

        
        $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idComunaSol']; 
       
        $comuna->addScript("$('#comuna_sol option[value=".$indice."]').attr('selected',true);");
     
        return $comuna;
    }
    
    function llena_sexo($sexo_sol)
    {

      $llena = new xajaxResponse();
      
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->llena_sexo();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('sexo_sol',$valor['descripcion'],$valor['codigo']);
          if($valor['codigo']==$sexo_sol)
          {
             $llena->addScript("document.formId.sexo_sol.selectedIndex='".$valor['codigo']."';");
          }
      }     
      
       $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sexoSol']; 
       
       $llena->addScript("$('#sexo_sol option[value=".$indice."]').attr('selected',true);");

      return $llena;
    }
    
     function llena_sexo2($sexo_pac)
    {

      $llena = new xajaxResponse();
      
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->llena_sexo();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('sexo_pac',$valor['descripcion'],$valor['codigo']);
          if($valor['codigo']==$sexo_pac)
          {
             $llena->addScript("document.formId.sexo_pac.selectedIndex='".$valor['codigo']."';");
          }
      }     
      
       $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sexoPac']; 
       
       $llena->addScript("$('#sexo_pac option[value=".$indice."]').attr('selected',true);");

      return $llena;
    }
    
    
    function llena_vinculacion($vin)
    {

      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->llena_vinculacion();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('vinculacion_pac',$valor['descripcion'],$valor['codigo']);
          if($valor['codigo']==$vin)
          {
            $llena->addScript("document.formId.vinculacion_pac.selectedIndex='".$valor['codigo']."';");
          }   
      
      }  
      
      $indice = $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_vinculacion']; 
       
      $llena->addScript("$('#vinculacion_pac option[value=".$vin."]').attr('selected',true);");
      
      return $llena;
    }
    

   

      $smarty->assign('xajax_js', $xajax->getJavascript('xajax')); 
      $smarty->assign('ver', $ver); 
      $smarty->assign('activo', $_SESSION['activo']); 
      $smarty->assign('estados', array(2,4,6,8)); 
      $smarty->display('VistaSolInternacionAdmin.tpl');
?>