<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoSecretaria.php';
   
   	
    
    $xajax = new xajax(); 

    $xajax->registerFunction("filtra_solinv");
    $xajax->registerFunction("filtra_solalta");
    $xajax->registerFunction("rakim_solinv");
    $xajax->registerFunction("lista_solicitudes");
    $xajax->registerFunction("rakim_solint");
    $xajax->registerFunction("rakim_solalta");
    $xajax->registerFunction("rakim_soleva");
    $xajax->registerFunction("pdf_solinv");
    $xajax->registerFunction("pdf_solint");
    $xajax->registerFunction("filtra_solevas");
        
    $xajax->processRequests(); 



    function filtra_solevas($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolevas']=$lista->lista_solevas($id_sol,$id_estado,$fecha,6);
      $smarty->assign('departamentito',6);
      $tabla = $smarty->fetch('grilla_solevas.tpl');
      $solinv->addAssign("evaluaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesEvaluaciones').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }

     function pdf_solinv() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solinv_secre.php');
        return $pdf;
     }
     
     function pdf_solint() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solint_secre.php');
        return $pdf;
     }
    
    function filtra_solinv($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolinvs']=$lista->lista_solinv($id_sol,$id_estado,$fecha,6);
      $smarty->assign('departamentito',6);
      $tabla = $smarty->fetch('grilla_solinv_secre.tpl');
      $solinv->addAssign("internaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesInternacionNoVoluntaria').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }
    
    function filtra_solalta($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      

       $id_sol = '';
       $id_estado = 'Seleccione';
       $fecha = '';
       
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolalta']=$lista->lista_solalta($id_sol,$id_estado,$fecha,6);
      $smarty->assign('departamentito',6);
      $tabla = $smarty->fetch('grilla_solalta_secre.tpl');
      $solinv->addAssign("alta","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesAltaAdministrativa').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }

   
   function rakim_solinv($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
   	 $rakim->addAlert($rakim_solinv->rakim_solinv($id_sol,$num));
   	 $rakim->addScript("xajax_filtra_solinv('','','');");
   	 }
   	 return $rakim;
   }
    
   function rakim_solint($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
   	 $rakim->addAlert($rakim_solinv->rakim_solint($id_sol,$num));
   	 $rakim->addScript("xajax_lista_solicitudes();");
   	 }
   	 return $rakim;
   }

   function rakim_soleva($id_sol,$num)
   {
     $rakim = new xajaxResponse();
     
      $rakim_solinv = new DAODepartamentoSecretaria();
     if(empty($num))
     {
      $rakim->addAlert("Rakim en blanco....ingrese Rakim");
     }
     else
     {
     $rakim->addAlert($rakim_solinv->rakim_soleva($id_sol,$num));
     $rakim->addScript("xajax_filtra_solevas();");
     }
     return $rakim;
   }
   
   function rakim_solalta($id_sol,$num)
   {
   	 $rakim = new xajaxResponse();
   	 
   	  $rakim_solinv = new DAODepartamentoSecretaria();
   	 if(empty($num))
   	 {
   	 	$rakim->addAlert("Rakim en blanco....ingrese Rakim");
   	 }
   	 else
   	 {
             $rakim->addAlert($rakim_solinv->rakim_solalta($id_sol,$num));
             $rakim->addScript("xajax_filtra_solalta('','','');");
   	 }
   	 return $rakim;
   }


   function lista_solicitudes($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 6; 
       $idSolicitud = '';
       $idEstado = 'Seleccione';
       $fecha = '';
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoSecretaria();
       $_SESSION['arrSolicitudes'] = $arr_solicitudes->lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha);
       $smarty->assign('departamentito',6);
       $tabla = $smarty->fetch('grilla_solint_secre.tpl');
       $xsolicitudes->addAssign("solicitudes","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesInternacionAdmin').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
       return $xsolicitudes;
       

    }

       

    if(isset($_SESSION['USUA_nombres']))
    { $smarty->assign('USUA_nombres', $_SESSION['USUA_nombres']);
      $smarty->assign('USUA_apellidos', $_SESSION['USUA_apellidos']);
      $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
      $smarty->display('AdminSecretaria.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
