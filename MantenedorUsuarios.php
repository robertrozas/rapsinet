<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_Usuarios.php';
    
   	
    
    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("muestra_modifica");
	$xajax->registerFunction("elimina");
	$xajax->registerFunction("lista_usuarios");
	$xajax->registerFunction("agrega_user");
	$xajax->registerFunction("limpia");
        $xajax->registerFunction("llena_departamentos");
        $xajax->registerFunction("llena_perfiles");
        $xajax->registerFunction("llena_establecimientos");
	$xajax->processRequests(); 


	function muestra_modifica($codigo)
	{
		$funcion = new xajaxResponse();
		$muestra = new DAO_Usuarios();
		$arrMuestra = array();
		$arrMuestra = $muestra->lista_uno($codigo);

		$funcion->addAssign("rut","value",$arrMuestra['USUA_rut']);
		$funcion->addAssign("nombres","value",$arrMuestra['USUA_nombres']);
		$funcion->addAssign("apellidos","value",$arrMuestra['USUA_apellidos']);
		$funcion->addAssign("direccion","value",$arrMuestra['USUA_direccion']);
		$funcion->addAssign("celular","value",$arrMuestra['USUA_celular']);
		$funcion->addAssign("fono","value",$arrMuestra['USUA_fono']);
		$funcion->addAssign("correo","value",$arrMuestra['USUA_correo']);
		$funcion->addAssign("usuario","value",$arrMuestra['USUA_usuario']);
		$funcion->addAssign("clave","value",$arrMuestra['USUA_contrasena']);
                $funcion->addAssign("establecimiento", "value", $arrMuestra['EST_id']);
                $funcion->addAssign("departamento", "value", $arrMuestra['DEPART_id']);
                $funcion->addAssign("perfil", "value", $arrMuestra['PER_id']);

		$_SESSION['opcion'] = "MOD";
		$_SESSION['clave'] = $codigo;
		return $funcion;
	}

	function elimina($codigo)
	{
	  $elimina = new xajaxResponse();
          $borra_usuario = new DAO_Usuarios();
          $borra_usuario->elimina_usuario($codigo);
          $elimina->addScript('xajax_lista_usuarios();');
          $elimina->addAlert("Ha eliminado al Usuario id: ".$codigo); 
	  return $elimina;
	}
        
            
        function llena_departamentos()
        {
            $departamentos = new xajaxResponse();
            $dep = new DAO_Usuarios(); 
            $listadep = $dep->departamentos();

            $departamentos->addScript("$('#departamento').empty();");
            $departamentos->CreaOpcion('departamento','Seleccione Departamento','0');
            foreach ($listadep as $valor) 
            {
                $departamentos->CreaOpcion('departamento',$valor['descripcion'],$valor['id_departamento']);
            }  

            return $departamentos;
         }

         function llena_establecimientos()
        {
            $establecimientos = new xajaxResponse();
            $est = new DAO_Usuarios(); 
            $listaest = $est->establecimientos();

            $establecimientos->addScript("$('#establecimiento').empty();");
            $establecimientos->CreaOpcion('establecimiento','Seleccione Establecimiento','0');
            foreach ($listaest as $valor) 
            {
                $establecimientos->CreaOpcion('establecimiento',$valor['descripcion'],$valor['id_establecimiento']);
            }  

            return $establecimientos;
         }

         function llena_perfiles()
        {
            $perfiles = new xajaxResponse();
            $per = new DAO_Usuarios(); 
            $listaper = $per->perfiles();

            $perfiles->addScript("$('#perfil').empty();");
            $perfiles->CreaOpcion('perfil','Seleccione Perfil','0');
            foreach ($listaper as $valor) 
            {
                $perfiles->CreaOpcion('perfil',$valor['descripcion'],$valor['id_perfil']);
            }  

            return $perfiles;
         }

	function lista_usuarios()
	{

	   global $smarty;
	   $xusuarios = new xajaxResponse();
	   $arr_usuarios = new DAO_Usuarios();
	   $_SESSION['arrUsuarios'] = $arr_usuarios->lista_completa();
	   $tabla = $smarty->fetch('grilla_usuarios.tpl');
	   $xusuarios->addAssign("view1","innerHTML",$tabla);
	   $_SESSION['opcion'] = "ING";
	   return $xusuarios;

	}

    function agrega_user($form)
    {
       $agrega = new xajaxResponse();
       
       $objUsado = new DTOUsuario(
       	 $form['rut'], $form['nombres'], $form['apellidos'], $form['direccion'],$form['celular'],$form['fono'],$form['correo'],
       	 $form['usuario'],$form['clave'],$form['departamento'],$form['perfil'],$form['establecimiento']
       	);

       $SW = 1; //un elemento que este vacio en el formulario y no hago nada ;)

       foreach ($form as $elemento)//checkeo por cada elemento del formulario
       {   if(empty($elemento))
           {
           	  $SW = 0;
              break;
           }
       }	


     if ($SW == 1)
     {
       $accion = new DAO_Usuarios();

       if ($_SESSION['opcion']=="ING") 
       {$agrega->addAlert("INGRESA");
       	$accion->ingresa($objUsado);
       }

       if ($_SESSION['opcion']=="MOD") 
        {$agrega->addAlert("MODIFICA");
         $accion->modifica($objUsado,$_SESSION['clave']);
         }

          $agrega->addScript('document.formUser.reset();');
          $agrega->addScript('xajax_lista_usuarios();');

      }
      else
      {
      	$agrega->addAlert("Campos Vacíos en el formulario");
      }

  
       return $agrega;
    }

    function limpia()
    {
       $limpia = new xajaxResponse();
       $_SESSION['opcion'] = "ING";

       return $limpia;
    }

	  
    
    if(isset($_SESSION['USUA_nombres']))
    {   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
        $smarty->display('MantenedorUsuarios.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
