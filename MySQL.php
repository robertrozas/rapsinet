<?php
class MySQL 
{
    private $conexion;
    private $total_consultas;

    public function __construct(){
        if(!isset($this->conexion))
        {
             $this->conexion = (mysql_connect("192.168.0.103","root","seremi"))
            or die(mysql_error());
            mysql_select_db("rapsinet",$this->conexion) or die(mysql_error());
        }
    }

	
    public function consulta($consulta){ 
        $this->total_consultas++; 
        
        if(!$resultado = mysql_query($consulta,$this->conexion))
            {
            throw new Exception("Error en Consulta....consulte error MySQL num: ".mysql_errno());
            $consulta = $consulta." error num: ".mysql_errno();
            }

        $logfile=fopen("log_consultas.csv","a"); 
        fputs($logfile,date("d-m-Y H:i:s")."  ".$consulta."\n");

        return $resultado;
   
    }

    public function fetch_array($resultado){
        return mysql_fetch_array($resultado);
    }
    
    public function fetch_assoc($resultado){
        return mysql_fetch_assoc($resultado);
    }
    
    public function result($resultado){
        return mysql_result($resultado, 0);
    }
    
    public function num_rows($resultado){
        return mysql_num_rows($resultado);
    }

    public function getTotalConsultas(){
        return $this->total_consultas; 
    }
    
    public function dispose(&$resultado){
        mysql_free_result($resultado);
    }

    public function close(){
        return mysql_close($this->conexion);
    }
}
?>