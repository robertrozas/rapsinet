<?php

    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_SolicitudEvaluacion.php';

    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("solicita");
	$xajax->registerFunction("provincias");
  $xajax->registerFunction("provincia");
	$xajax->registerFunction("comunas");
  $xajax->registerFunction("comuna");
	$xajax->registerFunction("hospital");
  $xajax->registerFunction("hospital_psi");
	$xajax->registerFunction("limpia");
	$xajax->registerFunction("texto");
	$xajax->registerFunction("numero");
	$xajax->registerFunction("validarut");

	$xajax->processRequests(); 

    $lista = new DAO_SolicitudEvaluacion();

	$_SESSION['Regiones'] = $lista->regiones();


	function solicita($form)
	{
		$solicita = new xajaxResponse();
    
		$ingresa = new DAO_SolicitudEvaluacion();
		$solicita->addAlert($ingresa->ingresa_solicitudtrat($form));

                 $solicita->addScript('document.formTrata.reset();');
		return $solicita;
	}

	

 function provincias($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_SolicitudEvaluacion(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincia').empty();");
      $provincias->addScript("$('#comuna').empty();");
      $provincias->CreaOpcion('provincia','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincia',$valor['nom_provincia'],$valor['id_provincias']);
      }  
          
      
    return $provincias;
  }


   function provincia($region)
  {
    $provincias = new xajaxResponse();
    
      
      $proves = new DAO_SolicitudEvaluacion(); 
      $listaprovs = array();

      $listaprovs = $proves->provincias($region);

      $provincias->addScript("$('#provincias').empty();");
      $provincias->addScript("$('#comunas').empty();");
      $provincias->CreaOpcion('provincias','Seleccione Provincia','0');

      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('provincias',$valor['nom_provincia'],$valor['id_provincias']);
      }  
          
      
    return $provincias;
  }


  function comunas($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_SolicitudEvaluacion(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna').empty();");
      $comuna->CreaOpcion('comuna','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
     
      return $comuna;

  }

  function comuna($idprovincia)
  {
      $comuna = new xajaxResponse();
      $comus = new DAO_SolicitudEvaluacion(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comunas').empty();");
      $comuna->CreaOpcion('comunas','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comunas',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
     
      return $comuna;

  }

 function hospital($id_comuna)
 {
 	$hospital = new xajaxResponse();
 	$dao = new DAO_SolicitudEvaluacion();
 	$hospitales = $dao->hospital_comuna($id_comuna);

 	$hospital->addScript("$('#hospital').empty();");
    $hospital->CreaOpcion('hospital','Seleccione Establecimiento','0');
      foreach ($hospitales as $valor) 
      {
      $hospital->CreaOpcion('hospital',$valor['nombre'],$valor['codigo']);
      }  
     

 	return $hospital;
 }
 
 function hospital_psi()
 {
 	$hospital_psi = new xajaxResponse();
 	$dao = new DAO_SolicitudEvaluacion();
 	$hospitales = $dao->hospital_psi();

          $hospital_psi->addScript("$('#hospital_psi').empty();");
          $hospital_psi->CreaOpcion('hospital_psi','Seleccione Establecimiento','0');
          foreach ($hospitales as $valor) 
          {
            $hospital_psi->CreaOpcion('hospital_psi',$valor['nombre'],$valor['codigo']);
          }  
 	return $hospital_psi;
 }


 function limpia()
 {
 	$limpia = new xajaxResponse();
 	$limpia->addScript("$('#provincia').empty();");
    $limpia->addScript("$('#comuna').empty();");
    $limpia->addScript("$('#hospital').empty();");
 	return $limpia;
 }

  function texto($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-ZáéíóúAÉÍÓÚñÑ\s]/g,'');});");
      return $texto;
    }

   function numero($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^0-9]/g,'');});");
      return $texto;
    }

    function validarut($rut,$nombre_input)
   { 
    $valida = new xajaxResponse();

    $pos = strripos($rut,"-");

    if($pos === false)
    {}
    else
     {
     $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];

    if($digito!=""){
     $s=1;
      for($m=0;$r!=0;$r/=10)
        $s=($s+$r%10*(9-$m++%6))%11;
      $dv = chr($s?$s+47:75);

    if($dv==$digito)
    { }
    else
    { $valida->addAlert("Rut incorrecto, ingrese nuevamente"); 
      $valida->addScript("document.formTrata.".$nombre_input.".focus();");}  }
    
      } 
    return $valida;
    }


	$smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->display('SolicitudEvaluacionYTratamiento.tpl');

?>
