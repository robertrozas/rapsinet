 <?php

    session_start();
    include 'xajax/xajax.inc.php';

    $xajax = new xajax(); 
    $xajax->registerFunction("logout");
    $xajax->processRequests(); 

    function logout()
    {
      session_destroy();
      session_unset($_SESSION['USUA_nombres']);
      $salir = new xajaxResponse();
      $salir->redirect('index.php');
      return $salir;  
        
    }

 ?>
<?php
$xajax->printJavascript("xajax");
?>

<script>
xajax_logout();
</script>