<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoSaludPublica.php';
   
   	
    
    $xajax = new xajax(); 

    $xajax->registerFunction("lista_solicitudes");
    $xajax->registerFunction("lista_historico_usuarios");
    $xajax->registerFunction("lista_log_solicitudes");
    $xajax->registerFunction("test");
    $xajax->registerFunction("llena_estado");
    $xajax->registerFunction("llena_estado2");
    $xajax->registerFunction("llena_estado3");
    $xajax->registerFunction("llena_estado4");
    $xajax->registerFunction("llena_tipo");
    $xajax->registerFunction("filtra_estado");
    $xajax->registerFunction("filtra_estado2");
    $xajax->registerFunction("filtra_tipo");
    $xajax->registerFunction("filtra_fecha_a");
    $xajax->registerFunction("filtra_fecha_a2");
    $xajax->registerFunction("filtra_log_solicitudes");
    $xajax->registerFunction("filtra_log_solicitudes2");
    $xajax->registerFunction("filtra_solicitudesinternacionadmin");
    $xajax->registerFunction("filtra_historico_usuarios");
    $xajax->registerFunction("filtra_id");
    $xajax->registerFunction("filtra_id2");
    $xajax->registerFunction("visarsolinv");
    $xajax->registerFunction("visarSolicitudAA");
    
    
    $xajax->registerFunction("filtra_solinv");
    $xajax->registerFunction("filtra_solaa");
    $xajax->registerFunction("detalle_solintadmin");
    $xajax->registerFunction("resolucion_SolicitudINV");
    $xajax->registerFunction("visarsolinternacionadmin");
    $xajax->registerFunction("eventos_solicitudes_internacion");
    $xajax->registerFunction("pdf");
    $xajax->registerFunction("visarsoleva");
    $xajax->registerFunction("filtra_solevas");
    $xajax->registerFunction("filtra_per_alta");
    
    $xajax->processRequests(); 


    function lista_historico_usuarios()
    {
       global $smarty;
       $xhistoricousuarios = new xajaxResponse();
       $arr_historico_usuarios = new DAODepartamentoSaludPublica();
       $_SESSION['arrHistoricoUsuarios'] = $arr_historico_usuarios->lista_historico_usuarios();

       $tabla = $smarty->fetch('grilla_historicoUsuarios.tpl');

       $xhistoricousuarios->addAssign("log_historico_usuarios","innerHTML",$tabla);
       $xhistoricousuarios->addScript("$('#loghistoricousuarios').dataTable();");
       return $xhistoricousuarios;
    }
  


    function visarsoleva($id_cab,$ver)
    {
      $visarsoleva = new xajaxResponse();
      
      $visar = new DAODepartamentoSaludPublica();
      $_SESSION['arrDetsoleva']=$visar->visar_soleva($id_cab); 
      $visarsoleva->redi_blank('VistaSolEva.php?ver='.$ver);

      return $visarsoleva;
    }


    function filtra_solevas($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSaludPublica();
      $_SESSION['arrSolevas']=$lista->lista_solevas($id_sol,$id_estado,$fecha,1);
      $smarty->assign('departamentito',1);
      $tabla = $smarty->fetch('grilla_solevas.tpl');
      $solinv->addAssign("evaluaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesEvaluaciones').dataTable();");
      
      return $solinv;
    }
    
    function visarsolinv($id_cab,$ver)
    {
      $visarsolinv = new xajaxResponse();
      $visarsolinv->addAlert($id_cab);

      $visar = new DAODepartamentoSaludPublica();
      $_SESSION['arrDetsolinv']=$visar->visar_solinv($id_cab); 
      $visarsolinv->redi_blank('VistaSolinvDSP.php?ver='.$ver);

      return $visarsolinv;
    }
    
    function visarsolinternacionadmin($idCabeceraSolicitud,$ver)
    {
      $visarsolinternacion = new xajaxResponse();
      $visarsolinternacion->addAlert($idCabeceraSolicitud);

      $visar = new DAODepartamentoSaludPublica();
      $_SESSION['arrDetsolinternacionadmin']=$visar->visar_solinternacionadmin($idCabeceraSolicitud);
      
      /*if($_SESSION['arrDetsolinternacionadmin']['ultimoTipoSolicitud'] == 7)
      {
        $ver = 1;  
      }   */ 
      
      $visarsolinternacion->redi_blank('VistaSolInternacionAdmin.php?ver='.$ver);
      return $visarsolinternacion;
    }
    
    function visarSolicitudAA($idCabeceraSolicitud,$ver)
    {
      $visarsolalta = new xajaxResponse();
      $visarsolalta->addAlert($idCabeceraSolicitud);
      $visarsolalta->addAlert($ver);
      $visar = new DAODepartamentoSaludPublica();
      $_SESSION['arrDetsolaa']=$visar->visar_solaa($idCabeceraSolicitud);
      
      
      $visarsolalta->redi_blank('VistaSolAltaHospitalizacion.php?ver='.$ver);
      return $visarsolalta;
    }
    
    

    function filtra_solinv($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSaludPublica();
      $_SESSION['arrSolinvs']=$lista->lista_solinv($id_sol,$id_estado,$fecha,1);
      $smarty->assign('departamentito',1);
      $tabla = $smarty->fetch('grilla_solinvs.tpl');
      $solinv->addAssign("internaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesInternacionNoVoluntaria').dataTable();");
      
      return $solinv;
    }

     function pdf() //invoco al pdf que me refleja la grilla historial evaluaciones
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_historico_evaluaciones.php');
        return $pdf;
     }
     
     
    function filtra_solaa($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      $solaa = new xajaxResponse();
      $lista = new DAODepartamentoSaludPublica();
      $_SESSION['arrSolaa']=$lista->lista_solaa($id_sol,$id_estado,$fecha,1);
      //$solaa->addAlert($_SESSION['arrSolaa']);
      $smarty->assign('departamentito',1);
      $tabla = $smarty->fetch('grilla_solaltaadmin.tpl');
      $solaa->addAssign("hospitalizacion","innerHTML",$tabla);
      $solaa->addScript("$('#solicitudesAltaAdministrativa').dataTable();");

      return $solaa;
    }
    
     function filtra_per_alta($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      $solaa = new xajaxResponse();
      $lista = new DAODepartamentoSaludPublica();
      $_SESSION['arrSolaa_listas']=$lista->lista_solperalta($id_sol,$id_estado,$fecha,1);
      //$solaa->addAlert($_SESSION['arrSolaa']);
      $smarty->assign('departamentito',1);
      $tabla = $smarty->fetch('grilla_solaltalistos.tpl');
      $solaa->addAssign("hospitalizacion_listas","innerHTML",$tabla);
      $solaa->addScript("$('#solicitudesAltasListas').dataTable();");

      return $solaa;
    }
    
     
     function resolucion_SolicitudINV($idCabeceraSolicitud) 
     {  
        
        $pdf = new xajaxResponse();
        $pdf->AddAlert($idCabeceraSolicitud);
        $arr_res_sol_inv = new DAODepartamentoSaludPublica();
        $_SESSION['arrResSolINV'] = $arr_res_sol_inv->resolucion_sol_inv($idCabeceraSolicitud);
        $pdf->redi_blank('fpdf/pdf_resolucion_inv.php');
        return $pdf;
     }


     function detalle_solintadmin($numsol,$ver) //detalle una sol en particular segun numsol
     {
       $detalle = new xajaxResponse();
       

       $solintadmin = new DAODepartamentoSaludPublica();

       $_SESSION['arrSols'] = $solintadmin->detalle_solintadmin_id($numsol);
       $detalle->redi_blank('VistaSolicitud.php?ver='.$ver);

       return $detalle;
     }
     
      function filtra_fecha($opcion)//filtro grilla historial evaluaciones por fecha asc o desc
      {

       global $smarty;
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSaludPublica();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_log_fecha($opcion);
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       return $xlogsolicitudes;
       
      }

    
    function filtra_tipo($filtro,$tipo,$fecha) //filtro grilla por tipo solicitud
    {
        
        global $smarty;
        $test = new xajaxResponse();
        
        $lista = new DAODepartamentoSaludPublica();
        /*$opcion = 2;*/ 
        $_SESSION['arrLogSolicitudes'] = $lista->lista_filtros_log($filtro,$tipo,$fecha);

        $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
        $test->addAssign("log_solicitudes","innerHTML",$tabla);
       
        //$test->addAlert($filtro);
        return $test;
    }

    
    function filtra_solicitudesinternacionadmin($idSolicitud, $idEstado, $fecha) //filtro grilla por tipo solicitud
    {
        
          global $smarty;
          $test = new xajaxResponse();
          $idDepartamento = '1';
          $lista = new DAODepartamentoSaludPublica();
          /*$opcion = 2;*/ 
          $_SESSION['arrSolicitudes'] = $lista->lista_completa_solicitudes($idDepartamento, $idSolicitud, $idEstado, $fecha);

          $tabla = $smarty->fetch('grilla_solicitudesInternacionAdmin.tpl');
          $test->addAssign("solicitudes","innerHTML",$tabla);
          $test->addScript("$('#solicitudesInternacionAdmin').dataTable();");
//       
//      //$test->addAlert($filtro);
        return $test;
    }
    
    function filtra_id2($idSolicitud, $fecha) //filtro grilla por tipo solicitud
    {
        
          global $smarty;
          $test = new xajaxResponse();
          $lista = new DAODepartamentoSaludPublica();
          /*$opcion = 2;*/ 
          $_SESSION['arrHistoricoUsuarios'] = $lista->id_lista_historico_usuarios($idSolicitud, $fecha);

          $tabla = $smarty->fetch('grilla_historicoUsuarios.tpl');
          $test->addAssign("log_historico_usuarios","innerHTML",$tabla);
//       
//      //$test->addAlert($filtro);
        return $test;
    }


    function llena_estado()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      
      
      $arreglo = $combo->llena_estados();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }
    
    function llena_estado2()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      
      
      $arreglo = $combo->llena_estados2();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado2',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }
    
     function llena_estado3()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      
      
      $arreglo = $combo->llena_estados2();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado4',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }

    function llena_estado4()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      
      
      $arreglo = $combo->llena_estados2();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('estado5',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }

    
    function llena_tipo()//lleno combo estados con lo que hay en la bd
    {
      
      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 
      
      
      $arreglo = $combo->llena_tipo();
      
      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('tipo',$valor['descripcion'],$valor['codigo']);
      }     
      
      return $llena;
    }
    
    
    


    function lista_solicitudes($idSolicitud,$idEstado,$fecha)
    {
       $idDepartamento = 1; 

       $idSolicitud = '';
       $idEstado = 'Seleccione';
       $fecha = '';
       global $smarty;
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoSaludPublica();
       $_SESSION['arrSolicitudes'] = $arr_solicitudes->lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha);
       $smarty->assign('departamentito',1);
       $tabla = $smarty->fetch('grilla_solicitudesInternacionAdmin.tpl');
       $xsolicitudes->addAssign("solicitudes","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesInternacionAdmin').dataTable();");

       return $xsolicitudes;

    }
    
    
     function filtra_log_solicitudes($filtro,$tipo,$fecha)//filtro grilla historial evaluaciones por fecha asc o desc
     {

       global $smarty;
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSaludPublica();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_filtros_log($filtro,$tipo,$fecha);
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       $xlogsolicitudes->addScript("$('#logsolicitudes').dataTable();");
       return $xlogsolicitudes;
       
      }
      
      function filtra_log_solicitudes2($filtro,$tipo,$fecha)//filtro grilla historial evaluaciones por fecha asc o desc
     {

       global $smarty;
       
       $filtro = 'Seleccione';
       $tipo = 'Seleccione';
       $fecha = '';
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSaludPublica();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_filtros_log($filtro,$tipo,$fecha);
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       $xlogsolicitudes->addScript("$('#logsolicitudes').dataTable();");
       return $xlogsolicitudes;
       
      }
      
    function lista_log_solicitudes()
    {

       global $smarty;
       $xlogsolicitudes = new xajaxResponse();
       $arr_log_solicitudes = new DAODepartamentoSaludPublica();
       $_SESSION['arrLogSolicitudes'] = $arr_log_solicitudes->lista_completa_log_solicitudes();
       $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');

       $xlogsolicitudes->addAssign("log_solicitudes","innerHTML",$tabla);
       $xlogsolicitudes->addScript("$('#logsolicitudes').dataTable();");
       return $xlogsolicitudes;
       

    }
    
     

    function test($fecha)//filtro grilla historial evaluaciones por fecha en particular
    {
        
        global $smarty;
        $test = new xajaxResponse();
        
        $lista = new DAODepartamentoSaludPublica();
        $opcion = 1;
        $_SESSION['arrLogSolicitudes'] = $lista->lista_log_filtros($fecha,$opcion);

        $tabla = $smarty->fetch('grilla_logsolicitudes.tpl');
        $test->addAssign("log_solicitudes","innerHTML",$tabla);
       
        return $test;
    }
    
    function filtra_historico_usuarios($idSolicitud, $fecha)
    {

       global $smarty;
       $xhistoricousuarios = new xajaxResponse();
       $arr_historico_usuarios = new DAODepartamentoSaludPublica();
       $_SESSION['arrHistoricoUsuarios'] = $arr_historico_usuarios->filtro_historico_usuarios($idSolicitud, $fecha);
       $tabla = $smarty->fetch('grilla_historicoUsuarios.tpl');
       $xhistoricousuarios->addAssign("log_historico_usuarios","innerHTML",$tabla);
       $xhistoricousuarios->addScript("$('#loghistoricousuarios').dataTable();");
       return $xhistoricousuarios;
       

    }


    if(isset($_SESSION['USUA_nombres']))
    { $smarty->assign('USUA_nombres', $_SESSION['USUA_nombres']);
      $smarty->assign('USUA_apellidos', $_SESSION['USUA_apellidos']);
      $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
      $smarty->display('AdminDepSaludPublica.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
