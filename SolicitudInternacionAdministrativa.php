<?php

session_start();
include("include/include.php");  
include 'xajax/xajax.inc.php';
include ("DAO/DAOSolicitudInternacionAdministrativa.php");


$xajax = new xajax(); 
$xajax->registerFunction("ingresar_solicitud");
$xajax->registerFunction("llena_establecimientos");
$xajax->registerFunction("llena_regiones");
$xajax->registerFunction("llena_provincias");
$xajax->registerFunction("llena_provincias2");
$xajax->registerFunction("llena_comunas");
$xajax->registerFunction("llena_comunas2");
$xajax->registerFunction("llena_establecimientos2");
$xajax->registerFunction("llena_vinculacion");
$xajax->registerFunction("llena_sexo");
$xajax->registerFunction("llena_sexo2");
$xajax->registerFunction("validarut");
$xajax->registerFunction("texto");
$xajax->registerFunction("numero");


$xajax->processRequests();

   
    function texto($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-ZáéíóúAÉÍÓÚñÑ\s]/g,'');});");
      return $texto;
    }

   function numero($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^0-9]/g,'');});");
      return $texto;
    }

function llena_regiones()//lleno combo regiones con lo que hay en la bd
{    
      $llena = new xajaxResponse();
      $combo = new DAOSolicitudInternacionAdministrativa(); 
      $_SESSION['arrRegiones'] = $combo->regiones();
      return $llena;
}
    

function llena_provincias($idregion)
{
      $provincias = new xajaxResponse();
      $proves = new DAOSolicitudInternacionAdministrativa(); 
      $listaprovs = $proves->provincias($idregion);

      $provincias->addScript("$('#ciudad_sol').empty();");
      $provincias->CreaOpcion('ciudad_sol','Seleccione Provincia','0');
      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('ciudad_sol',$valor['nom_provincia'],$valor['id_provincias']);
      }  
     
      return $provincias;

}

function llena_provincias2($idregion)
{
      $provincias = new xajaxResponse();
      $proves = new DAOSolicitudInternacionAdministrativa(); 
      $listaprovs = $proves->provincias($idregion);
      
      $provincias->addScript("$('#ciudad_pac').empty();");
      $provincias->CreaOpcion('ciudad_pac','Seleccione Provincia','0');
      foreach ($listaprovs as $valor) 
      {
      $provincias->CreaOpcion('ciudad_pac',$valor['nom_provincia'],$valor['id_provincias']);
      }  
     
      return $provincias;

}


function llena_comunas($idprovincia)
{
      $comuna = new xajaxResponse();
      $comus = new DAOSolicitudInternacionAdministrativa(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna_sol').empty();");
      $comuna->CreaOpcion('comuna_sol','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna_sol',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      
      return $comuna;

}

function llena_comunas2($idprovincia)
{
      $comuna = new xajaxResponse();
      $comus = new DAOSolicitudInternacionAdministrativa(); 
      $listacoms = $comus->comunas($idprovincia);

      $comuna->addScript("$('#comuna_pac').empty();");
      $comuna->CreaOpcion('comuna_pac','Seleccione Comuna','0');
      foreach ($listacoms as $valor) 
      {
      $comuna->CreaOpcion('comuna_pac',$valor['nom_comuna'],$valor['id_comuna']);
      }  
     
      
      return $comuna;

}


function ingresar_solicitud($form)
{ 
   $xingresarsolicitud = new xajaxResponse();
   /*CABECERA*/ 
    
   $cmb_establecimiento_sol = $form['establecimiento'];
   
   /*DETALLE*/
   
   $diagnostico = $form['diagnostico'];
   
   $opcion_a = $form['opcion_a']; 
   $opcion_b = $form['opcion_b'];
   $opcion_c = $form['opcion_c'];
   $opciones = ($opcion_a.",".$opcion_b.",".$opcion_c);
  
   $otro_antecedente = $form['otro_antecedente'];
   $ultimo_tratamiento = $form['ultimo_tratamiento'];
   $cmb_establecimiento_int = $form['establecimiento2'];
   $nombre_medico = $form['nombre_medico'];
   $rut_medico = $form['rut_medico'];
   $correo_medico = $form['correo_medico'];
   $quien_solicito = $form['quien_solicito'];
   
   
   
   
   $nombres_sol = $form['nombres_sol'];
   $apaterno_sol = $form['apaterno_sol'];
   $amaterno_sol = $form['amaterno_sol'];
   $rut_sol = $form['rut_sol'];
   $edad_sol = $form['edad_sol'];
   $sexo_sol = $form['sexo_sol'];
   $calle_sol = $form['calle_sol'];
   $numerocasa_sol = $form['numerocasa_sol'];
   $sector_sol = $form['sector_sol'];
   $comuna_sol = $form['comuna_sol'];
   $ciudad_sol = $form['ciudad_sol'];
   $region_sol = $form['region_sol'];

   
   $telefono_sol = $form['telefono_sol'];
   $celular_sol = $form['celular_sol'];
   $correo_sol = $form['correo_sol'];
   
   /*------------------------------------*/
   
   $nombres_pac = $form['nombres_pac'];
   $apaterno_pac = $form['apaterno_pac'];
   $amaterno_pac = $form['amaterno_pac'];
   $rut_pac = $form['rut_pac'];
   $edad_pac = $form['edad_pac'];
   $sexo_pac = $form['sexo_pac'];
   $calle_pac = $form['calle_pac'];
   $numerocasa_pac = $form['numerocasa_pac'];
   $sector_pac = $form['sector_pac'];
   $comuna_pac = $form['comuna_pac'];
   $ciudad_pac = $form['ciudad_pac'];
   $region_pac = $form['region_pac'];
   
   
   /*$xingresarsolicitud->addAlert($comuna_sol);
   $xingresarsolicitud->addAlert($ciudad_sol);
   $xingresarsolicitud->addAlert($region_sol);
   
   $xingresarsolicitud->addAlert($comuna_pac);
   $xingresarsolicitud->addAlert($ciudad_pac);
   $xingresarsolicitud->addAlert($region_pac);*/
   
   $vinculacion_pac = $form['vinculacion_pac'];
   
   
   
   if(empty($cmb_establecimiento_sol))
   {
      $xingresarsolicitud->addAlert("Ingrese el establecimiento de solicitud");
      
   }
   
   else if(empty($diagnostico))
   {
      $xingresarsolicitud->addAlert("Ingrese el diagnostico");
      return false;
      
   }
   
   else if(empty($cmb_establecimiento_int))
   {
      $xingresarsolicitud->addAlert("Ingrese el establecimiento donde se internara");
      return false;
   }
   
   else if(empty($nombre_medico))
   {
      $xingresarsolicitud->addAlert("Ingrese el nombre del medico solicitante");
      
   }
   
   else if(empty($rut_medico))
   {
      $xingresarsolicitud->addAlert("Ingrese el rut del medico solicitante");
      return false;
   }
   
   else if(empty($correo_medico))
   {
      $xingresarsolicitud->addAlert("Ingrese el correo del medico solicitante");
      return false;
   }

    $ingresar_solicitud = new DAOSolicitudInternacionAdministrativa();
    $datosIngreso = $ingresar_solicitud->ingresar_solicitud($cmb_establecimiento_sol, $opciones,
    $diagnostico, $otro_antecedente, $ultimo_tratamiento, $cmb_establecimiento_int, 
    $nombre_medico, $rut_medico, $correo_medico,
    $nombres_sol,$apaterno_sol,$amaterno_sol,$rut_sol,
    $edad_sol,$sexo_sol,$calle_sol,$numerocasa_sol,$sector_sol,$comuna_sol,$ciudad_sol,$region_sol,
    $telefono_sol, $celular_sol,$correo_sol,$nombres_pac,$apaterno_pac,$amaterno_pac,
    $rut_pac,$edad_pac,$sexo_pac,$calle_pac,$numerocasa_pac,$sector_pac,$comuna_pac,$ciudad_pac,
    $region_pac,$vinculacion_pac,$quien_solicito);
    
    $xingresarsolicitud->addAlert($datosIngreso['@mensaje']);
    
    $xingresarsolicitud->addScript('document.formId.reset();');
    
    return $xingresarsolicitud;
}

function llena_establecimientos()
{

  $llena = new xajaxResponse();
  $combo = new DAOSolicitudInternacionAdministrativa(); 


  $arreglo = $combo->llena_establecimientos();

  foreach ($arreglo as $valor) 
  {
      $llena->CreaOpcion('establecimiento',utf8_encode($valor['nombre']),$valor['codigo']);
  }     

  return $llena;
}

function validarut($rut,$nombre_input)
{ 
    $valida = new xajaxResponse();

    $pos = strripos($rut,"-");

    if($pos === false)
    {}
    else
     {
    $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];

    if($digito!=""){
     $s=1;
      for($m=0;$r!=0;$r/=10)
        $s=($s+$r%10*(9-$m++%6))%11;
      $dv = chr($s?$s+47:75);

    if($dv==$digito)
    { }
    else
    { $valida->addAlert("Rut incorrecto, ingrese nuevamente"); 
      $valida->addScript("document.formId.".$nombre_input.".focus();");}  }
      

      } 


    return $valida;
}

function llena_establecimientos2()
{

  $llena = new xajaxResponse();
  $combo = new DAOSolicitudInternacionAdministrativa(); 


  $arreglo = $combo->llena_establecimientos();

  foreach ($arreglo as $valor) 
  {
      $llena->CreaOpcion('establecimiento2',utf8_encode($valor['nombre']),$valor['codigo']);
  }     

  return $llena;
}

function llena_sexo()
{

  $llena = new xajaxResponse();
  $combo = new DAOSolicitudInternacionAdministrativa(); 


  $arreglo = $combo->llena_sexo();

  foreach ($arreglo as $valor) 
  {
      $llena->CreaOpcion('sexo_pac',$valor['descripcion'],$valor['codigo']);
  }     

  return $llena;
}

function llena_sexo2()
{

  $llena = new xajaxResponse();
  $combo = new DAOSolicitudInternacionAdministrativa(); 


  $arreglo = $combo->llena_sexo();

  foreach ($arreglo as $valor) 
  {
      $llena->CreaOpcion('sexo_sol',$valor['descripcion'],$valor['codigo']);
  }     

  return $llena;
}

function llena_vinculacion()
{

  $llena = new xajaxResponse();
  $combo = new DAOSolicitudInternacionAdministrativa(); 


  $arreglo = $combo->llena_vinculacion();

  foreach ($arreglo as $valor) 
  {
      $llena->CreaOpcion('vinculacion_pac',$valor['descripcion'],$valor['codigo']);
  }     

  return $llena;
}


 if(isset($_SESSION['USUA_nombres'])){
   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
   $smarty->display('SolicitudInternacionAdministrativa.tpl');
   }
   else
   {$smarty->display('404.tpl');}

 
?>
