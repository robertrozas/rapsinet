<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include("DAO/DAODepartamentoSaludPublica.php");
   
    $xajax = new xajax(); 

    $xajax->registerFunction("rechazar_alta");

    $xajax->processRequests(); 

    function rechazar_alta($id_cabecera,$id_estado,$observaciones)
    {
       $rechazo = new xajaxResponse();
       /*$rechazo->addAlert($id_cabecera);
       $rechazo->addAlert($id_estado);
       $rechazo->addAlert($observaciones);*/
       $rechazar = new DAODepartamentoSaludPublica();
       $rechazar->rechazo_solalta($id_cabecera,$id_estado,$observaciones);
       $rechazo->addAlert($rechazar['@mensaje']);
      
       $rechazo->addScript("opener.xajax_filtra_solinv('','','');");
       
       return $rechazo; 
    }
    
    
    $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->display('popupObservaciones3.tpl');
?>
