<?php
session_start();
include("include/include.php");  
include 'xajax/xajax.inc.php';
//include_once("lib/Smarty/Smarty.class.php");
include ("DAO/DAOInicioSesion.php");


$xajax = new xajax(); 
$xajax->registerFunction("verificar_usuario");
$xajax->processRequests();

function verificar_usuario($form)
{  

  $xverificar_usuario = new xajaxResponse();

    $usuario = $form['usuario'];
    $contrasena = $form['contrasena']; 	
   


   if (empty($usuario) || empty($contrasena))
   {
      
      $xverificar_usuario->addAlert("Uno de los campos esta vacio, porfavor ingresar usuario y password");
   }
   else
   {

      $verificar_usuario = new DAOInicioSesion();
      $resultadoFila = $verificar_usuario->verifica_usuario($usuario, $contrasena);
       
      //$xverificar_usuario->addAlert($resultadoFila);
      if ($resultadoFila == 1)
      {

         
        $arr_datos_usuario = $verificar_usuario->datosUsuario($usuario, $contrasena); 
        $_SESSION['USUA_nombres'] = $arr_datos_usuario[0];
        $_SESSION['USUA_apellidos'] = $arr_datos_usuario[1];
        $_SESSION['USUA_rut'] = $arr_datos_usuario[2];
        $_SESSION['USUA_idUsuario'] = $arr_datos_usuario[3];
        $_SESSION['DEPART_id'] = $arr_datos_usuario[4];
        $_SESSION['EST_id'] = $arr_datos_usuario[6];

       
       
        $verificar_usuario->funcionesPerfil($arr_datos_usuario[5]);
       

        $xverificar_usuario->addRedirect("inicio_menu.php");
          
      }    
      else
      {
          $xverificar_usuario->addAlert("El usuario o contraseņa ingresados son incorrectos");
      }    
   } 
   
   return $xverificar_usuario;
}


$smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
$smarty->display('index.tpl');
?>
