<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoSaludPublica.php';
    
    $xajax = new xajax(); 
    
    $xajax->registerFunction("llena_establecimientos_pac");
    $xajax->registerFunction("llena_establecimientos_centro");
    $xajax->registerFunction("llena_medicos");
    $xajax->registerFunction("llena_medicos2");
    $xajax->registerFunction("lista_adjuntos");
    $xajax->registerFunction("comentario");
    $xajax->registerFunction("lista_comentarios");
    $xajax->registerFunction("lista_eventos");
    $xajax->registerFunction("ver_adjunto");
    $xajax->registerFunction("visar_alta");
    $xajax->registerFunction("confirmar_alta");
    
    
 
    $xajax->processRequests(); 
    
    $_SESSION['activo'] = "disabled='true'";
    $rechazos = array(2,4,6,8);
    $estado = $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_idEstadoSolicitud'];
    
    
    $ver = $_GET['ver'];

    if (in_array($estado, $rechazos)&& $ver ==0)
    {
      $_SESSION['activo'] = "";
    }
    
    
    
    function confirmar_alta($idCabeceraSolicitud,$form)
    { 
        global $smarty; 
        $xaprobarsolicitud = new xajaxResponse();
        $opcion = $form['opcion'];
        $aprobar_solicitud = new DAODepartamentoSaludPublica();
        $aprobarSolicitud = $aprobar_solicitud->confirmar_alta($idCabeceraSolicitud, $opcion);
    
        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        /*$xaprobarsolicitud->addScript("opener.xajax_lista_primera_llamada();");*/
        
        return $xaprobarsolicitud;  

    } 
 
    function llena_establecimientos_pac($est_sol)
    {

      $llena = new xajaxResponse();
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->llena_establecimientos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento_pac',$valor['nombre'],$valor['codigo']);
          if($valor['codigo']==$est_sol)
          {
            $llena->addScript("document.formSol.establecimiento_pac.selectedIndex='".$valor['codigo']."';");
          }

      }    

      return $llena;

    }
    
    function visar_alta($idCabeceraSolicitud,$estadoDocumento,$form)
    { 
        
   
        $xaprobarsolicitud = new xajaxResponse();
        $aprobar_solicitud = new DAODepartamentoSaludPublica();
        
        if($_SESSION['activo']=="")//significa que los input estan enabled
        {
            $xaprobarsolicitud->addAlert("lalala");
            $aprobar_solicitud->actualiza_solaa($idCabeceraSolicitud,$form);
         
        } 
        
        
        $aprobarSolicitud = $aprobar_solicitud->aprobar_alta($idCabeceraSolicitud,$estadoDocumento);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        $xaprobarsolicitud->addScript("opener.xajax_lista_solicitudes();");
        return $xaprobarsolicitud;  
       
    }
    
    
    
    
    function llena_establecimientos_centro($est_sol)
    {

      $llena = new xajaxResponse();
      //$llena->addAlert($est_sol);
      $combo = new DAODepartamentoSaludPublica(); 


      $arreglo = $combo->llena_establecimientos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('centro_salud',$valor['nombre'],$valor['codigo']);
          if($valor['codigo']==$est_sol)
          {
            $llena->addScript("document.formSol.centro_salud.selectedIndex='".$valor['codigo']."';");
          }

      }    

      return $llena;

    }
  
    function llena_medicos($medico)
    {

      $llena = new xajaxResponse();
      //$llena->addAlert($medico);
      $combo = new DAODepartamentoSaludPublica(); 
      $arreglo = $combo->medicos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('medicos',$valor['descripcion'],$valor['codigo']);
         

      }    
      $llena->addScript("$('#medicos option[value=".$medico."]').attr('selected',true);");
      
      
      return $llena;
    }
    
    function llena_medicos2($medico2)
    {

      $llena = new xajaxResponse();
      //$llena->addAlert($medico);
      $combo = new DAODepartamentoSaludPublica(); 
      $arreglo = $combo->medicos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('medicos2',$valor['descripcion'],$valor['codigo']);
         

      }    
      $llena->addScript("$('#medicos2 option[value=".$medico2."]').attr('selected',true);");
      
      
      return $llena;
    }
    
    function ingresarSolicitud($form)
    {
       $xingresarsolicitud = new xajaxResponse();
      
       $ingresar_solicitud = new DAOSolicitudAltaAdministrativa();
       $datosIngreso = $ingresar_solicitud->ingresa_solicitudaa($form);
       $xingresarsolicitud->addAlert($datosIngreso['@mensaje']);
       $xingresarsolicitud->addScript('document.formId.reset();');
   
       return $xingresarsolicitud;
    }
    
    function lista_adjuntos($id)
    {   global $smarty; 
        $adjunto = new xajaxResponse();
        
        $archivos = new DAODepartamentoSaludPublica();
        $_SESSION['arrAdjuntos']  = $archivos->lista_archivos_solalta($id);
       
        $tabla = $smarty->fetch('grilla_adjuntos.tpl');
        $adjunto->addAssign("detalle1","innerHTML",$tabla);
        return $adjunto;

    }
    
    function ver_adjunto($id_adjunto)
    {
       $ver = new xajaxResponse();
       $ventana = $ver->redi_cierro('visualizar_adjunto.php?clave='.$id_adjunto);

       return $ver; 
    }
    
    
   function comentario($cabid,$texto)
   {
     $comenta = new xajaxResponse();
     $comenta->addAlert($cabid.$texto);
     $agregacom = new DAODepartamentoSaludPublica();
     $agregacom->agrega_comentario_alta($cabid, $texto);
     $comenta->addScript('xajax_lista_comentarios("'.$cabid.'");');
     //$comenta->addAlert($cabid.$texto);

     return $comenta;
   }
   
    function lista_comentarios($idCabeceraSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        $evento->addAlert($idCabeceraSolicitud);
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesComentarios']=$eventos->comentarios_solicitud_alta($idCabeceraSolicitud);
        $tabla = $smarty->fetch('grilla_comentarios.tpl');
        $evento->addAssign("detalle_comentarios","innerHTML",$tabla);
        return $evento;
    }
    
    function lista_eventos($idCabeceraSolicitud,$tipoSolicitud)
    {   
        global $smarty; 
        $evento = new xajaxResponse();
        $evento->addAlert($idCabeceraSolicitud);
        $eventos = new DAODepartamentoSaludPublica();    
        $_SESSION['arrLogSolicitudesEvento']=$eventos->eventos_solicitud($idCabeceraSolicitud, $tipoSolicitud);
        $tabla = $smarty->fetch('grilla_eventos.tpl');
        $evento->addAssign("detalle_eventos","innerHTML",$tabla);
        return $evento;

    }
    
    
   
   
    
    $smarty->assign('xajax_js', $xajax->getJavascript('xajax')); 
    $smarty->assign('ver', $ver); 
    $smarty->assign('activo', $_SESSION['activo']); 
    $smarty->assign('estados', array(2,4,6,8));  
    $smarty->display('VistaSolAltaHospitalizacion_hpsi.tpl');
?>

