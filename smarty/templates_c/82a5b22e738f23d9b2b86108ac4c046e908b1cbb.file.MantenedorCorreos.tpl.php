<?php /* Smarty version Smarty-3.0.6, created on 2013-01-25 17:17:35
         compiled from "smarty/templates\MantenedorCorreos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:160905102be2fe1ffb5-00112042%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82a5b22e738f23d9b2b86108ac4c046e908b1cbb' => 
    array (
      0 => 'smarty/templates\\MantenedorCorreos.tpl',
      1 => 1359128467,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '160905102be2fe1ffb5-00112042',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_capitalize')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.capitalize.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>


<script type="text/javascript">
    
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=30");
}
</script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

<?php $_template = new Smarty_Internal_Template("menu_principal.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>  
	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Mantenedor de Hospitales</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
                <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        <?php echo $_SESSION['USUA_nombres'];?>
 <?php echo $_SESSION['USUA_apellidos'];?>

                        <br>
                        Informatica
                        <br>
                       
                        <?php echo smarty_modifier_capitalize(smarty_modifier_date_format(time(),"%A, %B %e, %Y"));?>

                        
                    </div>      
      </div>
		
	</div>
	<div id="page">
		<div id="content">
			<h3>Mantenedor de Correos</h3>

<div class="tabcontents"> 
	   <form id='formUser' name='formUser'>
              
              <label>Descripcion:</label> <input type="text" id="descripcion" name="descripcion" value="" />
              <br><br>
              <label>Correo:</label> <input type="text" id="mail" name="mail" value="" /> 

              <br class="clearfix" /><br class="clearfix" />
              <input type="button" value="Modificar" class="button large green"  onClick="xajax_modificar_correo(xajax.getFormValues('formUser'));"/>
            </form>
</div>                        
                        
                        <div class="tabcontents">
                           		      
                           <div id="correos" class="tabcontent">
				
                            	<script >
                          				xajax_lista_correos();
                      		</script>

		                      </div> 
			  
				
                    </div>	
			   </div>
			</div>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>