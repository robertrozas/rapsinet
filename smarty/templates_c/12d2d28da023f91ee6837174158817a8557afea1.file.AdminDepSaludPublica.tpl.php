<?php /* Smarty version Smarty-3.0.6, created on 2013-01-30 11:40:51
         compiled from "smarty/templates\AdminDepSaludPublica.tpl" */ ?>
<?php /*%%SmartyHeaderCode:626510906c33f9a84-22454479%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12d2d28da023f91ee6837174158817a8557afea1' => 
    array (
      0 => 'smarty/templates\\AdminDepSaludPublica.tpl',
      1 => 1359546047,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '626510906c33f9a84-22454479',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_capitalize')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.capitalize.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>
 

<?php $_template = new Smarty_Internal_Template("menu_principal.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>  

	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Administradores</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
                <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        <?php echo $_smarty_tpl->getVariable('USUA_nombres')->value;?>
 <?php echo $_smarty_tpl->getVariable('USUA_apellidos')->value;?>

                        <br>
                        Informatica
                        <br>
                       
                        <?php echo smarty_modifier_capitalize(smarty_modifier_date_format(time(),"%A, %B %e, %Y"));?>

                        
                    </div>      
                </div>
		<br class="clearfix" />
	</div>
	
        <div id="page"></h3>
            
		<div id="content">
			 <h3>Bandeja Departamento Salud Publica</h3>
 
                      <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Internaciones</a></li>
                            <li><a href="#" rel="solinv">No Voluntarias</a></li>
                            <li><a href="#" rel="soleva">Evaluaciones</a></li>
                            <li><a href="#" rel="solaa">Altas</a></li>
                            <li><a href="#" rel="hospitalizacion_listas">Personas de Alta</a></li>
                            <li><a href="#" rel="view2">Historico Evaluaciones</a></li>
                            <li><a href="#" rel="view3">Hist&oacute;rico Usuarios</a></li>
                        </ul>
                        <div class="tabcontents">
                            <div id="view1" class="tabcontent">
                            ID: <input type="text" id="id2" name="id2" onChange="xajax_filtra_solicitudesinternacionadmin(this.value, estado2.value, fecha2.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha2" name="fecha2" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solicitudesinternacionadmin(id2.value,estado2.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado2" id="estado2" onChange="xajax_filtra_solicitudesinternacionadmin(id2.value,this.value,fecha2.value);">
                            <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado2();
                                </script>
                                
                                 </select>   
                                 
                                <div id="solicitudes">
				                 <script>
                                    xajax_lista_solicitudes();
                                 </script> 
                                </div>                            
		            </div> 
                     <div id="solinv" class="tabcontent">
                            ID: <input type="text" id="id4" name="id4" onChange="xajax_filtra_solinv(this.value,estado4.value, fecha4.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha4" name="fecha4" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solinv(id4.value,estado4.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado4" id="estado4" onChange="xajax_filtra_solinv(id4.value,this.value,fecha4.value);">
                            <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado3();
                                </script>
                                
                                 </select>  
                          <div id="internaciones">
                          <script>xajax_filtra_solinv('','','');</script>
                          </div>         
                          

                   </div>     

                   <div id="soleva" class="tabcontent">
                           <div id="evaluaciones">
                           <script>xajax_filtra_solevas('','','');</script>
                           </div> 
                  </div>   

                    <div id="solaa" class="tabcontent">
                            ID: <input type="text" id="id5" name="id5" onChange="xajax_filtra_solaa(this.value,estado5.value, fecha5.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha5" name="fecha5" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solaa(id5.value,estado5.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado5" id="estado5" onChange="xajax_filtra_solaa(id5.value,this.value,fecha5.value);">
                            <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado4();
                                </script>
                                
                                 </select>  
                          <div id="hospitalizacion">
                          <script>xajax_filtra_solaa('','','');</script>
                          </div>     
                   </div>  
                            
                   <div id="hospitalizacion_listas" class="tabcontent">
                           <div id="hospitalizacion_listas">
                           <script>xajax_filtra_per_alta('','','');</script>
                           </div> 
                   </div>            
                            

			   <div id="view2" class="tabcontent">
                            Fecha: <input type="text" id="fecha" name="fecha" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_log_solicitudes(estado.value,tipo.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado" id="estado" onChange="xajax_filtra_log_solicitudes(this.value,tipo.value,fecha.value);">
                                <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado();
                                </script>
                                
                                 </select>
                            &nbsp;&nbsp;&nbsp;
                            Tipo: <select name="tipo" id="tipo" onChange="xajax_filtra_log_solicitudes(estado.value,this.value,fecha.value);">
                                <option>Seleccione</option>
                                    <script>
                                        xajax_llena_tipo();
                                    </script>  
                                </select>   
                               
                             <div id="log_solicitudes">
                                <script>
                                    xajax_lista_log_solicitudes();
                                </script>  
                             </div>      
		           </div>   
                            
                           <div id="view3" class="tabcontent">
                               
                            ID: <input type="text" id="id3" name="id3" onChange="xajax_filtra_historico_usuarios(this.value,fecha3.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha3" name="fecha3" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_historico_usuarios(id3.value,this.value);" />    
                               
                             <div id="log_historico_usuarios">   
                                <script>
                                    xajax_lista_historico_usuarios();
                                </script>     
                            </div>
                          </div>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>  

	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>

 