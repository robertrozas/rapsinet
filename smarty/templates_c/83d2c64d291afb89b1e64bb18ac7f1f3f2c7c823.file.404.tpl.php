<?php /* Smarty version Smarty-3.0.6, created on 2013-01-30 18:34:14
         compiled from "smarty/templates\404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1087510967a60dd4e9-12887266%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83d2c64d291afb89b1e64bb18ac7f1f3f2c7c823' => 
    array (
      0 => 'smarty/templates\\404.tpl',
      1 => 1357656821,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1087510967a60dd4e9-12887266',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ro-RO">
<head profile="http://gmpg.org/xfn/11">
<title>Robotik Custom Error Pages</title>
<meta charset="utf-8"/>
<meta http-equiv="Content-Language" content="ro"/>
<meta name="robots" content="all,index,follow"/>
<meta name="keywords" content="mogoolab, templates, 404 error page"/>
<meta name="description" content="Robotik HTML Error Pages v 1.0 . Developed by MogooLab - www.mogoolab.com"/>
<meta name="publisher" content="mogoolab.com" />
<meta name="author" content="mogoolab.com" />
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href='http://fonts.googleapis.com/css?family=Istok+Web|Chivo' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/backgrounds.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/blue/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/green/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/gray/css/style.css" />


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-global.js"></script>

<!--[if IE]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>


<div class="wrapper">

	<div class="mainWrapper">
        <div class="leftHolder">
        	<a href="http://mogoolab.com" title="Robotik Logo" class="logo">Robotik Logo</a>
            <div class="errorNumber">404</div> 
        </div>
        <div class="rightHolder">
            <div class="message"><p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p></div>
            <div class="robotik"><img src="images/robotik.png" alt="Oooops....we can’t find that page." title="Oooops....we can’t find that page." id="robot"></div>
            <div class="tryToMessage">
                Try to:
                <ul>
                    <li>Use the search form</li>
                    <li>Visit the <a href="http://mogoolab.com" title="Robotik Sitemap">Sitemap</a></li>
                    <li>Go <a href="javascript:history.go(-1)" title="Back">back</a></li>
                </ul>
            </div>
            <!-- Search Form -->
            <div class="search">
            <span class="errorSearch">Please fill the search field</span>
            <form action="" method="post">
              <div class="searchInput">
                <input type="text" name="search_term" value="Search" />
              </div>
              <div class="searchButton">
                <input type="submit" name="submit" value="Go" />
              </div>
            
            </form>
            </div>
            <!-- end .search -->
          </div>
      


        <footer>
        <p class="copy">&copy 2011 Robotik 404. All rights reserved.</p>
        <menu>
            <li><a href="http://mogoolab.com" title="Home">Home</a></li>
            <li><a href="http://mogoolab.com/who-we-are" title="About Us">About us</a></li>
            <li><a href="http://mogoolab.com/category/portfolio" title="Services">Services</a></li>
            <li><a href="http://mogoolab.com/" title="Partners">Partners</a></li>
            <li class="last"><a href="http://mogoolab.com/contact" title="Contact">Contact</a></li>
        </menu>
        </footer>
        <!-- end footer -->

	</div>

</div>
<!-- end .wrapper -->


</body>
</html>