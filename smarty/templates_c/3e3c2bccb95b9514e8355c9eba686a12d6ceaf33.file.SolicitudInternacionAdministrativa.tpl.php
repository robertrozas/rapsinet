<?php /* Smarty version Smarty-3.0.6, created on 2013-01-29 11:51:26
         compiled from "smarty/templates\SolicitudInternacionAdministrativa.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44635107b7be1906b8-51596822%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e3c2bccb95b9514e8355c9eba686a12d6ceaf33' => 
    array (
      0 => 'smarty/templates\\SolicitudInternacionAdministrativa.tpl',
      1 => 1359402319,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44635107b7be1906b8-51596822',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_capitalize')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.capitalize.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
   
 

<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>

<?php $_template = new Smarty_Internal_Template("menu_principal.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>  
	   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
      <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        <?php echo $_SESSION['USUA_nombres'];?>
 <?php echo $_SESSION['USUA_apellidos'];?>

                        <br>
                        Informatica
                        <br>
                       
                        <?php echo smarty_modifier_capitalize(smarty_modifier_date_format(time(),"%A, %B %e, %Y"));?>

                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n Administrativa</h3>
			</div>
			<form id="formId" name="formId">		
				<script>
						xajax_llena_regiones();
				</script>
			
				<div class="col-tri">
					<label>Fecha Evaluaci&oacute;n: </label>
					<input type="text" id="fecha_evaluacion" name="fecha_evaluacion" class="calendar" disabled>
					<br class="clearfix" />
						
					<label>Establecimiento Solicitante: </label>
					<select name="establecimiento" id="establecimiento" tabindex="6">
						<option>Seleccione</option>
						<script>
							xajax_llena_establecimientos();
						</script>  
					</select>
					<br class="clearfix" />

					<label>Diagnostico: </label>
					<textarea  name="diagnostico" id="diagnostico" style="width: 318px;height: 60px;"></textarea>
					<br class="clearfix" />

					<label>Otro Antenecente: </label>
					<textarea name="otro_antecedente" id="otro_antecedente" style="width: 318px;height: 60px;"></textarea>
					<br class="clearfix" />
							   
					<label>Ultimo tratamiento: </label>
					<textarea name="ultimo_tratamiento" id="ultimo_tratamiento" style="width: 318px;height: 60px;"></textarea>
					<br class="clearfix" />
					<br>            
					<label><b>Lugar de Internacion:</b></label>
					<select name="establecimiento2" id="establecimiento2" tabindex="6">
						<option>Seleccione</option>
						<script>
							xajax_llena_establecimientos2();
						</script>  
					</select>
					<br class="clearfix" />
					
					<br><br>
					<b>Identificaci&oacute;n del medico solicitante:</b>
					<br><br>
					
					<label>Nombre: </label>
					<input type="text" id="nombre_medico" name="nombre_medico" placeholder="Ingrese Nombre" onkeyup="xajax_texto('nombre_medico');">
					<br class="clearfix" />
					
					<label>C&eacute;dula de identidad: </label>
					<input type="text" name="rut_medico" id="rut_medico" placeholder="Ingrese Rut" onkeyup="xajax_validarut(this.value,'rut_medico');"> 
					<!--<input type="text" id="dv" name="dv" onkeyup="xajax_validarut(rut_pac.value);" style="width : 30px; heigth : 10px" /> -->
					<br class="clearfix" />
					
					<label>Correo: </label>
					<input type="text" name="correo_medico" id="correo_medico" placeholder="Ingrese el correo">
					<br class="clearfix" />
				</div>
				<div class="col-tri">
					<label>Nombres:</label>
					<input type="text" id="nombres_sol" name="nombres_sol" onkeyup="xajax_texto('nombres_sol');" >
					
					<label>A.Materno:</label>
					<input type="text" id="amaterno_sol" name="amaterno_sol" onkeyup="xajax_texto('amaterno_sol');" > 
					
					<label>Rut:</label>
					<input type="text" id="rut_sol" name="rut_sol" onkeyup="xajax_validarut(this.value,'rut_sol');"> 
					<!--<input type="text" id="dv" name="dv" onkeyup="xajax_validarut(rut_pac.value);" style="width : 30px; heigth : 10px" /> -->
					
					<label>Calle:</label>
					<input type="text" id="calle_sol" name="calle_sol">  
					
					<label>Regiones:</label>
					<select name="region_sol" id="region_sol" onchange="xajax_llena_provincias(this.value);">
						<option value ="0">Lista regiones</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['arrRegiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
						<option value="<?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
							<?php endfor; endif; ?>
					</select>
					<label>Provincia:</label>
					<select name="ciudad_sol" id="ciudad_sol" onchange="xajax_llena_comunas(this.value);"></select> 
					
					<label>Comuna:</label>
					<select name="comuna_sol" id="comuna_sol"></select>
					
					<div class="sep"></div>
					<label>Nombres:</label>
					<input type="text" id="nombres_pac" name="nombres_pac" onkeyup="xajax_texto('nombres_pac');">

					<label>A.Materno:</label>
					<input type="text" id="amaterno_pac" name="amaterno_pac" onkeyup="xajax_texto('amaterno_pac');"> 
					
					<label>Edad:</label>
					<input type="text" id="edad_pac" name="edad_pac" onkeyup="xajax_numero('edad_pac');">
					
					<label>Calle:</label>
					<input type="text" id="calle_pac" name="calle_pac">
					
					<label>Sector:</label>
					<input type="text" id="sector_pac" name="sector_pac">
					
					<label>Vinculaci&oacute;n:</label>
					<select name="vinculacion_pac" id="vinculacion_pac" tabindex="6">
						<option value ="NO">Seleccione Vinculacion</option>
						<script>
							xajax_llena_vinculacion();
						</script>  
					</select>
					<br class="clearfix" />
				</div>     
				<div class="col-tri">
					<label>A.Paterno:</label>	
					<input type="text" id="apaterno_sol" name="apaterno_sol" onkeyup="xajax_texto('apaterno_sol');" >
					<br class="clearfix" />
					
					<label>Edad:</label>
					<input type="text" id="edad_sol" name="edad_sol" onkeyup="xajax_numero('edad_sol');">
					<br class="clearfix" />
					
					<label>Sexo:</label>
					<select name="sexo_sol" id="sexo_sol" tabindex="6">
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo2();
						</script>  
					</select>
					<br class="clearfix" />
					
					<label>N&deg; Casa:</label>
					<input type="text" id="numerocasa_sol" name="numerocasa_sol"> 
					<br class="clearfix" />
					
					<label>Sector:</label>
					<input type="text" id="sector_sol" name="sector_sol">
					<br class="clearfix" />
					
					<label>Celular:</label>
					<input  ut type="text" id="celular_sol" name="celular_sol"> 
					<br class="clearfix" />
					
					<label>T.Fijo:</label>
					<input type="text" id="telefono_sol" name="telefono_sol">
					<br class="clearfix" />
					
					<label>Correo:</label>
					<input type="text" id="correo_sol" name="correo_sol" onkeyup="xajax_correo('correo_sol');" /> 
					<br class="clearfix" />
					<br><br>
					
					<label>A.Paterno:</label>  
					<input type="text" id="apaterno_pac" name="apaterno_pac" onkeyup="xajax_texto('apaterno_pac');">
					<br class="clearfix" />
					
					<label>Rut:</label>   
					<input type="text" id="rut_pac" name="rut_pac" onkeyup="xajax_validarut(this.value,'rut_pac');"> 
					<!--<input type="text" id="dv" name="dv" onkeyup="xajax_validarut(rut_pac.value);" style="width : 30px; heigth : 10px" /> -->
					<br class="clearfix" />
					
					<label>Sexo:</label>
					<select name="sexo_pac" id="sexo_pac" tabindex="6">
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo();
						</script>  
					</select>
					<br class="clearfix" />
					
					<label>N&deg; casa:</label>
					<input type="text" id="numerocasa_pac" name="numerocasa_pac"> 
					<br class="clearfix" />
					
					<label>Regi&oacute;n:</label>
					<select name="region_pac" id="region_pac" onchange="xajax_llena_provincias2(this.value);">
						<option value ="0">Lista regiones</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['arrRegiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
						<option value="<?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
							<?php endfor; endif; ?>
					</select>
					<br class="clearfix" />
					
					<label>Provincia:</label>
					<select name="ciudad_pac" id="ciudad_pac" onchange="xajax_llena_comunas2(this.value);"></select>
					<br class="clearfix" />
					
					<label>Comuna:</label>
					<select name="comuna_pac" id="comuna_pac"></select> 
					<br class="clearfix" />					
				</div> 
				<br class="clearfix" />
				<br><br>
				<div id="pieDePagina">    
					<input type="checkbox" name="opcion_a" id="opcion_a" value="A"><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
					<br class="clearfix" />  
						<input type="checkbox" name="opcion_b" id="opcion_b" value="B"><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
					<br class="clearfix" />  
						<input type="checkbox" name="opcion_c" id="opcion_c" value="C"><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
					<br/>
					<br class="clearfix" />
								
					<fieldset>
						<b>Estado:</b><br>
						<input type="checkbox" name="rakim" value="dsp"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="rakim" value="juridica"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="rakim" value="seremi"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="rakim" value="contraloria"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</fieldset>	
					<br><br>
					<div class="right">
						<input type="button" class="button medium blue" id="enviar" value="Enviar Solicitud" onclick="xajax_ingresar_solicitud(xajax.getFormValues('formId'));">
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</form>	
		</div>  
	</div>
	<div id="footer">Rapsinet </div>

	</html>