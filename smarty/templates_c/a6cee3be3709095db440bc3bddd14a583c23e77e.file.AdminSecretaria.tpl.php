<?php /* Smarty version Smarty-3.0.6, created on 2013-01-28 11:50:58
         compiled from "smarty/templates\AdminSecretaria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:385251066622648a38-82452049%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6cee3be3709095db440bc3bddd14a583c23e77e' => 
    array (
      0 => 'smarty/templates\\AdminSecretaria.tpl',
      1 => 1359139512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '385251066622648a38-82452049',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_capitalize')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.capitalize.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>


<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>


<?php $_template = new Smarty_Internal_Template("menu_principal.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>  
	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Administradores</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
              <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        <?php echo $_SESSION['USUA_nombres'];?>
 <?php echo $_SESSION['USUA_apellidos'];?>

                        <br>
                        Informatica
                        <br>
                       
                        <?php echo smarty_modifier_capitalize(smarty_modifier_date_format(time(),"%A, %B %e, %Y"));?>

                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
                    <h3>Bandeja de Secretar&iacute;a</h3>
                        
                         <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Solicitud Internaci&oacute;n</a></li>
                            <li><a href="#" rel="view2">Solicitud No Voluntaria</a></li>
                            <li><a href="#" rel="soleva">Solicitud Evaluaci&oacute;n</a></li>
                            <li><a href="#" rel="view3">Solicitud Alta</a></li>
                            <li><a href="#" rel="view4">Hist&oacute;rico Usuarios</a></li>
                        </ul>
                        <div class="tabcontents">	
                            <div id="view1" class="tabcontent">
                            	<div id="solicitudes">
				 				 <script>
                                    xajax_lista_solicitudes();
                                 </script> 
                                </div> 
		           </div>
				   		
				
			   <div id="view2" class="tabcontent">
                                <div id="internaciones">
                                    <script>xajax_filtra_solinv('','','');</script>
                                    </div> 
                          </div> 

              <div id="soleva" class="tabcontent">
                           <div id="evaluaciones">
                           <script>xajax_filtra_solevas();</script>
                           </div> 
              </div> 


				
                          <div id="view3" class="tabcontent">
				<div id="alta">
                                    <script>xajax_filtra_solalta('','','');</script>
                                </div>		
		          </div> 
                            
                          <div id="view4" class="tabcontent">
						
		                      </div>   
               



                    </div>	
	         </div>
			
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>