<?php /* Smarty version Smarty-3.0.6, created on 2013-01-30 13:37:03
         compiled from "smarty/templates\menu_principal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2165510921ff9a54f9-38668904%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4f63cf049fe0810ab5f72855f9ea048c5624ca9' => 
    array (
      0 => 'smarty/templates\\menu_principal.tpl',
      1 => 1359553015,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2165510921ff9a54f9-38668904',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!--
<link rel="stylesheet" type="text/css" href="css/style.css" />
-->
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<!-- Actualiza tu Navegador -->
<script type="text/javascript" src="http://updateyourbrowser.net/asn2.js"></script>

<script type="text/javascript">
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top="+posicion_y+"");
}
</script>

<!--- Calendario --->
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<link rel="stylesheet" href="css/calendar.css" type="text/css">
<script>
$(function() {
	$( ".calendar" ).datepicker({ 
		autoSize: true,
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});
});
</script>

<!-- Script Menu -->
<script type="text/javascript">
	$(function() {
		$('#menu > ul').dropotron({
			mode: 'fade',
			globalOffsetY: 11,
			offsetY: -15
		});
	});
</script>


</head>
<body>
<div id="wrapper">
	<div id="menu">
		<ul>
			<?php if ($_SESSION['Solicitudes']==1){?>
			<li class="first">
			<span class="opener">Solicitudes<b></b></span>
			<ul>
				<li><a href="SolicitudNoVoluntaria.php">Urgencia No Voluntaria</a></li>
				<li><a href="SolicitudEvaluacionYTratamiento.php">Evaluaci&oacute;n y Tratamiento</a></li>
				<li><a href="SolicitudInternacionAdministrativa.php">Internaci&oacute;n Administrativa</a></li>
				<li><a href="SolicitudAltaHospitalizacion.php">Alta Hospitalizaci&oacute;n</a></li>
			</ul>
			</li>
			<?php }?>
			<?php if ($_SESSION['Bandejas']==1){?>
			<li>
				<span class="opener">Bandejas <b></b></span>
				<ul>
				<?php if ($_SESSION['DEPART_id']==1){?> <li><a href="AdminDepSaludPublica.php">Departamento Salud Publica</a></li><?php }?>
				<?php if ($_SESSION['DEPART_id']==6){?>	<li><a href="AdminSecretaria.php">Secretaria</a></li><?php }?>
                <?php if ($_SESSION['DEPART_id']==2){?> <li><a href="AdminJuridica.php">Jur&iacute;dica</a></li><?php }?>
				<?php if ($_SESSION['DEPART_id']==3){?> <li><a href="AdminSeremi.php">Seremi</a></li><?php }?>
			    <?php if ($_SESSION['DEPART_id']==4){?> <li><a href="AdminContraloria.php">Contraloria</a></li><?php }?>
			    <?php if ($_SESSION['DEPART_id']==7){?> <li><a href="AdminUSMH.php">USMH</a></li><?php }?>
                <?php if ($_SESSION['DEPART_id']==8){?> <li><a href="AdminHPSI.php">H. Psiqui&aacute;trico</a></li><?php }?>
			    <?php if ($_SESSION['DEPART_id']==5||$_SESSION['PER_id']==4){?>
			    <li><a href="AdminDepSaludPublica.php">Departamento Salud Publica</a></li>
			    <li><a href="AdminSecretaria.php">Secretaria</a></li>
			    <li><a href="AdminJuridica.php">Jur&iacute;dica</a></li>
			    <li><a href="AdminSeremi.php">Seremi</a></li>
			    <li><a href="AdminContraloria.php">Contraloria</a></li>
                <li><a href="AdminUSMH.php">USMH</a></li>
                <li><a href="AdminHPSI.php">H. Psiqui&aacute;trico</a></li>
			    <?php }?>
			   </ul>
			</li> 
			<?php }?>

			<?php if ($_SESSION['Mantenedores']==1){?>
			<li>
				<span class="opener">Mantenedores<b></b></span>
				<ul>
					<li><a href="MantenedorMedicos.php">M&eacute;dicos</a></li>
					<li><a href="MantenedorUsuarios.php">Usuarios</a></li>
					<li><a href="MantenedorRoles.php">Perfiles-Funciones</a></li>
					<li><a href="MantenedorHospitales.php">Hospitales</a></li>
                    <li><a href="MantenedorCorreos.php">Correos</a></li>
			   </ul>
			</li>
			<?php }?>
			<?php if ($_SESSION['Graficos']==1){?>
			<li>
				<span class="opener">Gr&aacute;ficos<b></b></span>
				<ul>
					<li><a href="javascript:popup('modeloarray.html',800,400)">Barra</a></li>
					<li><a href="javascript:popup('modelotabla.html',800,400)">Torta</a></li>
					<li><a href="javascript:popup('Grafico_Distribucion_EvaTrat.php',800,400)">Distribucion segun Lugar de Evaluacion</a></li>
				</ul>
			</li>
			<?php }?>

			<?php if ($_SESSION['Reportes']==1){?>
			<li><a href="#">Reportes</a></li>
			<?php }?>
			<li><span class="opener">Mis Datos<b></b></span>
				<ul>
					<li><a href="MisDatos.php">Cambiar Configuraci&oacute;n</a></li>	
			   </ul>	
			</li>
			<li><a href="Salir.php">Cerrar sesion</a></li>
		</ul>
		<br class="clearfix" />
	</div>



 