<?php /* Smarty version Smarty-3.0.6, created on 2013-01-29 14:30:35
         compiled from "smarty/templates\VistaSolAltaHospitalizacion.tpl" */ ?>
<?php /*%%SmartyHeaderCode:212005107dd0bf1e2a3-59901716%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c2a51c1f857831df833afbfdac8b9311a21ad92' => 
    array (
      0 => 'smarty/templates\\VistaSolAltaHospitalizacion.tpl',
      1 => 1359469832,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212005107dd0bf1e2a3-59901716',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>



<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>


</head>
<body>

<div id="wrapper">
	
    <form id="formSol" name="formSol">
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Alta Hospitalizacion Administrativa</h3>
			</div>
				
			<div class="col-left">
				<label>Rut: </label><input type="text" id="rut_pac" name="rut_pac"  value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_rutPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label>A Paterno: </label><input type="text" id="apaterno" name="apaterno" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_apaternoPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<label>Fecha de Ingreso: </label><input type="text" id="fec_ing"  name="fec_ing" class="calendar" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_fechaIngreso'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_pac" name="direccion_pac" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_direccionPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                <label>Establecimiento:</label>
                                   <select name="establecimiento_pac" id="establecimiento_pac"  <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                            <option value ="0">Seleccione Establecimiento</option>
                                        <script>
                                            xajax_llena_establecimientos_pac('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_idEstablecimiento'];?>
');
                                        </script>  
                                   </select>
			</div>
			<div class="col-right">
				<label>Nombre: </label><input type="text" id="nombre_pac" name="nombre_pac" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_nombrePac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label>A Materno: </label><input type="text" id="amaterno" name="amaterno" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_amaternoPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
				<br class="clearfix" />
				<label> Fecha de Egreso: </label><input type="text" id="fec_egre" name="fec_egre" class="calendar" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_fechaEgreso'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label>Fono: </label><input type="text" id="fono" name="fono" value="<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_fonoPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
			</div>
			<br class="clearfix" />
			<br>
			
			<label> Diagnostico: </label><textarea cols="25" rows="2" id="diagnostico" name="diagnostico" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
><?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_diagnostico'];?>
 </textarea><br>
			<label> Evoluci&oacute;n Clinica: </label><textarea cols="25" rows="2" id="evolucion_clinica" name="evolucion_clinica" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
><?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_evolucionClinica'];?>
</textarea><br>
                        <label> Tratamiento: </label><textarea cols="25" rows="2" id="tratamiento" name="tratamiento" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
><?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_tratamiento'];?>
</textarea>
            <br><br>
           
			<b>Referencia de Alta</b>
			<br><br>
			<div class="col-left">
                        <label> Centro de Salud: </label>    
				 <select name="centro_salud" id="centro_salud"  <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                              <option value ="0">Seleccione Establecimiento</option>
                                        <script>
                                            xajax_llena_establecimientos_centro('<?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_idCentroSalud'];?>
');
                                        </script>  
                                   </select>
                                   </select>
				<br class="clearfix" />
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_2" name="direccion_2" value="<?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_direccionRef'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
			
			</div>
			<div class="col-right">
				<label> Profesional: </label> 
                                <select name="medicos" id="medicos" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                          <option value ="0">Seleccione medico</option>
                                    <script>
                                        xajax_llena_medicos('<?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_idProfesionalMedico'];?>
');
                                    </script>  
                                </select>    
				<label> Fecha Referencia: </label><input type="text" id="fecha_ref" name="fecha_ref" class="calendar" value="<?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_fechaRef'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
			</div>
			<br class="clearfix" />
                        <br/>
                        <input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
                        <b>Datos m&eacute;dico tratante:</b>
                        <br><br>
                        <label>M&eacute;dico: </label>
			<select name="medicos2" id="medicos2" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                            <option>Seleccione</option>
                                <option value ="0">Seleccione medico</option>
                                    <script>
                                        xajax_llena_medicos2('<?php echo $_SESSION['arrDetsolaa']['DETALTHOSPADMIN_idMedicoTrat'];?>
');
                                    </script>  
                                </select>     
                        </select>
        

            <br class="clearfix" />
            <br/>
            <b>Motivo de la Solicitud:</b>
            <br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="A" <?php echo $_smarty_tpl->getVariable('OP1')->value;?>
><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
            <br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="B" <?php echo $_smarty_tpl->getVariable('OP2')->value;?>
><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
            <br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="C" <?php echo $_smarty_tpl->getVariable('OP3')->value;?>
><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
            <br/>
            <br class="clearfix" />
		
			<fieldset>
            <br class="clearfix" />   
				<b>Estado:</b><br>
				<input type="checkbox" name="rakim" value="dsp"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="juridica"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="seremi"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="contraloria"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</fieldset>	
			<br><br> 
                        
			<div class="right">	
			<?php if ($_smarty_tpl->getVariable('ver')->value==0){?>
				<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar_alta('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_id'];?>
','<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_idEstadoSolicitud'];?>
',xajax.getFormValues('formSol'));">
				<input type="button" id="limpia" class="button medium red" value="Rechazar" onclick="xajax_rechaza();" /> </span> 
			<?php }?>
			</div>
			<br>
            <br class="clearfix" />
            <?php if ($_smarty_tpl->getVariable('ver')->value==1){?>
              <iframe src="adjuntos_solalta.php" width="500" height="100" frameborder="0">
              </iframe>
              <br class="clearfix" />
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_id'];?>
',comenta.value);"/>
              <br><br>
                           <ul class="tabs" persist="true">
                                    <li><a href="#" rel="view1">Comentarios</a></li>
                                    <li><a href="#" rel="view2">Eventos</a></li>
                                    <li><a href="#" rel="view3">Firmas</a></li>
                                    <li><a href="#" rel="view4">Adjuntos</a></li>   
                           </ul>
      
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                                <script>
                                    xajax_lista_comentarios('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_id'];?>
');
                                </script> 
                                 <div id="detalle_comentarios">
                                </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                                <script>
                                    xajax_lista_eventos('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_id'];?>
','<?php echo $_SESSION['arrDetsolaa']['tipoSolicitud'];?>
');
                                </script>
                                <div id="detalle_eventos">
                                </div>
                              
                        </div>
                        <div id="view3" class="tabcontent">
                        <!--   
                                Firmas
                        -->    
                        </div>
                        <div id="view4" class="tabcontent">
                         
                                Grilla Adjuntos
                                <script>
                                    xajax_lista_adjuntos('<?php echo $_SESSION['arrDetsolaa']['CABALTAHOSPADMIN_id'];?>
');
                                </script> 
                                <div id="detalle1">
                                </div>
                       
                        
                        </div>
                    </div>
        <?php }?>
            
            
	</div>
	</div>
</form>	
	<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet </div>
</html>