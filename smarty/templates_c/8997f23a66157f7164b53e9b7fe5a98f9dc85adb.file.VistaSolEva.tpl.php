<?php /* Smarty version Smarty-3.0.6, created on 2013-01-29 18:53:18
         compiled from "smarty/templates\VistaSolEva.tpl" */ ?>
<?php /*%%SmartyHeaderCode:268651081a9e94dd39-32633163%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8997f23a66157f7164b53e9b7fe5a98f9dc85adb' => 
    array (
      0 => 'smarty/templates\\VistaSolEva.tpl',
      1 => 1359485447,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '268651081a9e94dd39-32633163',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'C:\wamp\www\RAPSINET\lib\Smarty\plugins\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />

<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<!--- Calendario --->
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<link rel="stylesheet" href="css/calendar.css" type="text/css">
<script>
$(function() {
	$( ".calendar" ).datepicker({ 
		autoSize: true,
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});
});
</script>

<!-- Script Menu -->
<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>


</head>

<body onload="xajax_carga();">

<div id="wrapper"> 
	
	<div id="page">
		<div id="content">
			<div class="box">
				<h3> Solicitud de Evaluaci&oacute;n y Tratamiento:</h3>
			</div>
		<form id="formTrata" name="formTrata">
			<label>Fecha Solicitud:</label> <input type="text" class="calendar" name="fec_sol" id="fec_sol" value="<?php echo smarty_modifier_date_format($_SESSION['arrDetsoleva']['fecha_sol'],"d-m-Y");?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Ficha Clinica:</label> <input type="text" id="ficha" name="ficha" onkeyup="xajax_numero('ficha');" value="<?php echo $_SESSION['arrDetsoleva']['ficha'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/> 
			<br class="clearfix" />
			<label>Visita Domiciliaria:</label> <input type="text" class="calendar" name="fec_vis" id="fec_vis" value="<?php echo smarty_modifier_date_format($_SESSION['arrDetsoleva']['fecha_visita'],"d-m-Y");?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
			<br class="clearfix" />
			<b>Tengo a bien solicitar a Usted dicte resoluci&oacute;n administrativa de traslado y evaluaci&oacute;n a un centro asistencial que permita una evaluaci&oacute;n psiqui&aacute;trica de la persona que se individualiza a continuaci&oacute;n:</b>
            <hr><br>

            <label>Regi&oacute;n:	</label><select id="region" name="region" onChange="xajax_provincias(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
            								<option value ="0">Lista regiones</option>
                                            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['Regiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
                                            <option value="<?php echo $_SESSION['Regiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['Regiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
                                            <?php endfor; endif; ?>
                                            </select>
            <label>Provincia: </label><select id="provincia" name="provincia" onChange="xajax_comunas(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
></select>
			<br class="clearfix" />
			<label>Comuna:	</label><select id="comuna" name="comuna" onChange="xajax_hospital(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
></select><label>Establecimiento: </label>
			<select id="hospital" name="hospital" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
			</select>
			<br class="clearfix" />
			<label>Nombres Paciente: </label><input type="text" id="identidad" name="identidad" onkeyup="xajax_texto('identidad');" value="<?php echo $_SESSION['arrDetsoleva']['nombres_pac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Apellido Paterno: </label><input type="text" id="ap_pat" name="ap_pat" onkeyup="xajax_texto('ap_pat');" value="<?php echo $_SESSION['arrDetsoleva']['ap_pat'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Apellido Materno: </label><input type="text" id="ap_mat" name="ap_mat" onkeyup="xajax_texto('ap_mat');" value="<?php echo $_SESSION['arrDetsoleva']['ap_mat'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Rut: </label><input type="text" id="rut" name="rut" onkeyup="xajax_validarut(this.value,'rut');" value="<?php echo $_SESSION['arrDetsoleva']['rut_pac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Edad: </label><input type="text" id="edad" name="edad" onkeyup="xajax_numero('edad');" value="<?php echo $_SESSION['arrDetsoleva']['edad_pac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Sexo: </label><select id="sexo" name="sexo" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
		    <label>Direcci&oacute;n: </label><input type="text" id="dire" name="dire" value="<?php echo $_SESSION['arrDetsoleva']['dire_pac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Fono: </label><input type="text" id="fono" name="fono" value="<?php echo $_SESSION['arrDetsoleva']['fono_pac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
            <br>  
      <br> 
            <b>Identificaci&oacute;n del Solicitante:</b>
            <hr><br> 
            <label>Nombres: </label><input type="text" name="nombre_sol" id="nombre_sol" value="<?php echo $_SESSION['arrDetsoleva']['nom_sol'];?>
" onkeyup="xajax_texto('nombre_sol');" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Apellidos: </label><input type="text" name="apel_sol" id="apel_sol" onkeyup="xajax_texto('apel_sol');" value="<?php echo $_SESSION['arrDetsoleva']['apel_sol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
            <br class="clearfix" />
            <label>Rut: </label><input type="text" name="rut_sol" id="rut_sol" value="<?php echo $_SESSION['arrDetsoleva']['rut_sol'];?>
" onkeyup="xajax_validarut(this.value,'rut_sol');" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Edad: </label><input type="text" name="edad_sol" id="edad_sol" value="<?php echo $_SESSION['arrDetsoleva']['edad_sol'];?>
" onkeyup="xajax_numero('edad_sol');" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
            <br class="clearfix" />
            <label>Direcci&oacute;n:</label><input type="text" name="dir_sol" id="dir_sol" value="<?php echo $_SESSION['arrDetsoleva']['dir_sol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
            <label>Regi&oacute;n:</label><select id="regiones" name="regiones" onChange="xajax_provincia(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                          <option value ="0">Lista regiones</option>
                                            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['Regiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
                                            <option value="<?php echo $_SESSION['Regiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['Regiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
                                            <?php endfor; endif; ?>
                         </select>
            <br class="clearfix" />
            <label>Provincia:</label><select id="provincias" name="provincias" onChange="xajax_comuna(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                      <option value="">Seleccione Provincia</option>
                  
                     </select>
            <label>Comuna:</label><select id="comunas" name="comunas" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                  <option value="">Seleccione Comuna</option>
                  
                  </select>

            <br class="clearfix" />
            <label>Sexo: </label><select id="sexo_sol" name="sexo_sol" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                  <option value="">Seleccione</option>
                  <option value="0">Masculino</option>
                  <option value="1">Femenino</option>
                </select>

        <label>Vinculaci&oacute;n:</label><select id="vincula" name="vincula" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                  <option value="0">Seleccione Parentezco</option>
                  <option value="1">Padre</option>
                  <option value="2">Madre</option>
                  <option value="3">Primo</option>
                  <option value="4">Prima</option>
                  <option value="5">Hijo</option>
                  <option value="6">Hija</option>
                  <option value="7">Hermano</option>
                  <option value="8">Hermana</option>
                  <option value="9">Vecino</option>
                  <option value="10">Vecina</option>
                </select>
            <br class="clearfix" />
            <br class="clearfix" />
		    <b>Identificaci&oacute;n del Profesional que solicita Resoluci&oacute;n:</b>
            <hr><br> 
            <label>Rut Profesional Solicitante:	</label><input type="text" id="rut_doc" name="rut_doc" onkeyup="xajax_validarut(this.value,'rut_doc');" value="<?php echo $_SESSION['arrDetsoleva']['rut_doc'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Nombres: </label><input type="text" id="nom_doc" name="nom_doc" onkeyup="xajax_texto('nom_doc');" value="<?php echo $_SESSION['arrDetsoleva']['nom_doc'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Profesi&oacute;n:</label><input type="text" id="pro_doc" name="pro_doc" onkeyup="xajax_texto('pro_doc');" value="<?php echo $_SESSION['arrDetsoleva']['prof_doc'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/><label>Fono Contacto: </label><input type="text" id="fono_doc" name="fono_doc" value="<?php echo $_SESSION['arrDetsoleva']['fono_doc'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			<br class="clearfix" />
			<label>Email Contacto:</label><input type="text" id="mail_doc" name="mail_doc" value="<?php echo $_SESSION['arrDetsoleva']['mail_doc'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
/>
			 <?php if (in_array($_SESSION['arrDetsoleva']['estado_sol'],$_smarty_tpl->getVariable('estados')->value)){?>  
            <label>Observaci&oacute;n:</label><input type="text" id="observacion" name="observacion" value="<?php echo $_SESSION['arrDetsoleva']['obser_soleva'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>  
            <?php }?>
            <br><br>
             Lugar donde se realizara la evaluaci&oacute;n:
                            
                        <select name="hospital_psi" id="hospital_psi" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                        <option value ="0">Lista Establecimientos
                         <script>
                                 xajax_hospital2(<?php echo $_SESSION['arrDetsoleva']['lugar_evaluacion'];?>
);
                         </script> 
                        </option>
                      </select>    
              <br><br> 

      <br class="clearfix" />
      <br/>
      <b>Motivo de la Solicitud:</b>
      <br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="A" <?php echo $_smarty_tpl->getVariable('OP1')->value;?>
><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
      <br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="B" <?php echo $_smarty_tpl->getVariable('OP2')->value;?>
><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
      <br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="C" <?php echo $_smarty_tpl->getVariable('OP3')->value;?>
><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
      <br/>
      <br class="clearfix" />          
             <?php if ($_smarty_tpl->getVariable('ver')->value==1){?>
                <iframe src="adjuntos_soleva.php" width="500" height="100" frameborder="0">
                </iframe>
             <br>
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('<?php echo $_SESSION['arrDetsoleva']['cab_solevatrat'];?>
',comenta.value);"/>
			<br><br>
		        <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Comentarios</a></li>
                            <li><a href="#" rel="view2">Eventos</a></li>
                            <?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
                            <li><a href="#" rel="view3">Firmas</a></li>
                            <?php }?>
                            <li><a href="#" rel="view4">Adjuntos</a></li>
                        </ul>
                  
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                            <script>
                                    xajax_lista_comentarios('<?php echo $_SESSION['arrDetsoleva']['cab_solevatrat'];?>
');
                                </script> 
                            <div id="comentarios">
                            	Comentarios
                            </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                         <script>
                                    xajax_lista_eventos('<?php echo $_SESSION['arrDetsoleva']['cab_solevatrat'];?>
');
                         </script>
                         <div id="eventos">
                         	Eventos
                         </div>
                         
                        </div>
                        <?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
                        <div id="view3" class="tabcontent">
                           
                                Firmas
                            
                        </div>
                        <?php }?>
                        <div id="view4" class="tabcontent">
                           
                               Grilla Adjuntos
                               <script>
                                    xajax_lista_adjuntos('<?php echo $_SESSION['arrDetsoleva']['cab_solevatrat'];?>
');
                               </script> 
                               
                                <div id="adjuntos">

                                </div>
                            
                        </div>
                        
                   
                    </div>
		  <?php }?>

                        <?php if ($_smarty_tpl->getVariable('ver')->value==0){?>
                            <div >
                                    <input type="button" id="enviar" class="button large green" value="Visar" onclick="xajax_solicita(xajax.getFormValues('formTrata'));" />
                                    <input type="button" id="limpia" class="button large red" value="Rechazar" onclick="xajax_rechaza();" />
                            </div>
			<?php }?>
		</form>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>