<?php /* Smarty version Smarty-3.0.6, created on 2013-01-21 14:08:17
         compiled from "smarty/templates\VistaSolinvDSP.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2370850fd4bd1ca9758-61417411%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '61795312841a71d94c09f71a761dfa39f7260f9f' => 
    array (
      0 => 'smarty/templates\\VistaSolinvDSP.tpl',
      1 => 1358777202,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2370850fd4bd1ca9758-61417411',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>
       
        
<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>

<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

</head>

<body>

    
<div id="wrapper">

	
	<div id="page">
	    <div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n de Urgencia no Voluntaria:</h3>
			</div>
		<form id="formSol" name="formSol">	
			<div class="col-left">
                <label> Rut: </label><input type="text" id="rut" name="rut" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_rutPaciente'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label> Apellidos: </label><input type="text" id="apellidos" name="apellidos" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_apellidosPaciente'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
                <input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
				<label> Fecha de Nacimiento: </label><input type="text" id="fec_nac" name="fec_nac"  value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_fechaNacPaciente'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label>Tipo Servicio:</label>
                                     <select name="servicios" id="servicios" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                        <option value ="0">Seleccione Servicio</option>
                                        <script>
                                            xajax_llena_servicios('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_idServicio'];?>
');
                                        </script>  
                                      </select>
				<br class="clearfix" />
				<label> Fecha Solicitud: </label><input type="text" id="fech_desde" name="fech_desde" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_fechaSolicitud'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				
			</div>
			<div class="col-right">
				<label> Nombres:	</label><input type="text" id="nombres" name="nombres" value="<?php echo utf8_decode(utf8_decode($_SESSION['arrDetsolinv']['CABSOLINV_nombresPaciente']));?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
				<br class="clearfix" />
				<label> Domicilio:	</label><input type="text" id="domicilio" name="domicilio" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_direccionPaciente'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label> Fecha Internaci&oacute;n: </label><input type="text" id="fec_int" name="fec_int" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_fechaInternacion'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				<br class="clearfix" />
				<label>Establecimiento:</label>
                                
                                <select name="establecimiento" id="establecimiento" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                <option value="0">
                                 Selecciones Hospital..  
                                 </option>  
                                <script>
                                        xajax_llena_establecimientos();
                                </script>  
                                  
                                </select>
				<br class="clearfix" />
                                <label>M&eacute;dico:</label>
                                <select name="medicos" id="medicos" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
                                        <option value ="0">Seleccione Medicos</option>
                                        <script>
                                            xajax_llena_medicos('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_IDMEDICO'];?>
');
                                        </script>  
                                </select>
                                <?php if (in_array($_SESSION['arrDetsolinv']['CABSOLINV_estadoSolicitud'],$_smarty_tpl->getVariable('estados')->value)){?>  
                                    <label>Observaci&oacute;n:</label>
                                    <input type="text" id="observacion" name="observacion" value="<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_observacion'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>  
                                <?php }?>
                        </div>
                        
                    <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="A" <?php echo $_smarty_tpl->getVariable('OP1')->value;?>
><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="B" <?php echo $_smarty_tpl->getVariable('OP2')->value;?>
><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="C" <?php echo $_smarty_tpl->getVariable('OP3')->value;?>
><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br/>
                    <br class="clearfix" />
            <?php if ($_smarty_tpl->getVariable('ver')->value==1){?>
            <iframe src="adjuntos_solint.php" width="500" height="100" frameborder="0">
            </iframe>
             <br class="clearfix" />
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_ID'];?>
',comenta.value);"/>
			<br><br>
		               <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Comentarios</a></li>
                            <li><a href="#" rel="view2">Eventos</a></li>
                            <?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
                            <li><a href="#" rel="view3">Firmas</a></li>
                            <?php }?>
                            <li><a href="#" rel="view4">Adjuntos</a></li>
                        </ul>
                  
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                            <script>
                                    xajax_lista_comentarios('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_ID'];?>
');
                                </script> 
                               <div id="detalle_comentarios">
                            </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                         <script>
                                    xajax_lista_eventos('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_ID'];?>
');
                         </script>
                         <div id="detalle">
                         </div>
                         
                        </div>
                        <?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
                        <div id="view3" class="tabcontent">
                           
                                Firmas
                            
                        </div>
                        <?php }?>
                        <div id="view4" class="tabcontent">
                           
                               Grilla Adjuntos
                                <script>
                                    xajax_lista_adjuntos('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_ID'];?>
');
                                </script> 
                                <div id="detalle1">
                                </div>
                            
                        </div>
                        
                   
                    </div>
		  <?php }?>
            <br><br>
			<div class="right">
			<?php if ($_smarty_tpl->getVariable('ver')->value==0){?>
				<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar('<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_ID'];?>
','<?php echo $_SESSION['arrDetsolinv']['CABSOLINV_estadoSolicitud'];?>
',xajax.getFormValues('formSol'));">
				<input type="button" class="button medium red" id="enviar" value="Rechazar" onClick="xajax_rechaza();"> </span> 
			<?php }?>
			</div>

		</form>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>