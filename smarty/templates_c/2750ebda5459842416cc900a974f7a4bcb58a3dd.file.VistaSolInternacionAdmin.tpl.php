<?php /* Smarty version Smarty-3.0.6, created on 2013-01-29 14:23:08
         compiled from "smarty/templates\VistaSolInternacionAdmin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:245935107db4c70cc14-77170363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2750ebda5459842416cc900a974f7a4bcb58a3dd' => 
    array (
      0 => 'smarty/templates\\VistaSolInternacionAdmin.tpl',
      1 => 1359402319,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '245935107db4c70cc14-77170363',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>

<?php echo $_smarty_tpl->getVariable('xajax_js')->value;?>


</head>

<body >

<div id="wrapper">
 

	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n Administrativa</h3>
			</div>
			<form id="formId" name="formId" onload="xajax_llena_regiones();">		
		
				<script>
						xajax_llena_regiones();
				</script>
				<div class="col-tri">
					<label>Fecha Evaluaci&oacute;n: </label>
					<input type="text" id="fecha_evaluacion" name="fecha_evaluacion" class="calendar" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_fechaEvaluacion'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>  
					<br class="clearfix" />
					
					<label>Establecimiento Solicitud: </label>  
					<select name="establecimiento_sol" id="establecimiento_sol" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Establecimientos
							<script>
								xajax_llena_establecimientos('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_idEstablecimientoSol'];?>
');
							</script> 
						</option>
					</select>      
					<br class="clearfix" />
					
					<label>Diagnostico: </label>
					<textarea  name="diagnostico" id="diagnostico" style="width: 318px;height: 60px;" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
><?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_diagnostico'];?>
</textarea>
					<br class="clearfix" />
							
					<label>Otro Antenecente: </label>
					<textarea name="otro_antecedente" id="otro_antecedente" style="width: 318px;height: 60px;" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 ><?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_otroAntenedente'];?>
</textarea>
					<br class="clearfix" />
						   
					<label>Ultimo tratamiento: </label>
					<textarea name="ultimo_tratamiento" id="ultimo_tratamiento" style="width: 318px;height: 60px;" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
><?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_ultimoTratamiento'];?>
</textarea>
					<br class="clearfix" />
					<br>
							
					<label><b>Lugar de Internacion:</b></label>
					<select name="establecimiento_int" id="establecimiento_int" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Establecimientos
							<script>
								xajax_llena_establecimientos2('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idEstablecimientoInt'];?>
');
							</script> 
						</option>
					</select>     
				
					<br><br><br>
					<b>Identificaci&oacute;n del medico solicitante:</b>
					<br><br>
				
					<label>Nombre: </label>
					<input type="text" id="nombre_medico" name="nombre_medico" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_nombreMedico'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
					<br class="clearfix" />
					
					<label>C&eacute;dula de identidad: </label>
					<input type="text" name="rut_medico" id="rut_medico" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_rutMedico'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
					<br class="clearfix" />
					
					<label>Correo: </label>
					<input type="text" name="correo_medico" id="correo_medico" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_correoMedico'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
					<br class="clearfix" />
					<br><br>
							
					<?php if (in_array($_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_idEstadoSolicitud'],$_smarty_tpl->getVariable('estados')->value)){?>                  
						<label>Observaci&oacute;n:</label>
						<input type="text" id="observacion" name="observacion" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_observaciones'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>  
					<?php }?>
				</div>
				<div class="col-tri">

					<label>Nombres:</label>
					<input type="text" id="nombres_sol" name="nombres_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_nombresSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>A.Materno:</label>
					<input type="text" id="amaterno_sol" name="amaterno_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_aMaternoSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Rut:</label>
					<input type="text" id="rut_sol" name="rut_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_rutSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 

					<label>Calle:</label>
					<input type="text" id="calle_sol" name="calle_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_calleSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 

					<label>Regiones:</label>
					<select name="region_sol" id="region_sol" onchange="xajax_llena_provincias(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista regiones</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['arrRegiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
						<option value="<?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
							<?php endfor; endif; ?>
					</select>  
					
					<label>Provincia:</label>
					<select name="ciudad_sol" id="ciudad_sol" onchange="xajax_llena_comunas(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Ciudades
						 <script>
								 xajax_llena_provincias('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idRegionSol'];?>
');
						 </script> 
						</option>
					</select>  
					
					<label>Comuna:</label>
					<select name="comuna_sol" id="comuna_sol" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Comunas
							<script>
								 xajax_llena_comunas('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idCiudadSol'];?>
');
							</script> 
						</option>
					</select> 
					
					<div class="sep"></div>
					<label>Nombres:</label> 
					<input type="text" id="nombres_pac" name="nombres_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_nombresPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>A.Materno:</label>
					<input type="text" id="amaterno_pac" name="amaterno_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_aMaternoPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 

					<label>Edad:</label>
					<input type="text" id="edad_pac" name="edad_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_edadPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Calle:</label>
					<input type="text" id="calle_pac" name="calle_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_callePac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Sector:</label> 
					<input type="text" id="sector_pac" name="sector_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sectorPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Vinculaci&oacute;n:</label>
					<select name="vinculacion_pac" id="vinculacion_pac" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="NO">Seleccione Vinculacion</option>
						<script>
							xajax_llena_vinculacion('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_vinculacion'];?>
'); 
						</script>  
				   </select>
					
				</div>     
				<div class="col-tri">

					<label>A.Paterno:</label>
					<input type="text" id="apaterno_sol" name="apaterno_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_aPaternoSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
				   
					<label>Edad:</label>
					<input type="text" id="edad_sol" name="edad_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_edadSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
					
					<label>Sexo:</label>
					<select name="sexo_sol" id="sexo_sol"  <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sexoSol'];?>
');
						</script>  
					</select>
					
					<label>N&deg; Casa:</label>
					<input type="text" id="numerocasa_sol" name="numerocasa_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_nCasaSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
					
					<label>Sector:</label>
					<input type="text" id="sector_sol" name="sector_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sectorSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
					
					<label>Celular:</label>
					<input  ut type="text" id="celular_sol" name="celular_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_celularSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
					
					<label>T. Fijo:</label>
					<input type="text" id="telefono_sol" name="telefono_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_telefonoSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 

					<label>Correo:</label>
					<input type="text" id="correo_sol" name="correo_sol" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_correoSol'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 
					
					<br><br><br>
					
					<label>A.Paterno:</label>
					<input type="text" id="apaterno_pac" name="apaterno_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_aMaternoPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Rut:</label>
					<input type="text" id="rut_pac" name="rut_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_rutPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
> 

					<label>Sexo:</label>
					<select name="sexo_pac" id="sexo_pac"  <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo2('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_sexoPac'];?>
');
						</script>  
					</select>

					<label>N&deg; Casa:</label>
					<input type="text" id="numerocasa_pac" name="numerocasa_pac" value="<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_nCasaPac'];?>
" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>

					<label>Regi&oacute;n:</label>
					<select name="region_pac" id="region_pac" onchange="xajax_llena_provincias2(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista regiones</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['num']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['name'] = 'num';
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'] = is_array($_loop=$_SESSION['arrRegiones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['num']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['num']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['num']['total']);
?>
						<option value="<?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['id_region'];?>
"><?php echo $_SESSION['arrRegiones'][$_smarty_tpl->getVariable('smarty')->value['section']['num']['index']]['nom_region'];?>
</option>
							<?php endfor; endif; ?>
					</select>  
					
					<label>Provincia:</label>
					<select name="ciudad_pac" id="ciudad_pac" onchange="xajax_llena_comunas2(this.value);" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Ciudades
						 <script>
								 xajax_llena_provincias2('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idRegionPac'];?>
');
						 </script> 
						</option>
					  </select> 

					<label>Comuna:</label>
					<select name="comuna_pac" id="comuna_pac" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
>
						<option value ="0">Lista Comunas
						 <script>
								 xajax_llena_comunas2('<?php echo $_SESSION['arrDetsolinternacionadmin']['DETINTADMIN_idCiudadPac'];?>
');
						 </script> 
						</option>
					</select> 
				</div>
				<br class="clearfix" />
				<div id="pieDePagina">  
					<br><br>
					<div class="right">
						<?php if ($_smarty_tpl->getVariable('ver')->value==0){?>
							<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];?>
','<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_idEstadoSolicitud'];?>
',xajax.getFormValues('formId'));">
							<input type="button" class="button medium red" id="enviar" value="Rechazar" onClick="popup('popupObservaciones.php','400','100');"> </span> 
						<?php }?>
					</div>
					<br class="clearfix" />
					<br/>
					<b>Motivo de la Solicitud:</b>
					<br/><br/>
					<input type="checkbox" name="opcion_a" id="opcion_a" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="A" <?php echo $_smarty_tpl->getVariable('OP1')->value;?>
><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
					<br class="clearfix" />  
					<input type="checkbox" name="opcion_b" id="opcion_b" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="B" <?php echo $_smarty_tpl->getVariable('OP2')->value;?>
><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
					<br class="clearfix" />  
					<input type="checkbox" name="opcion_c" id="opcion_c" <?php echo $_smarty_tpl->getVariable('activo')->value;?>
 value="C" <?php echo $_smarty_tpl->getVariable('OP3')->value;?>
><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
					<br/>
					<br class="clearfix" />                  
		
					<?php if ($_smarty_tpl->getVariable('ver')->value==1){?>
						<iframe src="adjuntos_soladmin.php" width="500" height="100" frameborder="0">
						</iframe>
						<br class="clearfix" />
						Agregar Comentario:
						<input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
						onclick="xajax_comentario('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];?>
',comenta.value);"/>
						<br><br>
						<ul class="tabs" persist="true">
							<li><a href="#" rel="view1">Comentarios</a></li>
							<li><a href="#" rel="view2">Eventos</a></li>
							<?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
							<li><a href="#" rel="view3">Firmas</a></li>
							<?php }?>
							<li><a href="#" rel="view4">Adjuntos</a></li>
						   
						</ul>
						<div class="tabcontents">
							<div id="view1" class="tabcontent">
									<script>
										xajax_lista_comentarios('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];?>
');
									</script> 
									Comentarios 
									 <div id="detalle_comentarios">
									</div>
							   
							</div>
							<div id="view2" class="tabcontent">
									<script>
										xajax_lista_eventos('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];?>
');
									</script>
									<div id="detalle_eventos">
									</div>
								  
							</div>
							<?php if ($_SESSION['DEPART_id']==3||$_SESSION['DEPART_id']==4||$_SESSION['DEPART_id']==5){?>
							<div id="view3" class="tabcontent">
							   
									Firmas
								
							</div>
							<?php }?>
							<div id="view4" class="tabcontent">
									<script>
										xajax_lista_adjuntos('<?php echo $_SESSION['arrDetsolinternacionadmin']['CABINTADMIN_id'];?>
');
									</script> 
									<div id="detalle1">
									</div>
							</div>

						</div>
					<?php }?>
					<br class="clearfix" />
				</div>
			   
			</form>	
		</div> 
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet </div>

	</html>