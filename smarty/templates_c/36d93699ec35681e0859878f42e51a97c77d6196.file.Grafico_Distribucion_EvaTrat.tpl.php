<?php /* Smarty version Smarty-3.0.6, created on 2013-01-31 17:37:20
         compiled from "smarty/templates\Grafico_Distribucion_EvaTrat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13602510aabd03f8a93-81983590%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36d93699ec35681e0859878f42e51a97c77d6196' => 
    array (
      0 => 'smarty/templates\\Grafico_Distribucion_EvaTrat.tpl',
      1 => 1359653836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13602510aabd03f8a93-81983590',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
  <title> Demo con Array</title>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta http-equiv="content-language" content="es">
  
  <script type='text/javascript' src='js/jquery.js'></script>
  
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/result-light.css">
  
 

<script type='text/javascript'>//<![CDATA[ 

$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Distribucion segun Lugar'
            },
            subtitle: {
                text: 'Fuente: Solicitudes de Evaluacion y Tratamiento'
            },
            xAxis: {
                categories: [
                    'Solicitudes Evaluacion y Tratamiento'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad Solicitudes'
                }
            },
            legend: {
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                y: 70,
                floating: true,
                shadow: true
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.x +' '+this.series.name+': '+ this.y;
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
                series: [{
                name: 'Hospital del Salvador',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[0];?>
]
    
            }, {
                name: 'Hospital Dr. Gustavo Fricke',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[1];?>
]
    
            }, {
                name: 'Hospital de Quilpue',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[2];?>
]
    
            }, {
                name: 'Clinica San Antonio',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[3];?>
]
    
            }, {
                name: 'Hospital Dr. Philippe Pinel',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[4];?>
]
    
            }, {
                name: 'Hospital San Martín de Quillota',
                data: [<?php echo $_smarty_tpl->getVariable('data')->value[5];?>
]
    
            }



            ]
        });
    });

    
});
//]]>  

</script>


</head>
<body>
  <script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

  
</body>

</html>
