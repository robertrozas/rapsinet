<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}   
	   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Alta Hospitalizaci&oacute;n Administrativa</h3>
			</div>

			<div class="col-left">
				<label>Rut: </label><input type="text" id="rut" placeholder="Ingrese Rut">
				<br class="clearfix" />
				<label>A Paterno: </label><input type="text" id="paterno" placeholder="Apellido Paterno">
				<label> Fecha de Ingreso: </label><input type="text" id="fec_ing" class="calendar" placeholder="26/11/2012">
				<label>Direcci&oacute;n: </label><input type="text" id="domicilio" placeholder="Ingrese la direcci&oacute;n">
			</div>
			<div class="col-right">
				<label>Nombre: </label><input type="text" id="nombre" placeholder="Ingrese Nombre">
				<br class="clearfix" />
				<label>A Materno: </label><input type="text" id="materno" placeholder="Apellido Materno"> 
				<br class="clearfix" />
				<label> Fecha de Egreso: </label><input type="text" id="fec_egre" class="calendar" placeholder="Ej: 26/11/2012">
				<br class="clearfix" />
				<label>Fono: </label><input type="text" id="comuna" placeholder="Ingrese el Fono">
			</div>
			<br class="clearfix" />
			<br>
			
			<label> Diagnostico: </label><textarea cols="25" rows="2"></textarea><br>
			<label> Evoluci&oacute;n Clinica: </label><textarea cols="25" rows="2"></textarea><br>
            <label> Tratamiento: </label><textarea cols="25" rows="2"></textarea>
            <br><br>
           
			<b>Referencia de Alta</b>
			<br><br>
			<div class="col-left">
				<label>Centro de Salud: </label><input type="text" id="domicilio" placeholder="Ingrese la direcci&oacute;n">
				<br class="clearfix" />
				<label>Direcci&oacute;n: </label><input type="text" id="comuna" placeholder="Ingrese la direcci&oacute;n">
				<br class="clearfix" />
				<label>Hora: </label><input type="text" id="comuna" placeholder="Ingrese el Fono">
			</div>
			<div class="col-right">
				<label> N&deg; Profesional:  </label><input type="text" id="domicilio" placeholder="Ingrese la direcci&oacute;n">
				<br class="clearfix" />
				<label>Fecha: </label><input type="text" id="fecha" class="calendar" placeholder="Seleccione la fecha">
			</div>
			<br class="clearfix" />
            <br/><br> 
			<div class="right">
				<input type="button" class="button medium green" id="enviar" value="Visar">
				<input type="button" id="enviar" class="button medium blue" value="Rechazar">
			</div>
			<br class="clearfix" />
		</div>	
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>