<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}  
	   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
					<div class="box">
				<h3>Formulario de Internaci&oacute;n de Urgencia no Voluntaria:</h3>
			</div>
			
			<div class="col-left">
                <label> Rut: </label><input type="text" id="rut" placeholder="Ingrese Rut">
				<br class="clearfix" />
				<label> A Paterno: </label><input type="text" id="paterno" placeholder="Ap Paterno">
				<br class="clearfix" />
				<label> Fecha de Nacimiento: </label><input type="text" id="fec_nac" class="calendar" placeholder="26/11/2012">
				<br class="clearfix" />
				<label> Comuna:	</label><input type="text" id="comuna" placeholder="Vi&ntilde;a...">
				<br class="clearfix" />
				<label> Tipo servicio: </label><select name="servicio"><option value="ambu">Ambulatorio</option><option value="perpetuo">Largo Plazo</option></select>
				<br class="clearfix" />
				<label> Fecha Desde: </label><input type="text" id="fech_desde" class="calendar" placeholder="desde...">
				<br class="clearfix" />
				<label> Cel. Identidad:	</label><input type="text" id="identidad" placeholder="ident...">
				<br class="clearfix" />
			</div>
			<div class="col-right">
				<label> Nombre:	</label><input type="text" id="nombre" placeholder="Ingrese Nombre"> 
				<br class="clearfix" />
				<label> A Materno:	</label><input type="text" id="materno" placeholder="Ap Materno">
				<br class="clearfix" />
				<label> Domicilio:	</label><input type="text" id="domicilio" placeholder="Calle, N&deg;...">
				<br class="clearfix" />
				<label> Est. Ref: </label><input type="text" id="estado" placeholder="ref.....">
				<br class="clearfix" />
				<label>	Hospital: </label><select name="servicio"><option value="fricke">Gustavo Fricke</option><option value="buren">Van Buren</option></select>
				<br class="clearfix" />
				<label> Medico:	</label><select name="medico"><option value="lagos">Jose Lagos</option><option value="perez">Juan Perez</option></select>
				<br class="clearfix" />
				<label> Fecha Actual: </label><input type="text" id="fech_actual" class="calendar" placeholder="26/11/2012">
				<br class="clearfix" />
			</div>
            <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
            <input type="radio" name="a" value="a"><b> A )</b> Necesidad de efectuar un diagnostico o evaluac&oacute;on clinica que no puede realizarse en forma ambulatoria.<tr>
			<br/>					
			
			<input type="radio" name="b" value="b"><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br/>
			
			<input type="radio" name="c" value="c"><b> C )</b> Que el estado o condicion psiquica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, psiquico o psicosocial inminente, para si mismo o para terceros.
            <br class="clearfix" />
            <br><br>
			<div class="right">
                <input type="button" class="button medium green" id="enviar" value="&nbsp;&nbsp;&nbsp;Visar&nbsp;&nbsp;&nbsp;">		
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" id="enviar" class="button medium blue" value="Rechazar"> </span> 
            </div>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>