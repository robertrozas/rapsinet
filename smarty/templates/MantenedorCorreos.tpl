<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

{$xajax_js}

<script type="text/javascript">
    
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=30");
}
</script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}  
	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Mantenedor de Hospitales</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
                <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		
	</div>
	<div id="page">
		<div id="content">
			<h3>Mantenedor de Correos</h3>

<div class="tabcontents"> 
	   <form id='formUser' name='formUser'>
              
              <label>Descripcion:</label> <input type="text" id="descripcion" name="descripcion" value="" />
              <br><br>
              <label>Correo:</label> <input type="text" id="mail" name="mail" value="" /> 

              <br class="clearfix" /><br class="clearfix" />
              <input type="button" value="Modificar" class="button large green"  onClick="xajax_modificar_correo(xajax.getFormValues('formUser'));"/>
            </form>
</div>                        
                        
                        <div class="tabcontents">
                           		      
                           <div id="correos" class="tabcontent">
				
                            	<script >
                          				xajax_lista_correos();
                      		</script>

		                      </div> 
			  
				
                    </div>	
			   </div>
			</div>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>