<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript">
    
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=30");
}
</script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{$xajax_js} 

{include file="menu_principal.tpl"}  
	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Administradores</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
                 <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
			<h3>Bandeja SEREMI</h3>
                        
                         <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Solicitud Internaci&oacute;n</a></li>
                            <li><a href="#" rel="solinv">Solicitud No Voluntaria</a></li>
                            <li><a href="#" rel="soleva">Solicitud Evaluaci&oacute;n</a></li>
                            <li><a href="#" rel="solaa">Solicitud Alta</a></li>
                            <li><a href="#" rel="view2">Historico Evaluaciones</a></li>
                            <li><a href="#" rel="view3">Hist&oacute;rico Usuarios</a></li>
                        </ul>
                        <div class="tabcontents">	
                            <div id="view1" class="tabcontent">  
                            ID: <input type="text" id="id2" name="id2" onChange="xajax_filtra_solicitudInternacionAdmin(this.value, estado2.value, fecha2.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha2" name="fecha2" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solicitudInternacionAdmin(id2.value,estado2.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado2" id="estado2" onChange="xajax_filtra_solicitudInternacionAdmin(id2.value,this.value,fecha2.value);">
                            <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado2();
                                </script>
                                
                            </select>   
                            
                                <div id="solicitudes">
				                 <script>
                                    xajax_lista_solicitudes();
                                 </script> 
                                </div>   
		            </div> 	


                        <div id="solinv" class="tabcontent">
                            ID: <input type="text" id="id4" name="id4" onChange="xajax_filtra_solinv(this.value,estado4.value, fecha4.value);">
                            &nbsp;&nbsp;&nbsp;   
                            Fecha: <input type="text" id="fecha4" name="fecha4" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solinv(id4.value,estado4.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado4" id="estado4" onChange="xajax_filtra_solinv(id4.value,this.value,fecha4.value);">
                            <option>Seleccione</option>
                               
                                 </select>  
                          <div id="internaciones"><script>xajax_filtra_solinv('','','');</script></div>         
                          

                        </div>  

                        <div id="soleva" class="tabcontent">
                           <div id="evaluaciones">
                           <script>xajax_filtra_solevas('','','');</script>
                           </div> 
                        </div>   

                           <div id="solaa" class="tabcontent">
                                    ID: <input type="text" id="id5" name="id5" onChange="xajax_filtra_solaa(this.value,estado5.value, fecha5.value);">
                                    &nbsp;&nbsp;&nbsp;   
                                    Fecha: <input type="text" id="fecha5" name="fecha5" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_solaa(id5.value,estado5.value,this.value);" />
                                    &nbsp;&nbsp;&nbsp;
                                    Estado:<select name="estado5" id="estado5" onChange="xajax_filtra_solaa(id5.value,this.value,fecha5.value);">
                                    <option>Seleccione</option>
                                        

                                         </select>  
                                  <div id="hospitalizacion">
                                  <script>xajax_filtra_solaa('','','');</script>
                                  </div>     
                           </div> 
			   <div id="view2" class="tabcontent">
			    Fecha: <input type="text" id="fecha" name="fecha" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_fecha_a(estado.value,tipo.value,this.value);" />
                            &nbsp;&nbsp;&nbsp;
                            Estado:<select name="estado" id="estado" onChange="xajax_filtra_estado(this.value,tipo.value,fecha.value);">
                                <option>Seleccione</option>
                                <script>
                                    xajax_llena_estado();
                                </script>
                                
                                 </select>
                            &nbsp;&nbsp;&nbsp;
                            Tipo: <select name="tipo" id="tipo" onChange="xajax_filtra_tipo(estado.value,this.value,fecha.value);">
                                <option>Seleccione</option>
                                <script>
                                    xajax_llena_tipo();
                                </script>  
                                
                                </select>   
                               
                             <div id="log_solicitudes">
                                <script>
                                    xajax_lista_log_solicitudes();
                                </script>  
                             </div>   			
				  
			   </div>	   
		           <div id="view3" class="tabcontent">
				ID: <input type="text" id="id3" name="id3" onChange="xajax_filtra_historico_usuarios(this.value,fecha3.value);">
                                &nbsp;&nbsp;&nbsp;   
                                Fecha: <input type="text" id="fecha3" name="fecha3" class="calendar" placeholder="26/11/2012" onChange="xajax_filtra_historico_usuarios(id3.value,this.value);" />
                                <div id="log_historico_usuarios">   
                                    <script>
                                        xajax_lista_historico_usuarios();
                                    </script>     
                                </div>
                            <br class="clearfix" />	
                            </div>	
			   </div>
			</div>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>