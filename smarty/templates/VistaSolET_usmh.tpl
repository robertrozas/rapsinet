<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />

<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<!--- Calendario --->
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<link rel="stylesheet" href="css/calendar.css" type="text/css">
<script>
$(function() {
	$( ".calendar" ).datepicker({ 
		autoSize: true,
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});
});
</script>

<!-- Script Menu -->
{$xajax_js}

</head>

<body onload="xajax_carga();">

<div id="wrapper"> 
	
	<div id="page">
		<div id="content">
			<div class="box">
				<h3> Solicitud de Evaluaci&oacute;n y Tratamiento:</h3>
			</div>
		<form id="formTrata" name="formTrata">
			<label>Fecha Solicitud:</label> <input type="text" class="calendar" name="fec_sol" id="fec_sol" value="{$smarty.session.arrDetsoleva.fecha_sol|date_format:"d-m-Y"}" {$activo}/><label>Ficha Clinica:</label> <input type="text" id="ficha" name="ficha" onkeyup="xajax_numero('ficha');" value="{$smarty.session.arrDetsoleva.ficha}" {$activo}/> 
			<br class="clearfix" />
			<label>Visita Domiciliaria:</label> <input type="text" class="calendar" name="fec_vis" id="fec_vis" value="{$smarty.session.arrDetsoleva.fecha_visita|date_format:"d-m-Y"}" {$activo}/>
			<input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
			<br class="clearfix" />
			<b>Tengo a bien solicitar a Usted dicte resoluci&oacute;n administrativa de traslado y evaluaci&oacute;n a un centro asistencial que permita una evaluaci&oacute;n psiqui&aacute;trica de la persona que se individualiza a continuaci&oacute;n:</b>
            <hr><br>

            <label>Regi&oacute;n:	</label><select id="region" name="region" onChange="xajax_provincias(this.value);" {$activo}>
            								<option value ="0">Lista regiones</option>
                                            {section name=num  loop=$smarty.session.Regiones}
                                            <option value="{$smarty.session.Regiones[num].id_region}">{$smarty.session.Regiones[num].nom_region}</option>
                                            {/section}
                                            </select>
            <label>Provincia: </label><select id="provincia" name="provincia" onChange="xajax_comunas(this.value);" {$activo}></select>
			<br class="clearfix" />
			<label>Comuna:	</label><select id="comuna" name="comuna" onChange="xajax_hospital(this.value);" {$activo}></select><label>Establecimiento: </label>
			<select id="hospital" name="hospital" {$activo}>
			</select>
			<br class="clearfix" />
			<label>Nombres Paciente: </label><input type="text" id="identidad" name="identidad" onkeyup="xajax_texto('identidad');" value="{$smarty.session.arrDetsoleva.nombres_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Apellido Paterno: </label><input type="text" id="ap_pat" name="ap_pat" onkeyup="xajax_texto('ap_pat');" value="{$smarty.session.arrDetsoleva.ap_pat}" {$activo}/><label>Apellido Materno: </label><input type="text" id="ap_mat" name="ap_mat" onkeyup="xajax_texto('ap_mat');" value="{$smarty.session.arrDetsoleva.ap_mat}" {$activo}/>
			<br class="clearfix" />
			<label>Rut: </label><input type="text" id="rut" name="rut" onkeyup="xajax_validarut(this.value,'rut');" value="{$smarty.session.arrDetsoleva.rut_pac}" {$activo}/><label>Edad: </label><input type="text" id="edad" name="edad" onkeyup="xajax_numero('edad');" value="{$smarty.session.arrDetsoleva.edad_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Sexo: </label><select id="sexo" name="sexo" {$activo}>
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
		    <label>Direcci&oacute;n: </label><input type="text" id="dire" name="dire" value="{$smarty.session.arrDetsoleva.dire_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Fono: </label><input type="text" id="fono" name="fono" value="{$smarty.session.arrDetsoleva.fono_pac}" {$activo}/>
            <br>  
		    <b>Identificaci&oacute;n del Profesional que solicita Resoluci&oacute;n:</b>
            <hr><br> 
            <label>Rut Profesional Solicitante:	</label><input type="text" id="rut_doc" name="rut_doc" onkeyup="xajax_validarut(this.value,'rut_doc');" value="{$smarty.session.arrDetsoleva.rut_doc}" {$activo}/><label>Nombres: </label><input type="text" id="nom_doc" name="nom_doc" onkeyup="xajax_texto('nom_doc');" value="{$smarty.session.arrDetsoleva.nom_doc}" {$activo}/>
			<br class="clearfix" />
			<label>Profesi&oacute;n:</label><input type="text" id="pro_doc" name="pro_doc" onkeyup="xajax_texto('pro_doc');" value="{$smarty.session.arrDetsoleva.prof_doc}" {$activo}/><label>Fono Contacto: </label><input type="text" id="fono_doc" name="fono_doc" value="{$smarty.session.arrDetsoleva.fono_doc}" {$activo}/>
			<br class="clearfix" />
			<label>Email Contacto:</label><input type="text" id="mail_doc" name="mail_doc" value="{$smarty.session.arrDetsoleva.mail_doc}" {$activo}/>
			 {if $smarty.session.arrDetsoleva['estado_sol']|in_array:$estados}  
            <label>Observaci&oacute;n:</label><input type="text" id="observacion" name="observacion" value="{$smarty.session.arrDetsoleva['obser_soleva']}" {$activo}>  
            {/if}
            <br><br>

        {if $ver == 3}
            ¿Requiere Internaci&oacute;n?	
            <br>
            <input type="radio" name="opcion" id="opcion" value="1" checked> SI<br>
            <input type="radio" name="opcion" id="opcion" value="0"> NO<br>
            <input type="button" class="button medium blue" id="enviar" value="Confirmar" onClick="xajax_confirmar_evaluacion('{$smarty.session.arrDetsoleva['cab_solevatrat']}',xajax.getFormValues('formTrata'));">
        {/if}        

        {if $ver == 4}
            ¿Se encontr&oacute; el paciente?	
            <br>
             <input type="radio" name="opcion" id="opcion" value="1" checked> SI<br>
             <input type="radio" name="opcion" id="opcion" value="0"> NO<br>

            <input type="button" class="button medium blue" id="enviar" value="Confirmar" onClick="xajax_confirmar_ET_primera_llamada('{$smarty.session.arrDetsoleva['cab_solevatrat']}',xajax.getFormValues('formTrata'));">
        {/if} 

        {if $ver == 5}
            ¿Se encontr&oacute; el paciente(2)?	
            <br>
             <input type="radio" name="opcion" id="opcion" value="1" checked> SI<br>
             <input type="radio" name="opcion" id="opcion" value="0"> NO<br>

            <input type="button" class="button medium blue" id="enviar" value="Confirmar" onClick="xajax_confirmar_ET_segunda_llamada('{$smarty.session.arrDetsoleva['cab_solevatrat']}',xajax.getFormValues('formTrata'));">
        {/if} 
        
        {if $ver == 6}
            ¿Derivar como Internaci&oacute;n y registrarlo?<br>	
           
             <input type="radio" name="opcion" id="opcion" value="1" checked> SI<br>
             <input type="radio" name="opcion" id="opcion" value="0"> NO<br>
             
            <input type="button" class="button medium blue" id="enviar" value="Confirmar" onClick="xajax_registrar_ET('{$smarty.session.arrDetsoleva['cab_solevatrat']}',xajax.getFormValues('formTrata'));">
        {/if} 
		</form>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>
        
        
        
        
