<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>
       
        
{$xajax_js}
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}  

    <div id="header">
        <div class="left inter"></div>
        <div class="left head-title">
            <h1>Solicitudes</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
        </div>
         <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
        <br class="clearfix" />
    </div>
	<div id="page">
	    <div id="content">
			<div class="box">
				<h3>Cambiar Configuraci&oacute;n de Usuario:</h3>
			</div>
		<form id="formSol" name="formSol">	
			<div class="col-left">
                <label> Correo: </label><input type="text" id="correo" name="correo" value="{$smarty.session.USUA_mail}" >
				
				
			</div>
			<div class="col-right">
				
				<label> Contrase&ntilde;a:    </label><input type="text" id="password" name="password" value="{$smarty.session.USUA_pass}" > 


             </div>
                        
             
			<div class="col-left">
			
			<label></label>	<input type="button" class="button medium blue" id="enviar" value="Actualizar Mis datos" onClick="xajax_modificar(xajax.getFormValues('formSol'));">
							
			</div>

		</form>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>