<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>



{$xajax_js}
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}  

	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		 <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
	    <div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n de Urgencia no Voluntaria:</h3>
			</div>
		<form id="formSol" name="formSol">	
			<div class="col-left">
                <label> Rut: </label><input type="text" id="rut" name="rut" placeholder="Ingrese Rut" onkeyup="xajax_validarut(this.value,'rut');" />
               
				<br class="clearfix" />
				<label> Apellidos: </label><input type="text" id="apellidos" name="apellidos" placeholder="Apellidos" onkeyup="xajax_texto('apellidos');">
				<br class="clearfix" />
				<label> Fecha de Nacimiento: </label><input type="text" id="fec_nac" name="fec_nac" class="calendar" placeholder="26/11/2012">
				
				<br class="clearfix" />
				<label> Fecha Solicitud: </label><input type="text" id="fech_desde" name="fech_desde" class="calendar" placeholder="Solicita...">
				
				<br class="clearfix" />
				<label>Tipo Servicio:</label>
                                    <select name="servicios" id="servicios" tabindex="6">
                                        <option>Seleccione</option>
                                    <script>
                                        xajax_llena_servicios();
                                    </script>  
                                    </select>
				<br class="clearfix" />
				<label>Medico:</label>
                                    <select name="medicos" id="medicos" tabindex="6" >
                                        <option>Seleccione</option>
                                    <script>
                                        xajax_llena_medicos();
                                    </script>  
                            </select>
				
			</div>
			<div class="col-right">
				<label> Nombres:	</label><input type="text" id="nombres" name="nombres" placeholder="Ingrese Nombre" onkeyup="xajax_texto('nombres');" > 
				<br class="clearfix" />

				<label> Domicilio:	</label><input type="text" id="domicilio" name="domicilio" placeholder="Calle, N&deg;...">
				<br class="clearfix" />
				<label>Sexo: </label><select id="sexo" name="sexo">
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
				<label> Fecha Internaci&oacute;n: </label><input type="text" id="fec_int" name="fec_int" class="calendar" placeholder="26/11/2012">
				<br class="clearfix" />
				<label>Establecimiento:</label>
                                    <select name="establecimiento" id="establecimiento" tabindex="6">
                                        <option>Seleccione</option>
                                    <script>
                                        xajax_llena_establecimientos();
                                    </script>  
                                    </select>
				
			</div>
                    <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" value="A"><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" value="B"><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" value="C"><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br/>
                    <br class="clearfix" />
		    <fieldset>
				<b>Estado:</b><br>
				<input type="checkbox" name="rakim" value="1"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="2"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="3"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="4"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		    </fieldset>	
            <br><br>
			<div class="right">
				
				<input type="button" class="button medium blue" id="enviar" value="Enviar Solicitud" onClick="xajax_test(xajax.getFormValues('formSol'));" >
				
			</div>

		</form>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>