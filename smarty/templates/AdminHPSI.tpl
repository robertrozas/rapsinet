<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>


{$xajax_js}

{include file="menu_principal.tpl"}  
	<div id="header">
		<div class="left admins"></div>
		<div class="left head-title">
			<h1>Administradores</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
              <div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
                    <h3>Bandeja de HPSI</h3>
                        
                         <ul class="tabs" persist="true">
                             <li><a href="#" rel="view1">Evaluaci&oacute;n y Tratamiento</a></li>
                             <li><a href="#" rel="view2">Internaci&oacute;n Administrativa</a></li>
                             <li><a href="#" rel="view3">Alta Hospitalizaci&oacute;n Administrativa</a></li>
                             <li><a href="#" rel="view4">Asignaci&oacute;n de Camas</a></li>
                        </ul>
                        <div class="tabcontents">	
                          <div id="view1" class="tabcontent">
                            	<div id="evaluaciones">
				  <script>
                                     xajax_filtra_solevas();
                                  </script> 
                                </div> 
		          </div>
			  	   		
			  <div id="view2" class="tabcontent">
                              <div id="solicitudes">
				  <script>
                                     xajax_lista_solicitudes();
                                  </script> 
                                </div> 
                          </div> 
				
                          <div id="view3" class="tabcontent">
				<div id="altas">
				  <script>
                                     xajax_filtra_solaa();
                                  </script> 
                                </div>   	
		          </div> 
                       <form id="formSol" name="formSol">    
                          <div id="view4" class="tabcontent">
                              <label>Camas a Disponer:</label> <input type="text" id="camas_disponer" name="camas_disponer">&nbsp;&nbsp;<input type="button" class="button medium blue" id="enviar" value="Actualizar Camas" onClick="xajax_actualizar_camas(xajax.getFormValues('formSol'));"> 
                              <br><br> 
                              <label>Camas Disponibles:</label> <input type="text" id="camas_disponibles" name="camas_disponibles" value="{$smarty.session.camasActuales}" disabled></input>   
                              <br><br>
                              <label>Camas Asignadas:</label> <input type="text" id="camas_asignadas" name="camas_asignadas" value="{$smarty.session.camasAsignadas}" disabled></input>  
				<div id="camas">
				  <script>
                                     xajax_filtra_camas();
                                  </script> 
                                </div>   	
		          </div>   
                       </form>         
                    </div>	
	         </div>
			
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</div>
</body>
</html>