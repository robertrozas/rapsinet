<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />

<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
{$xajax_js}

{include file="menu_principal.tpl"}   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
			<div class="box">
				<h3> Solicitud de Evaluaci&oacute;n y Tratamiento:</h3>
			</div>
		<form id="formTrata" name="formTrata">
			<label>Fecha Solicitud:</label> <input type="text" class="calendar" name="fec_sol" id="fec_sol" /><label>Ficha Clinica:</label> <input type="text" id="ficha" name="ficha" onkeyup="xajax_numero('ficha');" /> 
			<br class="clearfix" />
			<label>Visita Domiciliaria:</label> <input type="text" class="calendar" name="fec_vis" id="fec_vis" />
			<br class="clearfix" />
			<b>Tengo a bien solicitar a Usted dicte resoluci&oacute;n administrativa de traslado y evaluaci&oacute;n a un centro asistencial que permita una evaluaci&oacute;n psiqui&aacute;trica de la persona que se individualiza a continuaci&oacute;n:</b>
            <hr><br>

            <label>Regi&oacute;n:	</label><select id="region" name="region" onChange="xajax_provincias(this.value);">
            								<option value ="0">Lista regiones</option>
                                            {section name=num  loop=$smarty.session.Regiones}
                                            <option value="{$smarty.session.Regiones[num].id_region}">{$smarty.session.Regiones[num].nom_region}</option>
                                            {/section}
                                            </select>
            <label>Provincia: </label><select id="provincia" name="provincia" onChange="xajax_comunas(this.value);"></select>
			<br class="clearfix" />
			<label>Comuna:	</label><select id="comuna" name="comuna" onChange="xajax_hospital(this.value);"></select><label>Establecimiento: </label>
			<select id="hospital" name="hospital" >
			</select>
                        
			<br class="clearfix" />
			<label>Nombres Paciente: </label><input type="text" id="identidad" name="identidad" onkeyup="xajax_texto('identidad');"/>
			<br class="clearfix" />
			<label>Apellido Paterno: </label><input type="text" id="ap_pat" name="ap_pat" onkeyup="xajax_texto('ap_pat');" /><label>Apellido Materno: </label><input type="text" id="ap_mat" name="ap_mat" onkeyup="xajax_texto('ap_mat');" />
			<br class="clearfix" />
			<label>Rut: </label><input type="text" id="rut" name="rut" onkeyup="xajax_validarut(this.value,'rut');" /><label>Edad: </label><input type="text" id="edad" name="edad" onkeyup="xajax_numero('edad');" />
			<br class="clearfix" />
			<label>Sexo: </label><select id="sexo" name="sexo">
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
		    <label>Direcci&oacute;n: </label><input type="text" id="dire" name="dire" />
			<br class="clearfix" />
			<label>Fono: </label><input type="text" id="fono" name="fono" />
            <br> 
            <b>Identificaci&oacute;n del Solicitante:</b>
            <hr><br> 
            <label>Nombres: </label><input type="text" name="nombre_sol" id="nombre_sol" onkeyup="xajax_texto('nombre_sol');" /><label>Apellidos: </label><input type="text" name="apel_sol" id="apel_sol" onkeyup="xajax_texto('apel_sol');" />
            <br class="clearfix" />
            <label>Rut: </label><input type="text" name="rut_sol" id="rut_sol" onkeyup="xajax_validarut(this.value,'rut_sol');" /><label>Edad: </label><input type="text" name="edad_sol" id="edad_sol" onkeyup="xajax_numero('edad_sol');"/>
            <br class="clearfix" />
            <label>Direcci&oacute;n:</label><input type="text" name="dir_sol" id="dir_sol" />
            <label>Regi&oacute;n:</label><select id="regiones" name="regiones" onChange="xajax_provincia(this.value);">
									        <option value ="0">Lista regiones</option>
                                            {section name=num  loop=$smarty.session.Regiones}
                                            <option value="{$smarty.session.Regiones[num].id_region}">{$smarty.session.Regiones[num].nom_region}</option>
                                            {/section}
								         </select>
            <br class="clearfix" />
            <label>Provincia:</label><select id="provincias" name="provincias" onChange="xajax_comuna(this.value);">
									    <option value="">Seleccione Provincia</option>
									
								     </select>
            <label>Comuna:</label><select id="comunas" name="comunas">
									<option value="">Seleccione Comuna</option>
									
								  </select>

            <br class="clearfix" />
            <label>Sexo: </label><select id="sexo_sol" name="sexo_sol">
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>

		    <label>Vinculaci&oacute;n:</label><select id="vincula" name="vincula">
									<option value="0">Seleccione Parentezco</option>
									<option value="1">Padre</option>
									<option value="2">Madre</option>
									<option value="3">Primo</option>
									<option value="4">Prima</option>
									<option value="5">Hijo</option>
									<option value="6">Hija</option>
									<option value="7">Hermano</option>
									<option value="8">Hermana</option>
									<option value="9">Vecino</option>
									<option value="10">Vecina</option>
								</select>
            <br class="clearfix" />
            <br class="clearfix" />
		    <b>Identificaci&oacute;n del Profesional que solicita Resoluci&oacute;n:</b>
            <hr><br> 
            <label>Rut Profesional Solicitante:	</label><input type="text" id="rut_doc" name="rut_doc" onkeyup="xajax_validarut(this.value,'rut_doc');" /><label>Nombres: </label><input type="text" id="nom_doc" name="nom_doc" onkeyup="xajax_texto('nom_doc');" />
			<br class="clearfix" />
			<label>Profesi&oacute;n:</label><input type="text" id="pro_doc" name="pro_doc" onkeyup="xajax_texto('pro_doc');" /><label>Fono Contacto: </label><input type="text" id="fono_doc" name="fono_doc" />
			<br class="clearfix" />
			<label>Email Contacto:</label><input type="text" id="mail_doc" name="mail_doc" />
            <br><br>
            Lugar donde se realizara la evaluaci&oacute;n:
                            
                        <select name="hospital_psi" id="hospital_psi" {$activo}>
                        <option value ="0">Lista Establecimientos
                         <script>
                                 xajax_hospital_psi();
                         </script> 
                        </option>
                      </select>   
             <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" value="A"><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" value="B"><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" value="C"><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br/>
                    <br class="clearfix" />   
            
			<div >
				<input type="button" id="enviar" class="button medium blue" value="Enviar" onclick="xajax_solicita(xajax.getFormValues('formTrata'));" />
				<input type="reset" id="limpia" class="button medium green" value="Reset" onclick="xajax_limpia();" />
			</div>

		</form>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>