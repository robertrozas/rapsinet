<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
{$xajax_js} 

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>

{include file="menu_principal.tpl"}   

	   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<br class="clearfix" />
	</div>
	<div id="page">
		<div id="content">
                   {section name=num  loop=$smarty.session.arrSolicitudesPorVisar} 
			<div class="box">
				<h3>Formulario de Internaci&oacute;n Administrativa</h3>
			</div>
		 <div id="columnaIzquierda"> 	
			<label>Fecha Evaluaci&oacute;n: </label><input type="text" id="fech_ev" class="calendar" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].fecha_sol}" disabled>
			<br class="clearfix" />
			<label>Establecimiento Solicitante: </label><input type="text" id="establecimiento" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].estsol}" disabled>
			<br><br>
			<div class="col-left">
				<label>Diagnostico: </label><textarea cols="48" rows="3" disabled>{$smarty.session.arrSolicitudesPorVisar[num].diagnostico}</textarea>
			</div>
			<div class="col-right">		
				<label>Otro Antecedente: </label><textarea cols="48" rows="3" disabled>{$smarty.session.arrSolicitudesPorVisar[num].otro_antecedente}</textarea>
			</div>
			<br class="clearfix" />
			<div class="col-left">
                                    <label>Ultimo tratamiento: </label><textarea name="ultimo_tratamiento" id="ultimo_tratamiento" cols="48" rows="3" disabled>{$smarty.session.arrSolicitudesPorVisar[num].ultimo_tratamiento}</textarea>
			</div>
                        <br class="clearfix" />
                        <label>Lugar de Internacion:</label><input type="text" id="establecimiento" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].estint}" disabled>

			<br><br>
                        <b>Identificaci&oacute;n del medico solicitante:</b>
			<br><br>
            
			<label>Nombre: </label><input type="text" id="rut" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].nombre_medico}" disabled>
			<br class="clearfix" />
                        <label>Cedula de identidad: </label><input type="text" id="rut" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].rut_medico}" disabled>
			<br class="clearfix" />
			<label>Correo: </label><input type="text" id="rut" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].correo_medico}" disabled>
			<br class="clearfix" />
			<br><br>
	    </div> 
            <div id="columnaDerecha"> 
              Nombres:<input type="text" id="nombres_sol" name="nombres_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].nombres_sol}" disabled>
              A.Paterno:<input type="text" id="apaterno_sol" name="apaterno_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].apaterno_sol}" disabled>
              <br><br>   
              A.Materno:<input type="text" id="amaterno_sol" name="amaterno_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].amaterno_sol}" disabled>  
              Rut:<input type="text" id="rut_sol" name="rut_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].rut_sol}" disabled> 
              <br><br>  
              Edad:<input type="text" id="edad_sol" name="edad_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].edad_sol}" disabled>
              Sexo:<input type="text" id="sexo_sol" name="sexo_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].sexo_sol}" disabled>
              <br><br>   
              Calle:<input type="text" id="calle_sol" name="calle_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].calle_sol}" disabled>  
              N casa:<input type="text" id="numerocasa_sol" name="numerocasa_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].ncasa_sol}" disabled> 
              <br><br>  
              Sector:<input type="text" id="sector_sol" name="sector_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].sector_sol}" disabled>
              Comuna:<input type="text" id="comuna_sol" name="comuna_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].comuna_sol}" disabled>
              <br><br>   
              Ciudad:<input type="text" id="ciudad_sol" name="ciudad_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].ciudad_sol}" disabled>  
              Region:<input type="text" id="region_sol" name="region_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].region_sol}" disabled> 
              <br><br>  
              T.Fijo:<input type="text" id="telefono_sol" name="telefono_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].telefono_sol}" disabled>  
              Celular:<input type="text" id="celular_sol" name="celular_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].celular_sol}" disabled> 
              <br><br>
              Correo:<input type="text" id="correo_sol" name="correo_sol" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].correo_sol}" disabled> 

              <br><br><br>
              Nombres:<input type="text" id="nombres_pac" name="nombres_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].nombres_pac}" disabled>
              A.Paterno:<input type="text" id="apaterno_pac" name="apaterno_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].apaterno_pac}" disabled>
              <br><br>   
              A.Materno:<input type="text" id="amaterno_pac" name="amaterno_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].amaterno_pac}" disabled>  
              Rut:<input type="text" id="rut_pac" name="rut_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].rut_pac}" disabled> 
              <br><br>  
              Edad:<input type="text" id="edad_pac" name="edad_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].edad_pac}" disabled>
              Sexo:<input type="text" id="sexo_pac" name="sexo_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].sexo_pac}" disabled>
              <br><br>   
              Calle:<input type="text" id="calle_pac" name="calle_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].calle_pac}" disabled>  
              N casa:<input type="text" id="numerocasa_pac" name="numerocasa_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].ncasa_pac}" disabled> 
              <br><br>  
              Sector:<input type="text" id="sector_pac" name="sector_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].sector_pac}" disabled>
              Comuna:<input type="text" id="comuna_pac" name="comuna_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].comuna_pac}" disabled>
              <br><br>   
              Ciudad:<input type="text" id="ciudad_pac" name="ciudad_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].ciudad_pac}" disabled>  
              Region:<input type="text" id="region_pac" name="region_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].region_pac}" disabled> 
              <br><br>
              Vinculacion:<input type="text" id="vinculacion_pac" name="vinculacion_pac" placeholder="{$smarty.session.arrSolicitudesPorVisar[num].vinculacion_pac}" disabled>   
            </div>
            <div id="pieDePagina">               
                      
                        
			<input type="checkbox" name="A" value="a" {$OP1}><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="B" value="b" {$OP2}><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="C" value="c" {$OP3}><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br class="clearfix" />
			<br><br>
			<div class="right">
                   		
                                <input type="button" class="button medium green" id="enviar" value="Aprobar" onClick="xajax_aprobar_solicitud('{$smarty.session.arrSolicitudesPorVisar[num].codigo}','{$smarty.session.arrSolicitudesPorVisar[num].estado_solicitud}');">
                
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" id="enviar" class="button medium blue" value="Rechazar" onClick="popup('popupObservaciones.php','400','100');"> </span> 
                        </div>
            {/section}                     
			<br>
			<br class="clearfix" />
	    </div>
		<br class="clearfix" />
	</div>
        </div>                
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>