<!--
<link rel="stylesheet" type="text/css" href="css/style.css" />
-->
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<!-- Actualiza tu Navegador -->
<script type="text/javascript" src="http://updateyourbrowser.net/asn2.js"></script>

<script type="text/javascript">
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top="+posicion_y+"");
}
</script>

<!--- Calendario --->
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<link rel="stylesheet" href="css/calendar.css" type="text/css">
<script>
$(function() {
	$( ".calendar" ).datepicker({ 
		autoSize: true,
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});
});
</script>

<!-- Script Menu -->
<script type="text/javascript">
	$(function() {
		$('#menu > ul').dropotron({
			mode: 'fade',
			globalOffsetY: 11,
			offsetY: -15
		});
	});
</script>


</head>
<body>
<div id="wrapper">
	<div id="menu">
		<ul>
			{if $smarty.session.Solicitudes == 1}
			<li class="first">
			<span class="opener">Solicitudes<b></b></span>
			<ul>
				<li><a href="SolicitudNoVoluntaria.php">Urgencia No Voluntaria</a></li>
				<li><a href="SolicitudEvaluacionYTratamiento.php">Evaluaci&oacute;n y Tratamiento</a></li>
				<li><a href="SolicitudInternacionAdministrativa.php">Internaci&oacute;n Administrativa</a></li>
				<li><a href="SolicitudAltaHospitalizacion.php">Alta Hospitalizaci&oacute;n</a></li>
			</ul>
			</li>
			{/if}
			{if $smarty.session.Bandejas == 1}
			<li>
				<span class="opener">Bandejas <b></b></span>
				<ul>
				{if $smarty.session.DEPART_id == 1} <li><a href="AdminDepSaludPublica.php">Departamento Salud Publica</a></li>{/if}
				{if $smarty.session.DEPART_id == 6}	<li><a href="AdminSecretaria.php">Secretaria</a></li>{/if}
                {if $smarty.session.DEPART_id == 2} <li><a href="AdminJuridica.php">Jur&iacute;dica</a></li>{/if}
				{if $smarty.session.DEPART_id == 3} <li><a href="AdminSeremi.php">Seremi</a></li>{/if}
			    {if $smarty.session.DEPART_id == 4} <li><a href="AdminContraloria.php">Contraloria</a></li>{/if}
			    {if $smarty.session.DEPART_id == 7} <li><a href="AdminUSMH.php">USMH</a></li>{/if}
                {if $smarty.session.DEPART_id == 8} <li><a href="AdminHPSI.php">H. Psiqui&aacute;trico</a></li>{/if}
			    {if $smarty.session.DEPART_id == 5 || $smarty.session.PER_id ==4}
			    <li><a href="AdminDepSaludPublica.php">Departamento Salud Publica</a></li>
			    <li><a href="AdminSecretaria.php">Secretaria</a></li>
			    <li><a href="AdminJuridica.php">Jur&iacute;dica</a></li>
			    <li><a href="AdminSeremi.php">Seremi</a></li>
			    <li><a href="AdminContraloria.php">Contraloria</a></li>
                <li><a href="AdminUSMH.php">USMH</a></li>
                <li><a href="AdminHPSI.php">H. Psiqui&aacute;trico</a></li>
			    {/if}
			   </ul>
			</li> 
			{/if}

			{if $smarty.session.Mantenedores == 1}
			<li>
				<span class="opener">Mantenedores<b></b></span>
				<ul>
					<li><a href="MantenedorMedicos.php">M&eacute;dicos</a></li>
					<li><a href="MantenedorUsuarios.php">Usuarios</a></li>
					<li><a href="MantenedorRoles.php">Perfiles-Funciones</a></li>
					<li><a href="MantenedorHospitales.php">Hospitales</a></li>
                    <li><a href="MantenedorCorreos.php">Correos</a></li>
			   </ul>
			</li>
			{/if}
			{if $smarty.session.Graficos == 1}
			<li>
				<span class="opener">Gr&aacute;ficos<b></b></span>
				<ul>
					<li><a href="javascript:popup('modeloarray.html',800,400)">Barra</a></li>
					<li><a href="javascript:popup('modelotabla.html',800,400)">Torta</a></li>
					<li><a href="javascript:popup('Grafico_Distribucion_EvaTrat.php',800,400)">Distribucion segun Lugar de Evaluacion</a></li>
				</ul>
			</li>
			{/if}

			{if $smarty.session.Reportes == 1}
			<li><a href="#">Reportes</a></li>
			{/if}
			<li><span class="opener">Mis Datos<b></b></span>
				<ul>
					<li><a href="MisDatos.php">Cambiar Configuraci&oacute;n</a></li>	
			   </ul>	
			</li>
			<li><a href="Salir.php">Cerrar sesion</a></li>
		</ul>
		<br class="clearfix" />
	</div>



 