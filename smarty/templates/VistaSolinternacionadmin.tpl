<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>

{$xajax_js}

</head>

<body >

<div id="wrapper">
 

	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n Administrativa</h3>
			</div>
			<form id="formId" name="formId" onload="xajax_llena_regiones();">		
		
				<script>
						xajax_llena_regiones();
				</script>
				<div class="col-tri">
					<label>Fecha Evaluaci&oacute;n: </label>
					<input type="text" id="fecha_evaluacion" name="fecha_evaluacion" class="calendar" value="{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_fechaEvaluacion']}" {$activo}>  
					<br class="clearfix" />
					
					<label>Establecimiento Solicitud: </label>  
					<select name="establecimiento_sol" id="establecimiento_sol" {$activo}>
						<option value ="0">Lista Establecimientos
							<script>
								xajax_llena_establecimientos('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_idEstablecimientoSol']}');
							</script> 
						</option>
					</select>      
					<br class="clearfix" />
					
					<label>Diagnostico: </label>
					<textarea  name="diagnostico" id="diagnostico" style="width: 318px;height: 60px;" {$activo}>{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_diagnostico']}</textarea>
					<br class="clearfix" />
							
					<label>Otro Antenecente: </label>
					<textarea name="otro_antecedente" id="otro_antecedente" style="width: 318px;height: 60px;" {$activo} >{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_otroAntenedente']}</textarea>
					<br class="clearfix" />
						   
					<label>Ultimo tratamiento: </label>
					<textarea name="ultimo_tratamiento" id="ultimo_tratamiento" style="width: 318px;height: 60px;" {$activo}>{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_ultimoTratamiento']}</textarea>
					<br class="clearfix" />
					<br>
							
					<label><b>Lugar de Internacion:</b></label>
					<select name="establecimiento_int" id="establecimiento_int" {$activo}>
						<option value ="0">Lista Establecimientos
							<script>
								xajax_llena_establecimientos2('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_idEstablecimientoInt']}');
							</script> 
						</option>
					</select>     
				
					<br><br><br>
					<b>Identificaci&oacute;n del medico solicitante:</b>
					<br><br>
				
					<label>Nombre: </label>
					<input type="text" id="nombre_medico" name="nombre_medico" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_nombreMedico']}" {$activo}>
					<br class="clearfix" />
					
					<label>C&eacute;dula de identidad: </label>
					<input type="text" name="rut_medico" id="rut_medico" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_rutMedico']}" {$activo}> 
					<br class="clearfix" />
					
					<label>Correo: </label>
					<input type="text" name="correo_medico" id="correo_medico" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_correoMedico']}" {$activo}>
					<br class="clearfix" />
					<br><br>
							
					{if $smarty.session.arrDetsolinternacionadmin['CABINTADMIN_idEstadoSolicitud']|in_array:$estados}                  
						<label>Observaci&oacute;n:</label>
						<input type="text" id="observacion" name="observacion" value="{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_observaciones']}" {$activo}>  
					{/if}
				</div>
				<div class="col-tri">

					<label>Nombres:</label>
					<input type="text" id="nombres_sol" name="nombres_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_nombresSol']}" {$activo}>

					<label>A.Materno:</label>
					<input type="text" id="amaterno_sol" name="amaterno_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_aMaternoSol']}" {$activo}>

					<label>Rut:</label>
					<input type="text" id="rut_sol" name="rut_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_rutSol']}" {$activo}> 

					<label>Calle:</label>
					<input type="text" id="calle_sol" name="calle_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_calleSol']}" {$activo}> 

					<label>Regiones:</label>
					<select name="region_sol" id="region_sol" onchange="xajax_llena_provincias(this.value);" {$activo}>
						<option value ="0">Lista regiones</option>
							{section name=num  loop=$smarty.session.arrRegiones}
						<option value="{$smarty.session.arrRegiones[num].id_region}">{$smarty.session.arrRegiones[num].nom_region}</option>
							{/section}
					</select>  
					
					<label>Provincia:</label>
					<select name="ciudad_sol" id="ciudad_sol" onchange="xajax_llena_comunas(this.value);" {$activo}>
						<option value ="0">Lista Ciudades
						 <script>
								 xajax_llena_provincias('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_idRegionSol']}');
						 </script> 
						</option>
					</select>  
					
					<label>Comuna:</label>
					<select name="comuna_sol" id="comuna_sol" {$activo}>
						<option value ="0">Lista Comunas
							<script>
								 xajax_llena_comunas('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_idCiudadSol']}');
							</script> 
						</option>
					</select> 
					
					<div class="sep"></div>
					<label>Nombres:</label> 
					<input type="text" id="nombres_pac" name="nombres_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_nombresPac']}" {$activo}>

					<label>A.Materno:</label>
					<input type="text" id="amaterno_pac" name="amaterno_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_aMaternoPac']}" {$activo}> 

					<label>Edad:</label>
					<input type="text" id="edad_pac" name="edad_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_edadPac']}" {$activo}>

					<label>Calle:</label>
					<input type="text" id="calle_pac" name="calle_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_callePac']}" {$activo}>

					<label>Sector:</label> 
					<input type="text" id="sector_pac" name="sector_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_sectorPac']}" {$activo}>

					<label>Vinculaci&oacute;n:</label>
					<select name="vinculacion_pac" id="vinculacion_pac" {$activo}>
						<option value ="NO">Seleccione Vinculacion</option>
						<script>
							xajax_llena_vinculacion('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_vinculacion']}'); 
						</script>  
				   </select>
					
				</div>     
				<div class="col-tri">

					<label>A.Paterno:</label>
					<input type="text" id="apaterno_sol" name="apaterno_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_aPaternoSol']}" {$activo}>
				   
					<label>Edad:</label>
					<input type="text" id="edad_sol" name="edad_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_edadSol']}" {$activo}>
					
					<label>Sexo:</label>
					<select name="sexo_sol" id="sexo_sol"  {$activo}>
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_sexoSol']}');
						</script>  
					</select>
					
					<label>N&deg; Casa:</label>
					<input type="text" id="numerocasa_sol" name="numerocasa_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_nCasaSol']}" {$activo}> 
					
					<label>Sector:</label>
					<input type="text" id="sector_sol" name="sector_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_sectorSol']}" {$activo}>
					
					<label>Celular:</label>
					<input  ut type="text" id="celular_sol" name="celular_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_celularSol']}" {$activo}> 
					
					<label>T. Fijo:</label>
					<input type="text" id="telefono_sol" name="telefono_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_telefonoSol']}" {$activo}> 

					<label>Correo:</label>
					<input type="text" id="correo_sol" name="correo_sol" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_correoSol']}" {$activo}> 
					
					<br><br><br>
					
					<label>A.Paterno:</label>
					<input type="text" id="apaterno_pac" name="apaterno_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_aMaternoPac']}" {$activo}>

					<label>Rut:</label>
					<input type="text" id="rut_pac" name="rut_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_rutPac']}" {$activo}> 

					<label>Sexo:</label>
					<select name="sexo_pac" id="sexo_pac"  {$activo}>
						<option value ="0">Seleccione Sexo</option>
						<script>
							xajax_llena_sexo2('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_sexoPac']}');
						</script>  
					</select>

					<label>N&deg; Casa:</label>
					<input type="text" id="numerocasa_pac" name="numerocasa_pac" value="{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_nCasaPac']}" {$activo}>

					<label>Regi&oacute;n:</label>
					<select name="region_pac" id="region_pac" onchange="xajax_llena_provincias2(this.value);" {$activo}>
						<option value ="0">Lista regiones</option>
							{section name=num  loop=$smarty.session.arrRegiones}
						<option value="{$smarty.session.arrRegiones[num].id_region}">{$smarty.session.arrRegiones[num].nom_region}</option>
							{/section}
					</select>  
					
					<label>Provincia:</label>
					<select name="ciudad_pac" id="ciudad_pac" onchange="xajax_llena_comunas2(this.value);" {$activo}>
						<option value ="0">Lista Ciudades
						 <script>
								 xajax_llena_provincias2('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_idRegionPac']}');
						 </script> 
						</option>
					  </select> 

					<label>Comuna:</label>
					<select name="comuna_pac" id="comuna_pac" {$activo}>
						<option value ="0">Lista Comunas
						 <script>
								 xajax_llena_comunas2('{$smarty.session.arrDetsolinternacionadmin['DETINTADMIN_idCiudadPac']}');
						 </script> 
						</option>
					</select> 
				</div>
				<br class="clearfix" />
				<div id="pieDePagina">  
					<br><br>
					<div class="right">
						{if $ver==0}
							<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_id']}','{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_idEstadoSolicitud']}',xajax.getFormValues('formId'));">
							<input type="button" class="button medium red" id="enviar" value="Rechazar" onClick="popup('popupObservaciones.php','400','100');"> </span> 
						{/if}
					</div>
					<br class="clearfix" />
					<br/>
					<b>Motivo de la Solicitud:</b>
					<br/><br/>
					<input type="checkbox" name="opcion_a" id="opcion_a" {$activo} value="A" {$OP1}><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
					<br class="clearfix" />  
					<input type="checkbox" name="opcion_b" id="opcion_b" {$activo} value="B" {$OP2}><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
					<br class="clearfix" />  
					<input type="checkbox" name="opcion_c" id="opcion_c" {$activo} value="C" {$OP3}><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
					<br/>
					<br class="clearfix" />                  
		
					{if $ver==1}
						<iframe src="adjuntos_soladmin.php" width="500" height="100" frameborder="0">
						</iframe>
						<br class="clearfix" />
						Agregar Comentario:
						<input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
						onclick="xajax_comentario('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_id']}',comenta.value);"/>
						<br><br>
						<ul class="tabs" persist="true">
							<li><a href="#" rel="view1">Comentarios</a></li>
							<li><a href="#" rel="view2">Eventos</a></li>
							{if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
							<li><a href="#" rel="view3">Firmas</a></li>
							{/if}
							<li><a href="#" rel="view4">Adjuntos</a></li>
						   
						</ul>
						<div class="tabcontents">
							<div id="view1" class="tabcontent">
									<script>
										xajax_lista_comentarios('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_id']}');
									</script> 
									Comentarios 
									 <div id="detalle_comentarios">
									</div>
							   
							</div>
							<div id="view2" class="tabcontent">
									<script>
										xajax_lista_eventos('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_id']}');
									</script>
									<div id="detalle_eventos">
									</div>
								  
							</div>
							{if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
							<div id="view3" class="tabcontent">
							   
									Firmas
								
							</div>
							{/if}
							<div id="view4" class="tabcontent">
									<script>
										xajax_lista_adjuntos('{$smarty.session.arrDetsolinternacionadmin['CABINTADMIN_id']}');
									</script> 
									<div id="detalle1">
									</div>
							</div>

						</div>
					{/if}
					<br class="clearfix" />
				</div>
			   
			</form>	
		</div> 
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet </div>

	</html>