<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />

<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<!--- Calendario --->
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<link rel="stylesheet" href="css/calendar.css" type="text/css">
<script>
$(function() {
	$( ".calendar" ).datepicker({ 
		autoSize: true,
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});
});
</script>

<!-- Script Menu -->
{$xajax_js}

</head>

<body onload="xajax_carga();">

<div id="wrapper"> 
	
	<div id="page">
		<div id="content">
			<div class="box">
				<h3> Solicitud de Evaluaci&oacute;n y Tratamiento:</h3>
			</div>
		<form id="formTrata" name="formTrata">
			<label>Fecha Solicitud:</label> <input type="text" class="calendar" name="fec_sol" id="fec_sol" value="{$smarty.session.arrDetsoleva.fecha_sol|date_format:"d-m-Y"}" {$activo}/><label>Ficha Clinica:</label> <input type="text" id="ficha" name="ficha" onkeyup="xajax_numero('ficha');" value="{$smarty.session.arrDetsoleva.ficha}" {$activo}/> 
			<br class="clearfix" />
			<label>Visita Domiciliaria:</label> <input type="text" class="calendar" name="fec_vis" id="fec_vis" value="{$smarty.session.arrDetsoleva.fecha_visita|date_format:"d-m-Y"}" {$activo}/>
			<input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
			<br class="clearfix" />
			<b>Tengo a bien solicitar a Usted dicte resoluci&oacute;n administrativa de traslado y evaluaci&oacute;n a un centro asistencial que permita una evaluaci&oacute;n psiqui&aacute;trica de la persona que se individualiza a continuaci&oacute;n:</b>
            <hr><br>

            <label>Regi&oacute;n:	</label><select id="region" name="region" onChange="xajax_provincias(this.value);" {$activo}>
            								<option value ="0">Lista regiones</option>
                                            {section name=num  loop=$smarty.session.Regiones}
                                            <option value="{$smarty.session.Regiones[num].id_region}">{$smarty.session.Regiones[num].nom_region}</option>
                                            {/section}
                                            </select>
            <label>Provincia: </label><select id="provincia" name="provincia" onChange="xajax_comunas(this.value);" {$activo}></select>
			<br class="clearfix" />
			<label>Comuna:	</label><select id="comuna" name="comuna" onChange="xajax_hospital(this.value);" {$activo}></select><label>Establecimiento: </label>
			<select id="hospital" name="hospital" {$activo}>
			</select>
			<br class="clearfix" />
			<label>Nombres Paciente: </label><input type="text" id="identidad" name="identidad" onkeyup="xajax_texto('identidad');" value="{$smarty.session.arrDetsoleva.nombres_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Apellido Paterno: </label><input type="text" id="ap_pat" name="ap_pat" onkeyup="xajax_texto('ap_pat');" value="{$smarty.session.arrDetsoleva.ap_pat}" {$activo}/><label>Apellido Materno: </label><input type="text" id="ap_mat" name="ap_mat" onkeyup="xajax_texto('ap_mat');" value="{$smarty.session.arrDetsoleva.ap_mat}" {$activo}/>
			<br class="clearfix" />
			<label>Rut: </label><input type="text" id="rut" name="rut" onkeyup="xajax_validarut(this.value,'rut');" value="{$smarty.session.arrDetsoleva.rut_pac}" {$activo}/><label>Edad: </label><input type="text" id="edad" name="edad" onkeyup="xajax_numero('edad');" value="{$smarty.session.arrDetsoleva.edad_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Sexo: </label><select id="sexo" name="sexo" {$activo}>
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
		    <label>Direcci&oacute;n: </label><input type="text" id="dire" name="dire" value="{$smarty.session.arrDetsoleva.dire_pac}" {$activo}/>
			<br class="clearfix" />
			<label>Fono: </label><input type="text" id="fono" name="fono" value="{$smarty.session.arrDetsoleva.fono_pac}" {$activo}/>
            <br>  
      <br> 
            <b>Identificaci&oacute;n del Solicitante:</b>
            <hr><br> 
            <label>Nombres: </label><input type="text" name="nombre_sol" id="nombre_sol" value="{$smarty.session.arrDetsoleva.nom_sol}" onkeyup="xajax_texto('nombre_sol');" {$activo}/><label>Apellidos: </label><input type="text" name="apel_sol" id="apel_sol" onkeyup="xajax_texto('apel_sol');" value="{$smarty.session.arrDetsoleva.apel_sol}" {$activo}/>
            <br class="clearfix" />
            <label>Rut: </label><input type="text" name="rut_sol" id="rut_sol" value="{$smarty.session.arrDetsoleva.rut_sol}" onkeyup="xajax_validarut(this.value,'rut_sol');" {$activo}/><label>Edad: </label><input type="text" name="edad_sol" id="edad_sol" value="{$smarty.session.arrDetsoleva.edad_sol}" onkeyup="xajax_numero('edad_sol');" {$activo}/>
            <br class="clearfix" />
            <label>Direcci&oacute;n:</label><input type="text" name="dir_sol" id="dir_sol" value="{$smarty.session.arrDetsoleva.dir_sol}" {$activo}/>
            <label>Regi&oacute;n:</label><select id="regiones" name="regiones" onChange="xajax_provincia(this.value);" {$activo}>
                          <option value ="0">Lista regiones</option>
                                            {section name=num  loop=$smarty.session.Regiones}
                                            <option value="{$smarty.session.Regiones[num].id_region}">{$smarty.session.Regiones[num].nom_region}</option>
                                            {/section}
                         </select>
            <br class="clearfix" />
            <label>Provincia:</label><select id="provincias" name="provincias" onChange="xajax_comuna(this.value);" {$activo}>
                      <option value="">Seleccione Provincia</option>
                  
                     </select>
            <label>Comuna:</label><select id="comunas" name="comunas" {$activo}>
                  <option value="">Seleccione Comuna</option>
                  
                  </select>

            <br class="clearfix" />
            <label>Sexo: </label><select id="sexo_sol" name="sexo_sol" {$activo}>
                  <option value="">Seleccione</option>
                  <option value="0">Masculino</option>
                  <option value="1">Femenino</option>
                </select>

        <label>Vinculaci&oacute;n:</label><select id="vincula" name="vincula" {$activo}>
                  <option value="0">Seleccione Parentezco</option>
                  <option value="1">Padre</option>
                  <option value="2">Madre</option>
                  <option value="3">Primo</option>
                  <option value="4">Prima</option>
                  <option value="5">Hijo</option>
                  <option value="6">Hija</option>
                  <option value="7">Hermano</option>
                  <option value="8">Hermana</option>
                  <option value="9">Vecino</option>
                  <option value="10">Vecina</option>
                </select>
            <br class="clearfix" />
            <br class="clearfix" />
		    <b>Identificaci&oacute;n del Profesional que solicita Resoluci&oacute;n:</b>
            <hr><br> 
            <label>Rut Profesional Solicitante:	</label><input type="text" id="rut_doc" name="rut_doc" onkeyup="xajax_validarut(this.value,'rut_doc');" value="{$smarty.session.arrDetsoleva.rut_doc}" {$activo}/><label>Nombres: </label><input type="text" id="nom_doc" name="nom_doc" onkeyup="xajax_texto('nom_doc');" value="{$smarty.session.arrDetsoleva.nom_doc}" {$activo}/>
			<br class="clearfix" />
			<label>Profesi&oacute;n:</label><input type="text" id="pro_doc" name="pro_doc" onkeyup="xajax_texto('pro_doc');" value="{$smarty.session.arrDetsoleva.prof_doc}" {$activo}/><label>Fono Contacto: </label><input type="text" id="fono_doc" name="fono_doc" value="{$smarty.session.arrDetsoleva.fono_doc}" {$activo}/>
			<br class="clearfix" />
			<label>Email Contacto:</label><input type="text" id="mail_doc" name="mail_doc" value="{$smarty.session.arrDetsoleva.mail_doc}" {$activo}/>
			 {if $smarty.session.arrDetsoleva['estado_sol']|in_array:$estados}  
            <label>Observaci&oacute;n:</label><input type="text" id="observacion" name="observacion" value="{$smarty.session.arrDetsoleva['obser_soleva']}" {$activo}>  
            {/if}
            <br><br>
             Lugar donde se realizara la evaluaci&oacute;n:
                            
                        <select name="hospital_psi" id="hospital_psi" {$activo}>
                        <option value ="0">Lista Establecimientos
                         <script>
                                 xajax_hospital2({$smarty.session.arrDetsoleva['lugar_evaluacion']});
                         </script> 
                        </option>
                      </select>    
              <br><br> 

      <br class="clearfix" />
      <br/>
      <b>Motivo de la Solicitud:</b>
      <br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" {$activo} value="A" {$OP1}><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
      <br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" {$activo} value="B" {$OP2}><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
      <br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" {$activo} value="C" {$OP3}><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
      <br/>
      <br class="clearfix" />          
             {if $ver==1}
                <iframe src="adjuntos_soleva.php" width="500" height="100" frameborder="0">
                </iframe>
             <br>
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('{$smarty.session.arrDetsoleva.cab_solevatrat}',comenta.value);"/>
			<br><br>
		        <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Comentarios</a></li>
                            <li><a href="#" rel="view2">Eventos</a></li>
                            {if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
                            <li><a href="#" rel="view3">Firmas</a></li>
                            {/if}
                            <li><a href="#" rel="view4">Adjuntos</a></li>
                        </ul>
                  
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                            <script>
                                    xajax_lista_comentarios('{$smarty.session.arrDetsoleva.cab_solevatrat}');
                                </script> 
                            <div id="comentarios">
                            	Comentarios
                            </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                         <script>
                                    xajax_lista_eventos('{$smarty.session.arrDetsoleva.cab_solevatrat}');
                         </script>
                         <div id="eventos">
                         	Eventos
                         </div>
                         
                        </div>
                        {if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
                        <div id="view3" class="tabcontent">
                           
                                Firmas
                            
                        </div>
                        {/if}
                        <div id="view4" class="tabcontent">
                           
                               Grilla Adjuntos
                               <script>
                                    xajax_lista_adjuntos('{$smarty.session.arrDetsoleva.cab_solevatrat}');
                               </script> 
                               
                                <div id="adjuntos">

                                </div>
                            
                        </div>
                        
                   
                    </div>
		  {/if}

                        {if $ver==0}
                            <div >
                                    <input type="button" id="enviar" class="button large green" value="Visar" onclick="xajax_solicita(xajax.getFormValues('formTrata'));" />
                                    <input type="button" id="limpia" class="button large red" value="Rechazar" onclick="xajax_rechaza();" />
                            </div>
			{/if}
		</form>
			<br class="clearfix" />
		</div>
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>