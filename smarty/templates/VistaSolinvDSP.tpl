<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>
       
        
{$xajax_js}
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

</head>

<body>

    
<div id="wrapper">

	
	<div id="page">
	    <div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n de Urgencia no Voluntaria:</h3>
			</div>
		<form id="formSol" name="formSol">	
			<div class="col-left">
                <label> Rut: </label><input type="text" id="rut" name="rut" value="{$smarty.session.arrDetsolinv['CABSOLINV_rutPaciente']}" {$activo}>
				<br class="clearfix" />
				<label> Apellidos: </label><input type="text" id="apellidos" name="apellidos" value="{$smarty.session.arrDetsolinv['CABSOLINV_apellidosPaciente']}" {$activo}>
				<br class="clearfix" />
                <input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
				<label> Fecha de Nacimiento: </label><input type="text" id="fec_nac" name="fec_nac"  value="{$smarty.session.arrDetsolinv['CABSOLINV_fechaNacPaciente']}" {$activo}>
				<br class="clearfix" />
				<label>Tipo Servicio:</label>
                                     <select name="servicios" id="servicios" {$activo}>
                                        <option value ="0">Seleccione Servicio</option>
                                        <script>
                                            xajax_llena_servicios('{$smarty.session.arrDetsolinv['CABSOLINV_idServicio']}');
                                        </script>  
                                      </select>
				<br class="clearfix" />
				<label> Fecha Solicitud: </label><input type="text" id="fech_desde" name="fech_desde" value="{$smarty.session.arrDetsolinv['CABSOLINV_fechaSolicitud']}" {$activo}>
				<br class="clearfix" />
				
			</div>
			<div class="col-right">
				<label> Nombres:	</label><input type="text" id="nombres" name="nombres" value="{$smarty.session.arrDetsolinv['CABSOLINV_nombresPaciente']|utf8_decode|utf8_decode}" {$activo}> 
				<br class="clearfix" />
				<label> Domicilio:	</label><input type="text" id="domicilio" name="domicilio" value="{$smarty.session.arrDetsolinv['CABSOLINV_direccionPaciente']}" {$activo}>
				<br class="clearfix" />
				<label> Fecha Internaci&oacute;n: </label><input type="text" id="fec_int" name="fec_int" value="{$smarty.session.arrDetsolinv['CABSOLINV_fechaInternacion']}" {$activo}>
				<br class="clearfix" />
				<label>Establecimiento:</label>
                                
                                <select name="establecimiento" id="establecimiento" {$activo}>
                                <option value="0">
                                 Selecciones Hospital..  
                                 </option>  
                                <script>
                                        xajax_llena_establecimientos();
                                </script>  
                                  
                                </select>
				<br class="clearfix" />
                                <label>M&eacute;dico:</label>
                                <select name="medicos" id="medicos" {$activo}>
                                        <option value ="0">Seleccione Medicos</option>
                                        <script>
                                            xajax_llena_medicos('{$smarty.session.arrDetsolinv['CABSOLINV_IDMEDICO']}');
                                        </script>  
                                </select>
                                {if $smarty.session.arrDetsolinv['CABSOLINV_estadoSolicitud']|in_array:$estados}  
                                    <label>Observaci&oacute;n:</label>
                                    <input type="text" id="observacion" name="observacion" value="{$smarty.session.arrDetsolinv['CABSOLINV_observacion']}" {$activo}>  
                                {/if}
                        </div>
                        
                    <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" {$activo} value="A" {$OP1}><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" {$activo} value="B" {$OP2}><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" {$activo} value="C" {$OP3}><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br/>
                    <br class="clearfix" />
            {if $ver==1}
            <iframe src="adjuntos_solint.php" width="500" height="100" frameborder="0">
            </iframe>
             <br class="clearfix" />
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('{$smarty.session.arrDetsolinv['CABSOLINV_ID']}',comenta.value);"/>
			<br><br>
		               <ul class="tabs" persist="true">
                            <li><a href="#" rel="view1">Comentarios</a></li>
                            <li><a href="#" rel="view2">Eventos</a></li>
                            {if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
                            <li><a href="#" rel="view3">Firmas</a></li>
                            {/if}
                            <li><a href="#" rel="view4">Adjuntos</a></li>
                        </ul>
                  
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                            <script>
                                    xajax_lista_comentarios('{$smarty.session.arrDetsolinv['CABSOLINV_ID']}');
                                </script> 
                               <div id="detalle_comentarios">
                            </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                         <script>
                                    xajax_lista_eventos('{$smarty.session.arrDetsolinv['CABSOLINV_ID']}');
                         </script>
                         <div id="detalle">
                         </div>
                         
                        </div>
                        {if $smarty.session.DEPART_id == 3 || $smarty.session.DEPART_id == 4 || $smarty.session.DEPART_id == 5}
                        <div id="view3" class="tabcontent">
                           
                                Firmas
                            
                        </div>
                        {/if}
                        <div id="view4" class="tabcontent">
                           
                               Grilla Adjuntos
                                <script>
                                    xajax_lista_adjuntos('{$smarty.session.arrDetsolinv['CABSOLINV_ID']}');
                                </script> 
                                <div id="detalle1">
                                </div>
                            
                        </div>
                        
                   
                    </div>
		  {/if}
            <br><br>
			<div class="right">
			{if $ver==0}
				<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar('{$smarty.session.arrDetsolinv['CABSOLINV_ID']}','{$smarty.session.arrDetsolinv['CABSOLINV_estadoSolicitud']}',xajax.getFormValues('formSol'));">
				<input type="button" class="button medium red" id="enviar" value="Rechazar" onClick="xajax_rechaza();"> </span> 
			{/if}
			</div>

		</form>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>