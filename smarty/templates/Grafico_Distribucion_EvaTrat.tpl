<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
  <title> Demo con Array</title>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta http-equiv="content-language" content="es">
  
  <script type='text/javascript' src='js/jquery.js'></script>
  
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/result-light.css">
  
 

<script type='text/javascript'>//<![CDATA[ 

$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Distribucion segun Lugar'
            },
            subtitle: {
                text: 'Fuente: Solicitudes de Evaluacion y Tratamiento'
            },
            xAxis: {
                categories: [
                    'Solicitudes Evaluacion y Tratamiento'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad Solicitudes'
                }
            },
            legend: {
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                y: 70,
                floating: true,
                shadow: true
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.x +' '+this.series.name+': '+ this.y;
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
                series: [{
                name: 'Hospital del Salvador',
                data: [{$data.0}]
    
            }, {
                name: 'Hospital Dr. Gustavo Fricke',
                data: [{$data.1}]
    
            }, {
                name: 'Hospital de Quilpue',
                data: [{$data.2}]
    
            }, {
                name: 'Clinica San Antonio',
                data: [{$data.3}]
    
            }, {
                name: 'Hospital Dr. Philippe Pinel',
                data: [{$data.4}]
    
            }, {
                name: 'Hospital San Martín de Quillota',
                data: [{$data.5}]
    
            }



            ]
        });
    });

    
});
//]]>  

</script>


</head>
<body>
  <script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

  
</body>

</html>
