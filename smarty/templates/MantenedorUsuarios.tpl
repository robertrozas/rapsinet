<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>

{$xajax_js}

<script type="text/javascript">
    
function popup(url,ancho,alto) {
var posicion_x; 
var posicion_y; 
posicion_x=(screen.width/2)-(ancho/2); 
posicion_y=(screen.height/2)-(alto/2); 
window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=30");
}
</script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}  
		<div id="header">
			<div class="left admins"></div>
			<div class="left head-title">
				<h1>Mantenedor Usuarios</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
			</div>
					<div id="InformacionUsuario">
						<div class="descripcionUsuario">
							<b>Bienvenido/a<br></b>
							Roxana Pena
							<br>
							Informatica
							<br>
							<script languaje="JavaScript">

							var mydate=new Date()
							var year=mydate.getYear()
							if (year < 1000)
							year+=1900
							var day=mydate.getDay()
							var month=mydate.getMonth()
							var daym=mydate.getDate()
							if (daym<10)
							daym="0"+daym
							var dayarray=new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado")
							var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
							document.write("<small><font color='000080' face='Arial'>"+dayarray[day]+" "+daym+" de "+montharray[month]+" de "+year+"</font></small>")

							</script>
							
						</div>      
					</div>
			
		</div>
		<div id="page">
			<div id="content">
				<h3>Ingreso y Administraci&oacute;n de Usuario</h3>

				<div> 
					<form id='formUser' name='formUser'>
						<div class="col-tri">
							<label>Usuario:</label> 
							<input type="text" id="usuario" name="usuario" value="" />
							<br class="clearfix" />
							
							<label>Rut:</label> 
							<input type="text" id="rut" name="rut" value="" /> 
							<br class="clearfix" />
							
							<label>Celular:</label>
							<input type="text" id="celular" name="celular" value="" />
							<br class="clearfix" />
							
							<label>Direcci&oacute;n:</label>
							<input type="text" id="direccion" name="direccion" value="" /> 
							<br class="clearfix" />
						</div>
						<div class="col-tri">
							<label>Contrasena:</label> 
							<input type="text" id="clave" name="clave" value="" />
							<br class="clearfix" />
							
							<label>Nombres:</label> 
							<input type="text" name="nombres" id="nombres" value="" />
							<br class="clearfix" />
							
							<label>Fono:</label>
							<input type="text" id="fono" name="fono" value="" />
							<br class="clearfix" />
							
							<label>Establecimiento:</label>
							<select name="establecimiento" id="establecimiento">
							   <option>Seleccione</option>
								  <script>
									  xajax_llena_establecimientos();
								  </script>     
							</select> 
							<br class="clearfix" />
						</div>
						<div class="col-tri">
							<label>Correo:</label> 
							<input type="text" id="correo" name="correo" value="" /> 
							<br class="clearfix" />
							
							<label>Apellidos:</label> 
							<input type="text" name="apellidos" id="apellidos" value="" /> 
							<br class="clearfix" />
							
							<label>Perfil:</label>
							<select name="perfil" id="perfil">
							   <option>Seleccione</option>
								  <script>
									  xajax_llena_perfiles();
								  </script>     
							</select> 
							<br class="clearfix" />
							
							<label>Departamento:</label>
							<select name="departamento" id="departamento">
								<option>Seleccione</option>
								<script>
								xajax_llena_departamentos();
								</script>     
							</select> 
							<br class="clearfix" />
						</div>
						<br class="clearfix" />
						<br>
						<input type="button" value="Guardar" class="button large green"  onClick="xajax_agrega_user(xajax.getFormValues('formUser'));"/>
						<input type="reset" value="Reset" class="button large blue"  onClick="xajax_limpia();"/>
					</form>
				</div>                        
				<div>	
					<div id="view1" class="tabcontent">
					<script >
					xajax_lista_usuarios();
					</script>
					</div> 
				</div>	
			</div>
		</div>
		<br class="clearfix" />
	<div id="footer">Rapsinet 2012 - Seremi de Salud</div>
</body>
</html>