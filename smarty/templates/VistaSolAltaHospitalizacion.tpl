<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>



{$xajax_js}

</head>
<body>

<div id="wrapper">
	
    <form id="formSol" name="formSol">
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Alta Hospitalizacion Administrativa</h3>
			</div>
				
			<div class="col-left">
				<label>Rut: </label><input type="text" id="rut_pac" name="rut_pac"  value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_rutPac']}" {$activo}>
				<br class="clearfix" />
				<label>A Paterno: </label><input type="text" id="apaterno" name="apaterno" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_apaternoPac']}" {$activo}>
				<label>Fecha de Ingreso: </label><input type="text" id="fec_ing"  name="fec_ing" class="calendar" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_fechaIngreso']}" {$activo}>
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_pac" name="direccion_pac" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_direccionPac']}" {$activo}>
                                <label>Establecimiento:</label>
                                   <select name="establecimiento_pac" id="establecimiento_pac"  {$activo}>
                                            <option value ="0">Seleccione Establecimiento</option>
                                        <script>
                                            xajax_llena_establecimientos_pac('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_idEstablecimiento']}');
                                        </script>  
                                   </select>
			</div>
			<div class="col-right">
				<label>Nombre: </label><input type="text" id="nombre_pac" name="nombre_pac" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_nombrePac']}" {$activo}>
				<br class="clearfix" />
				<label>A Materno: </label><input type="text" id="amaterno" name="amaterno" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_amaternoPac']}" {$activo}> 
				<br class="clearfix" />
				<label> Fecha de Egreso: </label><input type="text" id="fec_egre" name="fec_egre" class="calendar" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_fechaEgreso']}" {$activo}>
				<br class="clearfix" />
				<label>Fono: </label><input type="text" id="fono" name="fono" value="{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_fonoPac']}" {$activo}>
			</div>
			<br class="clearfix" />
			<br>
			
			<label> Diagnostico: </label><textarea cols="25" rows="2" id="diagnostico" name="diagnostico" {$activo}>{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_diagnostico']} </textarea><br>
			<label> Evoluci&oacute;n Clinica: </label><textarea cols="25" rows="2" id="evolucion_clinica" name="evolucion_clinica" {$activo}>{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_evolucionClinica']}</textarea><br>
                        <label> Tratamiento: </label><textarea cols="25" rows="2" id="tratamiento" name="tratamiento" {$activo}>{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_tratamiento']}</textarea>
            <br><br>
           
			<b>Referencia de Alta</b>
			<br><br>
			<div class="col-left">
                        <label> Centro de Salud: </label>    
				 <select name="centro_salud" id="centro_salud"  {$activo}>
                                              <option value ="0">Seleccione Establecimiento</option>
                                        <script>
                                            xajax_llena_establecimientos_centro('{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_idCentroSalud']}');
                                        </script>  
                                   </select>
                                   </select>
				<br class="clearfix" />
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_2" name="direccion_2" value="{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_direccionRef']}" {$activo}>
			
			</div>
			<div class="col-right">
				<label> Profesional: </label> 
                                <select name="medicos" id="medicos" {$activo}>
                                          <option value ="0">Seleccione medico</option>
                                    <script>
                                        xajax_llena_medicos('{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_idProfesionalMedico']}');
                                    </script>  
                                </select>    
				<label> Fecha Referencia: </label><input type="text" id="fecha_ref" name="fecha_ref" class="calendar" value="{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_fechaRef']}" {$activo}>
			</div>
			<br class="clearfix" />
                        <br/>
                        <input type="hidden" name="observacion" id="observacion" value="" onChange="xajax_observa(this.value);" />
                        <b>Datos m&eacute;dico tratante:</b>
                        <br><br>
                        <label>M&eacute;dico: </label>
			<select name="medicos2" id="medicos2" {$activo}>
                            <option>Seleccione</option>
                                <option value ="0">Seleccione medico</option>
                                    <script>
                                        xajax_llena_medicos2('{$smarty.session.arrDetsolaa['DETALTHOSPADMIN_idMedicoTrat']}');
                                    </script>  
                                </select>     
                        </select>
        

            <br class="clearfix" />
            <br/>
            <b>Motivo de la Solicitud:</b>
            <br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" {$activo} value="A" {$OP1}><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
            <br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" {$activo} value="B" {$OP2}><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
            <br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" {$activo} value="C" {$OP3}><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
            <br/>
            <br class="clearfix" />
		
			<fieldset>
            <br class="clearfix" />   
				<b>Estado:</b><br>
				<input type="checkbox" name="rakim" value="dsp"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="juridica"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="seremi"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="contraloria"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</fieldset>	
			<br><br> 
                        
			<div class="right">	
			{if $ver==0}
				<input type="button" class="button medium blue" id="enviar" value="Visar Solicitud" onClick="xajax_visar_alta('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_id']}','{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_idEstadoSolicitud']}',xajax.getFormValues('formSol'));">
				<input type="button" id="limpia" class="button medium red" value="Rechazar" onclick="xajax_rechaza();" /> </span> 
			{/if}
			</div>
			<br>
            <br class="clearfix" />
            {if $ver==1}
              <iframe src="adjuntos_solalta.php" width="500" height="100" frameborder="0">
              </iframe>
              <br class="clearfix" />
              Agregar Comentario:
              <input type="text" name="comenta" id="comenta" /> <input type="button" name="cometario" id="comentario" value="Agregar Comentario" 
              onclick="xajax_comentario('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_id']}',comenta.value);"/>
              <br><br>
                           <ul class="tabs" persist="true">
                                    <li><a href="#" rel="view1">Comentarios</a></li>
                                    <li><a href="#" rel="view2">Eventos</a></li>
                                    <li><a href="#" rel="view3">Firmas</a></li>
                                    <li><a href="#" rel="view4">Adjuntos</a></li>   
                           </ul>
      
                    <div class="tabcontents">
                        <div id="view1" class="tabcontent">
                                <script>
                                    xajax_lista_comentarios('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_id']}');
                                </script> 
                                 <div id="detalle_comentarios">
                                </div>
                           
                        </div>
                        <div id="view2" class="tabcontent">
                                <script>
                                    xajax_lista_eventos('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_id']}','{$smarty.session.arrDetsolaa['tipoSolicitud']}');
                                </script>
                                <div id="detalle_eventos">
                                </div>
                              
                        </div>
                        <div id="view3" class="tabcontent">
                        <!--   
                                Firmas
                        -->    
                        </div>
                        <div id="view4" class="tabcontent">
                         
                                Grilla Adjuntos
                                <script>
                                    xajax_lista_adjuntos('{$smarty.session.arrDetsolaa['CABALTAHOSPADMIN_id']}');
                                </script> 
                                <div id="detalle1">
                                </div>
                       
                        
                        </div>
                    </div>
        {/if}
            
            
	</div>
	</div>
</form>	
	<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet </div>
</html>