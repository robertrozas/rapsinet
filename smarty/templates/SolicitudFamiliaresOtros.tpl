<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->

{include file="menu_principal.tpl"}   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<div id="InformacionUsuario">
                    <div class="descripcionUsuario">
                        <b>Bienvenido/a<br></b>
                        {$smarty.session.USUA_nombres} {$smarty.session.USUA_apellidos}
                        <br>
                        Informatica
                        <br>
                       
                        {$smarty.now|date_format:"%A, %B %e, %Y"|capitalize}
                        
                    </div>      
      </div>
		<br class="clearfix" />
	</div>   
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Solicitud de Resoluci&oacute;n Administrativa para ser Dictada por Seremi de Salud:</h3>
			</div>
			

                <label><b>Yo:</b></label>
				<br>
				
				<div class="col-tri">
					<label>Apellido Paterno: </label><input type="text" id="ap_pat" placeholder="Apellido Paterno...">
					<br class="clearfix" />
					<label>Carnet Identidad: </label><input type="text" id="rut" placeholder="Rut.....">
					<br class="clearfix" />
					<label>Email: </label><input type="text" id="rut" placeholder="Correo Electr&oacute;nico...">
					<br class="clearfix" />
				</div>
				<div class="col-tri">
					<label>Apellido Materno: </label><input type="text" id="ap_mat" placeholder="Apellido Materno...">
					<br class="clearfix" />
					<label>Edad: </label><input type="text" id="edad" placeholder="Edad....">
					<br class="clearfix" />
                </div>
				<div class="col-tri">
					<label>Nombres:	</label><input type="text" id="nombres" placeholder="Nombres">
					<br class="clearfix" />
					<label>Sexo: </label><input type="text" id="sexo" placeholder="Sexo...">
					<br class="clearfix" />
                </div>
				<br class="clearfix" />
				<br><br>
				
                <b>Solicito a la Autoridad Sanitaria representada por el Secretario Regional Ministerial de Salud de la Regi&oacute;n de Valpara&iacute;so, dicte Resoluci&oacute;n Administrativa que disponga la evaluaci&oacute;n, tratamiento y eventualmente internaci&oacute;n en contra de su voluntad, para Don(&ntilde;a):</b>
				<br><br><br>
				
				<div class="col-tri">
					<label>Apellido Paterno: </label><input type="text" id="ap_pat" placeholder="Apellido Paterno...">
					<br class="clearfix" />
					<label>Carnet Identidad: </label><input type="text" id="rut" placeholder="Rut...">
					<br class="clearfix" />
					<label>Calle: </label><input type="text" id="calle" placeholder="Calle...">
					<br class="clearfix" />
					<label>Sector: </label><input type="text" id="calle" placeholder="Sector...">
					<br class="clearfix" />
					<label>Regi&oacute;n: </label><input type="text" id="calle" placeholder="Regi&oacute;n...">
					<br class="clearfix" />
				</div>
				<div class="col-tri">
					<label>Apellido Materno: </label><input type="text" id="ap_mat" placeholder="Apellido Materno...">
					<br class="clearfix" />
					<label>Edad: </label><input type="text" id="edad" placeholder="Edad....">
					<br class="clearfix" />
					<label>N&deg;: </label><input type="text" id="edad" placeholder="Número....">
					<br class="clearfix" />
					<label>Comuna: </label><input type="text" id="edad" placeholder="Comuna....">
					<br class="clearfix" />
				</div>
				<div class="col-tri">
					<label>Nombres:	</label><input type="text" id="nombres" placeholder="Nombres...">
					<br class="clearfix" />
					<label>Sexo: </label><input type="text" id="sexo" placeholder="Sexo...">
					<br class="clearfix" />
					<label>N&deg; Depto: </label><input type="text" id="sexo" placeholder="Depto...">
					<br class="clearfix" />
					<label>Ciudad: </label><input type="text" id="sexo" placeholder="Ciudad...">
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
				
				<br><br>
                <b>Con quien me vincula una relaci&oacute;n de:</b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="edad" placeholder="Relaci&oacute;n....">	</span>
				<br/>
				<br class="clearfix" />
				<fieldset>
					<b>Estado:</b><br>
					<input type="checkbox" name="rakim" value="dsp"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="rakim" value="juridica"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="rakim" value="seremi"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="rakim" value="contraloria"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</fieldset>	
				<br><br>
				<div class="right">
					<input type="button" class="button medium green" id="enviar" value="Enviar">
				</div>
			<br class="clearfix" />
		</div>
		
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet &copy; 2012 - Seremi de Salud</div>
</body>
</html>