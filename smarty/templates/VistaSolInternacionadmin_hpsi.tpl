<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/tabla.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>

<script type="text/javascript">
    
    function popup(url,ancho,alto) {
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Formulario", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top=80");
    }
    
</script>

{$xajax_js}

</head>

<body >

<div id="wrapper">
 

	<div id="page">
	<div id="content">
			<div class="box">
				<h3>Formulario de Internaci&oacute;n Administrativa</h3>
			</div>
 <form id="formId" name="formId" onload="xajax_llena_regiones();">		
    
    <script>
            xajax_llena_regiones();
    </script>

	  <div id="columnaIzquierda"> 
            <label>Fecha Evaluaci&oacute;n: </label><input type="text" id="fecha_evaluacion" name="fecha_evaluacion" class="calendar" value="{$smarty.session.arrDetsolIA['CABINTADMIN_fechaEvaluacion']}" {$activo}>
                       
                <br class="clearfix" />
          <label>Establecimiento Solicitud: </label>  
                <select name="establecimiento_sol" id="establecimiento_sol" {$activo}>
                    <option value ="0">Lista Establecimientos
                        <script>
                            xajax_llena_establecimientos('{$smarty.session.arrDetsolIA['CABINTADMIN_idEstablecimientoSol']}');
                        </script> 
                    </option>
                </select>      

                        <br><br>
			<div class="col-left">
				<label>Diagnostico: </label><textarea  name="diagnostico" id="diagnostico" cols="48" rows="3" {$activo}>{$smarty.session.arrDetsolIA['DETINTADMIN_diagnostico']}</textarea>
			</div>
                        <br class="clearfix" />
                        <div class="col-left">
                                    <label>Otro Antenecente: </label><textarea name="otro_antecedente" id="otro_antecedente" cols="48" rows="3" {$activo} >{$smarty.session.arrDetsolIA['DETINTADMIN_otroAntenedente']}</textarea>
			</div>
                       
                        <br class="clearfix" />
                        <div class="col-left">
                                    <label>Ultimo tratamiento: </label><textarea name="ultimo_tratamiento" id="ultimo_tratamiento" cols="48" rows="3" {$activo}>{$smarty.session.arrDetsolIA['DETINTADMIN_ultimoTratamiento']}</textarea>
			</div>
                        
                        
                        <br class="clearfix" />
                        <label><b>Lugar de Internacion:</b></label>
                            <select name="establecimiento_int" id="establecimiento_int" {$activo}>
                                <option value ="0">Lista Establecimientos
                                    <script>
                                        xajax_llena_establecimientos2('{$smarty.session.arrDetsolIA['DETINTADMIN_idEstablecimientoInt']}');
                                    </script> 
                                </option>
                            </select>     
            
			<br><br>
                        <b>Identificaci&oacute;n del medico solicitante:</b>
			<br><br>
            
			<label>Nombre: </label><input type="text" id="nombre_medico" name="nombre_medico" value="{$smarty.session.arrDetsolIA['DETINTADMIN_nombreMedico']}" {$activo}>
			<br class="clearfix" />
                        <label>C&eacute;dula de identidad: </label><input type="text" name="rut_medico" id="rut_medico" value="{$smarty.session.arrDetsolIA['DETINTADMIN_rutMedico']}" {$activo}> 
			<br class="clearfix" />
			<label>Correo: </label><input type="text" name="correo_medico" id="correo_medico" value="{$smarty.session.arrDetsolIA['DETINTADMIN_correoMedico']}" {$activo}>
			<br class="clearfix" />
			<br><br>
                        
      {if $smarty.session.arrDetsolIA['CABINTADMIN_idEstadoSolicitud']|in_array:$estados}                  
                                    <label>Observaci&oacute;n:</label>
                                    <input type="text" id="observacion" name="observacion" value="{$smarty.session.arrDetsolIA['CABINTADMIN_observaciones']}" {$activo}>  
      {/if}
                       
	</div>
        <div id="columnaDerecha"> 
         <div id="columnaDerechaIzqTexto"> 
             Nombres:
              <br><br>   
              A.Materno:
              <br><br>
              Rut:
              <br><br><br>   
              Calle:
              <br><br>
              Regiones:
              <br><br>
              Provincia:
              <br><br>
              Comuna:
              
         </div>
              
         <div id="columnaDerechaDerDatos"> 
              <input type="text" id="nombres_sol" name="nombres_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_nombresSol']}" {$activo}>
              <br><br>   
              <input type="text" id="amaterno_sol" name="amaterno_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_aMaternoSol']}" {$activo}> 
              <br><br>
              <input type="text" id="rut_sol" name="rut_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_rutSol']}" {$activo}> 
              <br><br>  
              <input type="text" id="calle_sol" name="calle_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_calleSol']}" {$activo}>  
              <br><br>
              
               <select name="region_sol" id="region_sol" onchange="xajax_llena_provincias(this.value);" {$activo}>
                <option value ="0">Lista regiones</option>
                    {section name=num  loop=$smarty.session.arrRegiones}
                <option value="{$smarty.session.arrRegiones[num].id_region}">{$smarty.session.arrRegiones[num].nom_region}</option>
                    {/section}
                
              </select>  
              
              <br><br>
              <select name="ciudad_sol" id="ciudad_sol" onchange="xajax_llena_comunas(this.value);" {$activo}>
                <option value ="0">Lista Ciudades
                 <script>
                         xajax_llena_provincias('{$smarty.session.arrDetsolIA['DETINTADMIN_idRegionSol']}');
                 </script> 
                </option>
              </select>  
              <br><br>
              <select name="comuna_sol" id="comuna_sol" {$activo}>
                <option value ="0">Lista Comunas
                 <script>
                         xajax_llena_comunas('{$smarty.session.arrDetsolIA['DETINTADMIN_idCiudadSol']}');
                 </script> 
                </option>
              </select> 
         </div>     
 
         <div id="columnaDerechaDer"> 
           <div id="columnaIzquierdaIzqTexto">   
            A.Paterno:
            <br><br>
            Edad:
            <br><br>
            Sexo:
            <br><br>
            N casa:
            <br><br>
            Sector:
            <br><br>
            Celular:
            <br><br>
            T.Fijo:
            <br><br> 
            Correo:
          </div>  
           <div id="columnaIzquierdaDerDatos">    
            <input type="text" id="apaterno_sol" name="apaterno_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_aPaternoSol']}" {$activo}>
            <br><br>
            <input type="text" id="edad_sol" name="edad_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_edadSol']}" {$activo}>
            <br><br>
            <select name="sexo_sol" id="sexo_sol"  {$activo}>
                    <option value ="0">Seleccione Sexo</option>
                    <script>
                        xajax_llena_sexo('{$smarty.session.arrDetsolIA['DETINTADMIN_sexoSol']}');
                    </script>  
            </select>
                    
            <br><br><br>
            <input type="text" id="numerocasa_sol" name="numerocasa_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_nCasaSol']}" {$activo}> 
            <br><br> 
            <input type="text" id="sector_sol" name="sector_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_sectorSol']}" {$activo}>
            <br><br>
            <input  ut type="text" id="celular_sol" name="celular_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_celularSol']}" {$activo}> 
            <br><br>  
            <input type="text" id="telefono_sol" name="telefono_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_telefonoSol']}" {$activo}> 
            <br><br>
            <input type="text" id="correo_sol" name="correo_sol" value="{$smarty.session.arrDetsolIA['DETINTADMIN_correoSol']}" {$activo}> 
           </div> 
            
         </div> 
          <div id="columnaIzquierdaIzqTexto"> 
           Nombres: 
           <br><br>
           A.Materno:
           <br><br>
           Edad:
           <br><br>
           Calle:
           <br><br>
           Sector:
           <br><br>
           Vinculacion:
          </div>  
              
          <div id="columnaIzquierdaDerDatos">
           <input type="text" id="nombres_pac" name="nombres_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_nombresPac']}" {$activo}>
           <br><br> 
           <input type="text" id="amaterno_pac" name="amaterno_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_aMaternoPac']}" {$activo}> 
           <br><br> 
           <input type="text" id="edad_pac" name="edad_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_edadPac']}" {$activo}>
           <br><br> 
           <input type="text" id="calle_pac" name="calle_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_callePac']}" {$activo}>
           <br><br> 
           <input type="text" id="sector_pac" name="sector_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_sectorPac']}" {$activo}>
           <br><br> 
           <select name="vinculacion_pac" id="vinculacion_pac" {$activo}>
                <option value ="NO">Seleccione Vinculacion</option>
                <script>
                    xajax_llena_vinculacion('{$smarty.session.arrDetsolIA['DETINTADMIN_vinculacion']}');
                                 
                </script>  
           </select>
          </div>    
              
          <div id="columnaIzquierdaIzqTexto">
           A.Paterno:   
           <br><br>
           Rut:   
           <br><br>
           Sexo:
           <br><br>
           N casa:
           <br><br>
           Region:
           <br><br>
           Provincia:
           <br><br>
           Comuna:
          </div> 
          <div id="columnaDerechaDerDatos">
              <input type="text" id="apaterno_pac" name="apaterno_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_aMaternoPac']}" {$activo}>
              <br><br>
              <input type="text" id="rut_pac" name="rut_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_rutPac']}" {$activo}> 
              <br><br>  
              <select name="sexo_pac" id="sexo_pac"  {$activo}>
                    <option value ="0">Seleccione Sexo</option>
                    <script>
                        xajax_llena_sexo2('{$smarty.session.arrDetsolIA['DETINTADMIN_sexoPac']}');
                    </script>  
            </select>
              <br><br>  
              <input type="text" id="numerocasa_pac" name="numerocasa_pac" value="{$smarty.session.arrDetsolIA['DETINTADMIN_nCasaPac']}" {$activo}> 
              <br><br>  
         
              <select name="region_pac" id="region_pac" onchange="xajax_llena_provincias2(this.value);" {$activo}>
                <option value ="0">Lista regiones</option>
                    {section name=num  loop=$smarty.session.arrRegiones}
                <option value="{$smarty.session.arrRegiones[num].id_region}">{$smarty.session.arrRegiones[num].nom_region}</option>
                    {/section}
              </select>             
              <br><br>
              <select name="ciudad_pac" id="ciudad_pac" onchange="xajax_llena_comunas2(this.value);" {$activo}>
                <option value ="0">Lista Ciudades
                 <script>
                         xajax_llena_provincias2('{$smarty.session.arrDetsolIA['DETINTADMIN_idRegionPac']}');
                 </script> 
                </option>
              </select> 
              <br><br>
              <select name="comuna_pac" id="comuna_pac" {$activo}>
                <option value ="0">Lista Comunas
                 <script>
                         xajax_llena_comunas2('{$smarty.session.arrDetsolIA['DETINTADMIN_idCiudadPac']}');
                 </script> 
                </option>
              </select>                
          </div>    
        </div>    
        <div id="pieDePagina">  
			<br><br>
			<div class="left">
                        {if $ver == 1}
                            ¿El paciente se esta internando en el hospital?	
                            <br>
                             <input type="radio" name="opcion" id="opcion" value="1" checked> SI<br>
                             <input type="radio" name="opcion" id="opcion" value="0"> NO<br>
                             <input type="button" class="button medium blue" id="enviar" value="Confirmar" onClick="xajax_confirmar_internacion('{$smarty.session.arrDetsolIA['CABINTADMIN_id']}',xajax.getFormValues('formId'));">
                        {/if}   
			</div>
            <br class="clearfix" />
	 </div>
         </div>   
	</form>	
		<br class="clearfix" />
	</div>
	<div id="footer">Rapsinet </div>
	</html>
        
        
        
        
