<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<html>

<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1"/>
<meta name="title" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<meta name="description" content="Sistema de Administraci&oacute;n de Bodega - SEREMI Salud Valpara&iacute;so" />
<title>Rapsinet 1.0</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dropotron-1.0.js"></script>
<!--<script type="text/javascript" src="jquery.slidertron-1.1.js"></script> -->
{$xajax_js}

{include file="menu_principal.tpl"}   
	<div id="header">
		<div class="left inter"></div>
		<div class="left head-title">
			<h1>Solicitudes</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt pellentesque ante, ut fermentum tellus mollis posuere.</p>
		</div>
		<br class="clearfix" />
	</div>
    <form id="formSol" name="formSol">
	<div id="page">
		<div id="content">
			<div class="box">
				<h3>Formulario de Alta Hospitalizacion Administrativa</h3>
			</div>
				
			<div class="col-left">
				<label>Rut: </label><input type="text" id="rut_pac" name="rut_pac" placeholder="Ingrese Rut">
				<br class="clearfix" />
				<label>A Paterno: </label><input type="text" id="apaterno" name="apaterno" placeholder="Apellido Paterno">
				<label> Fecha de Ingreso: </label><input type="text" id="fec_ing"  name="fec_ing" class="calendar" placeholder="Ej: 26/11/2012">
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_pac" name="direccion_pac" placeholder="Ingrese la direcci&oacute;n">

                                <label>Establecimiento:</label>
                                   <select name="establecimiento" id="establecimiento"  tabindex="6">
                                            <option>Seleccione</option>
                                        <script>
                                            xajax_llena_establecimientos();
                                        </script>  
                                   </select>
			</div>
			<div class="col-right">
				<label>Nombre: </label><input type="text" id="nombre_pac" name="nombre_pac" placeholder="Ingrese Nombre">
				<br class="clearfix" />
				<label>A Materno: </label><input type="text" id="amaterno" name="amaterno" placeholder="Apellido Materno"> 
				<br class="clearfix" />
				<label> Fecha de Egreso: </label><input type="text" id="fec_egre" name="fec_egre" class="calendar" placeholder="Ej: 26/11/2012">
				<br class="clearfix" />
				<label>Sexo: </label><select id="sexo" name="sexo">
									<option value="">Seleccione</option>
									<option value="0">Masculino</option>
									<option value="1">Femenino</option>
								</select>
				<label>Fono: </label><input type="text" id="fono" name="fono" placeholder="Ingrese el Fono">
			</div>
			<br class="clearfix" />
			<br>
			
			<label> Diagnostico: </label><textarea cols="25" rows="2" id="diagnostico" name="diagnostico"></textarea><br>
			<label> Evoluci&oacute;n Clinica: </label><textarea cols="25" rows="2" id="evolucion_clinica" name="evolucion_clinica"></textarea><br>
                        <label> Tratamiento: </label><textarea cols="25" rows="2" id="tratamiento" name="tratamiento"></textarea>
            <br><br>
           
			<b>Referencia de Alta</b>
			<br><br>
			<div class="col-left">
                        <label> Centro de Salud: </label>    
				 <select name="establecimiento2" id="establecimiento2"  tabindex="6">
                                            <option>Seleccione</option>
                                        <script>
                                            xajax_llena_establecimientos2();
                                        </script>  
                                   </select>
				<br class="clearfix" />
				<label>Direcci&oacute;n: </label><input type="text" id="direccion_2" name="direccion_2" placeholder="Ingrese la direcci&oacute;n">
			
			</div>
			<div class="col-right">
				<label> Profesional: </label> 
                                <select name="medicos" id="medicos" tabindex="6" >
                                        <option>Seleccione</option>
                                    <script>
                                        xajax_llena_medicos();
                                    </script>  
                                </select>    
				<label> Fecha Referencia: </label><input type="text" id="fecha_ref" name="fecha_ref" class="calendar" placeholder="Seleccione la fecha">
			</div>
			<br class="clearfix" />
                        <br/>
                        
                        <b>Datos m&eacute;dico tratante:</b>
                        <br><br>
                        <label>M&eacute;dico: </label>
			<select name="medicos2" id="medicos2" tabindex="6" >
                            <option>Seleccione</option>
                                <script>
                                    xajax_llena_medicos2();
                                </script>  
                        </select>
            <br class="clearfix" />
			<br/>
			<b>Motivo de la Solicitud:</b>
			<br/><br/>
                    <input type="checkbox" name="opcion_a" id="opcion_a" value="A"><b> A )</b> Necesidad de efectuar un diagnostico o evaluaci&oacute;n cl&iacute;nica que no puede realizarse en forma ambulatoria.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_b" id="opcion_b" value="B"><b> B )</b> Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.
			<br class="clearfix" />  
                        <input type="checkbox" name="opcion_c" id="opcion_c" value="C"><b> C )</b> Que el estado o condici&oacute;n ps&iacute;quica o conductual de la persona representa un riesgo de da&ntilde;o f&iacute;sico, ps&iacute;quico o psicosocial inminente, para s&iacute; mismo o para terceros.
			<br/>
                    <br class="clearfix" />
		
			<fieldset>
                        <br class="clearfix" />   
				<b>Estado:</b><br>
				<input type="checkbox" name="rakim" value="dsp"> Visado por DSP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="juridica"> Visado por Jur&iacute;dica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="seremi"> Visado por Seremi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="rakim" value="contraloria"> Visado por Contraloria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</fieldset>	
			<br><br> 
                        
			<div class="right">
				<input type="button" class="button medium blue" id="enviar" value="Enviar Solicitud" onClick="xajax_ingresarSolicitud(xajax.getFormValues('formSol'));" >
            </div>
			<br>
            <br class="clearfix" />
		</div>
	</div>
</form>	
	<div id="footer">Rapsinet � 2012 - Seremi de Salud</div>
</body>
</html>