<?php

include('MySQL.php');
class DAODepartamentoUSMH {

   

      function lista_solinv($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudinv('$id_estado','$id_sol','$fecha',$departamento);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => $resultado['USUARIO'],'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['CABSOLINV_FECHAINTERNACION'],'solicitud'=>$resultado['TIP_DESCRIPCION'],
                'id_cabecera'=>$resultado['CABSOLINV_ID'],'estado'=>$resultado['ESTA_DESCRIPCION']
            ));
         }   
         
         $conn->close();
         return $retorno;
      }

      function lista_solalta($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudaa('$id_estado','$id_sol','$fecha',$departamento);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => $resultado['nombre_completo'],'usuario_id'=>$resultado['USUA_id'],
                'fecha'=>$resultado['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>$resultado['TIP_descripcion'],
                'id_cabecera'=>$resultado['CABALTAHOSPADMIN_id'],'estado'=>$resultado['ESTA_descripcion']
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
  
      function visar_solET($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_splistasoleva('".$idCabeceraSolicitud."');";
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;   
      } 

     function lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $est_id = $_SESSION['EST_id'];
        $consulta = "call rapsinet_splistasolevas('".$idEstado."','".$idSolicitud."','".$fecha."','".$idDepartamento."','".$est_id."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['cab_solevatrat'],
                'fechaEvaluacion' => $row['fecha_sol'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'usuario' => $row['USUARIO'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    
     function lista_completa_primera_llamada($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolevas('".$idEstado."','".$idSolicitud."','".$fecha."','".$idDepartamento."','".$est_id."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['cab_solevatrat'],
                'fechaEvaluacion' => $row['fecha_sol'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'usuario' => $row['USUARIO'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }

    
     function lista_completa_segunda_llamada($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolevas('".$idEstado."','".$idSolicitud."','".$fecha."','".$idDepartamento."','".$est_id."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['cab_solevatrat'],
                'fechaEvaluacion' => $row['fecha_sol'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'usuario' => $row['USUARIO'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
     
     function rakim_solinv($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solinv($id_sol,'$num');"; 
         
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }


    function rakim_solint($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solint($id_sol,'$num');"; 
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }
     
      function rakim_solalta($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solalta($id_sol,'$num');"; 
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }
    

    

}
?>
