<?php

include('MySQL.php');
class DAODepartamentoSaludPublica {

    function visar_soleva($id_cab)
    {
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasoleva($id_cab);"; 
         $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;
    }

       function lista_solevas($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $est_id = $_SESSION['EST_id'];
         
         $consulta = "call rapsinet_splistasolevas('$id_estado','$id_sol','$fecha',$departamento,$est_id);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => $resultado['USUARIO'],'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['FECHA_SOL'],'solicitud'=>$resultado['TIPO'],
                'id_cabecera'=>$resultado['CAB_SOLEVATRAT'],'estado'=>$resultado['ESTADO']
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      
    function confirmar_alta($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spregistraraltaadmin("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }


    function rechazo_soladmin($cabecera,$id_estado,$observacion)
    {
            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_sprechazarsolicitudinternacionadmin($cabecera,'$observacion',$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user,$id_estado);";
            $mensaje = "Solicitud Rechazada";
            
             try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

            return $mensaje; 
    }
    
    
    function rechazo_solalta($cabecera,$id_estado,$observacion)
    {
            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_sprechazarsolicitudaltaadmin($cabecera,'$observacion',$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user,$id_estado);";
            $mensaje = "Solicitud Rechazada";
            
             try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

            return $mensaje; 
    }
    
      
    function confirmar_internacion($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spregistrarinternacion("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }


    function agrega_comentario($cabid,$comentario)
    {
        $consulta = "call rapsinet_spcomentarios_sia($cabid,'$comentario');";
         $conn = new MySQL;
             $conn->consulta($consulta);
             $conn->close();

    }
    
    function agrega_comentario_alta($cabid,$comentario)
    {
        $consulta = "call rapsinet_spcomentarios_alta($cabid,'$comentario');";
         $conn = new MySQL;
             $conn->consulta($consulta);
             $conn->close();

    }
    
     function agrega_comentario_inv($cabid,$comentario)
    {
        $consulta = "call rapsinet_spcomentarios_inv($cabid,'$comentario');";
         $conn = new MySQL;
             $conn->consulta($consulta);
             $conn->close();

    }
    
 
   function lista_comentario($cabid)
   {

     $consulta = "call rapsinet_splistacomentarios_sia($cabid);";
     $conn = new MySQL();

      $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('fecha' => $resultado['COMSIA_FECHA'],
                'comentario' => $resultado['ARCHAD_id']

            ));
         }   
         


   }

    function visado_solinv($idCabecera,$idEstado)
    {

            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "CALL rapsinet_spaprovarsolicitudnovoluntaria($idCabecera,$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user,$idEstado)";

            
             $conn = new MySQL;
             $conn->consulta($consulta);
             $conn->close();
       
    }
    
    function aprobar($idCabeceraSolicitud,$estadoDocumento)
    {
        $consulta = 'CALL rapsinet_spaprobarsolicitudinternacionadmin("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $estadoDocumento.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        
        $conn->close();

        //return $mensaje;
        
    }
    
        
    function aprobar_alta($idCabeceraSolicitud,$estadoDocumento)
    {
        $consulta = 'CALL rapsinet_spaprovarsolicitudaltaadministrativa("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $estadoDocumento.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }


    function visar_solinv($id_cab)
    {
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasolinv($id_cab);"; 
         $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;
    }
    
     function visar_solinternacionadmin($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudinternacionadminporvisar('".$idCabeceraSolicitud."');";
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;   
      }
      
       function visar_solaa($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudaltaporvisar('".$idCabeceraSolicitud."');";
                          
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos
        
        $conn->dispose($resultado);
        $conn->close();
        return $row;   
      }
      
      
      
      function eventos_solicitud($idCabecera,$tipoSolicitud)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_spmostrarlogsolicitudeseventos($idCabecera,$tipoSolicitud);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('codigo_log' => $resultado['LOGSOL_id'],
                'codigo' => $resultado['CABSOL_id'],
                'fecha' => $resultado['LOGSOL_fecha'],
                'nombres' => $resultado['USUA_nombres'],
                'apellidos' => $resultado['USUA_apellidos'],
                'departamento' => $resultado['DEP_descripcion'],
                'tipo' => $resultado['TIP_descripcion'],
                'estado' => $resultado['ESTA_descripcion'],
                'observaciones' => $resultado['LOGSOL_observaciones']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      
      function comentarios_solicitud($idCabecera)
      {
         $conn = new MySQL();
         $comentarios = array();
         $consulta = "call rapsinet_spmostrarcomentarios($idCabecera);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($comentarios,array('codigo' => $resultado['COMSIA_id'],
                'idcab' => $resultado['COMSIA_idCabeceraSol'],
                'comentario' => $resultado['COMSIA_comentario'],
                'fecha' => $resultado['COMSIA_fecha']
            ));
         }   
         
         $conn->close();
         return $comentarios;
      }
      
      function comentarios_solicitud_alta($idCabecera)
      {
         $conn = new MySQL();
         $comentarios = array();
         $consulta = "call rapsinet_spmostrarcomentarios_alta($idCabecera);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($comentarios,array('codigo' => $resultado['COMALTA_id'],
                'idcab' => $resultado['COMALTA_idCabeceraSol'],
                'comentario' => $resultado['COMALTA_comentario'],
                'fecha' => $resultado['COMALTA_fecha']
            ));
         }   
         
         $conn->close();
         return $comentarios;
      }
      
      function comentarios_solicitud_inv($idCabecera)
      {
         $conn = new MySQL();
         $comentarios = array();
         $consulta = "call rapsinet_spmostrarcomentarios_inv($idCabecera);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($comentarios,array('codigo' => $resultado['COMINV_id'],
                'idcab' => $resultado['COMINV_idCabeceraSol'],
                'comentario' => $resultado['COMINV_comentario'],
                'fecha' => $resultado['COMINV_fecha']
            ));
         }   
         
         $conn->close();
         return $comentarios;
      }



      function ingresa_archivo($id_cabecera,$nombre,$tipo,$extension,$archivo)
      {
         $conn = new MySQL();
         
         $consulta = "call rapsinet_spingresa_adjuntos_soladmin($id_cabecera,'$nombre','$tipo','$extension','$archivo');"; 
         $conn->consulta($consulta);
         $conn->close();
        
      }
      
      function ingresa_archivo_alta($id_cabecera,$nombre,$tipo,$extension,$archivo)
      {
         $conn = new MySQL();
         
         $consulta = "call rapsinet_spingresa_adjuntos_solalta($id_cabecera,'$nombre','$tipo','$extension','$archivo');"; 
         $conn->consulta($consulta);
         $conn->close();
        
      }

      function lista_archivos($clave)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistaadjuntos($clave);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('nombre' => $resultado['ARCHAD_nombre'],
                'codigo' => $resultado['ARCHAD_id']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }

      function lista_archivos_solint($clave)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_spadjuntossolint($clave);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('nombre' => $resultado['ARCHAD_nombre'],
                'codigo' => $resultado['ARCHAD_id']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      
      
      function lista_archivos_solalta($clave)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_spadjuntossolalta($clave);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('nombre' => $resultado['ARCHAD_nombre'],
                'codigo' => $resultado['ARCHAD_id']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }

      function lista_solinv($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudinv('$id_estado','$id_sol','$fecha',$departamento);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['CABSOLINV_FECHAINTERNACION'],'solicitud'=>utf8_encode($resultado['TIP_DESCRIPCION']),
                'id_cabecera'=>$resultado['CABSOLINV_ID'],'estado'=>utf8_encode($resultado['ESTA_DESCRIPCION'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      
      function lista_solaa($id_sol,$id_estado,$fecha,$departamento)
      {
         $retornos = array();
         $conn = new MySQL();
         
         $consulta = "call rapsinet_splistasolicitudaa('$id_estado','$id_sol','$fecha',$departamento,'');"; 
 
         $query = $conn->consulta($consulta);
         
         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retornos,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['USUA_id'],
                'fecha'=>$resultado['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>utf8_encode($resultado['TIPO']),
                'id_cabecera'=>$resultado['CABALTAHOSPADMIN_id'],'estado'=>utf8_encode($resultado['ESTADO'])
            ));
           
         }   
         
      
         $conn->dispose($query);
         $conn->close();
         return $retornos;
      }
      
      
      function lista_solperalta($id_sol,$id_estado,$fecha,$departamento)
      {
         $retornos = array();
         $conn = new MySQL();
         
         $consulta = "call rapsinet_splistasolperalta('$id_estado','$id_sol','$fecha',$departamento);"; 
 
         $query = $conn->consulta($consulta);
         
         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retornos,array('paciente' => utf8_encode($resultado['PACIENTE']),'rut_paciente'=>$resultado['CABALTAHOSPADMIN_rutPac'],
                'fecha'=>$resultado['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>utf8_encode($resultado['TIPO']),'fono'=>utf8_encode($resultado['CABALTAHOSPADMIN_fonoPac']),'direccion'=>utf8_encode($resultado['CABALTAHOSPADMIN_direccionPac']),'centro_salud'=>utf8_encode($resultado['EST_nombre']),
                'id_cabecera'=>$resultado['CABALTAHOSPADMIN_id'],'estado'=>utf8_encode($resultado['ESTADO'])
            ));
           
         }   
         
      
         $conn->dispose($query);
         $conn->close();
         return $retornos;
      }
      

        function lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."','');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => utf8_encode($row['ESTA_descripcion']),
                'nombres' => utf8_encode($row['USUA_nombres']),
                'apellidos' => utf8_encode($row['USUA_apellidos']),
                'tiposolicitud' => utf8_encode($row['TIP_descripcion'])
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    
    
     function filtra_solicitudesinternacionadmin($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    
    function filtra_fecha2($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }

      
    function id_lista_historico_usuarios($idSolicitud, $fecha) {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlistahistoricousuarios('".$idSolicitud."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogSolicitudes, array('codigo' => $row['HIST_id'],
                'nombres' => $row['HIST_nombres'],
                'apellidos' => $row['HIST_apellidos'],
                'fecha' => $row['HIST_fecha'],
                'idaccion' => $row['HIST_idAccion'],
                'accion' => $row['ACC_descripcion'],
                'idsolicitud' => $row['HIST_idTipoSolicitud'],
                'tiposol' => $row['TIP_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
    }
    
    
   
    
    function llena_estados() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestados();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstados, array('codigo' => $row['ESTA_ID'],
                'descripcion' => $row['ESTA_DESCRIPCION']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 
    
    function llena_estados2() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_spmostrarestadosolintadmin_dsp();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstados, array('codigo' => $row['ESTA_id'],
                'descripcion' => $row['ESTA_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 
    
      function lista_historico_usuarios() {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlistahistoricousuarios();";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        $cadena = "";
        while ($row = $conn->fetch_assoc($resultado)) {
          array_push($arrLogSolicitudes, 
          array('codigo' => $row['HIST_id'],
                'nombres' => utf8_encode($row['HIST_nombres']),
                'apellidos' => utf8_encode($row['HIST_apellidos']),
                'fecha' => $row['HIST_fecha'],
                'idaccion' => $row['HIST_idAccion'],
                'accion' => utf8_encode($row['Accion']),
                'idsolicitud' => $row['HIST_idCabecera'],
                'tiposol' => utf8_encode($row['Tipo'])
            ));
           }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
       
    }
    
    function llena_tipo() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_listatiposolicitudes();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstados, array('codigo' => $row['TIP_id'],
                'descripcion' => utf8_encode($row['TIP_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 


        function lista_completa_log_solicitudes() {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlogsolicitudes;";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogSolicitudes, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => utf8_encode($row['USUA_nombres']),
                'apellidos' => utf8_encode($row['USUA_apellidos']),
                'departamento' => utf8_encode($row['DEP_descripcion']),
                'tipo' => utf8_encode($row['TIP_descripcion']),
                'estado' => utf8_encode($row['ESTA_descripcion']),
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
    }


    function detalle_solintadmin_id($sol_id)//detalle de una solicitud en particular 
    {
        
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_spverdetalle_solintadmin($sol_id);"; 
         $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;
    } 
    
    
    function lista_filtros($filtro,$opcion) //de acuerdo al tipo de filtro, el cual se ve segun la opcion
    {
        $consulta = "call rapsinet_spmostrarlogsoladmin_filtros('$filtro',$opcion);";
        $arrLogFiltrada = array();
        $conn = new MySQL();
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogFiltrada, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => $row['DEP_descripcion'],
                'tipo' => $row['TIP_descripcion'],
                'estado' => $row['ESTA_descripcion'],
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        
        return $arrLogFiltrada;
    }
    function lista_filtros_log($filtro,$tipo,$fecha) //de acuerdo al tipo de filtro, el cual se ve segun la opcion
    {
        $consulta = "call rapsinet_spfiltrologsolintadmin('".$filtro."','".$tipo."','".$fecha."');";
        $arrLogFiltrada = array();
        $conn = new MySQL();
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogFiltrada, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => $row['DEP_descripcion'],
                'tipo' => $row['TIP_descripcion'],
                'estado' => $row['ESTA_descripcion'],
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        
        return $arrLogFiltrada;
    }
    
    function filtro_historico_usuarios($idSolicitud, $fecha) {
        $arrHistoricoSolicitudes = array();
        $consulta = "call rapsinet_spfiltrohistorialusuarios('".$idSolicitud."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrHistoricoSolicitudes, array('codigo' => $row['HIST_id'],
                'nombres' => $row['HIST_nombres'],
                'apellidos' => $row['HIST_apellidos'],
                'fecha' => $row['HIST_fecha'],
                'idaccion' => $row['HIST_idAccion'],
                'accion' => $row['ACC_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrHistoricoSolicitudes;
    }

     function lista_log_fecha($opcion)//en opcion envio el metodo de ordenamiento fecha des o asc
    {
        $consulta = "call rapsinet_mostrarlogsolicitudes_orden($opcion);";
        $arrLogFiltrada = array();
        $conn = new MySQL();
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogFiltrada, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => $row['DEP_descripcion'],
                'tipo' => $row['TIP_descripcion'],
                'estado' => $row['ESTA_descripcion'],
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        
        return $arrLogFiltrada;
    }
    
    
    
    function resolucion_sol_int_admin($idCabeceraSolicitud)
    {
        $arrResolucionInternacionAdmin = array();
        $consulta = "call rapsinet_spresinternacionsolicitudadmin('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionInternacionAdmin, array('codigo' => $row['CABINTADMIN_id'],
                'nombrecompleto_sol' => $row['nombrecompleto_sol'],
                'rut_sol' => $row['DETINTADMIN_rutSol'],
                'vinculacion' => $row['DETINTADMIN_vinculacion'],
                'nombrecompleto_pac' => $row['nombrecompleto_pac'],
                'rut_pac' => $row['DETINTADMIN_rutPac'],
                'fecha_actual' => $row['fecha_actual'],
                'fecha_evaluacion' => $row['fecha_evaluacion'],
                'nombre_medico' => $row['DETINTADMIN_nombreMedico'],
                'establecimiento_sol' => $row['EST_nombre'],
                'nombrecompleto_ac' => $row['nombrecompleto_ac'],
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrResolucionInternacionAdmin;
    }
    
    function resolucion_sol_inv($idCabeceraSolicitud)
    {
        $arrResolucionINV = array();
        $consulta = "call rapsinet_spresinv('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionINV, array('codigo' => $row['CABSOLINV_id'],
                'nombre_medico' => $row['nombre_medico'],
                'nombre_paciente' => $row['nombre_paciente'],
                'rut_paciente' => $row['CABSOLINV_rutPaciente'],
                'fecha_internacion' => $row['CABSOLINV_fechaInternacion'],
                'fecha_actual' => $row['fecha_actual'],
                'opciones' => $row['DETSOLINV_opcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrResolucionINV;
    }
    
    
    

    function rechazo_solinv($id_cabecera,$id_estado,$observaciones)
    {
            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

        $consulta = "call rapsinet_sprechaza_solinv($id_cabecera,'$observaciones',$id_estado,'$nombres_user','$apellidos_user','$rut_user',$id_user,$id_depto);";
        $conn = new MySQL();

        $conn->consulta($consulta);
        $conn->close();


        

    }
    
   

    function actualiza_solinv($id_cabecera,$form)
    {
       //variables de session rescatadas
       $id_medico = $form['medicos'];
       $rut_paciente = $form['rut'];
       $nombres_pac = $form['nombres'];
       $apell_pac = $form['apellidos'];
       $dir_pac = $form['domicilio'];
       $nac_pac = $form['fec_nac'];
       $int_pac = $form['fec_int'];
       $sol_pac = $form['fech_desde'];
       $id_servicio = $form['servicios'];
       $id_establecimiento = $form['establecimiento'];

       $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];   
       
        $consulta = "call rapsinet_spmodsolinv($id_cabecera,$id_medico,'$rut_paciente','$nombres_pac','$apell_pac','$dir_pac','$nac_pac','$int_pac','$sol_pac',$id_servicio,$id_establecimiento,$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user)";

        $conn = new MySQL();
        $conn->consulta($consulta);
        $conn->close();

    }
    
    
    function actualiza_solaa($id_cabecera,$form)
    {
       //variables de session rescatadas
       $rut_pac  = $form['rut_pac'];
       $apaterno = $form['apaterno'];
       $fec_ing = $form['fec_ing'];
       $direccion_pac = $form['direccion_pac'];
       $establecimiento_pac = $form['establecimiento_pac'];
       $nombre_pac = $form['nombre_pac'];
       $amaterno = $form['amaterno']; 
       $fec_egre = $form['fec_egre'];
       $fono = $form['fono'];
       $diagnostico = $form['diagnostico'];
       $evolucion_clinica = $form['evolucion_clinica'];
       $tratamiento = $form['tratamiento'];
       $centro_salud = $form['centro_salud'];
       $direccion_2 = $form['direccion_2'];
       $medicos = $form['medicos'];
       $fecha_ref = $form['fecha_ref'];
       $medicos2 = $form['medicos2'];
       
       $id_depto=$_SESSION['DEPART_id'];
       $nombres_user=$_SESSION['USUA_nombres'];
       $apellidos_user=$_SESSION['USUA_apellidos'];
       $rut_user=$_SESSION['USUA_rut'];
       $id_user=$_SESSION['USUA_idUsuario'];   
       
       $consulta = "call rapsinet_spmodsolaltaadmin($id_cabecera,$establecimiento_pac,'$fec_ing','$fec_egre','$nombre_pac',
       '$apaterno','$amaterno','$rut_pac','$direccion_pac',$fono,'$diagnostico','$evolucion_clinica','$tratamiento',$centro_salud,
       '$direccion_2',$medicos,'$fecha_ref',$medicos2,$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user)";
        

        $conn = new MySQL();
        $conn->consulta($consulta);
        $conn->close();

    }


     function llena_establecimientos()
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    }  
    
   function regiones()
    {
       $conn = new MySQL();
       $arrRegiones = array();
       $consulta = "call rapsinet_spregiones();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrRegiones, array('id_region' => $row['REG_ID'],
                'nom_region' => utf8_encode($row['REGION'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrRegiones;

    }
    
    function servicios()
    {
       $conn = new MySQL();
       $arrServicios = array();
       $consulta = "call rapsinet_splistaservicios();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrServicios, array('codigo' => $row['SERV_id'],
                'descripcion' => utf8_encode($row['SERV_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrServicios;

    }
    
    function medicos()
    {
       $conn = new MySQL();
       $arrMedicos = array();
       $consulta = "call rapsinet_splistamedicos();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrMedicos, array('codigo' => $row['MED_id'],
                'descripcion' => utf8_encode($row['nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrMedicos;

    }
    
    
    
    function provincias($id_region)
    {
       $conn = new MySQL();
       $arrProvincias = array();
       $consulta = "call rapsinet_spprovinciasxregion($id_region);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrProvincias, array('id_provincias' => $row['PROV_id'],
                'nom_provincia' => utf8_encode($row['PROV_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrProvincias;

    }
    
    function comunas($id_provincia)
    {
       $conn = new MySQL();
       $arrComunas = array();
       $consulta = "call rapsinet_spcomunasxprovincia($id_provincia);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrComunas, array('id_comuna' => $row['COM_id'],
                'nom_comuna' => utf8_encode($row['COM_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrComunas;

    }
    
    function llena_sexo()
    {
        $arrSexo = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasexo();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSexo, array('codigo' => $row['SEX_descripcion'],
                'descripcion' => utf8_encode($row['SEX_detalle'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrSexo;
    } 
    function llena_vinculacion()
    {
        $arrVinculacion = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistavinculacion();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrVinculacion, array('codigo' => $row['TIPFAM_id'],
                'descripcion' => utf8_encode($row['TIPFAM_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrVinculacion;
    } 

}
?>
