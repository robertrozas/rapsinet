<?php

include('MySQL.php');
class DAOSolicitudInternacionAdministrativa {


    function ingresar_solicitud($cmb_establecimiento_sol,$opciones,
    $diagnostico,$otro_antecedente,$ultimo_tratamiento,$cmb_establecimiento_int,
    $nombre_medico,$rut_medico,$correo_medico,$nombres_sol,$apaterno_sol,$amaterno_sol,$rut_sol,
    $edad_sol,$sexo_sol,$calle_sol,$numerocasa_sol,$sector_sol,$comuna_sol,$ciudad_sol,$region_sol,
    $telefono_sol, $celular_sol,$correo_sol,$nombres_pac,$apaterno_pac,$amaterno_pac,
    $rut_pac,$edad_pac,$sexo_pac,$calle_pac,$numerocasa_pac,$sector_pac,$comuna_pac,$ciudad_pac,
    $region_pac,$vinculacion_pac,$quien_solicito)
    {
        $consulta = 'CALL rapsinet_spingresarsolicitudinternacionadmin("'.
                $cmb_establecimiento_sol.'","'.
                $opciones.'","'.
                $diagnostico.'","'.
                $otro_antecedente.'","'.
                $ultimo_tratamiento.'","'.
                $cmb_establecimiento_int.'","'.
                $nombre_medico.'","'.
                $rut_medico.'","'.
                $correo_medico.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $nombres_sol.'","'.
                $apaterno_sol.'","'.
                $amaterno_sol.'","'.
                $rut_sol.'","'.
                $edad_sol.'","'.
                $sexo_sol.'","'.
                $calle_sol.'","'.
                $numerocasa_sol.'","'.
                $sector_sol.'","'.
                $comuna_sol.'","'.
                $ciudad_sol.'","'.
                $region_sol.'","'.
                $telefono_sol.'","'.
                $celular_sol.'","'.
                $correo_sol.'","'.
                $nombres_pac.'","'.
                $apaterno_pac.'","'.
                $amaterno_pac.'","'.
                $rut_pac.'","'.
                $edad_pac.'","'.
                $sexo_pac.'","'.
                $calle_pac.'","'.
                $numerocasa_pac.'","'.
                $sector_pac.'","'.
                $comuna_pac.'","'.
                $ciudad_pac.'","'.
                $region_pac.'","'.
                $vinculacion_pac.'","'. 
                $quien_solicito.'")';     
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        
        $conn->dispose($resultado);
        $conn->close();
       
        return $mensaje;
    }
    
    function llena_establecimientos()
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => $row['EST_nombre']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    } 
    
    function llena_vinculacion()
    {
        $arrVinculacion = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistavinculacion();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrVinculacion, array('codigo' => $row['TIPFAM_id'],
                'descripcion' => $row['TIPFAM_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrVinculacion;
    } 
    
     function llena_sexo()
    {
        $arrSexo = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasexo();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSexo, array('codigo' => $row['SEX_descripcion'],
                'descripcion' => $row['SEX_detalle']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrSexo;
    } 
    
    function regiones()
    {
       $conn = new MySQL();
       $arrRegiones = array();
       $consulta = "call rapsinet_spregiones();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrRegiones, array('id_region' => $row['REG_ID'],
                'nom_region' => utf8_encode($row['REGION'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrRegiones;

    }
    
    function provincias($id_region)
    {
       $conn = new MySQL();
       $arrProvincias = array();
       $consulta = "call rapsinet_spprovinciasxregion($id_region);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrProvincias, array('id_provincias' => $row['PROV_id'],
                'nom_provincia' => utf8_encode($row['PROV_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrProvincias;

    }
    
    function comunas($id_provincia)
    {
       $conn = new MySQL();
       $arrComunas = array();
       $consulta = "call rapsinet_spcomunasxprovincia($id_provincia);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrComunas, array('id_comuna' => $row['COM_id'],
                'nom_comuna' => utf8_encode($row['COM_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrComunas;

    }
     

}
?>
