<?php

include('MySQL.php');
class DAODepartamentoHPSI {


        function visar_solaa($idCabeceraSolicitud) {
          
        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudaltaporvisar('".$idCabeceraSolicitud."');";
                          
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos
        
        $conn->dispose($resultado);
        $conn->close();
        return $row;   
      }
    
      
   
    
      function lista_solaa($id_sol,$id_estado,$fecha,$departamento,$est_id)
      {
         $conn = new MySQL();
         $retornado = array();
         $consultado = "call rapsinet_splistasolicitudaa('$id_estado','$id_sol','$fecha',$departamento,$est_id);"; 
         $holas = $conn->consulta($consultado);
         
         while($resul = $conn->fetch_assoc($holas))
         {
           array_push($retornado,array('usuario' => utf8_encode($resul['USUARIO']),'usuario_id'=>$resul['USUA_id'],
                'fecha'=>$resul['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>utf8_encode($resul['TIPO']),
                'id_cabecera'=>$resul['CABALTAHOSPADMIN_id'],'estado'=>utf8_encode($resul['ESTADO'])
            ));
         }   
         $conn->dispose($resul);
         $conn->close();
         return $retornado;
         
        
      }
        
      
       function lista_camas($est_id)
      {
         $conn = new MySQL();
         $retornado = array();
         $consultado = "call rapsinet_splistaasignacioncamas('$est_id');"; 
         $holas = $conn->consulta($consultado);
         
         while($resul = $conn->fetch_assoc($holas))
         {
            array_push($retornado,array('codigo' => $resul['ASIGCAM_id'],'fecha'=>$resul['ASIGCAM_fecha'],
                'paciente'=>$resul['paciente'],'establecimiento'=>$resul['EST_nombre']
            ));
         }   
         $conn->dispose($resul);
         $conn->close();
         return $retornado;
         
         
      }
      
      function actualizar_camas($est_id,$camas)
    {

        $consulta = "call rapsinet_spactualizarcamas('$est_id','$camas');";

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_assoc($resultado);
        $conn->dispose($consulta);
        

        return $mensaje;
        $conn->close();

    }
    
     function eliminar_camas($codigo)
    {

        $consulta = "call rapsinet_speliminarcamas('$codigo');";

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_assoc($resultado);
        $conn->dispose($resultado);
       
        $conn->close();
        
         

        return $mensaje;

    }
      
      function lista_camas2($est_id)
      {
         $conn = new MySQL();
         $consultado = "call rapsinet_splistaasignacioncamas('$est_id');"; 
         $resultado = $conn->consulta($consultado);

         $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

         $conn->dispose($resultado);
         $conn->close();

         return $row;   
      }
        
    
    
    
      function lista_solevasHPSI($id_sol,$id_estado,$fecha,$departamento,$est_id)
      {
         
          
         $conn = new MySQL();
         $retorno = array();
        
         $consulta = "call rapsinet_splistasolevas('$id_estado','$id_sol','$fecha',$departamento,$est_id);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('codigo' => $resultado['cab_solevatrat'],'fechaEvaluacion'=>$resultado['fecha_sol'],
                'estadosolicitud'=>$resultado['ESTA_descripcion'],'usuario'=>$resultado['USUARIO'],
                'tiposolicitud'=>$resultado['TIP_descripcion']
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
     
      
      function visar_solET($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_splistasoleva('".$idCabeceraSolicitud."');";
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;   
      } 
      
      function visar_solIA($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudinternacionadminporvisar('".$idCabeceraSolicitud."');";
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;   
      } 
      

        function lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."','".$est_id."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'usuario' => $row['usuario'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    

    
     
     
    

    

}
?>
