<?php
    include('MySQL.php');
    class DAO_Correos
    {
        

         function lista_correos()
        {
            $arrCorreos = array();
            $consulta = "call rapsinet_splistacorreos();";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrCorreos, 
                  array('codigo' => $row['CO_id'], 
                        'descripcion' => $row['CO_descripcion'],
                        'mail' => $row['CO_mail']                
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrCorreos;
        }
        
       function lista_uno($correo_id)
       {

            $consulta = "call rapsinet_splistauncorreo($correo_id);";
            $conn = new MySQL;
                $resultado =  $conn->consulta($consulta);
                $retorna = $conn->fetch_assoc($resultado);
                return $retorna;
            $conn->dispose($resultado);
            $conn->close();

       }
       
    function modificar_correo($id,$descripcion,$correo)
    {
        //recojo datos

        $consulta = "call rapsinet_spmodcorreos($id,'$descripcion','$correo');";
        

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_assoc($resultado);
        $conn->dispose($consulta);
        $conn->close();
        
        return $mensaje;

    }

 

    }
?>