<?php
    include('MySQL.php');
    include('DTO/Maestros/DTOMedicos.php');
    class DAO_Medicos 
    {
        

       function llena_establecimientos()
        {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => $row['EST_nombre']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
         }  

       

        function lista_completa()
        {
            $arrUsuarios = array();
            $consulta = "call rapsinet_splistamedicos;";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrUsuarios, 
                  array('rut' => $row['MED_RUT'], 
                        'nombres' => $row['nombre'],
                        'direccion' => $row['MED_DIRECCION'],
                        'correo' => $row['MED_CORREO'],
                        'celular' => $row['MED_CELULAR'],
                        'codigo' => $row['MED_id']                   
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrUsuarios;
        }
        
        
        function elimina_medico($USUA_id)
        {
            $consulta = "call rapsinet_speliminamedico($USUA_id);";
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();

        }

       function lista_uno($USUA_id)
       {

            $consulta = "call raspsinet_splistaunmedico($USUA_id);";
            $conn = new MySQL;
                $resultado =  $conn->consulta($consulta);
                $retorna = $conn->fetch_assoc($resultado);
                return $retorna;
            $conn->dispose($resultado);
            $conn->close();

       }

       function ingresa(DTOMedico $objUsuario)
       {
            //recojo datos
            $rut = $objUsuario->get_USUA_rut();
            $nombres = $objUsuario->get_USUA_nombres();
            $apellidos = $objUsuario->get_USUA_apellidos();
            $direccion = $objUsuario->get_USUA_direccion();
            $celular = $objUsuario->get_USUA_celular();
            $fono = $objUsuario->get_USUA_fono();
            $correo = $objUsuario->get_USUA_correo();
            $establecimiento = $objUsuario->get_ESTAB_id();

            $departamento = 2;

            $consulta = "call rapsinet_spingresomedico('$rut','$nombres','$apellidos','$direccion','$celular','$fono',
                         '$correo',$establecimiento);";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }
        

        function modifica(DTOMedico $objUsuario,$id_usuario)
       {
            //recojo datos
            $rut = $objUsuario->get_USUA_rut();
            $nombres = $objUsuario->get_USUA_nombres();
            $apellidos = $objUsuario->get_USUA_apellidos();
            $direccion = $objUsuario->get_USUA_direccion();
            $celular = $objUsuario->get_USUA_celular();
            $fono = $objUsuario->get_USUA_fono();
            $correo = $objUsuario->get_USUA_correo();
            $establecimiento = $objUsuario->get_ESTAB_id();
            
            //Falta agregar establecimiento y departamento

            $consulta = "call rapsinet_spmodificamedico('$rut','$nombres','$apellidos','$direccion','$celular','$fono',
                         '$correo','$establecimiento','$id_usuario');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }
 

    }
?>