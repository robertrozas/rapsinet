<?php
    include('MySQL.php');
   
    class DAO_SolicitudNoVoluntaria
    {

      function ingresa_archivo($id_cabecera,$nombre,$tipo,$extension,$archivo)
      {
         $conn = new MySQL();
         $archivo=mysql_real_escape_string($archivo);
         $consulta = "call rapsinet_spingresa_adjuntos_solint($id_cabecera,'$nombre','$tipo','$extension','$archivo');"; 
         $conn->consulta($consulta);
         $conn->close();
        
      }


       function ingresa_solicitudinv($form)
       {    
            $sexo = $form['sexo'];
            //recojo datos
            $opciones = $form['opcion_a'].','.$form['opcion_b'].','.$form['opcion_c'];
            $rut = $form['rut']; $nombres = $form['nombres']; $apellidos = $form['apellidos']; $direccion = $form['domicilio']; $fecnac = $form['fec_nac'];
            $fecint = $form['fec_int']; $fecsol = $form['fech_desde']; $servicio = $form['servicios']; $estable = $form['establecimiento'];
            $medico = $form['medicos']; $estado = 1;

                $id_depto=$_SESSION['DEPART_id'];
                $nombres_user=$_SESSION['USUA_nombres'];
                $apellidos_user=$_SESSION['USUA_apellidos'];
                $rut_user=$_SESSION['USUA_rut'];
                $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_spingresasolicitudinv('$rut','$nombres','$apellidos','$direccion','$fecnac','$fecint','$fecsol','$servicio',
                         '$estable','$medico','$estado','$opciones',$id_user,'$rut_user','$nombres_user','$apellidos_user',$id_depto,$sexo);";

            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
           
           return $consulta;

       }
  
    function llena_medicos()
    {
        $arrmedicos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistamedicos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrmedicos, array('codigo' => $row['MED_id'],
                'nombre' => $row['nombre']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrmedicos;
    } 
      

    
    function  llena_servicios()
    {
        $arrservicios = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaservicios();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrservicios, array('codigo' => $row['SERV_id'],
                'descripcion' => $row['SERV_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrservicios;
    } 

    function llena_establecimientos()
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => $row['EST_nombre']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    }  

    }
?>