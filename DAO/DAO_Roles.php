<?php
    include('MySQL.php');
   
    class DAO_Roles
    {
        
       

        function lista_roles()
        {
            $arrPerfiles = array();
            $consulta = "call rapsinet_splistaperfiles;";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrPerfiles, 
                  array('codigo' => $row['PER_id'], 
                        'descripcion' => $row['PER_descripcion']
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrPerfiles;
        }


        function lista_funciones()
        {
            $arrFunciones = array();
            $consulta = "call rapsinet_splistafunciones;";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrFunciones, 
                  array('codigo' => $row['FUNC_id'], 
                        'descripcion' => $row['FUNC_descripcion']
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrFunciones;
        }



        function lista_funciones_x_perfil($id_perfil)
        {
            $arrFuncxPerfil = array();
            $llamada = "call rapsinet_splistafuncionesperfil($id_perfil);";

              //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($llamada);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrFuncxPerfil, 
                  array(
                        'codigo' => $row['FUNC_ID']
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrFuncxPerfil;

        }
        


       
       function ingresa_perfil($perfil)
       {
            //recojo datos
            $consulta = "call rapsinet_spingresoperfil('$perfil');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }


       function elimina_perfil($perfil)
       {
            //recojo datos
            $consulta = "call rapsinet_speliminaperfil('$perfil');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }



       function elimina_perfil_funciones($perfil)
       {
            //recojo datos
            $consulta = "call rapsinet_eliminaperfilfuncionalidad('$perfil');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }

        function asigna_perfil_funciones($perfil,$funcion)
       {
            //recojo datos
            $consulta = "call rapsinet_spingresaperfilfuncionalidad('$perfil','$funcion');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }
        
        

       

    }
?>