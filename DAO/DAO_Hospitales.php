<?php
    include('MySQL.php');
    include('DTO/Maestros/DTOHospitales.php');
    class DAO_Hospital
    {
        
        function servicios($id_region)
        {
            $consulta = "CALL rapsinet_sp_listaserviciossalud($id_region);";
            $arrServicios = array();
            $conn = new MySQL();

            $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrServicios, array('id_servicio' => $row['SSALUD_id'],
                'nom_servicio' => utf8_encode($row['SSALUD_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrServicios;

        }

      function asistentes()
      {
        
      }

      function ingresa(DTOHospital $objUsuario)
       {
            //recojo datos
            
            $nombres = $objUsuario->get_EST_nombres();
            $direccion = $objUsuario->get_EST_direccion();
            $fono = $objUsuario->get_EST_fono();
            $correo = $objUsuario->get_EST_correo();
            $region = $objUsuario->get_EST_region();
            $provincia = $objUsuario->get_EST_provincia();
            $comuna = $objUsuario->get_EST_comuna();
            $condicion = $objUsuario->get_EST_condicion();
            $SSALUD_id = $objUsuario->get_SSALUD_id();
            $ASSOL_id = $objUsuario->get_ASSOL_id();
            $consulta = "call rapsinet_spingresahospital('$nombres','$direccion','$fono','$correo',$region,$provincia,$comuna,$condicion,$SSALUD_id,'$ASSOL_id');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
                        
            $conn->dispose($consulta);
            $conn->close();
          

       }
        

        function modifica(DTOHospital $objUsuario,$id_hospital)
       {
            //recojo datos
            $nombres = $objUsuario->get_EST_nombres();
            $direccion = $objUsuario->get_EST_direccion();
            $fono = $objUsuario->get_EST_fono();
            $correo = $objUsuario->get_EST_correo();
            $region = $objUsuario->get_EST_region();
            $provincia = $objUsuario->get_EST_provincia();
            $comuna = $objUsuario->get_EST_comuna();
            $condicion = $objUsuario->get_EST_condicion();
            $SSALUD_id = $objUsuario->get_SSALUD_id();
            $ASSOL_id = $objUsuario->get_ASSOL_id();

            $consulta = "call rapsinet_spmodificahospital('$nombres','$direccion','$fono','$correo',$region,$provincia,$comuna,$condicion,$id_hospital,$SSALUD_id,'$ASSOL_id');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
         

       }
       
            

        function lista_completa()
        {
            $arrUsuarios = array();
            $consulta = "call rapsinet_splistahospitales;";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrUsuarios, 
                  array('nombres' => utf8_encode($row['est_nombre']), 
                        'direccion' => utf8_encode($row['est_ubicacion']),
                        'fono' => $row['est_fono'],
                        'provincia' => utf8_encode($row['prov_nombre']),
                        'comuna' => utf8_encode($row['com_nombre']),
                        'id_hospital' => $row['est_id'],             
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrUsuarios;
        }
        
        
        function elimina_hospital($EST_id)
        {
            $consulta = "call rapsinet_speliminahospital($EST_id);";
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();

        }

       function lista_uno($Esta_id)
       {

            $consulta = "call raspsinet_splistaunhospital($Esta_id);";
            $conn = new MySQL;
                $resultado =  $conn->consulta($consulta);
                $retorna = $conn->fetch_assoc($resultado);
            $conn->dispose($resultado);
            $conn->close();
            return $retorna;
       }


    function regiones()
    {
       $conn = new MySQL();
       $arrRegiones = array();
       $consulta = "call rapsinet_spregiones();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrRegiones, array('id_region' => $row['REG_ID'],
                'nom_region' => utf8_encode($row['REGION'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrRegiones;

    }
    
    function provincias($id_region)
    {
       $conn = new MySQL();
       $arrProvincias = array();
       $consulta = "call rapsinet_spprovinciasxregion($id_region);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrProvincias, array('id_provincias' => $row['PROV_id'],
                'nom_provincia' => utf8_encode($row['PROV_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrProvincias;

    }
    
    function comunas($id_provincia)
    {
       $conn = new MySQL();
       $arrComunas = array();
       $consulta = "call rapsinet_spcomunasxprovincia($id_provincia);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrComunas, array('id_comuna' => $row['COM_id'],
                'nom_comuna' => utf8_encode($row['COM_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrComunas;

    }
      
        

 

    }
?>