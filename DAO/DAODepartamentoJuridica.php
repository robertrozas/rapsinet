<?php

include('MySQL.php');
class DAODepartamentoJuridica {

 function resolucion_sol_et($idCabeceraSolicitud)
    {
        $arrResolucionInternacionAdmin = array();
        $consulta = "call rapsinet_spresevaluacionytratamiento('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionInternacionAdmin, array('codigo' => $row['cab_solevatrat'],
                'nombrecompleto_sol' => $row['nombrecompleto_sol'],
                'rut_sol' => $row['rut_sol'],
                'vinculacion' => $row['TIPFAM_descripcion'],
                'nombrecompleto_pac' => $row['nombrecompleto_pac'],
                'rut_pac' => $row['rut_pac'],
                'fecha_actual' => $row['fecha_actual'],
                'fecha_evaluacion' => $row['fecha_evaluacion'],
                'nombre_medico' => $row['nom_doc'],
                'establecimiento_sol' => $row['estab_solicitante'],
                'nombrecompleto_ac' => $row['nombrecompleto_ac'],
                'establecimiento_int' => $row['estab_internacion'],
                'opciones' => $row['det_opciones']
                
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrResolucionInternacionAdmin;
    }
    
    
    function visar_solinv($id_cab)
    {
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasolinv($id_cab);"; 
         $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;
    }
    
 function resolucion_sol_AA($idCabeceraSolicitud)
    {
        $arrResolucionINV = array();
        $consulta = "call rapsinet_spresaltaadministrativa('".$idCabeceraSolicitud."');";
        
        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionINV, array('codigo' => $row['CABALTAHOSPADMIN_id'],
                'medico_tratante' => $row['medico_tratante'],
                'nombrecompleto_pac' => $row['nombrecompleto_pac'],
                'rut_paciente' => $row['CABALTAHOSPADMIN_rutPac'],
                'fecha_actual' => $row['CABALTAHOSPADMIN_fechaActual'],
                'opciones' => $row['DETALTHOSPADMIN_opciones'],
                'establecimiento' => $row['establecimiento'],
                'centro_salud' => $row['centro_salud']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();
        return $arrResolucionINV;
    }     
    
      function lista_solaa($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudaa('$id_estado','$id_sol','$fecha',$departamento,'');"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
             array_push($retorno,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['USUA_id'],
                'fecha'=>$resultado['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>utf8_encode($resultado['TIPO']),
                'id_cabecera'=>$resultado['CABALTAHOSPADMIN_id'],'estado'=>utf8_encode($resultado['ESTADO'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
    
      
      function visar_solaa($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudaltaporvisar('".$idCabeceraSolicitud."');";
                          
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos
        
        $conn->dispose($resultado);
        $conn->close();
        return $row;   
      }
      
      
       function resolucion_sol_int_admin($idCabeceraSolicitud)
    {
        $arrResolucionInternacionAdmin = array();
        $consulta = "call rapsinet_spresinternacionsolicitudadmin('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionInternacionAdmin, array('codigo' => $row['CABINTADMIN_id'],
                'nombrecompleto_sol' => $row['nombrecompleto_sol'],
                'rut_sol' => $row['DETINTADMIN_rutSol'],
                'vinculacion' => $row['DETINTADMIN_vinculacion'],
                'nombrecompleto_pac' => $row['nombrecompleto_pac'],
                'rut_pac' => $row['DETINTADMIN_rutPac'],
                'fecha_actual' => $row['fecha_actual'],
                'fecha_evaluacion' => $row['fecha_evaluacion'],
                'nombre_medico' => $row['DETINTADMIN_nombreMedico'],
                'establecimiento_sol' => $row['EST_nombre'],
                'nombrecompleto_ac' => $row['nombrecompleto_ac'],
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrResolucionInternacionAdmin;
    } 

   function resolucion_sol_inv($idCabeceraSolicitud)
    {
        $arrResolucionINV = array();
        $consulta = "call rapsinet_spresinv('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrResolucionINV, array('codigo' => $row['CABSOLINV_id'],
                'nombre_medico' => $row['nombre_medico'],
                'nombre_paciente' => $row['nombre_paciente'],
                'rut_paciente' => $row['CABSOLINV_rutPaciente'],
                'fecha_internacion' => $row['CABSOLINV_fechaInternacion'],
                'fecha_actual' => $row['fecha_actual'],
                'opciones' => $row['DETSOLINV_opcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrResolucionINV;
    }

    function visar_soleva($id_cab)
    {
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistasoleva($id_cab);"; 
         $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;
    }


    function lista_solinv($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudinv('$id_estado','$id_sol','$fecha',$departamento);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
             array_push($retorno,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['CABSOLINV_FECHAINTERNACION'],'solicitud'=>utf8_encode($resultado['TIP_DESCRIPCION']),
                'id_cabecera'=>$resultado['CABSOLINV_ID'],'estado'=>utf8_encode($resultado['ESTA_DESCRIPCION'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }



       function lista_solevas($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $est_id = $_SESSION['EST_id'];
         $consulta = "call rapsinet_splistasolevas('$id_estado','$id_sol','$fecha',$departamento,$est_id);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => $resultado['USUARIO'],'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['FECHA_SOL'],'solicitud'=>$resultado['TIPO'],
                'id_cabecera'=>$resultado['CAB_SOLEVATRAT'],'estado'=>$resultado['ESTADO']
            ));
         }   
         
         $conn->close();
         return $retorno;
      }


     function visar_solinternacionadmin($idCabeceraSolicitud) {

        $conn = new MySQL(); 
        $consulta = "call rapsinet_spmostrarsolicitudinternacionadminporvisar('".$idCabeceraSolicitud."');";
        $resultado = $conn->consulta($consulta);

        $row = $conn->fetch_assoc($resultado); //retorna solo 1 set de datos

        $conn->dispose($resultado);
        $conn->close();

        return $row;   
    }

    
     function lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."','');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => utf8_encode($row['ESTA_descripcion']),
                'nombres' => utf8_encode($row['USUA_nombres']),
                'apellidos' => utf8_encode($row['USUA_apellidos']),
                'tiposolicitud' => utf8_encode($row['TIP_descripcion'])
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    
     function lista_log_solicitudes() {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlogsolicitudes;";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogSolicitudes, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => $row['DEP_descripcion'],
                'tipo' => $row['TIP_descripcion'],
                'estado' => $row['ESTA_descripcion'],
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
    }
        function lista_historico_usuarios() {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlistahistoricousuarios();";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
          array_push($arrLogSolicitudes, 
          array('codigo' => $row['HIST_id'],
                'nombres' => utf8_encode($row['HIST_nombres']),
                'apellidos' => utf8_encode($row['HIST_apellidos']),
                'fecha' => $row['HIST_fecha'],
                'idaccion' => $row['HIST_idAccion'],
                'accion' => utf8_encode($row['Accion']),
                'idsolicitud' => $row['HIST_idCabecera'],
                'tiposol' => utf8_encode($row['Tipo'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
        }
    
      function solicitud_por_visar($idCabeceraSolicitud) {
         
        $arrSolicitudPorVisar = array();
        $consulta = "call rapsinet_spmostrarsolicitudinternacionadminporvisar('".$idCabeceraSolicitud."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudPorVisar, array('codigo' => $row['CABINTADMIN_id'],
                'fecha_sol' => $row['CABINTADMIN_fechaEvaluacion'],
                'estsol' => $row['estsol'],
                'opciones' => $row['DETINTADMIN_opciones'],
                'diagnostico' => $row['DETINTADMIN_diagnostico'],
                'otro_antecedente' => $row['DETINTADMIN_otroAntenedente'],
                'ultimo_tratamiento' => $row['DETINTADMIN_ultimoTratamiento'],
                'estint' => $row['estint'],
                'nombre_medico' => $row['DETINTADMIN_nombreMedico'],
                'rut_medico' => $row['DETINTADMIN_rutMedico'],
                'correo_medico' => $row['DETINTADMIN_correoMedico'],
                'estado_solicitud' => $row['CABINTADMIN_idEstadoSolicitud'],
                'apaterno_sol' => $row['DETINTADMIN_aPaternoSol'],
                'amaterno_sol' => $row['DETINTADMIN_aMaternoSol'],
                'nombres_sol' => $row['DETINTADMIN_nombresSol'],
                'rut_sol' => $row['DETINTADMIN_rutSol'],
                'edad_sol' => $row['DETINTADMIN_edadSol'],
                'sexo_sol' => $row['DETINTADMIN_sexoSol'],
                'calle_sol' => $row['DETINTADMIN_calleSol'],
                'ncasa_sol' => $row['DETINTADMIN_nCasaSol'],
                'sector_sol' => $row['DETINTADMIN_sectorSol'],
                'comuna_sol' => $row['DETINTADMIN_idComunaSol'],
                'ciudad_sol' => $row['DETINTADMIN_idCiudadSol'],
                'region_sol' => $row['DETINTADMIN_idRegionSol'],
                'telefono_sol' => $row['DETINTADMIN_telefonoSol'],
                'celular_sol' => $row['DETINTADMIN_celularSol'],
                'correo_sol' => $row['DETINTADMIN_correoSol'],
                'apaterno_pac' => $row['DETINTADMIN_aPaternoPac'],
                'amaterno_pac' => $row['DETINTADMIN_aMaternoPac'],
                'nombres_pac' => $row['DETINTADMIN_nombresPac'],
                'rut_pac' => $row['DETINTADMIN_rutPac'],
                'edad_pac' => $row['DETINTADMIN_edadPac'],
                'sexo_pac' => $row['DETINTADMIN_sexoPac'],
                'calle_pac' => $row['DETINTADMIN_callePac'],
                'ncasa_pac' => $row['DETINTADMIN_nCasaPac'],
                'sector_pac' => $row['DETINTADMIN_sectorPac'],
                'comuna_pac' => $row['DETINTADMIN_idComuna'],
                'ciudad_pac' => $row['DETINTADMIN_idCiudad'],
                'region_pac' => $row['DETINTADMIN_idRegion'],
                'vinculacion_pac' => $row['DETINTADMIN_vinculacion'] 
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudPorVisar;
    }
    
    function llena_estados2() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_spmostrarestadosolintadmin_juridica();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstados, array('codigo' => $row['ESTA_id'],
                'descripcion' => $row['ESTA_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 
    
    function filtra_estado2($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => $row['ESTA_descripcion'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'tiposolicitud' => $row['TIP_descripcion']
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }
    
    function lista_filtros_log($filtro,$tipo,$fecha) //de acuerdo al tipo de filtro, el cual se ve segun la opcion
    {
        $consulta = "call rapsinet_spfiltrologsolintadmin('".$filtro."','".$tipo."','".$fecha."');";
        $arrLogFiltrada = array();
        $conn = new MySQL();
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogFiltrada, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => $row['DEP_descripcion'],
                'tipo' => $row['TIP_descripcion'],
                'estado' => $row['ESTA_descripcion'],
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        
        return $arrLogFiltrada;
    }
    
    function llena_estados() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestados();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstados, array('codigo' => $row['ESTA_ID'],
                'descripcion' => utf8_encode($row['ESTA_DESCRIPCION'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 
    
    function llena_tipo() //lleno combo con los tipos de estados par posteriormente aplicar filtro a grilla
    {
        $arrEstados = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_listatiposolicitudes();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
             array_push($arrEstados, array('codigo' => $row['TIP_id'],
                'descripcion' => utf8_encode($row['TIP_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstados;
    } 
    
    function lista_completa_log_solicitudes() {
        $arrLogSolicitudes = array();
        $consulta = "call rapsinet_spmostrarlogsolicitudes;";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrLogSolicitudes, array('codigo' => $row['LOGSOL_id'],
                'codigosolicitud' => $row['CABSOL_id'],
                'fecha' => $row['LOGSOL_fecha'],
                'nombres' => $row['USUA_nombres'],
                'apellidos' => $row['USUA_apellidos'],
                'departamento' => utf8_encode($row['DEP_descripcion']),
                'tipo' => utf8_encode($row['TIP_descripcion']),
                'estado' => utf8_encode($row['ESTA_descripcion']),
                'observaciones' => $row['LOGSOL_observaciones']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrLogSolicitudes;
    }
    
    function filtro_historico_usuarios($idSolicitud, $fecha) {
        $arrHistoricoSolicitudes = array();
        $consulta = "call rapsinet_spfiltrohistorialusuarios('".$idSolicitud."','".$fecha."');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrHistoricoSolicitudes, array('codigo' => $row['HIST_id'],
                'nombres' => $row['HIST_nombres'],
                'apellidos' => $row['HIST_apellidos'],
                'fecha' => $row['HIST_fecha'],
                'idaccion' => $row['HIST_idAccion'],
                'accion' => $row['ACC_descripcion']
            ));
        }

        $conn->dispose($resultado);
        $conn->close();

        return $arrHistoricoSolicitudes;
    }
    
    

}
?>
