<?php
    include('MySQL.php');
   
    class DAO_Graficos
    {

    	function distribucion_solevatrat_x_hosp()
    	{
    		$arrDatos = array();
    		$conn = new MySQL();
    		$consulta = "CALL rapsinet_spdistribucion_solevatrat_x_hosp();";

    		$resultado = $conn->consulta($consulta);

	        while ($row = $conn->fetch_assoc($resultado)) {
	            array_push($arrDatos, array('id_hosp' => $row['EST_id'],
	                'nombre_hosp' => utf8_encode($row['EST_nombre']),
	                'numero'=>$row['Numero']
	            ));
	        }

	        $conn->dispose($resultado);
	        $conn->close();
	        return $arrDatos;

    	}




   }

 ?>