<?php

include('MySQL.php');

class DAOVisacionInternacionAdministrativa {

    function aprobar($idCabeceraSolicitud,$estadoDocumento)
    {
        $consulta = 'CALL rapsinet_spaprobarsolicitudinternacionadmin("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $estadoDocumento.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
    function rechazar_solicitud($idCabeceraSolicitud,$observacion,$EstadoSolicitud)
    {
        $consulta = 'CALL rapsinet_sprechazarsolicitudinternacionadmin("'.
                $idCabeceraSolicitud.'","'.
                $observacion.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $EstadoSolicitud.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
     function rechazar_solicitud2($idCabeceraSolicitud,$observacion,$EstadoSolicitud)
    {
        $consulta = 'CALL rapsinet_sprechaza_solinv("'.
                $idCabeceraSolicitud.'","'.
                $observacion.'","'.
                $EstadoSolicitud.'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $_SESSION['DEPART_id'].'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }

}
?>
