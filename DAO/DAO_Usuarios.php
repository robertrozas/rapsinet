<?php
    include('MySQL.php');
    include('DTO/Maestros/DTOUsuario.php');
    class DAO_Usuarios 
    {
        
       

        function lista_completa()
        {
            $arrUsuarios = array();
            $consulta = "call rapsinet_splistausuarios;";

            //conexion
            $conn = new MySQL;
            $resultado = $conn->consulta($consulta);

            while ($row=$conn->fetch_assoc($resultado))
            {
                 array_push($arrUsuarios, 
                  array('rut' => $row['USUA_rut'], 
                        'nombres' => $row['USUA_nombres'],
                        'apellidos' => $row['USUA_apellidos'],
                        'direccion' => $row['PER_descripcion'],
                        'correo' => $row['USUA_correo'],
                        'celular' => $row['USUA_celular'],
                        'codigo' => $row['USUA_id']                   
                  ));

            }

            $conn->dispose($resultado);
            $conn->close();

            return $arrUsuarios;
        }
        
        
        function elimina_usuario($USUA_id)
        {
            $consulta = "call rapsinet_speliminausuario($USUA_id);";
            $conn = new MySQL;
            $conn->consulta($consulta);
            
            $conn->close();

        }

       function lista_uno($USUA_id)
       {

            $consulta = "call rapsinet_splistaunusuario($USUA_id);";
            $conn = new MySQL;
                $resultado =  $conn->consulta($consulta);
                $retorna = $conn->fetch_assoc($resultado);
                return $retorna;
            $conn->dispose($resultado);
            $conn->close();

       }

       function ingresa(DTOUsuario $objUsuario)
       {
            //recojo datos
            $rut = $objUsuario->get_USUA_rut();
            $nombres = $objUsuario->get_USUA_nombres();
            $apellidos = $objUsuario->get_USUA_apellidos();
            $direccion = $objUsuario->get_USUA_direccion();
            $celular = $objUsuario->get_USUA_celular();
            $fono = $objUsuario->get_USUA_fono();
            $correo = $objUsuario->get_USUA_correo();
            $usuario = $objUsuario->get_USUA_usuario();
            $clave = $objUsuario->get_USUA_contrasena();
            $establecimiento = $objUsuario->get_USUA_establecimiento();
            $departamento = $objUsuario->get_USUA_depto();
            $perfil = $objUsuario->get_USUA_perfil();

            $consulta = "call rapsinet_spingresousuario('$rut','$nombres','$apellidos','$direccion','$celular','$fono',
                         '$correo','$usuario','$clave',$establecimiento,$departamento,$perfil);";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            //return $consulta;
            
            $conn->dispose($consulta);
            $conn->close();
          

       }
        

        function modifica(DTOUsuario $objUsuario,$id_usuario)
       {
            //recojo datos
            $rut = $objUsuario->get_USUA_rut();
            $nombres = $objUsuario->get_USUA_nombres();
            $apellidos = $objUsuario->get_USUA_apellidos();
            $direccion = $objUsuario->get_USUA_direccion();
            $celular = $objUsuario->get_USUA_celular();
            $fono = $objUsuario->get_USUA_fono();
            $correo = $objUsuario->get_USUA_correo();
            $usuario = $objUsuario->get_USUA_usuario();
            $clave = $objUsuario->get_USUA_contrasena();
            $establecimiento = $objUsuario->get_USUA_establecimiento();
            $departamento = $objUsuario->get_USUA_depto();
            $perfil = $objUsuario->get_USUA_perfil();
            //Falta agregar establecimiento, departamento y perfiiilllllllllll !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            $consulta = "call rapsinet_spmodificausuario('$rut','$nombres','$apellidos','$direccion','$celular','$fono',
                         '$correo','$usuario','$clave','$id_usuario');";
            
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($resultado);
            $conn->close();
          

       }
       
    function departamentos()
    {
       $conn = new MySQL();
       $arrDepartamentos = array();
       $consulta = "call rapsinet_splistadepartementos();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrDepartamentos, array('id_departamento' => $row['DEP_id'],
                'descripcion' => utf8_encode($row['DEP_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrDepartamentos;

    }
    
    function perfiles()
    {
       $conn = new MySQL();
       $arrPerfiles = array();
       $consulta = "call rapsinet_splistaperfiles();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrPerfiles, array('id_perfil' => $row['PER_id'],
                'descripcion' => ($row['PER_descripcion'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrPerfiles;

    }
    
    function establecimientos()
    {
       $conn = new MySQL();
       $arrEstablecimientos = array();
       $consulta = "call rapsinet_splistaestablecimientos();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('id_establecimiento' => $row['EST_id'],
                'descripcion' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;

    }


    function MisDatos($form)
    {
        $correo = $form['correo'];
        $password = $form['password'];
        $id_usuario = $_SESSION['USUA_idUsuario'];
        $mensaje = "Datos actualizados correctamente OK";

        $consulta = "call rapsinet_spmisdatos('$correo','$password',$id_usuario);";

        $conn = new MySQL();

        try {

            $conn->consulta($consulta);
            
            }

            catch(Exception $e){

            $mensaje = $e->getMessage();

            }


            $conn->close();
    
        return $mensaje;

    }

    function setea()
    {
        $id_usuario = $_SESSION['USUA_idUsuario'];
        $conn = new MySQL();
        
        try{
        
        $resultado = $conn->consulta("Select USUA_correo,USUA_contrasena from rapsinet_usuarios Where USUA_id = $id_usuario");
        $row = $conn->fetch_assoc($resultado);
        $_SESSION['USUA_mail'] = $row['USUA_correo'];
        $_SESSION['USUA_pass'] = $row['USUA_contrasena'];

        }
        catch(Exception $e){

            $e->getMessage();

            }

        $conn->close();

    }
 

    }
?>