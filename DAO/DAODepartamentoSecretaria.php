<?php

include('MySQL.php');
class DAODepartamentoSecretaria {

   

      function lista_solinv($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolicitudinv('$id_estado','$id_sol','$fecha',$departamento);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
             array_push($retorno,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['CABSOLINV_FECHAINTERNACION'],'solicitud'=>utf8_encode($resultado['TIP_DESCRIPCION']),
                'id_cabecera'=>$resultado['CABSOLINV_ID'],'estado'=>utf8_encode($resultado['ESTA_DESCRIPCION'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      

       function lista_solevas($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistasolevas('$id_estado','$id_sol','$fecha',$departamento,'');"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => utf8_encode($resultado['USUARIO']),'usuario_id'=>$resultado['ID_USUARIO'],
                'fecha'=>$resultado['FECHA_SOL'],'solicitud'=>utf8_encode($resultado['TIPO']),
                'id_cabecera'=>$resultado['CAB_SOLEVATRAT'],'estado'=>utf8_encode($resultado['ESTADO'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      
      
      function lista_solalta($id_sol,$id_estado,$fecha,$departamento)
      {
         $conn = new MySQL();
         $retorno = array();
         
         $consulta = "call rapsinet_splistasolicitudaa('$id_estado','$id_sol','$fecha',$departamento,'');"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('usuario' => utf8_encode($resultado['nombre_completo']),'usuario_id'=>$resultado['USUA_id'],
                'fecha'=>$resultado['CABALTAHOSPADMIN_fechaActual'],'solicitud'=>utf8_encode($resultado['TIP_descripcion']),
                'id_cabecera'=>$resultado['CABALTAHOSPADMIN_id'],'estado'=>utf8_encode($resultado['ESTA_descripcion'])
            ));
         }   
         
         $conn->close();
         return $retorno;
      }
      

     function lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha) {
        $arrSolicitudes = array();
        $consulta = "call rapsinet_splistasolicitudesinternacionadministrativa('".$idDepartamento."','".$idSolicitud."','".$idEstado."','".$fecha."','');";

        //conexion
        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
             array_push($arrSolicitudes, array('codigo' => $row['CABINTADMIN_id'],
                'fechaEvaluacion' => $row['CABINTADMIN_fechaEvaluacion'],
                'estadosolicitud' => utf8_encode($row['ESTA_descripcion']),
                'nombres' => utf8_encode($row['USUA_nombres']),
                'apellidos' => utf8_encode($row['USUA_apellidos']),
                'tiposolicitud' => utf8_encode($row['TIP_descripcion'])
            ));
        }
        $conn->dispose($resultado);
        $conn->close();

        return $arrSolicitudes;
    }

     
     function rakim_solinv($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solinv($id_sol,'$num');"; 
         
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }

     function rakim_soleva($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_soleva($id_sol,'$num');"; 
         
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }

    function rakim_solint($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solint($id_sol,'$num');"; 
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }
     
      function rakim_solalta($id_sol,$num)
     {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_sprakim_solalta($id_sol,'$num');"; 
         $mensaje = $conn->fetch_assoc($conn->consulta($consulta));
         $conn->close();

         return $mensaje['@MENSAJE'];
     }
    

    

}
?>
