<?php
    include('MySQL.php');
   
    class DAO_SolicitudEvaluacion
    {

       function ingresa_archivo($id_cabecera,$nombre,$tipo,$extension,$archivo)
      {
         $conn = new MySQL();
         $archivo=mysql_real_escape_string($archivo);
         $consulta = "call rapsinet_spingresa_adjuntos_soleva($id_cabecera,'$nombre','$tipo','$extension','$archivo');"; 
         $conn->consulta($consulta);
         $conn->close();
        
      }

    function agrega_comentario_soleva($cabid,$comentario)
    {
        $consulta = "call rapsinet_spcomentarios_soleva($cabid,'$comentario');";
         $conn = new MySQL;
         $conn->consulta($consulta);
        $conn->close();

    }
    
    function confirmar_evaluacion($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spconfirmarevaluacion("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
 
    

       function ingresa_solicitudtrat($form)
      {    
            $opciones = $form['opcion_a'].','.$form['opcion_b'].','.$form['opcion_c'];
            //Datos Solicitante, 10 datos en total
            $nombres_sol = $form['nombre_sol']; $apel_sol = $form['apel_sol'];
            $rut_sol = $form['rut_sol']; $edad_sol = $form['edad_sol'];
            $dire_sol = $form['dir_sol']; $reg_sol = $form['regiones'];
            $prov_sol = $form['provincias']; $com_sol = $form['comunas'];
            $sexo_sol = $form['sexo_sol']; $vincula_sol = $form['vincula'];
            //recojo datos
            $fec_sol = $form['fec_sol']; $ficha = $form['ficha']; $fec_vis = $form['fec_vis'];
            $region = $form['region']; $provincia = $form['provincia']; $comuna = $form['comuna']; $hospital = $form['hospital'];
            $nombre_pac = utf8_decode($form['identidad']); $ap_pat = utf8_decode($form['ap_pat']); $ap_mat = utf8_decode($form['ap_mat']); $rut_pac = $form['rut']; 
            $edad_pac = $form['edad']; $sexo = $form['sexo']; $direccion = utf8_decode($form['dire']); $fono = $form['fono'];
            $rut_doc = $form['rut_doc']; $nom_doc = utf8_decode($form['nom_doc']); $pro_doc = utf8_decode($form['pro_doc']); $fono_doc = $form['fono_doc'];
            $mail_doc = $form['mail_doc']; $estado = 1; $hospital_psi = $form['hospital_psi'];

                $id_depto=$_SESSION['DEPART_id'];
                $nombres_user=$_SESSION['USUA_nombres'];
                $apellidos_user=$_SESSION['USUA_apellidos'];
                $rut_user=$_SESSION['USUA_rut'];
                $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_spingresasoltratamiento('$fec_sol',$ficha,'$fec_vis',$region,$provincia,$comuna,$hospital,'$nombre_pac','$ap_pat','$ap_mat','$rut_pac',$edad_pac,$sexo,'$direccion','$fono','$rut_doc','$nom_doc','$pro_doc','$fono_doc','$mail_doc',$estado,$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user,$hospital_psi,'$opciones'";
            $consulta = $consulta.",'$nombres_sol','$apel_sol','$rut_sol',$edad_sol,'$dire_sol',$reg_sol,$prov_sol,$com_sol,$sexo_sol,$vincula_sol);" ;   
            $mensaje = "Solicitud Ingresada Correctamente";

          try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

           return $mensaje; 
       }
  
    
    

    function hospital_comuna($id_comuna)
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_sphospitalesxcomunas($id_comuna);"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    }  
    
    function hospital_2()
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    }  
    
    
    function hospital_psi()
    {
        $arrEstablecimientosPSI = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistahospitalespsi();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientosPSI, array('codigo' => $row['EST_id'],
                'nombre' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientosPSI;
    }  



       function regiones()
    {
       $conn = new MySQL();
       $arrRegiones = array();
       $consulta = "call rapsinet_spregiones();";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrRegiones, array('id_region' => $row['REG_ID'],
                'nom_region' => utf8_encode($row['REGION'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrRegiones;

    }
    
    function provincias($id_region)
    {
       $conn = new MySQL();
       $arrProvincias = array();
       $consulta = "call rapsinet_spprovinciasxregion($id_region);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrProvincias, array('id_provincias' => $row['PROV_id'],
                'nom_provincia' => utf8_encode($row['PROV_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrProvincias;

    }
    
    function comunas($id_provincia)
    {
       $conn = new MySQL();
       $arrComunas = array();
       $consulta = "call rapsinet_spcomunasxprovincia($id_provincia);";

       $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrComunas, array('id_comuna' => $row['COM_id'],
                'nom_comuna' => utf8_encode($row['COM_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrComunas;

    }



    function visado_soleva($cabecera,$id_estado)
    {
            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_spvisasoleva($id_estado,$cabecera,$id_user,$id_depto,'$nombres_user','$apellidos_user','$rut_user');";
            $mensaje = "Solicitud Ingresada Correctamente";

             try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

           return $mensaje; 

    }


     function rechazo_soleva($cabecera,$id_estado,$observacion)
    {
            $id_depto=$_SESSION['DEPART_id'];
            $nombres_user=$_SESSION['USUA_nombres'];
            $apellidos_user=$_SESSION['USUA_apellidos'];
            $rut_user=$_SESSION['USUA_rut'];
            $id_user=$_SESSION['USUA_idUsuario'];

            $consulta = "call rapsinet_sprechazasoleva($id_estado,$cabecera,$id_user,$id_depto,'$nombres_user','$apellidos_user','$rut_user','$observacion');";
            $mensaje = "Solicitud Rechazada";
            
             try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

           return $mensaje; 

    }


    function confirmar_ET_primera_llamada($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spprimerallamadaET("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
    
    function confirmar_ET_segunda_llamada($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spsegundallamadaET("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
     function registrar_ET($idCabeceraSolicitud,$opcion)
    {
        $consulta = 'CALL rapsinet_spregistrarevaluacion("'.
                $idCabeceraSolicitud.'","'.
                $_SESSION['DEPART_id'].'","'.
                $_SESSION['USUA_nombres'].'","'.
                $_SESSION['USUA_apellidos'].'","'.
                $_SESSION['USUA_rut'].'","'.
                $_SESSION['USUA_idUsuario'].'","'.
                $opcion.'")'; 

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_array($resultado);
        return $mensaje;
        $conn->close();
        
    }
    
    function actualiza_soleva($id_cabecera,$form)
    {
       //recojo datos
            $fec_sol = $form['fec_sol']; $ficha = $form['ficha']; $fec_vis = $form['fec_vis'];
            $region = $form['region']; $provincia = $form['provincia']; $comuna = $form['comuna']; $hospital = $form['hospital'];
            $nombre_pac = utf8_decode($form['identidad']); $ap_pat = utf8_decode($form['ap_pat']); $ap_mat = utf8_decode($form['ap_mat']); $rut_pac = $form['rut']; 
            $edad_pac = $form['edad']; $sexo = $form['sexo']; $direccion = utf8_decode($form['dire']); $fono = $form['fono'];
            $rut_doc = $form['rut_doc']; $nom_doc = utf8_decode($form['nom_doc']); $pro_doc = utf8_decode($form['pro_doc']); $fono_doc = $form['fono_doc'];
            $mail_doc = $form['mail_doc'];$hospital_psi = $form['hospital_psi']; 

                $id_depto=$_SESSION['DEPART_id'];
                $nombres_user=$_SESSION['USUA_nombres'];
                $apellidos_user=$_SESSION['USUA_apellidos'];
                $rut_user=$_SESSION['USUA_rut'];
                $id_user=$_SESSION['USUA_idUsuario'];


            $consulta = "call rapsinet_spmodificasoltratamiento('$fec_sol',$ficha,'$fec_vis',$region,$provincia,$comuna,$hospital,'$nombre_pac','$ap_pat','$ap_mat','$rut_pac',$edad_pac,$sexo,'$direccion','$fono','$rut_doc','$nom_doc','$pro_doc','$fono_doc','$mail_doc',$id_depto,'$nombres_user','$apellidos_user','$rut_user',$id_user,$id_cabecera,$hospital_psi);";

            $mensaje = "Solicitud Modificada Correctamente";

          try{
            $conn = new MySQL;
            $conn->consulta($consulta);
            $conn->dispose($consulta);
            $conn->close(); 
               }
            catch(Exception $e){
            $mensaje = $e->getMessage();
            }  

           return $mensaje; 

    }
    

    function lista_archivos($clave)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_splistaadjuntossoleva($clave);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('nombre' => $resultado['ARCHAD_nombre'],
                'codigo' => $resultado['ARCHAD_id']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }



      function comentarios_solicitud($idCabecera)
      {
         $conn = new MySQL();
         $comentarios = array();
         $consulta = "call rapsinet_spmostrarcomentarios_eva($idCabecera);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($comentarios,array('codigo' => $resultado['COMINV_id'],
                'idcab' => $resultado['COMINV_idCabeceraSol'],
                'comentario' => $resultado['COMINV_comentario'],
                'fecha' => $resultado['COMINV_fecha']
            ));
         }   
         
         $conn->close();
         return $comentarios;
      }


       function eventos_solicitud($idCabecera,$tipoSolicitud)
      {
         $conn = new MySQL();
         $retorno = array();
         $consulta = "call rapsinet_spmostrarlogsolicitudeseventos($idCabecera,$tipoSolicitud);"; 
         $query = $conn->consulta($consulta);

         while($resultado = $conn->fetch_assoc($query))
         {
            array_push($retorno,array('codigo_log' => $resultado['LOGSOL_id'],
                'codigo' => $resultado['CABSOL_id'],
                'fecha' => $resultado['LOGSOL_fecha'],
                'nombres' => $resultado['USUA_nombres'],
                'apellidos' => $resultado['USUA_apellidos'],
                'departamento' => $resultado['DEP_descripcion'],
                'tipo' => $resultado['TIP_descripcion'],
                'estado' => $resultado['ESTA_descripcion'],
                'observaciones' => $resultado['LOGSOL_observaciones']

            ));
         }   
         
         $conn->close();
         return $retorno;
      }


}
?>