<?php
 include('MySQL.php');
   
    class DAOSolicitudAltaAdministrativa
    {


    function llena_medicos()
    {
        $arrmedicos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistamedicos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrmedicos, array('codigo' => $row['MED_id'],
                'nombre' => utf8_encode($row['nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrmedicos;
    } 
    

    function llena_establecimientos()
    {
        $arrEstablecimientos = array();
        $conn = new MySQL();
                  
         $consulta = "call rapsinet_splistaestablecimientos();"; 
         $resultado = $conn->consulta($consulta);

        while ($row = $conn->fetch_assoc($resultado)) {
            array_push($arrEstablecimientos, array('codigo' => $row['EST_id'],
                'nombre' => utf8_encode($row['EST_nombre'])
            ));
        }

        $conn->dispose($resultado);
        $conn->close();
        return $arrEstablecimientos;
    }
    
    function ingresa_solicitudaa($form)
    {
        $opciones = $form['opcion_a'].','.$form['opcion_b'].','.$form['opcion_c'];
        $sexo = $form['sexo'];
        //recojo datos
        $establecimiento = $form['establecimiento']; 
        $fec_ing = $form['fec_ing']; 
        $fec_egre = $form['fec_egre']; 
        $nombre_pac = $form['nombre_pac']; 
        $apaterno = $form['apaterno']; 
        $amaterno = $form['amaterno']; 
        $rut_pac = $form['rut_pac']; 
        $direccion_pac = $form['direccion_pac']; 
        $fono = $form['fono']; 
        $diagnostico = $form['diagnostico']; 
        $evolucion_clinica = $form['evolucion_clinica']; 
        $tratamiento = $form['tratamiento'];
        $centro_salud = $form['establecimiento2']; 
        $direccion_2 = $form['direccion_2']; 
        $medicos = $form['medicos']; 
        $fecha_ref = $form['fecha_ref']; 
        $medico_trat = $form['medicos2'];

        $id_depto=$_SESSION['DEPART_id'];
        $nombres_user=$_SESSION['USUA_nombres'];
        $apellidos_user=$_SESSION['USUA_apellidos'];
        $rut_user=$_SESSION['USUA_rut'];
        $id_user=$_SESSION['USUA_idUsuario'];

        $consulta = "call rapsinet_spingresasolicitudhospadmin('$establecimiento','$fec_ing','$fec_egre','$nombre_pac','$apaterno','$amaterno','$rut_pac','$direccion_pac',
        '$fono','$diagnostico','$evolucion_clinica','$tratamiento','$centro_salud','$direccion_2','$medicos','$fecha_ref',
        '$medico_trat', '$id_depto', '$nombres_user', '$apellidos_user', '$rut_user','$id_user','$opciones',$sexo);";
        

        $conn = new MySQL;
        $resultado = $conn->consulta($consulta);
        $mensaje = $conn->fetch_assoc($resultado);
        $conn->dispose($consulta);
        $conn->close();
        
        return $mensaje;

    }
    
    
    

    }
?>
