<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include("DAO/DAOVisacionInternacionAdministrativa.php");
   
    $xajax = new xajax(); 

    $xajax->registerFunction("rechazar_solicitud2");

    $xajax->processRequests(); 

    function rechazar_solicitud2($observacion_rechazo,$idCabeceraSolicitud,$EstadoSolicitud)
    { 
        
      $observacion = $observacion_rechazo['observacion'];  
      
      $xaprobarsolicitud = new xajaxResponse();
     
      
      $aprobar_solicitud = new DAOVisacionInternacionAdministrativa();
      $aprobarSolicitud = $aprobar_solicitud->rechazar_solicitud2($idCabeceraSolicitud,$observacion,$EstadoSolicitud);

      $xaprobarsolicitud->addAlert(utf8_encode($aprobarSolicitud['@mensaje']));
      $xaprobarsolicitud->addScript("opener.xajax_refresco();");
      $xaprobarsolicitud->addScript('window.close();');

      
      return $xaprobarsolicitud;
      
    }
    
    
    $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->display('popupObservaciones2.tpl');
?>
