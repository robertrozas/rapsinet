<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAODepartamentoHPSI.php';
   
   	
    
    $xajax = new xajax(); 

    $xajax->registerFunction("filtra_solinv");
    $xajax->registerFunction("filtra_solalta");
    $xajax->registerFunction("rakim_solinv");
    $xajax->registerFunction("rakim_solint");
    $xajax->registerFunction("rakim_solalta");
    $xajax->registerFunction("pdf_solinv");
    $xajax->registerFunction("pdf_solint");
    $xajax->registerFunction("visarsolET");
    $xajax->registerFunction("visarsolIA");
    
    
    
    $xajax->registerFunction("filtra_solevas");
    $xajax->registerFunction("filtra_solint");
    $xajax->registerFunction("lista_solicitudes");
    $xajax->registerFunction("filtra_solaa");
    $xajax->registerFunction("filtra_camas");
    $xajax->registerFunction("visarSolicitudAA");
    $xajax->registerFunction("actualizar_camas");
    $xajax->registerFunction("eliminar_camas");
    
    
    
    
        
    $xajax->processRequests(); 


      $lista = new DAODepartamentoHPSI();
      $camas = $lista->lista_camas2($_SESSION['EST_id']);
      $camasActuales = $camas['camasActuales'];
      $camasAsignadas = $camas['camasAsignadas'];
      $_SESSION['camasActuales'] = $camasActuales;
      $_SESSION['camasAsignadas'] = $camasAsignadas;
 
    
    function visarSolicitudAA($idCabeceraSolicitud,$ver)
    {
        
      $visarsolalta = new xajaxResponse();
      $visarsolalta->addAlert($idCabeceraSolicitud);
      $visarsolalta->addAlert($ver);
      $visar = new DAODepartamentoHPSI();
      $_SESSION['arrDetsolaa']=$visar->visar_solaa($idCabeceraSolicitud);
      
      $visarsolalta->redi_blank('VistaSolAltaHospitalizacion_hpsi.php?ver='.$ver);
      return $visarsolalta;
      
    }
    
     
    function filtra_solaa($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      $est_id = $_SESSION['EST_id'];
      
      $solaa = new xajaxResponse();
      $solaa->addAlert($est_id);
      $lista = new DAODepartamentoHPSI();
      $_SESSION['arrSolaa_hpsi']=$lista->lista_solaa($id_estado,$id_sol,$fecha,10,$est_id);
      
      $smarty->assign('departamentito',10);
      $tabla = $smarty->fetch('grilla_solAA_hpsi.tpl');
      $solaa->addAssign("altas","innerHTML",$tabla);
      $solaa->addScript("$('#solicitudesAltaAdministrativa').dataTable();");
      
      return $solaa;
    }
    
    
     function filtra_camas()
    {
      global $smarty;
      $est_id = $_SESSION['EST_id'];
      
      $solaa = new xajaxResponse();
      
      $lista = new DAODepartamentoHPSI();
      $_SESSION['arrAsignacioncamas']=$lista->lista_camas($est_id);
      $camas = $lista->lista_camas2($est_id);
      $camasActuales = $camas['camasActuales'];
      $camasAsignadas = $camas['camasAsignadas'];
      $_SESSION['camasActuales'] = $camasActuales;
      $_SESSION['camasAsignadas'] = $camasAsignadas;
      
      
      //$camasAsignadas = $camas['camasAsignadas'];
      $smarty->assign('departamentito',10);
      $tabla = $smarty->fetch('grilla_asignacioncamas.tpl');
      $solaa->addAssign("camas","innerHTML",$tabla);
      //$smarty->assign('camasActuales',$camasActuales);
      $solaa->addScript("$('#asignacionDeCamas').dataTable();");
      
      return $solaa;
    }
    
    
    
    
    function lista_solicitudes($idSolicitud,$idEstado,$fecha)
    {
        
       global $smarty; 
       $est_id = $_SESSION['EST_id']; 
       $idDepartamento = 10; 
       $idSolicitud = '';
       $idEstado = '';
       $fecha = '';
       $xsolicitudes = new xajaxResponse();
       $arr_solicitudes = new DAODepartamentoHPSI();
       $_SESSION['arrSolicitudesIA'] = $arr_solicitudes->lista_completa_solicitudes($idDepartamento,$idSolicitud,$idEstado,$fecha,$est_id);
       $smarty->assign('departamentito',10);
       $tabla = $smarty->fetch('grilla_solinternacionadmin_hpsi.tpl');
       
       $xsolicitudes->addAssign("solicitudes","innerHTML",$tabla);
       $xsolicitudes->addScript("$('#solicitudesInternacionAdmin_hpsi').dataTable();");
       return $xsolicitudes;
       
    }
    
    function actualizar_camas($form)
    {
        
       $camas = $form['camas_disponer']; 
       $xactualizarcamas = new xajaxResponse();
       //$xactualizarcamas->addAlert($camas);
       $est_id = $_SESSION['EST_id'];
       $ingresar_solicitud = new DAODepartamentoHPSI();
       $datosIngreso = $ingresar_solicitud->actualizar_camas($est_id,$camas);
       $xactualizarcamas->addAssign("camas_disponibles", "value", $camas);
       $xactualizarcamas->addAlert($datosIngreso['@mensaje']);
       $xactualizarcamas->addScript('document.formSol.camas_disponer.value = "";');
   
       return $xactualizarcamas;
       
    }
    
    
    function eliminar_camas($codigo)
    {
       $est_id = $_SESSION['EST_id']; 
       $xeliminarcamas = new xajaxResponse();
       //$xeliminarcamas->addAlert($codigo);
       $ingresar_solicitud = new DAODepartamentoHPSI();
       $datosIngreso = $ingresar_solicitud->eliminar_camas($codigo);
      

       $xeliminarcamas->addAlert($datosIngreso['@mensaje']);
       
       $camas = $ingresar_solicitud->lista_camas2($est_id);
       $camasActuales = $camas['camasActuales'];
       $camasAsignadas = $camas['camasAsignadas'];
     
       $xeliminarcamas->addAssign("camas_asignadas", "value", $camasAsignadas);
       $xeliminarcamas->addAssign("camas_disponibles", "value", $camasActuales);
       $xeliminarcamas->addScript("xajax_filtra_camas();");
       return $xeliminarcamas;
       
    }
    
  
    
     function pdf_solinv() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solinv_secre.php');
        return $pdf;
     }
     
     function pdf_solint() //invoco al pdf que me refleja la grilla solinv
     {  $pdf = new xajaxResponse();
        
        $pdf->redi_blank('fpdf/pdf_grilla_solint_secre.php');
        return $pdf;
     }
     
    function filtra_solevas($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      $solinv = new xajaxResponse();
    
      $est_id = $_SESSION['EST_id']; 
      //$solinv->addAlert($est_id);
      
      $idDepartamento = 10; 
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      
      $lista = new DAODepartamentoHPSI();
      $_SESSION['arrSolevasHPSI']=$lista->lista_solevasHPSI($id_sol,$id_estado,$fecha,$idDepartamento,$est_id);
      $smarty->assign('departamentito',10);
      $tabla = $smarty->fetch('grilla_solevaluacionytratamiento_hpsi.tpl');
      $solinv->addAssign("evaluaciones","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesEvaluacionesHPSI').dataTable();");
      
      return $solinv;
    }
    
    
    function visarsolIA($idCabeceraSolicitud,$ver)
    {
      $visarsolET = new xajaxResponse();
      $visarsolET->addAlert($idCabeceraSolicitud);
      $visarsolET->addAlert($ver);
      $visar = new DAODepartamentoHPSI();
      $_SESSION['arrDetsolIA']=$visar->visar_solIA($idCabeceraSolicitud);
      
      $visarsolET->redi_blank('VistaSolInternacionadmin_hpsi.php?ver='.$ver);
      return $visarsolET;
    } 
     
     
    function visarsolET($idCabeceraSolicitud,$ver)
    {
      $visarsolET = new xajaxResponse();
      $visarsolET->addAlert($idCabeceraSolicitud);
      $visarsolET->addAlert($ver);
      $visar = new DAODepartamentoHPSI();
      $_SESSION['arrDetsolET']=$visar->visar_solET($idCabeceraSolicitud);
      
      $visarsolET->redi_blank('VistaSolET_usmh.php?ver='.$ver);
      return $visarsolET;
    } 
   
     
    
   
    
    function filtra_solalta($id_sol,$id_estado,$fecha)
    {
      global $smarty;
      
      $id_sol = '';
      $id_estado = '';
      $fecha = '';
      
      $solinv = new xajaxResponse();
      $lista = new DAODepartamentoSecretaria();
      $_SESSION['arrSolalta']=$lista->lista_solalta($id_sol,$id_estado,$fecha,10);
      $smarty->assign('departamentito',10);
      $tabla = $smarty->fetch('grilla_solalta_secre.tpl');
      $solinv->addAssign("alta","innerHTML",$tabla);
      $solinv->addScript("$('#solicitudesAltaAdministrativa').dataTable({'sPaginationType': 'full_numbers','aaSorting': [[ 0, 'desc' ]],'bAutoWidth': false} );");
      
      return $solinv;
    }

 
   
   
    
   

       

    if(isset($_SESSION['USUA_nombres']))
    { $smarty->assign('USUA_nombres', $_SESSION['USUA_nombres']);
      $smarty->assign('USUA_apellidos', $_SESSION['USUA_apellidos']);
      $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
      $smarty->display('AdminHPSI.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
