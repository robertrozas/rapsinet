<?php
    //contador de inicio de sesion, utilizado para expirar la sesion despues
    //de 120 segundos inactivo
    
    session_start();
    $t = time();
    $t0 = $_SESSION['hora_inicio_sesion'];
    echo "<script>alert('.$t0.');</script>";
    $diff = $t - $t0;
    if ($diff > 15 || !ISSET ($t0))
    {
        session_unset();
        session_destroy();
        Header('Location: main_fin_sesion.php');
    }
    
    else
    {
        $_SESSION['time'] = time();
        $_SESSION['hora_inicio_sesion'] = time();
    }
?>
