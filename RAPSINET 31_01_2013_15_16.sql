-- --------------------------------------------------------
-- Host:                         192.168.0.103
-- Server version:               5.5.8-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4234
-- Date/time:                    2013-01-31 15:32:11
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rapsinet
DROP DATABASE IF EXISTS `rapsinet`;
CREATE DATABASE IF NOT EXISTS `rapsinet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rapsinet`;


-- Dumping structure for procedure rapsinet.asasfa
DROP PROCEDURE IF EXISTS `asasfa`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `asasfa`()
BEGIN
set @anio_actual = left(now(),4);
set @fecha_actual = left(now(),10);
set @diferencia = datediff(@fecha_actual,'2012-01-11');
set @mensaje = ("No cumple requisitos");

if (@diferencia > 365) then
set @mensaje = "Esta apto y se cargan dias";
end if;

select @anio_actual,@fecha_actual,@diferencia,@mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.prueba
DROP PROCEDURE IF EXISTS `prueba`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `prueba`(IN `ID_SOL` INT, IN `ID_TIPO` INT, IN `FECHA` VARCHAR(10))
BEGIN
	select
		logsol.LOGSOL_id,
		logsol.CABSOL_id,
		logsol.LOGSOL_fecha,
		logsol.LOGSOL_idUsuario,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		logsol.LOGSOL_idDepartamento,
		dep.DEP_descripcion,
		logsol.LOGSOL_idTipoSolicitud,
		tipsol.TIP_descripcion,
		logsol.LOGSOL_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		logsol.LOGSOL_observaciones
		from rapsinet_logsolicitudes logsol
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = logsol.LOGSOL_idUsuario)
		inner join rapsinet_departamentos dep
		on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
		inner join rapsinet_estadossolicitudes estsol
		on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
	where (LEFT(logsol.LOGSOL_fecha,10) = FECHA and
	logsol.LOGSOL_idEstadoSolicitud = ID_SOL and
	logsol.LOGSOL_idTipoSolicitud = ID_TIPO)
	OR
	(LEFT(logsol.LOGSOL_fecha,10) = FECHA and 
	logsol.LOGSOL_idEstadoSolicitud = ID_SOL AND ID_TIPO = "" 
	)
	OR
	(LEFT(logsol.LOGSOL_fecha,10) = FECHA and ID_SOL = "" AND
	 logsol.LOGSOL_idTipoSolicitud = ID_TIPO)
	 OR(
	logsol.LOGSOL_idEstadoSolicitud = ID_SOL and FECHA = "" AND
	logsol.LOGSOL_idTipoSolicitud = ID_TIPO)
	 OR
	 (LEFT(logsol.LOGSOL_fecha,10) = FECHA  and
	ID_SOL = "" and
	ID_TIPO = "")
	 OR(
	 FECHA = ""  and
	ID_TIPO = "" AND
	logsol.LOGSOL_idTipoSolicitud = ID_TIPO)
	OR
	(FECHA = ""  and 
	logsol.LOGSOL_idEstadoSolicitud = ID_SOL AND ID_TIPO = ""
	)
	OR(ID_SOL="" AND ID_TIPO="" AND FECHA ="");
END//
DELIMITER ;


-- Dumping structure for table rapsinet.rapsinet_acciones
DROP TABLE IF EXISTS `rapsinet_acciones`;
CREATE TABLE IF NOT EXISTS `rapsinet_acciones` (
  `ACC_id` bigint(20) NOT NULL DEFAULT '0',
  `ACC_descripcion` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ACC_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_acciones: ~22 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_acciones` DISABLE KEYS */;
INSERT INTO `rapsinet_acciones` (`ACC_id`, `ACC_descripcion`) VALUES
	(1, 'Envio Formulario Internacion No Voluntaria'),
	(2, 'Aprueba Formulario Internacion No Voluntaria'),
	(3, 'Rechazo Formulario Internacion No Voluntaria'),
	(4, 'Envio Formulario Internacion Administrativa'),
	(5, 'Aprueba Formulario Internacion Administrativa'),
	(6, 'Rechazo Formulario Internacion Administrativa'),
	(7, 'Edicion Formulario Internacion No Voluntaria'),
	(8, 'Edicion Formulario Internacion Administrativa'),
	(9, 'Envio Formulario Hospitalizacion Administrativa'),
	(10, 'Aprueba Formulario Hospitalizacion Administrativa'),
	(11, 'Rechazo Formulario Hospitalizacion Administrativa'),
	(12, 'Edicion Formulario Hospitalizacion Administrativa'),
	(13, 'Aprueba Formulario Evaluacion y Tratamiento'),
	(14, 'Edita Formulario Evaluacion y Tratamiento'),
	(15, 'Rechaza Formulario Evaluacion y Tratamiento'),
	(16, 'Realiza Primera Llamada '),
	(17, 'Realiza segunda Llamada'),
	(18, 'Cerrado Formulario Evaluación y Tratamiento'),
	(19, 'Cambio de Formulario Internación a Traslado'),
	(20, 'Registro Completo Formulario Evaluacion y Tratamiento'),
	(21, 'Registro Completo Formulario Internacion Administrativa'),
	(22, 'Registro Completo Formulario Alta Administrativa');
/*!40000 ALTER TABLE `rapsinet_acciones` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_adjuntos_soleva
DROP TABLE IF EXISTS `rapsinet_adjuntos_soleva`;
CREATE TABLE IF NOT EXISTS `rapsinet_adjuntos_soleva` (
  `ARCHAD_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPARCH_id` int(10) DEFAULT NULL,
  `CAB_id` int(10) DEFAULT NULL,
  `ARCHAD_fecha` datetime DEFAULT NULL,
  `ARCHAD_nombre` varchar(100) DEFAULT NULL,
  `ARCHAD_tipo` varchar(50) DEFAULT NULL,
  `ARCHAD_extension` varchar(50) DEFAULT NULL,
  `ARCHAD_archivo` mediumblob,
  PRIMARY KEY (`ARCHAD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_adjuntos_soleva: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_adjuntos_soleva` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_adjuntos_soleva` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_adjuntos_solint
DROP TABLE IF EXISTS `rapsinet_adjuntos_solint`;
CREATE TABLE IF NOT EXISTS `rapsinet_adjuntos_solint` (
  `ARCHAD_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPARCH_id` int(10) DEFAULT NULL,
  `CAB_id` int(10) DEFAULT NULL,
  `ARCHAD_fecha` datetime DEFAULT NULL,
  `ARCHAD_nombre` varchar(100) DEFAULT NULL,
  `ARCHAD_tipo` varchar(50) DEFAULT NULL,
  `ARCHAD_extension` varchar(50) DEFAULT NULL,
  `ARCHAD_archivo` mediumblob,
  PRIMARY KEY (`ARCHAD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_adjuntos_solint: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_adjuntos_solint` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_adjuntos_solint` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_archivosadjuntos
DROP TABLE IF EXISTS `rapsinet_archivosadjuntos`;
CREATE TABLE IF NOT EXISTS `rapsinet_archivosadjuntos` (
  `ARCHAD_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPARCH_id` int(10) DEFAULT NULL,
  `CAB_id` int(10) DEFAULT NULL,
  `ARCHAD_fecha` datetime DEFAULT NULL,
  `ARCHAD_nombre` varchar(100) DEFAULT NULL,
  `ARCHAD_tipo` varchar(50) DEFAULT NULL,
  `ARCHAD_extension` varchar(50) DEFAULT NULL,
  `ARCHAD_archivo` mediumblob,
  PRIMARY KEY (`ARCHAD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_archivosadjuntos: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_archivosadjuntos` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_archivosadjuntos` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_archivosadjuntosalta
DROP TABLE IF EXISTS `rapsinet_archivosadjuntosalta`;
CREATE TABLE IF NOT EXISTS `rapsinet_archivosadjuntosalta` (
  `ARCHAD_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `TIPARCH_id` int(10) DEFAULT NULL,
  `CAB_id` int(10) DEFAULT NULL,
  `ARCHAD_fecha` datetime DEFAULT NULL,
  `ARCHAD_nombre` varchar(100) DEFAULT NULL,
  `ARCHAD_tipo` varchar(100) DEFAULT NULL,
  `ARCHAD_extension` varchar(100) DEFAULT NULL,
  `ARCHAD_archivo` mediumblob,
  PRIMARY KEY (`ARCHAD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_archivosadjuntosalta: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_archivosadjuntosalta` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_archivosadjuntosalta` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_asignacion_camas
DROP TABLE IF EXISTS `rapsinet_asignacion_camas`;
CREATE TABLE IF NOT EXISTS `rapsinet_asignacion_camas` (
  `ASIGCAM_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ASIGCAM_fecha` varchar(50) DEFAULT NULL,
  `ASIGCAM_idEstablecimiento` int(10) DEFAULT NULL,
  `ASIGCAM_nombrePac` varchar(80) DEFAULT NULL,
  `ASIGCAM_apaternoPac` varchar(80) DEFAULT NULL,
  `ASIGCAM_amaternoPac` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`ASIGCAM_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_asignacion_camas: ~1 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_asignacion_camas` DISABLE KEYS */;
INSERT INTO `rapsinet_asignacion_camas` (`ASIGCAM_id`, `ASIGCAM_fecha`, `ASIGCAM_idEstablecimiento`, `ASIGCAM_nombrePac`, `ASIGCAM_apaternoPac`, `ASIGCAM_amaternoPac`) VALUES
	(1, '2013-01-28 14:41:57', 1, 'Cristian Eduardo', 'Perez', 'Araya');
/*!40000 ALTER TABLE `rapsinet_asignacion_camas` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_asistentessociales
DROP TABLE IF EXISTS `rapsinet_asistentessociales`;
CREATE TABLE IF NOT EXISTS `rapsinet_asistentessociales` (
  `ASSOC_id` int(10) NOT NULL AUTO_INCREMENT,
  `EST_id` int(10) DEFAULT NULL,
  `ASSOC_nombres` varchar(60) DEFAULT NULL,
  `ASSOC_apellidos` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`ASSOC_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_asistentessociales: ~1 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_asistentessociales` DISABLE KEYS */;
INSERT INTO `rapsinet_asistentessociales` (`ASSOC_id`, `EST_id`, `ASSOC_nombres`, `ASSOC_apellidos`) VALUES
	(1, 1, 'Paula', 'Morales Tardis');
/*!40000 ALTER TABLE `rapsinet_asistentessociales` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_cabeceraaltahospadmin
DROP TABLE IF EXISTS `rapsinet_cabeceraaltahospadmin`;
CREATE TABLE IF NOT EXISTS `rapsinet_cabeceraaltahospadmin` (
  `CABALTAHOSPADMIN_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CABALTAHOSPADMIN_idEstablecimiento` int(10) DEFAULT NULL,
  `CABALTAHOSPADMIN_fechaIngreso` date DEFAULT NULL,
  `CABALTAHOSPADMIN_fechaEgreso` date DEFAULT NULL,
  `CABALTAHOSPADMIN_fechaActual` datetime DEFAULT NULL,
  `CABALTAHOSPADMIN_nombrePac` varchar(50) DEFAULT NULL,
  `CABALTAHOSPADMIN_apaternoPac` varchar(50) DEFAULT NULL,
  `CABALTAHOSPADMIN_amaternoPac` varchar(50) DEFAULT NULL,
  `CABALTAHOSPADMIN_rutPac` varchar(50) DEFAULT NULL,
  `CABALTAHOSPADMIN_direccionPac` varchar(150) DEFAULT NULL,
  `CABALTAHOSPADMIN_fonoPac` int(15) DEFAULT NULL,
  `CABALTAHOSPADMIN_idEstadoSolicitud` int(11) DEFAULT NULL,
  `CABALTAHOSPADMIN_observaciones` varchar(200) DEFAULT NULL,
  `CABALTAHOSPADMIN_rakim` varchar(50) DEFAULT NULL,
  `CABALTAHOSPADMIN_sexo` int(10) DEFAULT NULL,
  PRIMARY KEY (`CABALTAHOSPADMIN_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_cabeceraaltahospadmin: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_cabeceraaltahospadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_cabeceraaltahospadmin` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_cabeceraevatratamiento
DROP TABLE IF EXISTS `rapsinet_cabeceraevatratamiento`;
CREATE TABLE IF NOT EXISTS `rapsinet_cabeceraevatratamiento` (
  `cab_solevatrat` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_sol` date DEFAULT NULL,
  `ficha` int(11) DEFAULT NULL,
  `fecha_visita` date DEFAULT NULL,
  `region` int(11) DEFAULT NULL,
  `provincia` int(11) DEFAULT NULL,
  `comuna` int(11) DEFAULT NULL,
  `hospital` int(11) DEFAULT NULL,
  `nombres_pac` varchar(120) DEFAULT NULL,
  `ap_pat` varchar(120) DEFAULT NULL,
  `ap_mat` varchar(120) DEFAULT NULL,
  `rut_pac` varchar(12) DEFAULT NULL,
  `edad_pac` int(11) DEFAULT NULL,
  `sexo_pac` int(1) DEFAULT NULL,
  `dire_pac` varchar(120) DEFAULT NULL,
  `fono_pac` varchar(10) DEFAULT NULL,
  `estado_sol` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `rakim_eva` varchar(50) DEFAULT NULL,
  `obser_soleva` varchar(120) DEFAULT NULL,
  `solicitud_traslado` int(10) DEFAULT NULL,
  PRIMARY KEY (`cab_solevatrat`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_cabeceraevatratamiento: ~1 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_cabeceraevatratamiento` DISABLE KEYS */;
INSERT INTO `rapsinet_cabeceraevatratamiento` (`cab_solevatrat`, `fecha_sol`, `ficha`, `fecha_visita`, `region`, `provincia`, `comuna`, `hospital`, `nombres_pac`, `ap_pat`, `ap_mat`, `rut_pac`, `edad_pac`, `sexo_pac`, `dire_pac`, `fono_pac`, `estado_sol`, `id_usuario`, `rakim_eva`, `obser_soleva`, `solicitud_traslado`) VALUES
	(1, '2013-01-30', 1, '2013-01-31', 5, 13, 45, 1, 'Ricardo Andres', 'Mesa', 'Soto', 'asdfsdfa', 25, 1, 'sdafsdfsa', '452345234', 5, 2, NULL, NULL, 0),
	(2, '2013-01-31', 2, '2013-02-08', 5, 13, 53, 16, 'Ricardo Alejandro', 'Mesa', 'Araya', '17948622-9', 24, 0, 'Mirador reñaca', '2484082', 5, 2, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rapsinet_cabeceraevatratamiento` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_cabecerainternacionadmin
DROP TABLE IF EXISTS `rapsinet_cabecerainternacionadmin`;
CREATE TABLE IF NOT EXISTS `rapsinet_cabecerainternacionadmin` (
  `CABINTADMIN_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CABINTADMIN_fechaEvaluacion` datetime NOT NULL,
  `CABINTADMIN_idEstablecimientoSol` int(10) NOT NULL,
  `CABINTADMIN_idEstadoSolicitud` int(10) NOT NULL,
  `CABINTADMIN_idUsuario` int(10) DEFAULT NULL,
  `CABINTADMIN_observaciones` varchar(500) DEFAULT NULL,
  `CABINTADMIN_rakim` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CABINTADMIN_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_cabecerainternacionadmin: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_cabecerainternacionadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_cabecerainternacionadmin` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_cabecerasolicitudinv
DROP TABLE IF EXISTS `rapsinet_cabecerasolicitudinv`;
CREATE TABLE IF NOT EXISTS `rapsinet_cabecerasolicitudinv` (
  `CABSOLINV_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CABSOLINV_rutPaciente` varchar(12) NOT NULL,
  `CABSOLINV_nombresPaciente` varchar(120) NOT NULL,
  `CABSOLINV_apellidosPaciente` varchar(120) NOT NULL,
  `CABSOLINV_direccionPaciente` varchar(120) NOT NULL,
  `CABSOLINV_fechaNacPaciente` date NOT NULL,
  `CABSOLINV_fechaInternacion` date NOT NULL,
  `CABSOLINV_fechaSolicitud` date NOT NULL,
  `CABSOLINV_idServicio` int(11) NOT NULL,
  `CABSOLINV_idEstablecimiento` int(11) NOT NULL,
  `CABSOLINV_idMedico` int(11) NOT NULL,
  `CABSOLINV_estadoSolicitud` int(11) NOT NULL DEFAULT '1',
  `CABSOLINV_observacion` varchar(200) DEFAULT NULL,
  `CABSOLINV_rakim` varchar(50) DEFAULT NULL,
  `CABSOLINV_sexo` int(10) DEFAULT NULL,
  PRIMARY KEY (`CABSOLINV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_cabecerasolicitudinv: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_cabecerasolicitudinv` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_cabecerasolicitudinv` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_comentarios_alta
DROP TABLE IF EXISTS `rapsinet_comentarios_alta`;
CREATE TABLE IF NOT EXISTS `rapsinet_comentarios_alta` (
  `COMALTA_id` int(10) NOT NULL AUTO_INCREMENT,
  `COMALTA_idCabeceraSol` int(10) DEFAULT NULL,
  `COMALTA_comentario` varchar(150) DEFAULT NULL,
  `COMALTA_fecha` date DEFAULT NULL,
  PRIMARY KEY (`COMALTA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_comentarios_alta: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_comentarios_alta` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_comentarios_alta` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_comentarios_inv
DROP TABLE IF EXISTS `rapsinet_comentarios_inv`;
CREATE TABLE IF NOT EXISTS `rapsinet_comentarios_inv` (
  `COMINV_id` int(10) NOT NULL AUTO_INCREMENT,
  `COMINV_idCabeceraSol` int(10) DEFAULT NULL,
  `COMINV_comentario` varchar(10) DEFAULT NULL,
  `COMINV_fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`COMINV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_comentarios_inv: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_comentarios_inv` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_comentarios_inv` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_comentarios_sia
DROP TABLE IF EXISTS `rapsinet_comentarios_sia`;
CREATE TABLE IF NOT EXISTS `rapsinet_comentarios_sia` (
  `COMSIA_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMSIA_idCabeceraSol` int(10) DEFAULT NULL,
  `COMSIA_comentario` varchar(300) DEFAULT NULL,
  `COMSIA_fecha` date DEFAULT NULL,
  PRIMARY KEY (`COMSIA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_comentarios_sia: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_comentarios_sia` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_comentarios_sia` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_comentarios_soleva
DROP TABLE IF EXISTS `rapsinet_comentarios_soleva`;
CREATE TABLE IF NOT EXISTS `rapsinet_comentarios_soleva` (
  `COMINV_id` int(10) NOT NULL AUTO_INCREMENT,
  `COMINV_idCabeceraSol` int(10) DEFAULT NULL,
  `COMINV_comentario` varchar(10) DEFAULT NULL,
  `COMINV_fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`COMINV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_comentarios_soleva: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_comentarios_soleva` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_comentarios_soleva` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_comunas
DROP TABLE IF EXISTS `rapsinet_comunas`;
CREATE TABLE IF NOT EXISTS `rapsinet_comunas` (
  `COM_id` int(11) NOT NULL AUTO_INCREMENT,
  `COM_nombre` varchar(80) NOT NULL,
  `rapsinet_provincias_PROV_id` int(11) NOT NULL,
  PRIMARY KEY (`COM_id`),
  KEY `fk_rapsinet_comunas_rapsinet_provincias1_idx` (`rapsinet_provincias_PROV_id`),
  CONSTRAINT `fk_rapsinet_comunas_rapsinet_provincias1` FOREIGN KEY (`rapsinet_provincias_PROV_id`) REFERENCES `rapsinet_provincias` (`PROV_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=348 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_comunas: ~347 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_comunas` DISABLE KEYS */;
INSERT INTO `rapsinet_comunas` (`COM_id`, `COM_nombre`, `rapsinet_provincias_PROV_id`) VALUES
	(1, 'ARICA', 1),
	(2, 'CAMARONES', 1),
	(3, 'PUTRE', 2),
	(4, 'GENERAL LAGOS', 2),
	(5, 'IQUIQUE', 3),
	(6, 'ALTO HOSPICIO', 3),
	(7, 'HUARA', 3),
	(8, 'CAMIÑA', 3),
	(9, 'COLCHANE', 3),
	(10, 'PICA', 3),
	(11, 'POZO ALMONTE', 3),
	(12, 'TOCOPILLA', 4),
	(13, 'MARÍA ELENA', 4),
	(14, 'CALAMA', 5),
	(15, 'OLLAGUE', 5),
	(16, 'SAN PEDRO DE ATACAMA', 5),
	(17, 'ANTOFAGASTA', 6),
	(18, 'MEJILLONES', 6),
	(19, 'SIERRA GORDA', 6),
	(20, 'TALTAL', 6),
	(21, 'CHAÑARAL', 7),
	(22, 'DIEGO DE ALMAGRO', 7),
	(23, 'COPIAPÓ', 8),
	(24, 'CALDERA', 8),
	(25, 'TIERRA AMARILLA', 8),
	(26, 'VALLENAR', 9),
	(27, 'FREIRINA', 9),
	(28, 'HUASCO', 9),
	(29, 'ALTO DEL CARMEN', 9),
	(30, 'LA SERENA', 10),
	(31, 'LA HIGUERA', 10),
	(32, 'COQUIMBO', 10),
	(33, 'ANDACOLLO', 10),
	(34, 'VICUÑA', 10),
	(35, 'PAIHUANO', 10),
	(36, 'OVALLE', 11),
	(37, 'RÍO HURTADO', 11),
	(38, 'MONTE PATRIA', 11),
	(39, 'COMBARBALÁ', 11),
	(40, 'PUNITAQUI', 11),
	(41, 'ILLAPEL', 12),
	(42, 'SALAMANCA', 12),
	(43, 'LOS VILOS', 12),
	(44, 'CANELA', 12),
	(45, 'VALPARAÍSO', 13),
	(46, 'CASABLANCA', 13),
	(47, 'CONCON', 13),
	(48, 'JUAN FERNÁNDEZ', 13),
	(49, 'PUCHUNCAVÍ', 13),
	(50, 'QUILPUÉ', 13),
	(51, 'QUINTERO', 13),
	(52, 'VILLA ALEMANA', 13),
	(53, 'VIÑA DEL MAR', 13),
	(54, 'PETORCA', 14),
	(55, 'LA LIGUA', 14),
	(56, 'CABILDO', 14),
	(57, 'PAPUDO', 14),
	(58, 'ZAPALLAR', 14),
	(59, 'LOS ANDES', 15),
	(60, 'SAN ESTEBAN', 15),
	(61, 'CALLE LARGA', 15),
	(62, 'RINCONADA', 15),
	(63, 'SAN FELIPE', 16),
	(64, 'CATEMU', 16),
	(65, 'LLAY LLAY', 16),
	(66, 'PANQUEHUE', 16),
	(67, 'PUTAENDO', 16),
	(68, 'SANTA MARÍA', 16),
	(69, 'QUILLOTA', 17),
	(70, 'CALERA', 17),
	(71, 'HIJUELAS', 17),
	(72, 'LIMACHE', 17),
	(73, 'LA CRUZ', 17),
	(74, 'NOGALES', 17),
	(75, 'OLMUÉ', 17),
	(76, 'SAN ANTONIO', 18),
	(77, 'ALGARROBO', 18),
	(78, 'CARTAGENA', 18),
	(79, 'EL QUISCO', 18),
	(80, 'EL TABO', 18),
	(81, 'SANTO DOMINGO', 18),
	(82, 'ISLA DE PASCUA', 19),
	(83, 'RANCAHUA', 20),
	(84, 'CODEGUA', 20),
	(85, 'COINCO', 20),
	(86, 'COLTAUCO', 20),
	(87, 'DOÑIHUE', 20),
	(88, 'GRANEROS', 20),
	(89, 'LAS CABRAS', 20),
	(90, 'MOSTAZAL', 20),
	(91, 'MACHALÍ', 20),
	(92, 'MALLOA', 20),
	(93, 'OLIVAR', 20),
	(94, 'PEUMO', 20),
	(95, 'PICHIDEGUA', 20),
	(96, 'QUINTA DE TILCOCO', 20),
	(97, 'RENGO', 20),
	(98, 'REQUINOA', 20),
	(99, 'SAN VICENTE', 20),
	(100, 'SAN FERNANDO', 21),
	(101, 'CHÉPICA', 21),
	(102, 'CHIMBARONGO', 21),
	(103, 'LOLOL', 21),
	(104, 'NANCAHUA', 21),
	(105, 'PALMILLA', 21),
	(106, 'PERALILLO', 21),
	(107, 'PLACILLA', 21),
	(108, 'PUMANQUE', 21),
	(109, 'SANTA CRUZ', 21),
	(110, 'PICHILEMU', 22),
	(111, 'LA ESTRELLA', 22),
	(112, 'LITUECHE', 22),
	(113, 'MARCHIGUE', 22),
	(114, 'NAVIDAD', 22),
	(115, 'PAREDONES', 22),
	(116, 'CURICÓ', 23),
	(117, 'TENO', 23),
	(118, 'ROMERAL', 23),
	(119, 'MOLINA', 23),
	(120, 'SAGRADA FAMILIA', 23),
	(121, 'HUALAÑÉ', 23),
	(122, 'LICANTÉN', 23),
	(123, 'VICHUQUÉN', 23),
	(124, 'RAUCO', 23),
	(125, 'TALCA', 24),
	(126, 'PELARCO', 24),
	(127, 'RÍO CLARO', 24),
	(128, 'SAN CLEMENTE', 24),
	(129, 'MAULE', 24),
	(130, 'SAN RAFAEL', 24),
	(131, 'EMPEDRADO', 24),
	(132, 'PENCAHUE', 24),
	(133, 'CONSTITUCIÓN', 24),
	(134, 'CUREPTO', 24),
	(135, 'LINARES', 25),
	(136, 'YERBAS BUENAS', 25),
	(137, 'COLBÚN', 25),
	(138, 'LONGAVÍ', 25),
	(139, 'PARRAL', 25),
	(140, 'RETIRO', 25),
	(141, 'VILLA ALEGRE', 25),
	(142, 'SAN JAVIER', 25),
	(143, 'CAUQUENES', 26),
	(144, 'PELUHUE', 26),
	(145, 'CHANCO', 26),
	(146, 'CHILLÁN', 27),
	(147, 'BULNES', 27),
	(148, 'CHILLAN VIEJO', 27),
	(149, 'COBQUECURA', 27),
	(150, 'COELEMU', 27),
	(151, 'COIHUECO', 27),
	(152, 'EL CARMEN', 27),
	(153, 'NINHUE', 27),
	(154, 'ÑIQUÉN', 27),
	(155, 'PEMUCO', 27),
	(156, 'PINTO', 27),
	(157, 'PORTEZUELO', 27),
	(158, 'QUILLÓN', 27),
	(159, 'QUIRIHUE', 27),
	(160, 'RANQUIL', 27),
	(161, 'SAN CARLOS', 27),
	(162, 'SAN FABIÁN', 27),
	(163, 'SAN IGNACIO', 27),
	(164, 'SAN NICOLÁS', 27),
	(165, 'TREHUACO', 27),
	(166, 'YUNGAY', 27),
	(167, 'LOS ANGELES', 28),
	(168, 'ALTO BIO BIO', 28),
	(169, 'ANTUCO', 28),
	(170, 'CABRERO', 28),
	(171, 'LAJA', 28),
	(172, 'MULCHÉN', 28),
	(173, 'NACIMIENTO', 28),
	(174, 'NEGRETE', 28),
	(175, 'QUILACO', 28),
	(176, 'QUILLECO', 28),
	(177, 'SANTA BÁRBARA', 28),
	(178, 'SAN ROSENDO', 28),
	(179, 'TUCAPEL', 28),
	(180, 'YUMBEL', 28),
	(181, 'CONCEPCIÓN', 29),
	(182, 'CHIGUAYANTE', 29),
	(183, 'CORONEL', 29),
	(184, 'FLORIDA', 29),
	(185, 'HUALPÉN', 29),
	(186, 'HUALQUI', 29),
	(187, 'LOTA', 29),
	(188, 'PENCO', 29),
	(189, 'SAN PEDRO DE LA PAZ', 29),
	(190, 'SANTA JUANA', 29),
	(191, 'TALCAHUANO', 29),
	(192, 'TOMÉ', 29),
	(193, 'ARAUCO', 30),
	(194, 'CAÑETE', 30),
	(195, 'CONTULMO', 30),
	(196, 'CURANILAHUE', 30),
	(197, 'LEBU', 30),
	(198, 'LOS ALAMOS', 30),
	(199, 'TIRUA', 30),
	(200, 'ANGOL', 31),
	(201, 'COLLIPULLI', 31),
	(202, 'CURACAUTÍN', 31),
	(203, 'ERCILLA', 31),
	(204, 'LONQUIMAY', 31),
	(205, 'LOS SAUCES', 31),
	(206, 'LUMACO', 31),
	(207, 'PURÉN', 31),
	(208, 'REINACO', 31),
	(209, 'TRAIGUÉN', 31),
	(210, 'VICTORIA', 31),
	(211, 'TEMUCO', 32),
	(212, 'CARAHUE', 32),
	(213, 'CHOLCHOL', 32),
	(214, 'CUNCO', 32),
	(215, 'CURARREHUE', 32),
	(216, 'FREIRE', 32),
	(217, 'GALVARINO', 32),
	(218, 'GORBEA', 32),
	(219, 'LAUTARO', 32),
	(220, 'LONCOCHE', 32),
	(221, 'MELIPEUCO', 32),
	(222, 'NUEVA IMPERIAL', 32),
	(223, 'PADRE LAS CASAS', 32),
	(224, 'PERQUENCO', 32),
	(225, 'PITRUFQUÉN', 32),
	(226, 'PUCÓN', 32),
	(227, 'SAAVEDRA', 32),
	(228, 'TEODORO SCHMIDT', 32),
	(229, 'TOLTÉN', 32),
	(230, 'VILCÚN', 32),
	(231, 'VILLARICA', 32),
	(232, 'OSORNO', 33),
	(233, 'PUERTO OCTAY', 33),
	(234, 'PURRANQUE', 33),
	(235, 'PUYEHUE', 33),
	(236, 'RÍO NEGRO', 33),
	(237, 'SAN JUAN DE LA COSTA', 33),
	(238, 'SAN PABLO', 33),
	(239, 'CALBUCO', 34),
	(240, 'COCHAMÓ', 34),
	(241, 'FRESIA', 34),
	(242, 'FRUTILLAR', 34),
	(243, 'LOS MUERMOS', 34),
	(244, 'LLANQUIHUE', 34),
	(245, 'MAULLÍN', 34),
	(246, 'PUERTO MONTT', 34),
	(247, 'PUERTO VARAS', 34),
	(248, 'CHILOÉ', 35),
	(249, 'ANCUD', 35),
	(250, 'CASTRO', 35),
	(251, 'CURACO DE VÉLEZ', 35),
	(252, 'CHONCHI', 35),
	(253, 'DALCAHUE', 35),
	(254, 'PUQUELDÓN', 35),
	(255, 'QUEILÉN', 35),
	(256, 'QUELLÓN', 35),
	(257, 'QUEMCHI', 35),
	(258, 'QUINCHAO', 35),
	(259, 'CHAITÉN', 36),
	(260, 'FUTALEUFÚ', 36),
	(261, 'HUALAIHUÉ', 36),
	(262, 'PALENA', 36),
	(263, 'COCHRANE', 37),
	(264, 'O\' HIGGINS', 37),
	(265, 'TORTEL', 37),
	(266, 'AYSÉN', 38),
	(267, 'CISNES', 38),
	(268, 'GUAITECAS', 38),
	(269, 'COIHAIQUE', 39),
	(270, 'LAGO VERDE', 39),
	(271, 'CHILE CHICO', 40),
	(272, 'RÍO IBAÑEZ', 40),
	(273, 'NATALES', 41),
	(274, 'TORRES DEL PAINE', 41),
	(275, 'PUNTA ARENAS', 42),
	(276, 'RÍO VERDE', 42),
	(277, 'LAGUNA BLANCA', 42),
	(278, 'SAN GREGORIO', 42),
	(279, 'PORVENIR', 43),
	(280, 'PRIMAVERA', 43),
	(281, 'TIMAUKEL', 43),
	(282, 'NAVARINO', 44),
	(283, 'ANTÁRTICA', 44),
	(284, 'CERRILLOS', 45),
	(285, 'CERRO NAVIA', 45),
	(286, 'CONCHALÍ', 45),
	(287, 'EL BOSQUE', 45),
	(288, 'ESTACIÓN CENTRAL', 45),
	(289, 'HUECHURABA', 45),
	(290, 'INDEPENDENCIA', 45),
	(291, 'LA CISTERNA', 45),
	(292, 'LA FLORIDA', 45),
	(293, 'LA GRANJA', 45),
	(294, 'LA PINTANA', 45),
	(295, 'LA REINA', 45),
	(296, 'LAS CONDES', 45),
	(297, 'LO BARNECHEA', 45),
	(298, 'LO ESPEJO', 45),
	(299, 'LO PRADO', 45),
	(300, 'MACÚL', 45),
	(301, 'MAIPÚ', 45),
	(302, 'ÑUÑOA', 45),
	(303, 'PEDRO AGUIRRE CERDA', 45),
	(304, 'PEÑALOLÉN', 45),
	(305, 'PROVIDENCIA', 45),
	(306, 'PUDAHUEL', 45),
	(307, 'QUILICURA', 45),
	(308, 'QUINTA NORMAL', 45),
	(309, 'RECOLETA', 45),
	(310, 'RENCA', 45),
	(311, 'SAN JOAQUÍN', 45),
	(312, 'SAN MIGUEL', 45),
	(313, 'SAN RAMÓN', 45),
	(314, 'SANTIAGO', 45),
	(315, 'VITACURA', 45),
	(316, 'PUENTE ALTO', 46),
	(317, 'PIRQUE', 46),
	(318, 'SAN JOSÉ DE MAIPO', 46),
	(319, 'MELIPILLA', 47),
	(320, 'MARÍA PINTO', 47),
	(321, 'CURACAVÍ', 47),
	(322, 'ALHUÉ', 47),
	(323, 'SAN PEDRO', 47),
	(324, 'TALAGANTE', 48),
	(325, 'EL MONTE', 48),
	(326, 'ISLA DE MAIPO', 48),
	(327, 'PADRE HURTADO', 48),
	(328, 'PEÑAFLOR', 48),
	(329, 'BUIN', 49),
	(330, 'CALERA DE TANGO', 49),
	(331, 'PAINE', 49),
	(332, 'SAN BERNARDO', 49),
	(333, 'COLINA', 50),
	(334, 'LAMPA', 50),
	(335, 'TIL TIL', 50),
	(336, 'VALDIVIA', 51),
	(337, 'CORRAL', 51),
	(338, 'LANCO', 51),
	(339, 'LOS LAGOS', 51),
	(340, 'MAFIL', 51),
	(341, 'MARIQUINA', 51),
	(342, 'PAILLACO', 51),
	(343, 'PANGUIPULLI', 51),
	(344, 'LA UNION', 52),
	(345, 'FUTRONO', 52),
	(346, 'LAGO RANCO', 52),
	(347, 'RÍO BUENO', 52);
/*!40000 ALTER TABLE `rapsinet_comunas` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_correos
DROP TABLE IF EXISTS `rapsinet_correos`;
CREATE TABLE IF NOT EXISTS `rapsinet_correos` (
  `CO_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CO_descripcion` varchar(100) DEFAULT NULL,
  `CO_mail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CO_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_correos: ~3 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_correos` DISABLE KEYS */;
INSERT INTO `rapsinet_correos` (`CO_id`, `CO_descripcion`, `CO_mail`) VALUES
	(1, 'Carabineros de Chile', 'carabineros@gmail.com'),
	(2, 'Seremi Salud', 'seremi@gmail.com'),
	(3, 'Contraloria', 'contraloria@gmail.com');
/*!40000 ALTER TABLE `rapsinet_correos` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_departamentos
DROP TABLE IF EXISTS `rapsinet_departamentos`;
CREATE TABLE IF NOT EXISTS `rapsinet_departamentos` (
  `DEP_id` int(10) NOT NULL AUTO_INCREMENT,
  `DEP_descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`DEP_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_departamentos: ~6 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_departamentos` DISABLE KEYS */;
INSERT INTO `rapsinet_departamentos` (`DEP_id`, `DEP_descripcion`) VALUES
	(1, 'Departamento Salud Publica'),
	(2, 'Departamento Juridica'),
	(3, 'Departamento Seremi'),
	(4, 'Departamento Contraloría'),
	(5, 'Departamento Informatica'),
	(6, 'Departamento Secretaria');
/*!40000 ALTER TABLE `rapsinet_departamentos` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_detallealtahospadmin
DROP TABLE IF EXISTS `rapsinet_detallealtahospadmin`;
CREATE TABLE IF NOT EXISTS `rapsinet_detallealtahospadmin` (
  `DETALTHOSPADMIN_id` int(10) NOT NULL AUTO_INCREMENT,
  `CABALTHOSPADMIN_id` int(10) DEFAULT NULL,
  `DETALTHOSPADMIN_diagnostico` varchar(200) DEFAULT NULL,
  `DETALTHOSPADMIN_evolucionClinica` varchar(200) DEFAULT NULL,
  `DETALTHOSPADMIN_tratamiento` varchar(200) DEFAULT NULL,
  `DETALTHOSPADMIN_idCentroSalud` int(10) DEFAULT NULL,
  `DETALTHOSPADMIN_direccionRef` varchar(50) DEFAULT NULL,
  `DETALTHOSPADMIN_idProfesionalMedico` int(50) DEFAULT NULL,
  `DETALTHOSPADMIN_fechaRef` date DEFAULT NULL,
  `DETALTHOSPADMIN_idMedicoTrat` int(50) DEFAULT NULL,
  `DETALTHOSPADMIN_opciones` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DETALTHOSPADMIN_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_detallealtahospadmin: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_detallealtahospadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_detallealtahospadmin` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_detalleevatratamiento
DROP TABLE IF EXISTS `rapsinet_detalleevatratamiento`;
CREATE TABLE IF NOT EXISTS `rapsinet_detalleevatratamiento` (
  `det_solevatrat` int(10) NOT NULL AUTO_INCREMENT,
  `cab_solevatrat` int(10) NOT NULL DEFAULT '0',
  `rut_doc` varchar(12) NOT NULL DEFAULT '0',
  `nom_doc` varchar(200) NOT NULL DEFAULT '0',
  `prof_doc` varchar(120) NOT NULL DEFAULT '0',
  `fono_doc` varchar(10) NOT NULL DEFAULT '0',
  `mail_doc` varchar(80) NOT NULL DEFAULT '0',
  `lugar_evaluacion` int(10) DEFAULT NULL,
  `det_opciones` varchar(50) DEFAULT NULL,
  `nom_sol` varchar(100) DEFAULT NULL,
  `apel_sol` varchar(100) DEFAULT NULL,
  `rut_sol` varchar(12) DEFAULT NULL,
  `edad_sol` int(11) DEFAULT NULL,
  `dir_sol` varchar(200) DEFAULT NULL,
  `reg_sol` int(11) DEFAULT NULL,
  `prov_sol` int(11) DEFAULT NULL,
  `com_sol` int(11) DEFAULT NULL,
  `sexo_sol` int(11) DEFAULT NULL,
  `vincula_sol` int(11) DEFAULT NULL,
  `quienSolicita` int(10) DEFAULT NULL,
  `hospital` int(10) DEFAULT NULL,
  PRIMARY KEY (`det_solevatrat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_detalleevatratamiento: ~1 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_detalleevatratamiento` DISABLE KEYS */;
INSERT INTO `rapsinet_detalleevatratamiento` (`det_solevatrat`, `cab_solevatrat`, `rut_doc`, `nom_doc`, `prof_doc`, `fono_doc`, `mail_doc`, `lugar_evaluacion`, `det_opciones`, `nom_sol`, `apel_sol`, `rut_sol`, `edad_sol`, `dir_sol`, `reg_sol`, `prov_sol`, `com_sol`, `sexo_sol`, `vincula_sol`, `quienSolicita`, `hospital`) VALUES
	(1, 1, 'dfsgsdfgdfsg', 'dsfgdfsg', 'dfsgdfsgsdfgdfs', '565463', 'dfgsdfgsdfg', 1, 'A,B,C', 'gvasdfgsdfg', 'sdfgdfsg', 'sdfgdfsgdf', 33, 'dfgdfsg', 5, 13, 45, 0, 7, 1, 2),
	(2, 2, '17948622-9', 'Jaime Soto', 'Doctor', '253698', 'jaime.s@gmail.com', 1, 'A,B,C', 'Mauricio Jaime', 'Valdenegro Arias', '17948622-9', 30, 'Miraflores 1555', 5, 13, 53, 0, 3, 1, 2),
	(3, 3, 'GDFGDFGD', 'DFGDFG', 'DFGD', 'DFGDFG', 'DFGDFGDFG', 1, 'A,,', 'dfgfdgd', 'dfgdfg', 'dfgddf', 30, 'dfgdfg', 5, 13, 53, 1, 1, 1, 3);
/*!40000 ALTER TABLE `rapsinet_detalleevatratamiento` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_detalleinternacionadmin
DROP TABLE IF EXISTS `rapsinet_detalleinternacionadmin`;
CREATE TABLE IF NOT EXISTS `rapsinet_detalleinternacionadmin` (
  `DETINTADMIN_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CABINTADMIN_id` int(10) DEFAULT NULL,
  `DETINTADMIN_opciones` varchar(10) DEFAULT NULL,
  `DETINTADMIN_diagnostico` varchar(500) NOT NULL,
  `DETINTADMIN_otroAntenedente` varchar(500) DEFAULT NULL,
  `DETINTADMIN_ultimoTratamiento` varchar(500) NOT NULL,
  `DETINTADMIN_idEstablecimientoInt` int(10) DEFAULT NULL,
  `DETINTADMIN_nombreMedico` varchar(50) DEFAULT NULL,
  `DETINTADMIN_rutMedico` varchar(15) DEFAULT NULL,
  `DETINTADMIN_correoMedico` varchar(60) DEFAULT NULL,
  `DETINTADMIN_aPaternoSol` varchar(50) DEFAULT NULL,
  `DETINTADMIN_aMaternoSol` varchar(50) DEFAULT NULL,
  `DETINTADMIN_nombresSol` varchar(50) DEFAULT NULL,
  `DETINTADMIN_rutSol` varchar(15) DEFAULT NULL,
  `DETINTADMIN_edadSol` varchar(2) DEFAULT NULL,
  `DETINTADMIN_sexoSol` char(1) DEFAULT NULL,
  `DETINTADMIN_calleSol` varchar(100) DEFAULT NULL,
  `DETINTADMIN_nCasaSol` varchar(20) DEFAULT NULL,
  `DETINTADMIN_sectorSol` varchar(40) DEFAULT NULL,
  `DETINTADMIN_idComunaSol` int(2) DEFAULT NULL,
  `DETINTADMIN_idCiudadSol` int(2) DEFAULT NULL,
  `DETINTADMIN_idRegionSol` int(2) DEFAULT NULL,
  `DETINTADMIN_telefonoSol` int(15) DEFAULT NULL,
  `DETINTADMIN_celularSol` int(15) DEFAULT NULL,
  `DETINTADMIN_correoSol` varchar(80) DEFAULT NULL,
  `DETINTADMIN_aPaternoPac` varchar(30) DEFAULT NULL,
  `DETINTADMIN_aMaternoPac` varchar(30) DEFAULT NULL,
  `DETINTADMIN_nombresPac` varchar(60) DEFAULT NULL,
  `DETINTADMIN_rutPac` varchar(15) DEFAULT NULL,
  `DETINTADMIN_edadPac` int(2) DEFAULT NULL,
  `DETINTADMIN_sexoPac` char(1) DEFAULT NULL,
  `DETINTADMIN_callePac` varchar(80) DEFAULT NULL,
  `DETINTADMIN_nCasaPac` varchar(50) DEFAULT NULL,
  `DETINTADMIN_sectorPac` varchar(60) DEFAULT NULL,
  `DETINTADMIN_idComunaPac` int(2) DEFAULT NULL,
  `DETINTADMIN_idCiudadPac` int(2) DEFAULT NULL,
  `DETINTADMIN_idRegionPac` int(2) DEFAULT NULL,
  `DETINTADMIN_vinculacion` int(100) DEFAULT NULL,
  `DETINTADMIN_quienSolicita` int(10) DEFAULT NULL,
  PRIMARY KEY (`DETINTADMIN_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_detalleinternacionadmin: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_detalleinternacionadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_detalleinternacionadmin` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_detallesolicitudinv
DROP TABLE IF EXISTS `rapsinet_detallesolicitudinv`;
CREATE TABLE IF NOT EXISTS `rapsinet_detallesolicitudinv` (
  `DETSOLINV_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CABSOLINV_id` bigint(20) NOT NULL,
  `DETSOLINV_opcion` varchar(45) NOT NULL,
  PRIMARY KEY (`DETSOLINV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_detallesolicitudinv: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_detallesolicitudinv` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_detallesolicitudinv` ENABLE KEYS */;


-- Dumping structure for procedure rapsinet.rapsinet_eliminaperfilfuncionalidad
DROP PROCEDURE IF EXISTS `rapsinet_eliminaperfilfuncionalidad`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_eliminaperfilfuncionalidad`(IN `PERFIL_ID` INT)
BEGIN
DELETE FROM RAPSINET_PERFILES_FUNCIONALIDADES WHERE PER_ID = PERFIL_ID; 
END//
DELIMITER ;


-- Dumping structure for table rapsinet.rapsinet_encargado_hospital
DROP TABLE IF EXISTS `rapsinet_encargado_hospital`;
CREATE TABLE IF NOT EXISTS `rapsinet_encargado_hospital` (
  `ENCHOSP_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ENCHOSP_idEstablecimiento` varchar(150) DEFAULT NULL,
  `ENCHOSP_nombreCompleto` varchar(250) DEFAULT NULL,
  `ENCHOSP_funcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ENCHOSP_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_encargado_hospital: ~2 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_encargado_hospital` DISABLE KEYS */;
INSERT INTO `rapsinet_encargado_hospital` (`ENCHOSP_id`, `ENCHOSP_idEstablecimiento`, `ENCHOSP_nombreCompleto`, `ENCHOSP_funcion`) VALUES
	(1, '1', 'JAIME JAMETT ROJAS', 'DIRECTOR'),
	(2, '2', 'CLAUDIO ARAYA MESA', 'SUBROGANTE');
/*!40000 ALTER TABLE `rapsinet_encargado_hospital` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_establecimientos
DROP TABLE IF EXISTS `rapsinet_establecimientos`;
CREATE TABLE IF NOT EXISTS `rapsinet_establecimientos` (
  `EST_id` int(10) NOT NULL AUTO_INCREMENT,
  `EST_nombre` varchar(80) DEFAULT NULL,
  `EST_ubicacion` varchar(120) DEFAULT NULL,
  `EST_fono` varchar(50) DEFAULT NULL,
  `EST_correo` varchar(100) DEFAULT NULL,
  `EST_psiquiatrico` tinyint(4) DEFAULT NULL,
  `EST_idComuna` int(10) DEFAULT NULL,
  `EST_idProvincia` int(10) DEFAULT NULL,
  `EST_idRegion` int(10) DEFAULT NULL,
  `ASSOL_id` varchar(200) DEFAULT NULL,
  `EST_camasDisponibles` int(10) DEFAULT NULL,
  `SSALUD_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`EST_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_establecimientos: ~24 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_establecimientos` DISABLE KEYS */;
INSERT INTO `rapsinet_establecimientos` (`EST_id`, `EST_nombre`, `EST_ubicacion`, `EST_fono`, `EST_correo`, `EST_psiquiatrico`, `EST_idComuna`, `EST_idProvincia`, `EST_idRegion`, `ASSOL_id`, `EST_camasDisponibles`, `SSALUD_id`) VALUES
	(1, 'Hospital del Salvador', 'Sub. Carvallo 200', '2281012', 'valpo', 1, 45, 13, 5, '1', 0, 3),
	(2, 'Hospital Dr. Gustavo Fricke', 'Alvarez 1532', '2675067', 'gustavo@fricke.cl', 0, 53, 13, 5, '1', 1, 3),
	(3, 'Hospital Dr. Moll de Cabildo', 'Zoila Gac y Aníbal Pinto', '761057', NULL, 0, 56, 14, 5, '1', 1, NULL),
	(4, 'Clínica Los Leones', 'Latorre Nº 98', '222630', NULL, 0, 70, 17, 5, '1', 4, NULL),
	(5, 'Hospital La Calera', 'Carrera 1603', '222196', NULL, 0, 70, 17, 5, '1', 1, NULL),
	(6, 'Hospital San José Casablanca', 'Yungay 124', '741003', NULL, 0, 46, 13, 5, '1', 1, NULL),
	(7, 'Clínica Los Carreras', 'Los Carreras 1298', '911425', NULL, 0, 50, 13, 5, '1', 1, NULL),
	(8, 'Hospital de Quilpué', 'San Martín 1270', '910445', NULL, 0, 50, 13, 5, '1', 1, NULL),
	(9, 'Clínica Dr. Manzur', 'Aníbal Pinto 818', '910295', NULL, 1, 50, 13, 5, '1', 1, NULL),
	(10, 'Hospital Alemán', 'G.Münich 203 Cº Alegre', '2217951', NULL, 0, 45, 13, 5, '1', 1, NULL),
	(11, 'Hospital Carlos Van Buren', 'San Ignacio 725', '2204000', NULL, 0, 45, 13, 5, '1', 1, NULL),
	(12, 'Hospital Clínico Barón', 'Diego Portales 449 Cº Barón', '2251213', NULL, 0, 45, 13, 5, '1', 1, NULL),
	(13, 'Hospital de la Seguridad', 'Av.Brasil 2350', '2234646', NULL, 0, 45, 13, 5, '1', 1, NULL),
	(14, 'Hospital de Valparaíso', 'Ibsen s/n', '2242524', NULL, 0, 45, 13, 5, '1', 1, NULL),
	(15, 'Clínica Ciudad del Mar', '13 Norte 635', '2451000', NULL, 0, 53, 13, 5, '1', 1, NULL),
	(16, 'Clínica Miraflores', 'Los Fresnos 276 Miradores', '2670897', 'clinica@miraflores.cl', 1, 53, 13, 5, '1', 1, 3),
	(17, 'Clínica Reñaca', 'Anabaena 336 Jardín del Mar', '2658000', 'aaaa', 2, 53, 13, 5, '1', 1, NULL),
	(18, 'Hospital de Niños', 'Limache 1667', '2675000', 'hospital@niÃ±os.cl', 2, 53, 13, 5, '1', 1, 2),
	(20, 'S. Marítimo S.J.de Dios', 'Avenida Atlántico Nº 4050 - 3º Sector- Gómez Carreño', '2540934', 'sanatorio@maritimo.cl', 0, 53, 13, 5, '1', 1, NULL),
	(21, 'Hospital Almirante Neff', 'Subida Alessandri s/n', '2573000', NULL, 0, 53, 13, 5, '1', 1, NULL),
	(22, 'Clínica San Antonio', 'Lauro Barros 163', '211237', 'clinica@sanantonio.cl', 0, 76, 18, 5, '1', 1, NULL),
	(23, 'Hospital San Agustín', 'Diego Portales 1020', '711032', 'hospital@sanagustin.cl', 0, 55, 14, 5, '1', 1, NULL),
	(24, 'Hospital Dr. Philippe Pinel', 'Jose Antonio Salinas Nº 2500', '(34)490600', 'dr@pinel.cl', 1, 67, 16, 5, NULL, NULL, 1),
	(25, 'Hospital San Martín de Quillota', 'La Concepción 1050', '33-298000', 'correo@quillota.cl', 2, 69, 17, 5, 'Juana de Arco', NULL, 1);
/*!40000 ALTER TABLE `rapsinet_establecimientos` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_estadossolicitudes
DROP TABLE IF EXISTS `rapsinet_estadossolicitudes`;
CREATE TABLE IF NOT EXISTS `rapsinet_estadossolicitudes` (
  `ESTA_id` bigint(20) NOT NULL DEFAULT '0',
  `ESTA_descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ESTA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_estadossolicitudes: ~16 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_estadossolicitudes` DISABLE KEYS */;
INSERT INTO `rapsinet_estadossolicitudes` (`ESTA_id`, `ESTA_descripcion`) VALUES
	(1, 'Por Visar DSP'),
	(2, 'Rechazado DSP'),
	(3, 'Por Visar Juridica'),
	(4, 'Rechazado Juridica'),
	(5, 'Por Visar Seremi'),
	(6, 'Rechazado Seremi'),
	(7, 'Por Visar Contraloria'),
	(8, 'Rechazado Contraloria'),
	(9, 'Por Visar USMH'),
	(10, 'Solicitud Cerrada'),
	(11, 'Primera Llamada telefonica '),
	(12, 'Segunda Llamada telefonica'),
	(13, 'Por Visar Hospital Psiquiatrico'),
	(14, 'Registro Completo ET'),
	(15, 'Registro Completo IA'),
	(16, 'Registro Completo AA');
/*!40000 ALTER TABLE `rapsinet_estadossolicitudes` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_funciones
DROP TABLE IF EXISTS `rapsinet_funciones`;
CREATE TABLE IF NOT EXISTS `rapsinet_funciones` (
  `FUNC_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `FUNC_descripcion` varchar(60) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FUNC_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_funciones: ~5 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_funciones` DISABLE KEYS */;
INSERT INTO `rapsinet_funciones` (`FUNC_id`, `FUNC_descripcion`) VALUES
	(1, 'Solicitudes'),
	(2, 'Bandejas'),
	(3, 'Mantenedores'),
	(4, 'Graficos'),
	(5, 'Reportes');
/*!40000 ALTER TABLE `rapsinet_funciones` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_historicousuarios
DROP TABLE IF EXISTS `rapsinet_historicousuarios`;
CREATE TABLE IF NOT EXISTS `rapsinet_historicousuarios` (
  `HIST_id` int(10) NOT NULL AUTO_INCREMENT,
  `HIST_nombres` varchar(100) DEFAULT NULL,
  `HIST_apellidos` varchar(100) DEFAULT NULL,
  `HIST_rut` varchar(15) DEFAULT NULL,
  `HIST_fecha` varchar(100) DEFAULT NULL,
  `HIST_idAccion` varchar(500) DEFAULT NULL,
  `HIST_idCabecera` int(10) DEFAULT NULL,
  `HIST_idTipoSolicitud` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`HIST_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_historicousuarios: ~4 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_historicousuarios` DISABLE KEYS */;
INSERT INTO `rapsinet_historicousuarios` (`HIST_id`, `HIST_nombres`, `HIST_apellidos`, `HIST_rut`, `HIST_fecha`, `HIST_idAccion`, `HIST_idCabecera`, `HIST_idTipoSolicitud`) VALUES
	(1, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-30 09:36:10', '13', 1, '2'),
	(2, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-30 09:36:23', '13', 1, '2'),
	(3, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-30 09:36:33', '13', 1, '2'),
	(4, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-30 10:45:46', '13', 1, '2'),
	(5, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-31 08:52:40', '13', 2, '2'),
	(6, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-31 08:53:35', '13', 2, '2'),
	(7, 'Cristian Eduardo', 'Perez Araya', '17948622-9', '2013-01-31 08:53:47', '13', 2, '2');
/*!40000 ALTER TABLE `rapsinet_historicousuarios` ENABLE KEYS */;


-- Dumping structure for procedure rapsinet.rapsinet_listatiposolicitudes
DROP PROCEDURE IF EXISTS `rapsinet_listatiposolicitudes`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_listatiposolicitudes`()
BEGIN
	select
	tip.TIP_id,
	tip.TIP_descripcion
	from rapsinet_tiposolicitudes tip;
END//
DELIMITER ;


-- Dumping structure for table rapsinet.rapsinet_logsolicitudes
DROP TABLE IF EXISTS `rapsinet_logsolicitudes`;
CREATE TABLE IF NOT EXISTS `rapsinet_logsolicitudes` (
  `LOGSOL_id` int(10) NOT NULL AUTO_INCREMENT,
  `CABSOL_id` int(10) DEFAULT NULL,
  `LOGSOL_fecha` varchar(50) DEFAULT NULL,
  `LOGSOL_idUsuario` int(10) DEFAULT NULL,
  `LOGSOL_idDepartamento` int(10) DEFAULT NULL,
  `LOGSOL_idTipoSolicitud` int(10) DEFAULT NULL,
  `LOGSOL_idEstadoSolicitud` int(10) DEFAULT NULL,
  `LOGSOL_observaciones` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`LOGSOL_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_logsolicitudes: ~4 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_logsolicitudes` DISABLE KEYS */;
INSERT INTO `rapsinet_logsolicitudes` (`LOGSOL_id`, `CABSOL_id`, `LOGSOL_fecha`, `LOGSOL_idUsuario`, `LOGSOL_idDepartamento`, `LOGSOL_idTipoSolicitud`, `LOGSOL_idEstadoSolicitud`, `LOGSOL_observaciones`) VALUES
	(1, 1, '2013-01-30 09:36:10', 2, 5, 2, 1, NULL),
	(2, 1, '2013-01-30 09:36:23', 2, 5, 2, 3, NULL),
	(3, 1, '2013-01-30 09:36:33', 2, 5, 2, 5, NULL),
	(4, 1, '2013-01-30 10:45:46', 2, 5, 2, 7, NULL),
	(5, 2, '2013-01-31 08:52:40', 2, 5, 2, 1, NULL),
	(6, 2, '2013-01-31 08:53:35', 2, 5, 2, 3, NULL),
	(7, 2, '2013-01-31 08:53:47', 2, 5, 2, 5, NULL);
/*!40000 ALTER TABLE `rapsinet_logsolicitudes` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_medicos
DROP TABLE IF EXISTS `rapsinet_medicos`;
CREATE TABLE IF NOT EXISTS `rapsinet_medicos` (
  `MED_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `MED_rut` varchar(15) DEFAULT NULL,
  `MED_nombres` varchar(50) DEFAULT NULL,
  `MED_apellidos` varchar(50) DEFAULT NULL,
  `MED_direccion` varchar(100) DEFAULT NULL,
  `MED_celular` varchar(15) DEFAULT NULL,
  `MED_fono` varchar(15) DEFAULT NULL,
  `MED_correo` varchar(60) DEFAULT NULL,
  `EST_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`MED_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_medicos: ~3 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_medicos` DISABLE KEYS */;
INSERT INTO `rapsinet_medicos` (`MED_id`, `MED_rut`, `MED_nombres`, `MED_apellidos`, `MED_direccion`, `MED_celular`, `MED_fono`, `MED_correo`, `EST_id`) VALUES
	(1, '15425628-5', 'Ricardo ', 'Mesa', 'Valparaiso', '8548475', '32584578', 'r.mesa@hospital.cl', 17),
	(5, '1-9', 'Ximena', 'Peralta', '7 Norte 1234', '555555', '33333', 'doctora@peralta.cl', 10),
	(6, '2-7', 'Juan', 'Carvajal', 'Miraflores 345', '6677666', '3523446', 'doctor@carvajal.cl', 17);
/*!40000 ALTER TABLE `rapsinet_medicos` ENABLE KEYS */;


-- Dumping structure for procedure rapsinet.rapsinet_mostrarlogsolicitudes_orden
DROP PROCEDURE IF EXISTS `rapsinet_mostrarlogsolicitudes_orden`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_mostrarlogsolicitudes_orden`(IN `ORDEN` INT)
BEGIN

IF ORDEN = 1 THEN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
	ORDER BY logsol.LOGSOL_fecha ASC;
	
END IF;

IF ORDEN = 2 THEN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
		ORDER BY logsol.LOGSOL_fecha DESC;
END IF;

END//
DELIMITER ;


-- Dumping structure for table rapsinet.rapsinet_parrafostexto
DROP TABLE IF EXISTS `rapsinet_parrafostexto`;
CREATE TABLE IF NOT EXISTS `rapsinet_parrafostexto` (
  `PARRTEX_id` int(10) NOT NULL DEFAULT '0',
  `PARRTEX_letra` char(1) NOT NULL DEFAULT '0',
  `PARRTEX_descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`PARRTEX_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_parrafostexto: ~3 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_parrafostexto` DISABLE KEYS */;
INSERT INTO `rapsinet_parrafostexto` (`PARRTEX_id`, `PARRTEX_letra`, `PARRTEX_descripcion`) VALUES
	(1, 'A', 'Necesidad de efectuar un diagnostico o evaluación clínica que no puede realizarse en forma ambulatoria.'),
	(2, 'B', 'Necesidad de incorporar a la persona a un plan de tratamiento que no es posible de llevarse a cabo de manera eficaz en forma ambulatoria, atendida la situacion de vida del sujeto.'),
	(3, 'C', 'Que el estado o condición psíquica o conductual de la persona representa un riesgo de daño físico, psíquico o psicosocial inminente, para sí mismo o para terceros. ');
/*!40000 ALTER TABLE `rapsinet_parrafostexto` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_perfiles
DROP TABLE IF EXISTS `rapsinet_perfiles`;
CREATE TABLE IF NOT EXISTS `rapsinet_perfiles` (
  `PER_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `PER_descripcion` varchar(60) NOT NULL,
  PRIMARY KEY (`PER_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_perfiles: ~10 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_perfiles` DISABLE KEYS */;
INSERT INTO `rapsinet_perfiles` (`PER_id`, `PER_descripcion`) VALUES
	(1, 'Admin DSP'),
	(2, 'Usuario DSP'),
	(3, 'Admin Juridica'),
	(4, 'GodMode'),
	(5, 'USUARIO Secretaria'),
	(6, 'USUARIO Juridica'),
	(7, 'USUARIO Seremi'),
	(8, 'USUARIO ContralorÃ­a'),
	(9, 'Usuario USMH'),
	(10, 'Usuario Psiquiatria');
/*!40000 ALTER TABLE `rapsinet_perfiles` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_perfiles_funcionalidades
DROP TABLE IF EXISTS `rapsinet_perfiles_funcionalidades`;
CREATE TABLE IF NOT EXISTS `rapsinet_perfiles_funcionalidades` (
  `PERFUNC_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `PER_id` int(11) NOT NULL,
  `FUNC_id` int(11) NOT NULL,
  PRIMARY KEY (`PERFUNC_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_perfiles_funcionalidades: ~33 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_perfiles_funcionalidades` DISABLE KEYS */;
INSERT INTO `rapsinet_perfiles_funcionalidades` (`PERFUNC_id`, `PER_id`, `FUNC_id`) VALUES
	(13, 1, 1),
	(14, 1, 2),
	(15, 1, 3),
	(16, 1, 4),
	(17, 1, 5),
	(18, 2, 1),
	(19, 2, 2),
	(20, 2, 5),
	(21, 3, 1),
	(22, 3, 2),
	(23, 3, 3),
	(24, 4, 1),
	(25, 4, 2),
	(26, 4, 3),
	(27, 4, 4),
	(28, 4, 5),
	(29, 5, 2),
	(30, 5, 4),
	(31, 5, 5),
	(32, 6, 1),
	(33, 6, 2),
	(34, 6, 5),
	(35, 7, 1),
	(36, 7, 2),
	(37, 7, 4),
	(38, 7, 5),
	(39, 8, 1),
	(40, 8, 2),
	(41, 8, 4),
	(42, 8, 5),
	(43, 9, 2),
	(44, 10, 2),
	(45, 10, 5);
/*!40000 ALTER TABLE `rapsinet_perfiles_funcionalidades` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_provincias
DROP TABLE IF EXISTS `rapsinet_provincias`;
CREATE TABLE IF NOT EXISTS `rapsinet_provincias` (
  `PROV_id` int(11) NOT NULL AUTO_INCREMENT,
  `PROV_nombre` varchar(80) NOT NULL,
  `rapsinet_regiones_REG_id` int(11) NOT NULL,
  PRIMARY KEY (`PROV_id`),
  KEY `fk_rapsinet_provincias_rapsinet_regiones1_idx` (`rapsinet_regiones_REG_id`),
  CONSTRAINT `fk_rapsinet_provincias_rapsinet_regiones1` FOREIGN KEY (`rapsinet_regiones_REG_id`) REFERENCES `rapsinet_regiones` (`REG_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_provincias: ~52 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_provincias` DISABLE KEYS */;
INSERT INTO `rapsinet_provincias` (`PROV_id`, `PROV_nombre`, `rapsinet_regiones_REG_id`) VALUES
	(1, 'ARICA', 15),
	(2, 'PARINACOTA', 15),
	(3, 'IQUIQUE', 1),
	(4, 'TOCOPILLA', 2),
	(5, 'EL LOA', 2),
	(6, 'ANTOFAGASTA', 2),
	(7, 'CHAÑARAL', 3),
	(8, 'COPIAPÓ', 3),
	(9, 'HUASCO', 3),
	(10, 'ELQUI', 4),
	(11, 'LIMARÍ', 4),
	(12, 'CHOAPA', 4),
	(13, 'VALPARAÍSO', 5),
	(14, 'PETORCA', 5),
	(15, 'LOS ANDES', 5),
	(16, 'SAN FELIPE DE ACONCAGUA', 5),
	(17, 'QUILLOTA', 5),
	(18, 'SAN ANTONIO', 5),
	(19, 'ISLA DE PASCUA', 5),
	(20, 'CACHAPOAL', 6),
	(21, 'COLCHAHUA', 6),
	(22, 'CARDENAL CARO', 6),
	(23, 'CURICÓ', 7),
	(24, 'TALCA', 7),
	(25, 'LINARES', 7),
	(26, 'CAUQUENES', 7),
	(27, 'ÑUBLE', 8),
	(28, 'BIO BIO', 8),
	(29, 'CONCEPCIÓN', 8),
	(30, 'ARAUCO', 8),
	(31, 'MALLECO', 9),
	(32, 'CAUTÍN', 9),
	(33, 'OSORNO', 10),
	(34, 'LLANQUIHUE', 10),
	(35, 'CHILOÉ', 10),
	(36, 'PALENA', 10),
	(37, 'CAPITÁN PRATT', 11),
	(38, 'AYSÉN', 11),
	(39, 'COIHAIQUE', 11),
	(40, 'GENERAL CARRERA', 11),
	(41, 'ÚLTIMA ESPERANZA', 12),
	(42, 'MAGALLANES', 12),
	(43, 'TIERRA DEL FUEGO', 12),
	(44, 'ANTÁRTICA CHILENA', 12),
	(45, 'SANTIAGO', 13),
	(46, 'CORDILLERA', 13),
	(47, 'MELIPILLA', 13),
	(48, 'TALAGANTE', 13),
	(49, 'MAIPO', 13),
	(50, 'CHACABUCO', 13),
	(51, 'VALDIVIA', 14),
	(52, 'RANCO', 14);
/*!40000 ALTER TABLE `rapsinet_provincias` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_regiones
DROP TABLE IF EXISTS `rapsinet_regiones`;
CREATE TABLE IF NOT EXISTS `rapsinet_regiones` (
  `REG_id` int(11) NOT NULL AUTO_INCREMENT,
  `REG_nombre` varchar(45) NOT NULL,
  `REG_romano` varchar(5) NOT NULL,
  `REG_titulo` varchar(80) NOT NULL,
  PRIMARY KEY (`REG_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_regiones: ~15 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_regiones` DISABLE KEYS */;
INSERT INTO `rapsinet_regiones` (`REG_id`, `REG_nombre`, `REG_romano`, `REG_titulo`) VALUES
	(1, 'PRIMERA', 'I', 'REGIÓN DE TARAPACÁ'),
	(2, 'SEGUNDA', 'II', 'REGIÓN DE ANTOFAGASTA'),
	(3, 'TERCERA', 'III', 'REGIÓN DE ATACAMA'),
	(4, 'CUARTA', 'IV', 'REGIÓN DE COQUIMBO'),
	(5, 'QUINTA', 'V', 'REGIÓN DE VALPARAISO'),
	(6, 'SEXTA', 'VI', 'REGIÓN DEL LIBERTADOR GENERAL BERNARDO O\'HIGGINS'),
	(7, 'SÉPTIMA', 'VII', 'REGIÓN DEL MAULE'),
	(8, 'OCTAVA', 'VIII', 'REGIÓN DEL BÍO - BÍO'),
	(9, 'NOVENA', 'IX', 'REGIÓN DE LA ARAUCANÍA'),
	(10, 'DÉCIMA', 'X', 'REGIÓN DE LOS LAGOS'),
	(11, 'DECIMOPRIMERA', 'XI', 'REGIÓN AYSÉN DEL GENERAL CARLOS IBÁÑEZ DEL CAMPO'),
	(12, 'DECIMOSEGUNDA', 'XII', 'REGIÓN DE MAGALLANES Y LA ANTÁRTICA CHILENA '),
	(13, 'METROPOLITANA', 'XIII', 'REGIÓN METROPOLITANA'),
	(14, 'DECIMOCUARTA', 'XIV', 'REGION DE LOS RÍOS'),
	(15, 'DECIMOQUINTA', 'XV', 'REGIÓN DE ARICA Y PARINACOTA');
/*!40000 ALTER TABLE `rapsinet_regiones` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_registro_aa
DROP TABLE IF EXISTS `rapsinet_registro_aa`;
CREATE TABLE IF NOT EXISTS `rapsinet_registro_aa` (
  `REGAA_id` int(10) NOT NULL AUTO_INCREMENT,
  `REGAA_idCabecera` int(10) DEFAULT NULL,
  `REGAA_fecha` datetime DEFAULT NULL,
  `USUA_id` int(10) DEFAULT NULL,
  `USUA_nombres` varchar(80) DEFAULT NULL,
  `USUA_apellidos` varchar(100) DEFAULT NULL,
  `DEP_id` int(10) DEFAULT NULL,
  `EST_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`REGAA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_registro_aa: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_registro_aa` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_registro_aa` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_registro_et
DROP TABLE IF EXISTS `rapsinet_registro_et`;
CREATE TABLE IF NOT EXISTS `rapsinet_registro_et` (
  `REGET_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `REGET_idCabecera` int(10) DEFAULT NULL,
  `REGET_fecha` datetime DEFAULT NULL,
  `USUA_id` int(10) DEFAULT NULL,
  `USUA_nombres` varchar(50) DEFAULT NULL,
  `USUA_apellidos` varchar(50) DEFAULT NULL,
  `DEP_id` int(11) DEFAULT NULL,
  `EST_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`REGET_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_registro_et: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_registro_et` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_registro_et` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_registro_ia
DROP TABLE IF EXISTS `rapsinet_registro_ia`;
CREATE TABLE IF NOT EXISTS `rapsinet_registro_ia` (
  `REGIA_id` int(10) NOT NULL AUTO_INCREMENT,
  `REGIA_idCabecera` int(10) DEFAULT NULL,
  `REGIA_fecha` datetime DEFAULT NULL,
  `USUA_id` int(10) DEFAULT NULL,
  `USUA_nombres` varchar(80) DEFAULT NULL,
  `USUA_apellidos` varchar(100) DEFAULT NULL,
  `DEP_id` int(10) DEFAULT NULL,
  `EST_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`REGIA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_registro_ia: ~0 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_registro_ia` DISABLE KEYS */;
/*!40000 ALTER TABLE `rapsinet_registro_ia` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_servicios
DROP TABLE IF EXISTS `rapsinet_servicios`;
CREATE TABLE IF NOT EXISTS `rapsinet_servicios` (
  `SERV_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `SERV_descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SERV_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_servicios: ~3 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_servicios` DISABLE KEYS */;
INSERT INTO `rapsinet_servicios` (`SERV_id`, `SERV_descripcion`) VALUES
	(1, 'Ambulatorio'),
	(2, 'Corto plazo'),
	(3, 'Largo plazo');
/*!40000 ALTER TABLE `rapsinet_servicios` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_servicios_de_salud
DROP TABLE IF EXISTS `rapsinet_servicios_de_salud`;
CREATE TABLE IF NOT EXISTS `rapsinet_servicios_de_salud` (
  `SSALUD_id` int(10) NOT NULL AUTO_INCREMENT,
  `SSALUD_descripcion` varchar(120) DEFAULT NULL,
  `SSALUD_abreviatura` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`SSALUD_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_servicios_de_salud: ~3 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_servicios_de_salud` DISABLE KEYS */;
INSERT INTO `rapsinet_servicios_de_salud` (`SSALUD_id`, `SSALUD_descripcion`, `SSALUD_abreviatura`) VALUES
	(1, 'Servicio Salud Aconcagua', 'SSA'),
	(2, 'Servicio Salud Viña-Quilpué', 'SSVQ'),
	(3, 'Servicio Salud Valparaíso-San Antonio', 'SSVSA');
/*!40000 ALTER TABLE `rapsinet_servicios_de_salud` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_sexo
DROP TABLE IF EXISTS `rapsinet_sexo`;
CREATE TABLE IF NOT EXISTS `rapsinet_sexo` (
  `SEX_id` int(10) NOT NULL AUTO_INCREMENT,
  `SEX_descripcion` char(1) DEFAULT NULL,
  `SEX_detalle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SEX_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_sexo: ~2 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_sexo` DISABLE KEYS */;
INSERT INTO `rapsinet_sexo` (`SEX_id`, `SEX_descripcion`, `SEX_detalle`) VALUES
	(1, 'M', 'Masculino'),
	(2, 'F', 'Femenino');
/*!40000 ALTER TABLE `rapsinet_sexo` ENABLE KEYS */;


-- Dumping structure for procedure rapsinet.rapsinet_spactualizarcamas
DROP PROCEDURE IF EXISTS `rapsinet_spactualizarcamas`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spactualizarcamas`(IN `idEstablecimiento` INT, IN `camas` INT)
BEGIN
	UPDATE RAPSINET_ESTABLECIMIENTOS SET EST_camasDisponibles = camas
	WHERE EST_id = idEstablecimiento;
	
	set @mensaje = 'Camas actualizadas correctamente.';
	
	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spadjuntossolalta
DROP PROCEDURE IF EXISTS `rapsinet_spadjuntossolalta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spadjuntossolalta`(IN `CLAVE` INT)
BEGIN
SELECT * FROM RAPSINET_ARCHIVOSADJUNTOSALTA
WHERE CAB_ID = CLAVE;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spadjuntossolint
DROP PROCEDURE IF EXISTS `rapsinet_spadjuntossolint`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spadjuntossolint`(IN `CLAVE` INT)
BEGIN
SELECT * FROM RAPSINET_ADJUNTOS_SOLINT
WHERE CAB_ID = CLAVE;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spaprobarsolicitudinternacionadmin
DROP PROCEDURE IF EXISTS `rapsinet_spaprobarsolicitudinternacionadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spaprobarsolicitudinternacionadmin`(IN `idSolicitud` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(100), IN `idUsuario` INT, IN `idEstadoDocumento` INT)
BEGIN

/*DSP*/

IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: POR VISAR JURIDICA*/
			 
			 set @idEstadoSolicitud = 3;
			
		 	 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABINTADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 3;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 5; /*APROBADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 5;
			
		 	 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABINTADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 5;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 5; /*APROBADO POR JURIDICA*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;
	
/*JSEREMI*/

IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 7;
			
		 	 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABINTADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 7;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 5; /*APROBADO POR SEREMI*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;	
	
IF idEstadoDocumento = 7 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 13;
			
		 	 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABINTADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 13;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 5; /*APROBADO POR SEREMI*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;		
	
	

	select @mensaje;
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spaprovarsolicitudaltaadministrativa
DROP PROCEDURE IF EXISTS `rapsinet_spaprovarsolicitudaltaadministrativa`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spaprovarsolicitudaltaadministrativa`(IN `idSolicitud` INT, IN `idDepartamento` INT, IN `nombresUsuario` vARCHAR(100), IN `apellidosUsuario` vARCHAR(100), IN `rutUsuario` vARCHAR(50), IN `idUsuario` INT, IN `idEstadoDocumento` INT)
BEGIN
/*DSP*/

IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: POR VISAR JURIDICA*/
			 
			 set @idEstadoSolicitud = 3;
			
		 	 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABALTAHOSPADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 3;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 10; /*APROBADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 5;
			
		 	 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABALTAHOSPADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 5;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 10; /*APROBADO POR JURIDICA*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;
	
/*SEREMI*/

IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 7;
			
		 	 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABALTAHOSPADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 7;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 10; /*APROBADO POR SEREMI*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;	
	
	
	/*SEREMI*/

	IF idEstadoDocumento = 7 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 13;
			
		 	 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud 
			 where  CABALTAHOSPADMIN_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 13;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 10; /*APROBADO POR SEREMI*/
			 	 
			 	 insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
	end if;	

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spaprovarsolicitudnovoluntaria
DROP PROCEDURE IF EXISTS `rapsinet_spaprovarsolicitudnovoluntaria`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spaprovarsolicitudnovoluntaria`(IN `idSolicitud` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(60), IN `apellidosUsuario` VARCHAR(60), IN `rutUsuario` VARCHAR(12), IN `idUsuario` INT, IN `idEstadoDocumento` INT)
BEGIN

/*DSP*/

 IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: POR VISAR JURIDICA*/
			 
			 SET @idEstadoSolicitud = 3;
			
		 	 UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 3;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 2; /*APROBADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 SET @mensaje = 'Visación aprobada exitósamente';
	
END IF;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 5;
			
		 	  UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 5;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 2; /*APROBADO POR JURIDICA*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
END IF;
	
/*SEREMI*/

	IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 7;
			
		 	  UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 7;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 2; /*APROBADO POR SEREMI*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
END IF;	

	/* SELECT @mensaje;  MENSAJE DE RETORNO QUE NOOOO ASEGURA QUE LA OPERACION HA TENIDO EXITO */
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spcomentarios_alta
DROP PROCEDURE IF EXISTS `rapsinet_spcomentarios_alta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spcomentarios_alta`(IN `CABECERA` INT, IN `COMENTARIO` VARCHAR(170))
BEGIN

	INSERT INTO RAPSINET_COMENTARIOS_ALTA(COMALTA_IDCABECERASOL,COMALTA_COMENTARIO,COMALTA_FECHA)
	VALUES(CABECERA,COMENTARIO,NOW());

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spcomentarios_inv
DROP PROCEDURE IF EXISTS `rapsinet_spcomentarios_inv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spcomentarios_inv`(IN `CABECERA` INT, IN `COMENTARIO` VARCHAR(150))
BEGIN

	INSERT INTO RAPSINET_COMENTARIOS_INV(COMINV_IDCABECERASOL,COMINV_COMENTARIO,COMINV_FECHA)
	VALUES(CABECERA,COMENTARIO,NOW());

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spcomentarios_sia
DROP PROCEDURE IF EXISTS `rapsinet_spcomentarios_sia`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spcomentarios_sia`(IN `CABECERA` INT, IN `COMENTARIO` VARCHAR(200))
BEGIN
INSERT INTO RAPSINET_COMENTARIOS_SIA(COMSIA_IDCABECERASOL,COMSIA_COMENTARIO,COMSIA_FECHA)
VALUES(CABECERA,COMENTARIO,NOW());
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spcomentarios_soleva
DROP PROCEDURE IF EXISTS `rapsinet_spcomentarios_soleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spcomentarios_soleva`(IN `CABECERA` INT, IN `COMENTARIO` VARCHAR(200))
BEGIN

	INSERT INTO RAPSINET_COMENTARIOS_SOLEVA(COMINV_IDCABECERASOL,COMINV_COMENTARIO,COMINV_FECHA)
	VALUES(CABECERA,COMENTARIO,NOW());

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spcomunasxprovincia
DROP PROCEDURE IF EXISTS `rapsinet_spcomunasxprovincia`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spcomunasxprovincia`(IN `ID_PROVINCIA` INT)
BEGIN
SELECT * FROM RAPSINET_COMUNAS
WHERE rapsinet_provincias_PROV_id = ID_PROVINCIA
Order By COM_nombre;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spconfirmarevaluacion
DROP PROCEDURE IF EXISTS `rapsinet_spconfirmarevaluacion`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spconfirmarevaluacion`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombreUsuario` vARCHAR(50), IN `apellidosUsuario` vARCHAR(50), IN `rutUsuario` vARCHAR(50), IN `idUsuario` INT, IN `opcion` INT)
BEGIN

IF opcion = 1 THEN

	set @camasDisponibles = (select est.EST_camasDisponibles from rapsinet_cabeceraevatratamiento cabeva
								inner join rapsinet_detalleevatratamiento deteva
								on(cabeva.cab_solevatrat = deteva.cab_solevatrat)
								inner join rapsinet_establecimientos est
								on est.EST_id = deteva.lugar_evaluacion
								where est.EST_psiquiatrico = 1 and cabeva.cab_solevatrat = idCabecera);
								
   set @establecimiento = (select est.EST_id from rapsinet_cabeceraevatratamiento cabeva
								inner join rapsinet_detalleevatratamiento deteva
								on(cabeva.cab_solevatrat = deteva.cab_solevatrat)
								inner join rapsinet_establecimientos est
								on est.EST_id = deteva.lugar_evaluacion
								where est.EST_psiquiatrico = 1 and cabeva.cab_solevatrat = idCabecera);
								
								
			/*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 11;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 16;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
							
	
	
	IF @camasDisponibles > 0 THEN
	
			set @resultado = @camasDisponibles - 1;
			
	  UPDATE rapsinet_establecimientos est SET EST_camasDisponibles= @resultado
     WHERE EST_id = @establecimiento;
     
     set @estadoSolicitud = 11; /*PASA A PRIMERA LLAMADA TELEFONICA*/
     
     UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @estadoSolicitud
     WHERE cab_solevatrat = idCabecera;
     
 	  set @nombrepac = (select nombres_pac from rapsinet_cabeceraevatratamiento where cab_solevatrat = idCabecera);
     set @apaternopac = (select ap_pat from rapsinet_cabeceraevatratamiento where cab_solevatrat = idCabecera);
     set @amaternopac = (select ap_mat from rapsinet_cabeceraevatratamiento where cab_solevatrat = idCabecera);
     
	  INSERT INTO rapsinet_asignacion_camas(ASIGCAM_fecha,ASIGCAM_idEstablecimiento,
     ASIGCAM_nombrePac,ASIGCAM_apaternoPac,ASIGCAM_amaternoPac)values(now(),
	  @establecimiento,@nombrepac,@apaternopac,@amaternopac);	
     
     
     		set @mensaje = concat("Se encontró cama disponible, cantidad de camas actual: ", @resultado);
			
	ELSE
	
			set @idEstadoSolicitud = 1; /*POR VISAR DSP*/
		   UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud,
		   solicitud_traslado = 1 /*Pasa a solicitud de traslado para la resolución final*/
     		WHERE cab_solevatrat = idCabecera;
     		
		    /*LOG*/
			 
			 set @tipoSolicitud = 7; /*SOLICITUD DE TRASLADO*/
			 
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			  set @idAccion = 19; /*CAMBIO DE FORMULARIO*/
				 	 
			  insert into rapsinet_historicousuarios(
			  HIST_nombres,HIST_apellidos,HIST_rut,
			  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
			  nombreUsuario, apellidosUsuario, rutUsuario,
			  now(), @idAccion, idCabecera, @tipoSolicitud); 
			
			
			  set @mensaje = "No hay camas disponibles, la solicitud cambiaba a Solicitud de Traslado";
	

	END IF;

END IF;

IF opcion = 0 THEN

		set @estadoSolicitud = 10;
		set @idCabecera = idCabecera;
	    
		UPDATE rapsinet_cabeceraevatratamiento SET estado_sol= @estadoSolicitud
	   WHERE cab_solevatrat = idCabecera;
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 10;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 18;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
	   
	   
	   
	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido cerrada.");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spdistribucion_solevatrat_x_hosp
DROP PROCEDURE IF EXISTS `rapsinet_spdistribucion_solevatrat_x_hosp`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spdistribucion_solevatrat_x_hosp`()
BEGIN

select EST_id,EST_nombre, 
(select
 count(1)
 from rapsinet_cabeceraevatratamiento
 where hospital = EST_id) As Numero

 from rapsinet_establecimientos
where EST_id IN(1,2,22,8,24,25)
Group By EST_id;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminafuncion
DROP PROCEDURE IF EXISTS `rapsinet_speliminafuncion`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_speliminafuncion`(IN `ID` INT)
BEGIN
DELETE FROM RAPSINET_FUNCIONES WHERE FUNC_ID = ID;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminahospital
DROP PROCEDURE IF EXISTS `rapsinet_speliminahospital`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_speliminahospital`(IN `codigo` INT)
BEGIN
DELETE From rapsinet_establecimientos WHERE EST_id = codigo;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminamedico
DROP PROCEDURE IF EXISTS `rapsinet_speliminamedico`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_speliminamedico`(IN `CODIGO` INT)
BEGIN

DELETE FROM RAPSINET_MEDICOS WHERE MED_id = CODIGO;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminaperfil
DROP PROCEDURE IF EXISTS `rapsinet_speliminaperfil`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_speliminaperfil`(IN `ID` INT)
BEGIN
DELETE FROM RAPSINET_PERFILES WHERE PER_ID = ID;
DELETE FROM RAPSINET_PERFILES_FUNCIONALIDADES WHERE PER_ID = ID;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminarcamas
DROP PROCEDURE IF EXISTS `rapsinet_speliminarcamas`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_speliminarcamas`(IN `idCodigo` INT)
BEGIN

set @establecimiento = (select EST_id from rapsinet_establecimientos inner join RAPSINET_ASIGNACION_CAMAS 
on(RAPSINET_ASIGNACION_CAMAS.ASIGCAM_idEstablecimiento = rapsinet_establecimientos.EST_id) where
RAPSINET_ASIGNACION_CAMAS.ASIGCAM_id = idCodigo);

DELETE  FROM RAPSINET_ASIGNACION_CAMAS WHERE ASIGCAM_id = idCodigo;

set @camasActual = (select est.EST_camasDisponibles from rapsinet_establecimientos est where EST_id = @establecimiento);
set @resultado = (@camasActual + 1);

UPDATE rapsinet_establecimientos  
SET EST_camasDisponibles = @resultado 
WHERE EST_id = @establecimiento;

set @mensaje = 'Cama eliminada exitosamente.';

select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_speliminausuario
DROP PROCEDURE IF EXISTS `rapsinet_speliminausuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_speliminausuario`(IN `CODIGO` VARCHAR(12))
    MODIFIES SQL DATA
BEGIN

DELETE FROM RAPSINET_USUARIOS WHERE USUA_id = CODIGO;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spfiltrohistorialusuarios
DROP PROCEDURE IF EXISTS `rapsinet_spfiltrohistorialusuarios`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spfiltrohistorialusuarios`(IN `idSolicitud` INT, IN `fecha` VARCHAR(50))
BEGIN

if (idSolicitud = '')then
		set idSolicitud = null;
end if;

if (fecha = '') then
		set fecha = null;
end if;

	select
	histusua.HIST_id,
	histusua.HIST_nombres,
	histusua.HIST_apellidos,
	histusua.HIST_fecha,
	histusua.HIST_idAccion,
	acc.ACC_descripcion
	
	from rapsinet_historicousuarios histusua
	inner join rapsinet_acciones acc
	on(histusua.HIST_idAccion = acc.ACC_id)
	where 
	(histusua.HIST_fecha >= coalesce(str_to_date(concat(fecha,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),histusua.HIST_fecha) and
	histusua.HIST_fecha <= coalesce(str_to_date(concat(fecha,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),histusua.HIST_fecha)) and
	histusua.HIST_id = coalesce(idSolicitud,histusua.HIST_id);
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spfiltrologsolintadmin
DROP PROCEDURE IF EXISTS `rapsinet_spfiltrologsolintadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spfiltrologsolintadmin`(IN `idEstadoSolicitud` VARCHAR(50), IN `idTipoSolicitud` VARCHAR(50), IN `fecha` VARCHAR(50))
BEGIN

if (idEstadoSolicitud = 'Seleccione') then
		set idEstadoSolicitud = null;
end if;

if (idTipoSolicitud = 'Seleccione')then
		set idTipoSolicitud = null;
end if;

if (fecha = '') then
		set fecha = null;
end if;

	select
		logsol.LOGSOL_id,
		logsol.CABSOL_id,
		date_format(logsol.LOGSOL_fecha, '%d-%m-%Y %H:%i:%s')as LOGSOL_fecha,
		logsol.LOGSOL_idUsuario,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		logsol.LOGSOL_idDepartamento,
		dep.DEP_descripcion,
		logsol.LOGSOL_idTipoSolicitud,
		tipsol.TIP_descripcion,
		logsol.LOGSOL_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		logsol.LOGSOL_observaciones
		from rapsinet_logsolicitudes logsol
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = logsol.LOGSOL_idUsuario)
		inner join rapsinet_departamentos dep
		on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
		inner join rapsinet_estadossolicitudes estsol
		on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
	
		where 
		(logsol.LOGSOL_fecha >= coalesce(str_to_date(concat(fecha,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),logsol.LOGSOL_fecha) and
		logsol.LOGSOL_fecha <= coalesce(str_to_date(concat(fecha,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),logsol.LOGSOL_fecha)) and
		logsol.LOGSOL_idEstadoSolicitud = coalesce(idEstadoSolicitud,logsol.LOGSOL_idEstadoSolicitud) and
		logsol.LOGSOL_idTipoSolicitud = coalesce(idTipoSolicitud, logsol.LOGSOL_idTipoSolicitud);

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sphospitalesxcomunas
DROP PROCEDURE IF EXISTS `rapsinet_sphospitalesxcomunas`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sphospitalesxcomunas`(IN `comuna` INT)
BEGIN
Select * from rapsinet_establecimientos
Where EST_idComuna = comuna
Order By EST_nombre;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresafuncion
DROP PROCEDURE IF EXISTS `rapsinet_spingresafuncion`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_spingresafuncion`(IN `FUNCION` VARCHAR(60))
BEGIN
INSERT INTO RAPSINET_FUNCIONES (FUNC_DESCRIPCION) VALUES (FUNCION);
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresahospital
DROP PROCEDURE IF EXISTS `rapsinet_spingresahospital`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresahospital`(IN `NOMBRES` VARCHAR(120), IN `UBICACION` VARCHAR(200), IN `FONO` VARCHAR(10), IN `CORREO` VARCHAR(80), IN `REGION` INT, IN `PROVINCIA` INT, IN `COMUNA` INT, IN `CONDICION` INT, IN `SALUD` INT, IN `ASISTENTE` VARCHAR(200))
BEGIN
Insert Into rapsinet_establecimientos
(EST_nombre,EST_ubicacion,EST_fono,EST_correo,EST_idRegion,EST_idProvincia,EST_idComuna,
 EST_psiquiatrico,SSALUD_id,ASSOL_id)
Values
(NOMBRES,UBICACION,FONO,CORREO,REGION,PROVINCIA,COMUNA,CONDICION,SALUD,ASISTENTE);
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresaperfilfuncionalidad
DROP PROCEDURE IF EXISTS `rapsinet_spingresaperfilfuncionalidad`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresaperfilfuncionalidad`(IN `PERFIL_ID` INT, IN `FUNCION_ID` INT)
BEGIN
INSERT INTO RAPSINET_PERFILES_FUNCIONALIDADES (PER_ID,FUNC_ID) VALUES (PERFIL_ID,FUNCION_ID);
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresarsolicitudinternacionadmin
DROP PROCEDURE IF EXISTS `rapsinet_spingresarsolicitudinternacionadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresarsolicitudinternacionadmin`(IN `idEstablecimientoSol` INT, IN `opciones` vARCHAR(50), IN `diagnostico` VARCHAR(500), IN `otroAntecedente` vARCHAR(500), IN `ultimoTratamiento` vARCHAR(500), IN `idEstablecimientoInt` INT, IN `nombreMedico` VARCHAR(100), IN `rutMedico` VARCHAR(100), IN `correoMedico` vARCHAR(100), IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(100), IN `idUsuario` INT, IN `nombres_sol` VARCHAR(50), IN `apaterno_sol` vARCHAR(50), IN `amaterno_sol` vARCHAR(50), IN `rut_sol` vARCHAR(50), IN `edad_sol` INT, IN `sexo_sol` vARCHAR(50), IN `calle_sol` vARCHAR(50), IN `numerocasa_sol` vARCHAR(50), IN `sector_sol` vARCHAR(50), IN `comuna_sol` INT, IN `ciudad_sol` INT, IN `region_sol` INT, IN `telefono_sol` INT, IN `celular_sol` INT, IN `correo_sol` vARCHAR(50), IN `nombres_pac` vARCHAR(50), IN `apaterno_pac` vARCHAR(50), IN `amaterno_pac` vARCHAR(50), IN `rut_pac` vARCHAR(50), IN `edad_pac` INT, IN `sexo_pac` vARCHAR(50), IN `calle_pac` vARCHAR(50), IN `numerocasa_pac` VARCHAR(50), IN `sector_pac` VARCHAR(50), IN `comuna_pac` INT, IN `ciudad_pac` INT, IN `region_pac` INT, IN `vinculacion_pac` VARCHAR(50), IN `quien_solicita` INT)
BEGIN

	 /*CABECERA*/
	 set @estadoSolicitud = 1;
	 
	 insert into rapsinet_cabecerainternacionadmin(
	 CABINTADMIN_fechaEvaluacion,CABINTADMIN_idEstablecimientoSol,CABINTADMIN_idEstadoSolicitud,CABINTADMIN_idUsuario)
	 values(now(),idEstablecimientoSol,@estadoSolicitud,idUsuario);

	 
	 set @UltimoRegistroCabecera = LAST_INSERT_ID();
	 
	 /*DETALLE*/
	 
	 insert into rapsinet_detalleinternacionadmin(
	 CABINTADMIN_id,DETINTADMIN_opciones,DETINTADMIN_diagnostico,DETINTADMIN_otroAntenedente,DETINTADMIN_ultimoTratamiento,
	 DETINTADMIN_idEstablecimientoInt,DETINTADMIN_nombreMedico,DETINTADMIN_rutMedico,
	 DETINTADMIN_correoMedico,DETINTADMIN_aPaternoSol,DETINTADMIN_aMaternoSol,DETINTADMIN_nombresSol,
	 DETINTADMIN_rutSol,DETINTADMIN_edadSol,DETINTADMIN_sexoSol,DETINTADMIN_calleSol,
	 DETINTADMIN_nCasaSol,DETINTADMIN_sectorSol,DETINTADMIN_idComunaSol,DETINTADMIN_idCiudadSol,
	 DETINTADMIN_idRegionSol,DETINTADMIN_telefonoSol,DETINTADMIN_celularSol,DETINTADMIN_correoSol,
	 DETINTADMIN_aPaternoPac,DETINTADMIN_aMaternoPac,DETINTADMIN_nombresPac,DETINTADMIN_rutPac,
	 DETINTADMIN_edadPac,DETINTADMIN_sexoPac,DETINTADMIN_callePac,DETINTADMIN_nCasaPac,DETINTADMIN_sectorPac,
	 DETINTADMIN_idComunaPac,DETINTADMIN_idCiudadPac,DETINTADMIN_idRegionPac,DETINTADMIN_vinculacion,DETINTADMIN_quienSolicita)
	 values(@UltimoRegistroCabecera,opciones,diagnostico,
	 otroAntecedente,ultimoTratamiento,idEstablecimientoInt,nombreMedico,rutMedico,correoMedico,
	 apaterno_sol,amaterno_sol,nombres_sol,rut_sol,edad_sol,sexo_sol,calle_sol,numerocasa_sol,sector_sol,
	 comuna_sol,ciudad_sol,region_sol,telefono_sol,celular_sol,correo_sol,apaterno_pac,amaterno_pac,nombres_pac,
	 rut_pac,edad_pac,sexo_pac,calle_pac,numerocasa_pac,sector_pac,comuna_pac,ciudad_pac,
	 region_pac,vinculacion_pac,quien_solicita);
	 
	 /*LOG*/
	 
	 set @tipoSolicitud = 3;
	 set @estadoSolicitud = 1;
	 
	 insert into rapsinet_logsolicitudes(
	 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
	 LOGSOL_idTipoSolicitud, LOGSOL_idEstadoSolicitud)values(
	 @UltimoRegistroCabecera,now(), idUsuario, idDepartamento, @tipoSolicitud,
	 @estadoSolicitud);
	 
	 /*SEGURIDAD*/
	 
	 set @idAccion = 4;
	 	 
	 insert into rapsinet_historicousuarios(
	 HIST_nombres,HIST_apellidos,HIST_rut,
	 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
	 nombresUsuario, apellidosUsuario, rutUsuario,
	 now(), @idAccion, @UltimoRegistroCabecera, @tipoSolicitud); 
	 
	 /*MENSAJE*/
	 
	 if(select count(CABINTADMIN_id) from rapsinet_cabecerainternacionadmin
		where CABINTADMIN_idEstablecimientoSol = idEstablecimientoSol)> 0 then
	 
	 		set @mensaje = "La solicitud ha sido ingresada exitosamente";
	 
	 else
	 
	 		set @mensaje = "Los datos no han sido ingresados";
	 end if;		 
	 select @mensaje;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresasolicitudhospadmin
DROP PROCEDURE IF EXISTS `rapsinet_spingresasolicitudhospadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresasolicitudhospadmin`(IN `idEstablecimiento` INT, IN `fechaIngreso` VARCHAR(50), IN `fechaEgreso` VARCHAR(50), IN `nombrePac` VARCHAR(50), IN `aPaterno` VARCHAR(50), IN `aMaterno` VARCHAR(50), IN `rutPac` VARCHAR(50), IN `direccionPac` VARCHAR(50), IN `fonoPac` VARCHAR(50), IN `diagnostico` vARCHAR(250), IN `evolucionClinica` vARCHAR(250), IN `tratamiento` vARCHAR(250), IN `centroSaludRef` INT, IN `direccionRef` vARCHAR(100), IN `nombreProfesional` VARCHAR(100), IN `fechaRef` vARCHAR(50), IN `idMedicoTrat` VARCHAR(50), IN `ID_DEPARTAMENTO` INT, IN `NOMBRE_USUARIO` VARCHAR(50), IN `APELLIDOS_USUARIO` vARCHAR(50), IN `RUT_USUARIO` vARCHAR(50), IN `ID_USUARIO` INT, IN `OPCIONES` VARCHAR(50), IN `SEXO` INT)
BEGIN

 /*CABECERA*/
	
	 INSERT INTO RAPSINET_CABECERAALTAHOSPADMIN(
	  CABALTAHOSPADMIN_idEstablecimiento,CABALTAHOSPADMIN_fechaIngreso,
	  CABALTAHOSPADMIN_fechaEgreso,CABALTAHOSPADMIN_fechaActual,CABALTAHOSPADMIN_nombrePac,
	  CABALTAHOSPADMIN_apaternoPac,CABALTAHOSPADMIN_amaternoPac,CABALTAHOSPADMIN_rutPac,
	  CABALTAHOSPADMIN_direccionPac,CABALTAHOSPADMIN_fonoPac,CABALTAHOSPADMIN_idEstadoSolicitud,
	  CABALTAHOSPADMIN_sexo)
	 VALUES
	  (idEstablecimiento,STR_TO_DATE(fechaIngreso,'%d-%m-%Y'),STR_TO_DATE(fechaEgreso,'%d-%m-%Y'),now(),nombrePac,aPaterno,aMaterno,
	  rutPac,direccionPac,fonoPac,1,SEXO);

	 SET @UltimoRegistroCabecera = LAST_INSERT_ID();
	 
	 
/*DETALLE*/
	 
	 INSERT INTO RAPSINET_DETALLEALTAHOSPADMIN(
	  CABALTHOSPADMIN_id,DETALTHOSPADMIN_diagnostico,DETALTHOSPADMIN_evolucionClinica,
	  DETALTHOSPADMIN_tratamiento,DETALTHOSPADMIN_idCentroSalud,DETALTHOSPADMIN_direccionRef,
	  DETALTHOSPADMIN_idProfesionalMedico,DETALTHOSPADMIN_fechaRef,DETALTHOSPADMIN_idMedicoTrat,DETALTHOSPADMIN_opciones)
	   VALUES
	 (@UltimoRegistroCabecera,diagnostico,evolucionClinica,tratamiento,centroSaludRef,direccionRef,
	  nombreProfesional,STR_TO_DATE(fechaRef,'%d-%m-%Y'),idMedicoTrat,OPCIONES);
	 
 /*LOG*/
	
	 insert into rapsinet_logsolicitudes(
	 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
	 LOGSOL_idTipoSolicitud, LOGSOL_idEstadoSolicitud)values(
	 @UltimoRegistroCabecera,now(), ID_USUARIO, ID_DEPARTAMENTO, 5,
	 1);

/*SEGURIDAD*/
 	 
	insert into rapsinet_historicousuarios(
	 HIST_nombres,HIST_apellidos,HIST_rut,
	 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
	 NOMBRE_USUARIO, APELLIDOS_USUARIO, RUT_USUARIO,
	 now(), 9, @UltimoRegistroCabecera, 5); 

/*MENSAJE*/
	 
	 IF(SELECT COUNT(1) FROM RAPSINET_CABECERAALTAHOSPADMIN
	    WHERE 
		 CABALTAHOSPADMIN_rutPac = rutPac)> 0 THEN
	 	 		
				SET @mensaje = "La solicitud ha sido ingresada exitosamente";
	 
	 ELSE
	 	 		SET @mensaje = "Los datos no han sido ingresados";
	 	 		
	 END IF;	
 
	 SELECT @mensaje; 
	 
/* SALIDA DEL MENSAJE */

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresasolicitudinv
DROP PROCEDURE IF EXISTS `rapsinet_spingresasolicitudinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresasolicitudinv`(IN `RUT_PACIENTE` VARCHAR(12), IN `NOMBRES` VARCHAR(60), IN `APELLIDOS` VARCHAR(60), IN `DIRECCION` VARCHAR(120), IN `FECHA_NAC` VARCHAR(10), IN `FECHA_INT` VARCHAR(10), IN `FECHA_SOL` VARCHAR(10), IN `ID_SERVICIO` INT, IN `ID_ESTABLECIMIENTO` INT, IN `ID_MEDICO` INT, IN `ESTADO_SOLICITUD` INT, IN `OPCIONES` VARCHAR(6), IN `ID_USUARIO` INT, IN `RUT_USUARIO` VARCHAR(12), IN `NOMBRE_USUARIO` VARCHAR(60), IN `APELLIDOS_USUARIO` VARCHAR(60), IN `ID_DEPARTAMENTO` INT, IN `SEXO` INT)
BEGIN
 /*CABECERA*/
	 	 
	 INSERT INTO RAPSINET_CABECERASOLICITUDINV(
	  CABSOLINV_rutPaciente,CABSOLINV_nombresPaciente,CABSOLINV_apellidosPaciente,
	  CABSOLINV_direccionPaciente,CABSOLINV_fechaNacPaciente,CABSOLINV_fechaInternacion,
	  CABSOLINV_fechaSolicitud,CABSOLINV_idServicio,CABSOLINV_idEstablecimiento,
	  CABSOLINV_idMedico,CABSOLINV_estadoSolicitud,CABSOLINV_sexo)
	 VALUES
	  (RUT_PACIENTE,NOMBRES,APELLIDOS,DIRECCION,STR_TO_DATE(FECHA_NAC,'%d-%m-%Y'),STR_TO_DATE(FECHA_INT,'%d-%m-%Y'),
	   STR_TO_DATE(FECHA_SOL,'%d-%m-%Y'),ID_SERVICIO,
	   ID_ESTABLECIMIENTO,ID_MEDICO,ESTADO_SOLICITUD,SEXO);

	 SET @UltimoRegistroCabecera = LAST_INSERT_ID();
	 
	 
/*DETALLE*/
	 
	 INSERT INTO RAPSINET_DETALLESOLICITUDINV(
	  CABSOLINV_id,DETSOLINV_opcion)
	   VALUES
	 (@UltimoRegistroCabecera,OPCIONES);
	 
 /*LOG*/
	 
	 
	 SET @tipoSolicitud = 1;
	 SET @estadoSolicitud = 1;
	
	 insert into rapsinet_logsolicitudes(
	 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
	 LOGSOL_idTipoSolicitud, LOGSOL_idEstadoSolicitud)values(
	 @UltimoRegistroCabecera,now(), ID_USUARIO, ID_DEPARTAMENTO, @tipoSolicitud,
	 @estadoSolicitud);
/*SEGURIDAD*/
	 
	 SET @idAccion = 1;
	 	 
	insert into rapsinet_historicousuarios(
	 HIST_nombres,HIST_apellidos,HIST_rut,
	 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
	 NOMBRE_USUARIO, APELLIDOS_USUARIO, RUT_USUARIO,
	 now(), @idAccion, @UltimoRegistroCabecera, @tipoSolicitud); 

	 
	 /*MENSAJE*/
	 
	 IF(SELECT COUNT(1) FROM RAPSINET_CABECERASOLICITUDINV
	    WHERE 
		 CABSOLINV_rutPaciente = RUT_PACIENTE AND 
		 CABSOLINV_fechaInternacion = FECHA_INT AND CABSOLINV_idMedico = ID_MEDICO )> 0 THEN
	 	 		SET @mensaje = "La solicitud ha sido ingresada exitosamente";
	 
	  ELSE
	 	 		SET @mensaje = "Los datos no han sido ingresados";
	 	 		
	 END IF;	
	 
	 	 
	 SELECT @mensaje; /* SALIDA DEL MENSAJE */
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresasoltratamiento
DROP PROCEDURE IF EXISTS `rapsinet_spingresasoltratamiento`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresasoltratamiento`(IN `FEC_SOL` VARCHAR(10), IN `FICHA` INT, IN `FEC_VIS` VARCHAR(10), IN `REGION` INT, IN `PROVINCIA` INT, IN `COMUNA` INT, IN `HOSPITAL` INT, IN `NOMBRE_PAC` VARCHAR(120), IN `AP_PAT` VARCHAR(80), IN `AP_MAT` VARCHAR(80), IN `RUT_PAC` VARCHAR(12), IN `EDAD_PAC` INT, IN `SEXO` INT, IN `DIRECCION` VARCHAR(200), IN `FONO` VARCHAR(10), IN `RUT_DOC` VARCHAR(12), IN `NOM_DOC` VARCHAR(200), IN `PRO_DOC` VARCHAR(80), IN `FONO_DOC` VARCHAR(10), IN `MAIL_DOC` VARCHAR(80), IN `ESTADO` INT, IN `ID_DEPTO` INT, IN `NOMBRES_USER` VARCHAR(100), IN `APELS_USER` VARCHAR(100), IN `RUT_USER` VARCHAR(12), IN `ID_USER` INT, IN `ID_HOSPITAL_PSI` INT, IN `OPCIONES` VARCHAR(50), IN `NO_SOL` VARCHAR(100), IN `AP_SOL` VARCHAR(100), IN `RU_SOL` VARCHAR(12), IN `ED_SOL` INT, IN `DI_SOL` VARCHAR(120), IN `RE_SOL` INT, IN `PR_SOL` INT, IN `CO_SOL` INT, IN `SE_SOL` INT, IN `VI_SOL` INT, IN `quienSolicita` INT)
BEGIN
/*CABECERA*/
	 	 
	 INSERT INTO RAPSINET_CABECERAEVATRATAMIENTO(
	  fecha_sol,ficha,fecha_visita,region,provincia,comuna,hospital,nombres_pac,
	  ap_pat,ap_mat,rut_pac,edad_pac,sexo_pac,dire_pac,fono_pac,estado_sol,id_usuario)
	 VALUES
	  (STR_TO_DATE(FEC_SOL,'%d-%m-%Y'),FICHA,STR_TO_DATE(FEC_VIS,'%d-%m-%Y'),REGION,PROVINCIA,COMUNA,
	   HOSPITAL,NOMBRE_PAC,AP_PAT,AP_MAT,RUT_PAC,EDAD_PAC,SEXO,DIRECCION,FONO,ESTADO,ID_USER
	   );
	   
	 SET @UltimoRegistroCabecera = LAST_INSERT_ID();
	 
/*DETALLE*/
	 
	 INSERT INTO RAPSINET_DETALLEEVATRATAMIENTO(
	  cab_solevatrat,rut_doc,nom_doc,prof_doc,fono_doc,mail_doc,lugar_evaluacion,det_opciones,
	  nom_sol,apel_sol,rut_sol,edad_sol,dir_sol,reg_sol,prov_sol,com_sol,sexo_sol,vincula_sol,quienSolicita)
	   VALUES
	 (@UltimoRegistroCabecera,RUT_DOC,NOM_DOC,PRO_DOC,FONO_DOC,MAIL_DOC,ID_HOSPITAL_PSI,OPCIONES,
	  NO_SOL,AP_SOL,RU_SOL,ED_SOL,DI_SOL,RE_SOL,PR_SOL,CO_SOL,SE_SOL,VI_SOL,quienSolicita);
	 
 /*LOG*/
	 
	 
	 SET @tipoSolicitud = 2;
	 SET @estadoSolicitud = 1;
	
	 insert into rapsinet_logsolicitudes(
	 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
	 LOGSOL_idTipoSolicitud, LOGSOL_idEstadoSolicitud)values(
	 @UltimoRegistroCabecera,now(), ID_USER, ID_DEPTO, @tipoSolicitud,
	 @estadoSolicitud);
/*SEGURIDAD*/
	 
	 SET @idAccion = 13;
	 	 
	insert into rapsinet_historicousuarios(
	 HIST_nombres,HIST_apellidos,HIST_rut,
	 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
	 NOMBRES_USER, APELS_USER, RUT_USER,
	 now(), @idAccion, @UltimoRegistroCabecera, @tipoSolicitud); 

	 
	 /*MENSAJE*/
	 
	 IF(SELECT COUNT(1) FROM RAPSINET_CABECERAEVATRATAMIENTO
	    WHERE 
		 rut_pac = RUT_PAC AND 
		 fec_sol = FEC_SOL)> 0 THEN
	 	 		SET @mensaje = "La solicitud ha sido ingresada exitosamente";
	 
	  ELSE
	 	 		SET @mensaje = "Los datos no han sido ingresados";
	 	 		
	 END IF;	
	 
	 	 
	 SELECT @mensaje; /* SALIDA DEL MENSAJE */

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresa_adjuntos_soladmin
DROP PROCEDURE IF EXISTS `rapsinet_spingresa_adjuntos_soladmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresa_adjuntos_soladmin`(IN `CABID` INT, IN `NOMBRE` VARCHAR(50), IN `TIPO` VARCHAR(50), IN `EXTENSION` VARCHAR(50), IN `ARCHIVO` MEDIUMBLOB)
BEGIN
DECLARE EXT_ARCHIVO INT;
SET EXT_ARCHIVO = (SELECT TIPARCH_ID FROM RAPSINET_TIPOARCHIVO WHERE TIPARCH_EXTENSION = EXTENSION);
IF EXT_ARCHIVO <> "" THEN 
INSERT INTO RAPSINET_ARCHIVOSADJUNTOS(CAB_ID,TIPARCH_ID,ARCHAD_NOMBRE,ARCHAD_TIPO,ARCHAD_EXTENSION,ARCHAD_ARCHIVO,ARCHAD_FECHA)
VALUES (CABID,EXT_ARCHIVO,NOMBRE,TIPO,EXTENSION,ARCHIVO,NOW());
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresa_adjuntos_solalta
DROP PROCEDURE IF EXISTS `rapsinet_spingresa_adjuntos_solalta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresa_adjuntos_solalta`(IN `CABID` INT, IN `NOMBRE` VARCHAR(100), IN `TIPO` VARCHAR(50), IN `EXTENSION` VARCHAR(50), IN `ARCHIVO` MEDIUMBLOB)
BEGIN
DECLARE EXT_ARCHIVO INT;
SET EXT_ARCHIVO = (SELECT TIPARCH_ID FROM RAPSINET_TIPOARCHIVO WHERE TIPARCH_EXTENSION = EXTENSION);
IF EXT_ARCHIVO <> "" THEN 
INSERT INTO RAPSINET_ARCHIVOSADJUNTOSALTA (CAB_ID,TIPARCH_ID,ARCHAD_NOMBRE,ARCHAD_TIPO,ARCHAD_EXTENSION,ARCHAD_ARCHIVO,ARCHAD_FECHA)
VALUES (CABID,EXT_ARCHIVO,NOMBRE,TIPO,EXTENSION,ARCHIVO,NOW());
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresa_adjuntos_soleva
DROP PROCEDURE IF EXISTS `rapsinet_spingresa_adjuntos_soleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresa_adjuntos_soleva`(IN `CABID` INT, IN `NOMBRE` VARCHAR(100), IN `TIPO` VARCHAR(50), IN `EXTENSION` VARCHAR(50), IN `ARCHIVO` MEDIUMBLOB)
BEGIN
DECLARE EXT_ARCHIVO INT;
SET EXT_ARCHIVO = (SELECT TIPARCH_ID FROM RAPSINET_TIPOARCHIVO WHERE TIPARCH_EXTENSION = EXTENSION);
IF EXT_ARCHIVO <> "" THEN 
INSERT INTO RAPSINET_ADJUNTOS_SOLEVA (CAB_ID,TIPARCH_ID,ARCHAD_NOMBRE,ARCHAD_TIPO,ARCHAD_EXTENSION,ARCHAD_ARCHIVO,ARCHAD_FECHA)
VALUES (CABID,EXT_ARCHIVO,NOMBRE,TIPO,EXTENSION,ARCHIVO,NOW());
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresa_adjuntos_solint
DROP PROCEDURE IF EXISTS `rapsinet_spingresa_adjuntos_solint`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresa_adjuntos_solint`(IN `CABID` INT, IN `NOMBRE` VARCHAR(100), IN `TIPO` VARCHAR(50), IN `EXTENSION` VARCHAR(50), IN `ARCHIVO` MEDIUMBLOB)
BEGIN
DECLARE EXT_ARCHIVO INT;
SET EXT_ARCHIVO = (SELECT TIPARCH_ID FROM RAPSINET_TIPOARCHIVO WHERE TIPARCH_EXTENSION = EXTENSION);
IF EXT_ARCHIVO <> "" THEN 
INSERT INTO RAPSINET_ADJUNTOS_SOLINT (CAB_ID,TIPARCH_ID,ARCHAD_NOMBRE,ARCHAD_TIPO,ARCHAD_EXTENSION,ARCHAD_ARCHIVO,ARCHAD_FECHA)
VALUES (CABID,EXT_ARCHIVO,NOMBRE,TIPO,EXTENSION,ARCHIVO,NOW());
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresomedico
DROP PROCEDURE IF EXISTS `rapsinet_spingresomedico`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spingresomedico`(IN `RUT` VARCHAR(12), IN `NOMBRES` VARCHAR(120), IN `APELLIDOS` VARCHAR(120), IN `DIRECCION` VARCHAR(200), IN `CELULAR` VARCHAR(20), IN `FONO` VARCHAR(20), IN `CORREO` VARCHAR(80), IN `COD_ESTABLECIMIENTO` INT)
BEGIN
DECLARE CUENTA INT(1);
SET CUENTA = (SELECT COUNT(1) FROM RAPSINET_MEDICOS WHERE MED_rut = RUT);
IF CUENTA = 0 THEN
INSERT INTO RAPSINET_MEDICOS 
( 
  MED_rut,MED_nombres,MED_apellidos,MED_direccion,MED_celular,MED_fono,MED_correo,EST_id
)
VALUES
(  
  RUT,NOMBRES,APELLIDOS,DIRECCION,CELULAR,FONO,CORREO,COD_ESTABLECIMIENTO
);
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresoperfil
DROP PROCEDURE IF EXISTS `rapsinet_spingresoperfil`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_spingresoperfil`(IN `VALOR` VARCHAR(60))
BEGIN
DECLARE count INT(1);
 SET count = (SELECT COUNT(1) FROM RAPSINET_PERFILES WHERE PER_DESCRIPCION = VALOR );
 SELECT count;
 IF count = 0 THEN
  INSERT INTO RAPSINET_PERFILES(PER_DESCRIPCION) VALUES (VALOR);
 END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spingresousuario
DROP PROCEDURE IF EXISTS `rapsinet_spingresousuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_spingresousuario`(IN `RUT` VARCHAR(12), IN `NOMBRES` VARCHAR(60), IN `APELLIDOS` VARCHAR(60), IN `DIRECCION` VARCHAR(120), IN `CELULAR` VARCHAR(10), IN `FONO` VARCHAR(10), IN `CORREO` VARCHAR(80), IN `USUARIO` VARCHAR(30), IN `CLAVE` VARCHAR(30), IN `COD_ESTABLECIMIENTO` INT, IN `COD_DEPARTAMENTO` INT, IN `COD_PERFIL` INT)
    MODIFIES SQL DATA
BEGIN
DECLARE CUENTA INT(1);
SET CUENTA = (SELECT COUNT(1) FROM RAPSINET_USUARIOS WHERE USUA_rut = RUT);
IF CUENTA = 0 THEN
INSERT INTO RAPSINET_USUARIOS 
( 
  USUA_rut,USUA_nombres,USUA_apellidos,USUA_direccion,USUA_celular,USUA_fono,USUA_correo,USUA_usuario,USUA_CONTRASENA,EST_id,DEPART_id,PER_id
)
VALUES
(  
  RUT,NOMBRES,APELLIDOS,DIRECCION,CELULAR,FONO,CORREO,USUARIO,CLAVE,COD_ESTABLECIMIENTO,COD_DEPARTAMENTO,COD_PERFIL
);
END IF;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaadjuntos
DROP PROCEDURE IF EXISTS `rapsinet_splistaadjuntos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaadjuntos`(IN `CLAVE` INT)
BEGIN
SELECT * FROM RAPSINET_ARCHIVOSADJUNTOS
WHERE CAB_ID = CLAVE;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaadjuntossoleva
DROP PROCEDURE IF EXISTS `rapsinet_splistaadjuntossoleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaadjuntossoleva`(IN `CLAVE` INT)
BEGIN
SELECT * FROM RAPSINET_ADJUNTOS_SOLEVA
WHERE CAB_ID = CLAVE;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaasignacioncamas
DROP PROCEDURE IF EXISTS `rapsinet_splistaasignacioncamas`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaasignacioncamas`(IN `idEstablecimiento` INT)
BEGIN
	
	set @camasActuales = (select distinct est.EST_camasDisponibles  from  rapsinet_cabeceraevatratamiento cab inner join 
	rapsinet_detalleevatratamiento det on(cab.cab_solevatrat = det.cab_solevatrat)
	inner join rapsinet_establecimientos est on(det.lugar_evaluacion = est.EST_id)
	where est.EST_id = idEstablecimiento and est.EST_psiquiatrico = 1);
	
	set @camasAsignadas = (select count(camas.ASIGCAM_id) from rapsinet_asignacion_camas camas inner join rapsinet_establecimientos est
	on(est.EST_id = camas.ASIGCAM_idEstablecimiento) where camas.ASIGCAM_idEstablecimiento = idEstablecimiento);


	select 
	asig.ASIGCAM_id,
	asig.ASIGCAM_fecha,
	concat(asig.ASIGCAM_nombrePac, " " , asig.ASIGCAM_apaternoPac, " " , asig.ASIGCAM_amaternoPac) as paciente,
	asig.ASIGCAM_idEstablecimiento,
	estab.EST_nombre,
	@camasActuales as camasActuales,
	@camasAsignadas as camasAsignadas 
	from rapsinet_asignacion_camas asig
	inner join rapsinet_establecimientos estab
	on(estab.EST_id = asig.ASIGCAM_idEstablecimiento)
	where asig.ASIGCAM_idEstablecimiento = idEstablecimiento;
	
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistacomentarios_sia
DROP PROCEDURE IF EXISTS `rapsinet_splistacomentarios_sia`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistacomentarios_sia`(IN `CABSOL` INT)
BEGIN
SELECT * FROM RAPSINET_COMENTARIOS_SIA
WHERE COMSIA_IDCABECERASOL = CABSOL;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistacorreos
DROP PROCEDURE IF EXISTS `rapsinet_splistacorreos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistacorreos`()
BEGIN
	select
	co.CO_id,
	co.CO_descripcion,
	co.CO_mail
	from rapsinet_correos co;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistadepartementos
DROP PROCEDURE IF EXISTS `rapsinet_splistadepartementos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistadepartementos`()
BEGIN
	select * from rapsinet_departamentos order by DEP_descripcion ASC;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaestablecimientos
DROP PROCEDURE IF EXISTS `rapsinet_splistaestablecimientos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaestablecimientos`()
BEGIN

SELECT EST_id,EST_nombre FROM RAPSINET_ESTABLECIMIENTOS
Order By EST_nombre;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaestados
DROP PROCEDURE IF EXISTS `rapsinet_splistaestados`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaestados`()
BEGIN
SELECT ESTA_ID,ESTA_DESCRIPCION FROM RAPSINET_ESTADOSSOLICITUDES;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistafunciones
DROP PROCEDURE IF EXISTS `rapsinet_splistafunciones`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_splistafunciones`()
BEGIN
SELECT * FROM RAPSINET_FUNCIONES;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistafuncionesperfil
DROP PROCEDURE IF EXISTS `rapsinet_splistafuncionesperfil`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_splistafuncionesperfil`(IN `ID_PERFIL` INT)
BEGIN
SELECT RAPSINET_PERFILES_FUNCIONALIDADES.PER_ID,RAPSINET_PERFILES_FUNCIONALIDADES.FUNC_ID,
RAPSINET_FUNCIONES.FUNC_DESCRIPCION
FROM 
RAPSINET_PERFILES_FUNCIONALIDADES
INNER JOIN RAPSINET_FUNCIONES ON RAPSINET_PERFILES_FUNCIONALIDADES.FUNC_ID = RAPSINET_FUNCIONES.FUNC_ID
WHERE RAPSINET_PERFILES_FUNCIONALIDADES.PER_ID = ID_PERFIL;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistahospitales
DROP PROCEDURE IF EXISTS `rapsinet_splistahospitales`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistahospitales`()
BEGIN
SELECT est_id,est_nombre,est_ubicacion,est_fono,
 prov_nombre,com_nombre
 FROM RAPSINET_establecimientos esta
 left join 
 rapsinet_provincias prov
 on esta.EST_idProvincia = prov.PROV_id
 left join
 rapsinet_comunas comu
 on esta.EST_idComuna = comu.COM_id
 order by com_nombre desc;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistahospitalespsi
DROP PROCEDURE IF EXISTS `rapsinet_splistahospitalespsi`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistahospitalespsi`()
BEGIN
select 
est.EST_id,
est.EST_nombre
from rapsinet_establecimientos est
where est.EST_psiquiatrico = 1;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistamedicos
DROP PROCEDURE IF EXISTS `rapsinet_splistamedicos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistamedicos`()
BEGIN
	SELECT
	MED_id,
	CONCAT(MED_nombres, ' ', MED_apellidos) as nombre,
	MED_RUT,MED_DIRECCION,MED_CELULAR,MED_CORREO
	from rapsinet_medicos
	Order By nombre;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaperfiles
DROP PROCEDURE IF EXISTS `rapsinet_splistaperfiles`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_splistaperfiles`()
BEGIN
SELECT * FROM RAPSINET_PERFILES ORDER BY PER_ID DESC;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaservicios
DROP PROCEDURE IF EXISTS `rapsinet_splistaservicios`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistaservicios`()
BEGIN
	select
	SERV_id,
	SERV_descripcion
	from rapsinet_servicios;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasexo
DROP PROCEDURE IF EXISTS `rapsinet_splistasexo`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasexo`()
BEGIN
	select
	sex.SEX_descripcion,
	sex.SEX_detalle
	from rapsinet_sexo sex;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasoleva
DROP PROCEDURE IF EXISTS `rapsinet_splistasoleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasoleva`(IN `CABECERA` INT)
BEGIN
set @ultimoTipoSolicitud = (select logsol.LOGSOL_idTipoSolicitud from rapsinet_logsolicitudes logsol
where logsol.CABSOL_id = CABECERA and logsol.LOGSOL_idTipoSolicitud = 7
order by logsol.LOGSOL_id DESC limit 1);


SELECT *,@ultimoTipoSolicitud AS ultimoTipoSolicitud FROM
RAPSINET_CABECERAEVATRATAMIENTO CAB
LEFT JOIN
RAPSINET_DETALLEEVATRATAMIENTO DET
ON CAB.cab_solevatrat = DET.cab_solevatrat

Where CAB.cab_solevatrat = CABECERA;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolevas
DROP PROCEDURE IF EXISTS `rapsinet_splistasolevas`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolevas`(IN `ID_ESTADO` INT, IN `ID_SOLICITUD` INT, IN `FECHA` VARCHAR(10), IN `DEPTO` INT, IN `ID_EST` INT)
BEGIN
if (ID_ESTADO = '') then
		set ID_ESTADO = null;
end if;


if (ID_SOLICITUD = '')then
		set ID_SOLICITUD = null; 
end if;

if (FECHA = '' or FECHA is null) then
		set @fechaIni = '01-01-1900';
		set @fechaFin = '01-01-2100';
else
		set @fechaIni = fecha;
		set @fechaFin = fecha;
end if;


SET @tipoSolicitud = 2;

IF DEPTO = 1 THEN	#DSP
	
	SELECT CAB_SOLEVATRAT, FECHA_SOL, ESTADO_SOL,
	   "Solicitud Evaluacion y Tratamiento" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CAB_SOLEVATRAT AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id asc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		 (SELECT ESTA_descripcion from rapsinet_estadossolicitudes 
		  Where ESTA_id = ESTADO_SOL) AS ESTADO
	   FROM  rapsinet_cabeceraevatratamiento
	WHERE ESTADO_SOL IN(1,2,4);
	   
	
END IF;	  


IF DEPTO = 2 THEN	#JURIDICA
	
	SELECT CAB_SOLEVATRAT, FECHA_SOL, ESTADO_SOL,
	   "Solicitud Evaluacion y Tratamiento" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CAB_SOLEVATRAT AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id asc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		 (SELECT ESTA_descripcion from rapsinet_estadossolicitudes 
		  Where ESTA_id = ESTADO_SOL) AS ESTADO,
		(SELECT solicitud_traslado from rapsinet_cabeceraevatratamiento 
		  Where cab_solevatrat = ID_SOLICITUD) AS ES_TRATAMIENTO
	   FROM  rapsinet_cabeceraevatratamiento
	WHERE ESTADO_SOL IN(3,6);
		
END IF; 

IF DEPTO = 3 THEN	#SEREMI
	
	SELECT CAB_SOLEVATRAT, FECHA_SOL, ESTADO_SOL,
	   "Solicitud Evaluacion y Tratamiento" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CAB_SOLEVATRAT AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id asc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		 (SELECT ESTA_descripcion from rapsinet_estadossolicitudes 
		 Where ESTA_id = ESTADO_SOL) AS ESTADO,
		 solicitud_traslado as ES_TRATAMIENTO
	    FROM  rapsinet_cabeceraevatratamiento
	   
	WHERE ESTADO_SOL IN(5,8);
	   
	
END IF; 


IF DEPTO = 4 THEN	#CONTRALORIA
	
	
	
	SELECT CAB_SOLEVATRAT, FECHA_SOL, ESTADO_SOL,
	   "Solicitud Evaluacion y Tratamiento" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		CABSOL_ID = CAB_SOLEVATRAT AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		order by LOGSOL_id asc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes 
		Where ESTA_id = ESTADO_SOL) AS ESTADO,
		solicitud_traslado as ES_TRATAMIENTO
	   FROM  rapsinet_cabeceraevatratamiento
	WHERE ESTADO_SOL IN(7);
	   
	
END IF; 

IF DEPTO = 6 THEN	#SECRETARIA
	
	SELECT CAB_SOLEVATRAT, FECHA_SOL, ESTADO_SOL,
	   "Solicitud Evaluacion y Tratamiento" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CAB_SOLEVATRAT AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id asc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		 (SELECT ESTA_descripcion from rapsinet_estadossolicitudes 
		  Where ESTA_id = ESTADO_SOL) AS ESTADO
	   FROM  rapsinet_cabeceraevatratamiento
	WHERE (isnull(rakim_eva) or rakim_eva = "");
	   
	
END IF; 

 if DEPTO = 7 then

		if (ID_SOLICITUD is null) then
			set @visarusmh = 9;
		end if;
		
		if (ID_SOLICITUD = '9') then
			set @visarusmh = 9;
		end if;


		set @tipoSolicitud = 2;
		select 

		cabeva.cab_solevatrat,
		cabeva.fecha_sol,
		cabeva.estado_sol,
		estsol.ESTA_descripcion,
		concat(usua.USUA_nombres ," ", usua.USUA_apellidos) as USUARIO,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabeceraevatratamiento cabeva
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabeva.id_usuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabeva.estado_sol = estsol.ESTA_id)
		
		where 
		(cabeva.fecha_sol >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol) and
		cabeva.fecha_sol <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol)) and
		(cabeva.estado_sol = coalesce(@visarusmh,cabeva.estado_sol)) and
		cabeva.cab_solevatrat = coalesce(ID_SOLICITUD, cabeva.cab_solevatrat) and tipsol.TIP_id = @tipoSolicitud
		;

  end if;

  if DEPTO = 8 then

		if (ID_SOLICITUD is null) then
			set @visarusmh = 11;
		end if;
		
		if (ID_SOLICITUD = '11') then
			set @visarusmh = 11;
		end if;


		set @tipoSolicitud = 2;
		select 

		cabeva.cab_solevatrat,
		cabeva.fecha_sol,
		cabeva.estado_sol,
		estsol.ESTA_descripcion,
		concat(usua.USUA_nombres ," ", usua.USUA_apellidos) as USUARIO,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabeceraevatratamiento cabeva
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabeva.id_usuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabeva.estado_sol = estsol.ESTA_id)
		
		where 
		(cabeva.fecha_sol >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol) and
		cabeva.fecha_sol <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol)) and
		(cabeva.estado_sol = coalesce(@visarusmh,cabeva.estado_sol)) and
		cabeva.cab_solevatrat = coalesce(ID_SOLICITUD, cabeva.cab_solevatrat) and tipsol.TIP_id = @tipoSolicitud;

  end if;	
  
  
   if DEPTO = 9 then

		if (ID_SOLICITUD is null) then
			set @visarusmh = 12;
		end if;
		
		if (ID_SOLICITUD = '12') then
			set @visarusmh = 12;
		end if;


		set @tipoSolicitud = 2;
		select 

		cabeva.cab_solevatrat,
		cabeva.fecha_sol,
		cabeva.estado_sol,
		estsol.ESTA_descripcion,
		concat(usua.USUA_nombres ," ", usua.USUA_apellidos) as USUARIO,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabeceraevatratamiento cabeva
		inner join rapsinet_detalleevatratamiento deteva
		on(deteva.cab_solevatrat = cabeva.cab_solevatrat)
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabeva.id_usuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabeva.estado_sol = estsol.ESTA_id)
		
		where 
		(cabeva.fecha_sol >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol) and
		cabeva.fecha_sol <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol)) and
		(cabeva.estado_sol = coalesce(@visarusmh,cabeva.estado_sol)) and
		cabeva.cab_solevatrat = coalesce(ID_SOLICITUD, cabeva.cab_solevatrat) and tipsol.TIP_id = @tipoSolicitud and
		deteva.lugar_evaluacion = 	ID_EST;
	
  end if;	
  
  if DEPTO = 10 then

		if (ID_SOLICITUD is null) then
			set @visarusmh = 13;
		end if;
		
		if (ID_SOLICITUD = '13') then
			set @visarusmh = 13;
		end if;


		set @tipoSolicitud = 2;
		
		select 

		cabeva.cab_solevatrat,
		cabeva.fecha_sol,
		cabeva.estado_sol,
		estsol.ESTA_descripcion,
		concat(usua.USUA_nombres ," ", usua.USUA_apellidos) as USUARIO,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabeceraevatratamiento cabeva
		inner join rapsinet_detalleevatratamiento deteva
		on(deteva.cab_solevatrat = cabeva.cab_solevatrat)
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabeva.id_usuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabeva.estado_sol = estsol.ESTA_id)
		
		where 
		(cabeva.fecha_sol >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol) and
		cabeva.fecha_sol <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabeva.fecha_sol)) and
		(cabeva.estado_sol = coalesce(@visarusmh,cabeva.estado_sol)) and
		cabeva.cab_solevatrat = coalesce(ID_SOLICITUD, cabeva.cab_solevatrat) and tipsol.TIP_id = @tipoSolicitud and
		deteva.lugar_evaluacion = 	ID_EST;

  end if;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolicitudaa
DROP PROCEDURE IF EXISTS `rapsinet_splistasolicitudaa`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolicitudaa`(IN `idEstadoSolicitud` INT, IN `idSolicitud` INT, IN `fecha` VARCHAR(50), IN `id_depto` INT, IN `ID_EST` INT)
BEGIN
/*if (idEstadoSolicitud = 'Seleccione') then
		set idEstadoSolicitud = null;
end if;*/

if (idSolicitud = '')then
		set idSolicitud = null; 
end if;

if (fecha = '' or fecha is null) then
		set @fechaIni = '01-01-1900';
		set @fechaFin = '01-01-2100';
else
		set @fechaIni = fecha;
		set @fechaFin = fecha;
end if;


IF id_depto = 1 THEN	
	
if (idEstadoSolicitud is null) then
			set @visarjuridica = 1;
			set @rechazadoSeremi = 4;
		end if;
		
		if (idEstadoSolicitud = '1') then
			set @visarjuridica = 1;
			set @rechazadoSeremi = 1;
		end if;
		
		if (idEstadoSolicitud = '4') then
			set @visarjuridica = 4;
			set @rechazadoSeremi = 4;
		end if;

	SET @tipoSolicitud = 5;

  		SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
	   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
		 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO
	    FROM  rapsinet_cabeceraaltahospadmin
	    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(1,4); 
		 
	
END IF;

IF id_depto = 2 THEN	

if (idEstadoSolicitud is null) then
			set @visarjuridica = 3;
			set @rechazadoSeremi = 6;
		end if;
		
		if (idEstadoSolicitud = '3') then
			set @visarjuridica = 3;
			set @rechazadoSeremi = 3;
		end if;
		
		if (idEstadoSolicitud = '6') then
			set @visarjuridica = 6;
			set @rechazadoSeremi = 6;
		end if;

	SET @tipoSolicitud = 5;

   	SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
	   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
		 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO
	    FROM  rapsinet_cabeceraaltahospadmin
	    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(3,6);
	
END IF;	



IF id_depto = 3 THEN	

if (idEstadoSolicitud is null) then
			set @visarseremi = 5;
			set @rechazadoContraloria = 8;
end if;
		
if (idEstadoSolicitud = '5') then
			set @visarseremi = 5;
			set @rechazadoContraloria = 5;
end if;
		
if (idEstadoSolicitud = '8') then
			set @visarseremi = 8;
			set @rechazadoContraloria = 8;
end if;


	SET @tipoSolicitud = 5;

    	SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
	   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
		 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO
	    FROM  rapsinet_cabeceraaltahospadmin
	    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(5,8);
	
END IF;	


IF id_depto = 4 THEN	/*CONTRALORIA*/
		
		if (idEstadoSolicitud is null) then
			set @visarcontraloria = 7;
		end if;
		
		if (idEstadoSolicitud = '7') then
			set @visarcontraloria = 7;
			set @visarcontraloria = 7;
		end if;
		

		set @tipoSolicitud = 5;
		
		
	    SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
	   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
		 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO
	    FROM  rapsinet_cabeceraaltahospadmin
	    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(7);

END IF;


IF id_depto = 6 THEN	
		
		set @tipoSolicitud = 5;

		select  distinct cabalta.CABALTAHOSPADMIN_id,
		cabalta.CABALTAHOSPADMIN_fechaActual,
		cabalta.CABALTAHOSPADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_id,
		concat(usua.USUA_nombres ," ", usua.USUA_apellidos) nombre_completo,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		from rapsinet_cabeceraaltahospadmin cabalta
		inner join rapsinet_estadossolicitudes estsol
		on(cabalta.CABALTAHOSPADMIN_idEstadoSolicitud = estsol.ESTA_id)
		inner join rapsinet_logsolicitudes logsol
		on(logsol.CABSOL_id = cabalta.CABALTAHOSPADMIN_id)
	   inner join rapsinet_usuarios usua
	   on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	   inner join rapsinet_tiposolicitudes tipsol
	   on(logsol.LOGSOL_idTipoSolicitud = tipsol.TIP_id)
		
		where 
		(cabalta.CABALTAHOSPADMIN_fechaActual >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabalta.CABALTAHOSPADMIN_fechaActual) and
		cabalta.CABALTAHOSPADMIN_fechaActual <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabalta.CABALTAHOSPADMIN_fechaActual)) and
		(cabalta.CABALTAHOSPADMIN_idEstadoSolicitud = coalesce(@visarcontraloria,cabalta.CABALTAHOSPADMIN_idEstadoSolicitud)) and
		cabalta.CABALTAHOSPADMIN_id = coalesce(idSolicitud, cabalta.CABALTAHOSPADMIN_id) and tipsol.TIP_id = @tipoSolicitud
		and (isnull(cabalta.CABALTAHOSPADMIN_rakim) Or cabalta.CABALTAHOSPADMIN_rakim = "");
	
		
END IF;



IF id_depto = 10 THEN


		if (idEstadoSolicitud is null) then
			set @visarcontraloria = 13;
		end if;
		
		if (idEstadoSolicitud = '13') then
			set @visarcontraloria = 13;
			set @visarcontraloria = 13;
		end if;	
		
		set @tipoSolicitud = 5;

		 SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
	   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
	   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
		 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
		 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
	   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
		 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
		(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
		 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO
	    FROM  rapsinet_cabeceraaltahospadmin
	    INNER JOIN rapsinet_detallealtahospadmin DET
	    ON(CABALTAHOSPADMIN_id = DET.CABALTHOSPADMIN_id)
	    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(13) AND
		 DET.DETALTHOSPADMIN_idCentroSalud = ID_EST;

		
END IF;
  
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolicitudesinternacionadministrativa
DROP PROCEDURE IF EXISTS `rapsinet_splistasolicitudesinternacionadministrativa`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolicitudesinternacionadministrativa`(IN `idDepartamento` INT, IN `idSolicitud` VARCHAR(50), IN `idEstadoSolicitud` VARCHAR(50), IN `fecha` VARCHAR(50), IN `ID_EST` INT)
BEGIN
if (idEstadoSolicitud = 'Seleccione') then
		set idEstadoSolicitud = null;
end if;

if (idSolicitud = '')then
		set idSolicitud = null; 
end if;

if (fecha = '' or fecha is null) then
		set @fechaIni = '01-01-1900';
		set @fechaFin = '01-01-2100';
else
		set @fechaIni = fecha;
		set @fechaFin = fecha;
end if;


  if idDepartamento = 1 then
		
/*TIPO SOLICITUD ADMINISTRATIVA*/

		if (idEstadoSolicitud is null) then
			set @visarDsp = 1;
			set @rechazadoJuridica = 4;
		end if;
		
		if (idEstadoSolicitud = '1') then
			set @visarDsp = 1;
			set @rechazadoJuridica = 1;
		end if;
		
		if (idEstadoSolicitud = '4') then
			set @visarDsp = 4;
			set @rechazadoJuridica = 4;
		end if;
		
		/*set @ultimoTipoSolicitud = (select LOGSOL_idEstadoSolicitud  from rapsinet_logsolicitudes
		where CABSOL_id = idSolicitud order by LOGSOL_fecha ASC  limit 1);*/

		set @tipoSolicitud = 3;
		
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@visarDsp,cabintadmin.CABINTADMIN_idEstadoSolicitud) or
		cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@rechazadoJuridica,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;

  end if;
  
  if idDepartamento = 2 then
		
		if (idEstadoSolicitud is null) then
			set @visarjuridica = 3;
			set @rechazadoSeremi = 6;
		end if;
		
		if (idEstadoSolicitud = '3') then
			set @visarjuridica = 3;
			set @rechazadoSeremi = 3;
		end if;
		
		if (idEstadoSolicitud = '6') then
			set @visarjuridica = 6;
			set @rechazadoSeremi = 6;
		end if;

 		set @tipoSolicitud = 3;
		
	select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		/*where estsol.ESTA_id = 1;*/
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@visarjuridica,cabintadmin.CABINTADMIN_idEstadoSolicitud) or
		cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@rechazadoSeremi,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;

  end if;

  if idDepartamento = 3 then
		
		if (idEstadoSolicitud is null) then
			set @visarseremi = 5;
			set @rechazadoContraloria = 8;
		end if;
		
		if (idEstadoSolicitud = '5') then
			set @visarseremi = 5;
			set @rechazadoContraloria = 5;
		end if;
		
		if (idEstadoSolicitud = '8') then
			set @visarseremi = 8;
			set @rechazadoContraloria = 8;
		end if;
		
		
		
		set @tipoSolicitud = 3;
		
	   select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		/*where estsol.ESTA_id = 1;*/
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@visarseremi,cabintadmin.CABINTADMIN_idEstadoSolicitud) or
		cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@rechazadoContraloria,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;
  end if;
  
  if idDepartamento = 4 then
		
		if (idEstadoSolicitud is null) then
			set @visarcontraloria = 7;
		end if;
		
		if (idEstadoSolicitud = '7') then
			set @visarcontraloria = 7;
			set @visarcontraloria = 7;
		end if;
		

		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@visarcontraloria,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;
  end if;
  
  
  if idDepartamento = 6 then
		
				

		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud
		and (isnull(cabintadmin.CABINTADMIN_rakim) Or cabintadmin.CABINTADMIN_rakim = "");
  end if;
  
  if idDepartamento = 7 then

		if (idEstadoSolicitud is null) then
			set @visarusmh = 9;
		end if;
		
		if (idEstadoSolicitud = '9') then
			set @visarusmh = 9;
		end if;



		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@visarusmh,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;

  end if;
  
  
   if idDepartamento = 8 then

		if (idEstadoSolicitud = '') then
			set @primera_llamada = 11;
		end if;
		
		if (idEstadoSolicitud = '11') then
			set @primera_llamada = 11;
		end if;

		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@primera_llamada,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;

  end if;
  
  
     if idDepartamento = 9 then

		if (idEstadoSolicitud = '') then
			set @primera_llamada = 12;
		end if;
		
		if (idEstadoSolicitud = '12') then
			set @primera_llamada = 12;
		end if;

		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		usua.USUA_nombres,
		usua.USUA_apellidos,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@primera_llamada,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud;

  end if;
  
   if idDepartamento = 10 then

		if (idEstadoSolicitud = '') then
			set @primera_llamada = 13;
		end if;
		
		if (idEstadoSolicitud = '13') then
			set @primera_llamada = 13;
		end if;

		set @tipoSolicitud = 3;
		select 
		
		cabintadmin.CABINTADMIN_id,
		cabintadmin.CABINTADMIN_fechaEvaluacion,
		cabintadmin.CABINTADMIN_idEstadoSolicitud,
		estsol.ESTA_descripcion,
		concat(usua.USUA_nombres, " ", usua.USUA_apellidos) as usuario,
		tipsol.TIP_id,
		tipsol.TIP_descripcion
		
		from rapsinet_cabecerainternacionadmin cabintadmin
		inner join rapsinet_detalleinternacionadmin detadmin
		on(cabintadmin.CABINTADMIN_id = detadmin.CABINTADMIN_id)
		inner join rapsinet_tiposolicitudes tipsol
		on(tipsol.TIP_id = @tipoSolicitud)
		inner join rapsinet_usuarios usua
		on(usua.USUA_id = cabintadmin.CABINTADMIN_idUsuario)
		inner join rapsinet_estadossolicitudes estsol
		on(cabintadmin.CABINTADMIN_idEstadoSolicitud = estsol.ESTA_id)
		
		where 
		(cabintadmin.CABINTADMIN_fechaEvaluacion >= coalesce(str_to_date(concat(@fechaIni,' 00:00:00'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion) and
		cabintadmin.CABINTADMIN_fechaEvaluacion <= coalesce(str_to_date(concat(@fechaFin,' 23:59:59'), '%d-%m-%Y %H:%i:%s'),cabintadmin.CABINTADMIN_fechaEvaluacion)) and
		(cabintadmin.CABINTADMIN_idEstadoSolicitud = coalesce(@primera_llamada,cabintadmin.CABINTADMIN_idEstadoSolicitud)) and
		cabintadmin.CABINTADMIN_id = coalesce(idSolicitud, cabintadmin.CABINTADMIN_id) and tipsol.TIP_id = @tipoSolicitud and
		detadmin.DETINTADMIN_idEstablecimientoInt = ID_EST;
  end if;
  
  
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolicitudinv
DROP PROCEDURE IF EXISTS `rapsinet_splistasolicitudinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolicitudinv`(IN `idEstadoSolicitud` INT, IN `idSolicitud` INT, IN `fecha` varchar(10), IN `id_depto` INT)
BEGIN
		/*TIPO SOLICITUD Internacion No Voluntaria*/
	
IF id_depto = 1 THEN	
	
SET @tipoSolicitud = 1;
	
	   SELECT CABSOLINV_ID,
	    CABSOLINV_FECHAINTERNACION, 
	    CABSOLINV_ESTADOSOLICITUD,
	    CABSOLINV_idServicio,
	   (SELECT ESTA_DESCRIPCION
	    FROM rapsinet_ESTADOSSOLICITUDES WHERE ESTA_ID = CABSOLINV_ESTADOSOLICITUD LIMIT 0,1) AS ESTA_DESCRIPCION,
	   (SELECT TIP_DESCRIPCION 
		 FROM RAPSINET_TIPOSOLICITUDES WHERE TIP_ID=@tipoSolicitud LIMIT 0,1) AS TIP_DESCRIPCION,
		(SELECT DISTINCT LOGSOL_IDUSUARIO FROM
		 RAPSINET_LOGSOLICITUDES WHERE (CABSOL_ID = CABSOLINV_ID AND LOGSOL_IDTIPOSOLICITUD = 1)
		 ORDER BY LOGSOL_FECHA DESC LIMIT 1) AS ID_USUARIO,
		(SELECT CONCAT(USUA_NOMBRES," ",USUA_APELLIDOS)
		 FROM RAPSINET_USUARIOS WHERE USUA_ID = ID_USUARIO) AS USUARIO
	   FROM 
	   rapsinet_cabecerasolicitudinv
	where 
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(1,2,4))
	OR
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	 CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	 idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(1,2,4)
	)
	OR
	( LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(1,2,4)
	 )
	 OR(
	   fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(1,2,4)) 
	 OR
	 (LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	  idEstadoSolicitud = "" and
	  idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(1,2,4))
	 OR(
	   fecha = "" and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(1,2,4))
	OR
	( fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(1,2,4)
	)
	OR
	(idEstadoSolicitud="" and idSolicitud="" and fecha="" and CABSOLINV_ESTADOSOLICITUD IN(1,2,4));
	
END IF;	   


IF id_depto = 2 THEN	
	
SET @tipoSolicitud = 1;
	
	   SELECT CABSOLINV_ID,
	    CABSOLINV_FECHAINTERNACION, 
	    CABSOLINV_ESTADOSOLICITUD,
	    CABSOLINV_idServicio,
	    1 as tipoSolicitud,
	   (SELECT ESTA_DESCRIPCION
	    FROM rapsinet_ESTADOSSOLICITUDES WHERE ESTA_ID = CABSOLINV_ESTADOSOLICITUD LIMIT 0,1) AS ESTA_DESCRIPCION,
	   (SELECT TIP_DESCRIPCION 
		 FROM RAPSINET_TIPOSOLICITUDES WHERE TIP_ID=@tipoSolicitud LIMIT 0,1) AS TIP_DESCRIPCION,
		(SELECT DISTINCT LOGSOL_IDUSUARIO FROM
		 RAPSINET_LOGSOLICITUDES WHERE (CABSOL_ID = CABSOLINV_ID AND LOGSOL_IDTIPOSOLICITUD = 1)
		 ORDER BY LOGSOL_FECHA DESC LIMIT 1) AS ID_USUARIO,
		(SELECT CONCAT(USUA_NOMBRES," ",USUA_APELLIDOS)
		 FROM RAPSINET_USUARIOS WHERE USUA_ID = ID_USUARIO) AS USUARIO
	   FROM 
	   rapsinet_cabecerasolicitudinv
	where 
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(3,6))
	OR
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	 CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	 idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(3,6)
	)
	OR
	( LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(3,6)
	 )
	 OR(
	   fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(3,6)) 
	 OR
	 (LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	  idEstadoSolicitud = "" and
	  idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(3,6))
	 OR(
	   fecha = "" and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(3,6))
	OR
	( fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(3,6)
	)
	OR
	(idEstadoSolicitud="" and idSolicitud="" and fecha="" and CABSOLINV_ESTADOSOLICITUD IN(3,6));
	
END IF;	   


IF id_depto = 3 THEN	
	
SET @tipoSolicitud = 1;
	
	   SELECT CABSOLINV_ID,
	    CABSOLINV_FECHAINTERNACION, 
	    CABSOLINV_ESTADOSOLICITUD,
	    CABSOLINV_idServicio,
	    1 as tipoSolicitud,
	  (SELECT ESTA_DESCRIPCION
	    FROM rapsinet_ESTADOSSOLICITUDES WHERE ESTA_ID = CABSOLINV_ESTADOSOLICITUD LIMIT 0,1) AS ESTA_DESCRIPCION,
	   (SELECT TIP_DESCRIPCION 
		 FROM RAPSINET_TIPOSOLICITUDES WHERE TIP_ID=@tipoSolicitud LIMIT 0,1) AS TIP_DESCRIPCION,
		(SELECT DISTINCT LOGSOL_IDUSUARIO FROM
		 RAPSINET_LOGSOLICITUDES WHERE (CABSOL_ID = CABSOLINV_ID AND LOGSOL_IDTIPOSOLICITUD = 1)
		 ORDER BY LOGSOL_FECHA DESC LIMIT 1) AS ID_USUARIO,
		(SELECT CONCAT(USUA_NOMBRES," ",USUA_APELLIDOS)
		 FROM RAPSINET_USUARIOS WHERE USUA_ID = ID_USUARIO) AS USUARIO
	   FROM 
	   rapsinet_cabecerasolicitudinv
	where 
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(5,8))
	OR
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	 CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	 idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(5,8)
	)
	OR
	( LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(5,8)
	 )
	 OR(
	   fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(5,8)) 
	 OR
	 (LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	  idEstadoSolicitud = "" and
	  idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(5,8))
	 OR(
	   fecha = "" and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(5,8))
	OR
	( fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(5,8)
	)
	OR
	(idEstadoSolicitud="" and idSolicitud="" and fecha="" and CABSOLINV_ESTADOSOLICITUD IN(5,8));
	
END IF;	   

IF id_depto = 4 THEN	
	
SET @tipoSolicitud = 1;
	
	   SELECT CABSOLINV_ID,
	    CABSOLINV_FECHAINTERNACION, 
	    CABSOLINV_ESTADOSOLICITUD,
	    CABSOLINV_idServicio,
	    1 as tipoSolicitud,
	  (SELECT ESTA_DESCRIPCION
	    FROM rapsinet_ESTADOSSOLICITUDES WHERE ESTA_ID = CABSOLINV_ESTADOSOLICITUD LIMIT 0,1) AS ESTA_DESCRIPCION,
	   (SELECT TIP_DESCRIPCION 
		 FROM RAPSINET_TIPOSOLICITUDES WHERE TIP_ID=@tipoSolicitud LIMIT 0,1) AS TIP_DESCRIPCION,
		(SELECT DISTINCT LOGSOL_IDUSUARIO FROM
		 RAPSINET_LOGSOLICITUDES WHERE (CABSOL_ID = CABSOLINV_ID AND LOGSOL_IDTIPOSOLICITUD = 1)
		 ORDER BY LOGSOL_FECHA DESC LIMIT 1) AS ID_USUARIO,
		(SELECT CONCAT(USUA_NOMBRES," ",USUA_APELLIDOS)
		 FROM RAPSINET_USUARIOS WHERE USUA_ID = ID_USUARIO) AS USUARIO
	   FROM 
	   rapsinet_cabecerasolicitudinv
	where 
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(7))
	OR
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	 CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	 idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(7)
	)
	OR
	( LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(7)
	 )
	 OR(
	   fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(7)) 
	 OR
	 (LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	  idEstadoSolicitud = "" and
	  idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(7))
	 OR(
	   fecha = "" and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud and CABSOLINV_ESTADOSOLICITUD IN(7))
	OR
	( fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   idSolicitud = "" and CABSOLINV_ESTADOSOLICITUD IN(7)
	)
	OR
	(idEstadoSolicitud="" and idSolicitud="" and fecha="" and CABSOLINV_ESTADOSOLICITUD IN(7));
	
END IF;	   


IF id_depto = 6 THEN	
	
SET @tipoSolicitud = 1;
	
	   SELECT CABSOLINV_ID,
	    CABSOLINV_FECHAINTERNACION, 
	    CABSOLINV_ESTADOSOLICITUD,
	    CABSOLINV_idServicio,
	    1 as tipoSolicitud,
	   (SELECT ESTA_DESCRIPCION
	    FROM rapsinet_ESTADOSSOLICITUDES WHERE ESTA_ID = CABSOLINV_ESTADOSOLICITUD LIMIT 0,1) AS ESTA_DESCRIPCION,
	   (SELECT TIP_DESCRIPCION 
		 FROM RAPSINET_TIPOSOLICITUDES WHERE TIP_ID=@tipoSolicitud LIMIT 0,1) AS TIP_DESCRIPCION,
		(SELECT DISTINCT LOGSOL_IDUSUARIO FROM
		 RAPSINET_LOGSOLICITUDES WHERE (CABSOL_ID = CABSOLINV_ID AND LOGSOL_IDTIPOSOLICITUD = 1)
		 ORDER BY LOGSOL_FECHA DESC LIMIT 1) AS ID_USUARIO,
		(SELECT CONCAT(USUA_NOMBRES," ",USUA_APELLIDOS)
		 FROM RAPSINET_USUARIOS WHERE USUA_ID = ID_USUARIO) AS USUARIO
	   FROM 
	   rapsinet_cabecerasolicitudinv
	where 
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud )
	OR
	(LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	 CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	 idSolicitud = "" 
	)
	OR
	( LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud 
	 )
	 OR(
	   fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   CABSOLINV_ID = idSolicitud ) 
	 OR
	 (LEFT(CABSOLINV_FECHAINTERNACION,10) = fecha and
	  idEstadoSolicitud = "" and
	  idSolicitud = "" )
	 OR(
	   fecha = "" and
	   idEstadoSolicitud = "" and
	   CABSOLINV_ID = idSolicitud )
	OR
	( fecha = "" and
	   CABSOLINV_ESTADOSOLICITUD = idEstadoSolicitud and
	   idSolicitud = "" 
	)
	OR
	(idEstadoSolicitud="" and idSolicitud="" and fecha="" )
	and (isnull(CABSOLINV_rakim) or CABSOLINV_rakim = "");
	
END IF;	   

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolinv
DROP PROCEDURE IF EXISTS `rapsinet_splistasolinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolinv`(IN `cab_id` INT)
BEGIN

SELECT
     RAPSINET_CABECERASOLICITUDINV.CABSOLINV_ID,
     CABSOLINV_IDMEDICO,
	  CABSOLINV_rutPaciente,CABSOLINV_nombresPaciente,CABSOLINV_apellidosPaciente,
	  CABSOLINV_direccionPaciente,CABSOLINV_fechaNacPaciente,CABSOLINV_fechaInternacion,
	  CABSOLINV_fechaSolicitud,CABSOLINV_idServicio,CABSOLINV_idEstablecimiento,
     CABSOLINV_estadoSolicitud,
	  (SELECT EST_NOMBRE FROM RAPSINET_ESTABLECIMIENTOS
	   WHERE EST_ID = CABSOLINV_idEstablecimiento) AS HOSPITAL, 
	  DETSOLINV_opcion,
	  CABSOLINV_observacion
FROM RAPSINET_CABECERASOLICITUDINV
LEFT JOIN RAPSINET_DETALLESOLICITUDINV 
     ON RAPSINET_DETALLESOLICITUDINV.CABSOLINV_ID = RAPSINET_CABECERASOLICITUDINV.CABSOLINV_ID
WHERE
RAPSINET_CABECERASOLICITUDINV.CABSOLINV_ID = CAB_ID;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistasolperalta
DROP PROCEDURE IF EXISTS `rapsinet_splistasolperalta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistasolperalta`(IN `idEstadoSolicitud` INT, IN `idSolicitud` INT, IN `fecha` INT, IN `id_depto` INT)
BEGIN

	if (idEstadoSolicitud = 'Seleccione') then
			set idEstadoSolicitud = null;
	end if;
		
	if (idSolicitud = '')then
			set idSolicitud = null; 
	end if;
		
	if (fecha = '' or fecha is null) then
			set @fechaIni = '01-01-1900';
			set @fechaFin = '01-01-2100';
	else
			set @fechaIni = fecha;
			set @fechaFin = fecha;
	end if;
	
	
	IF id_depto = 1 THEN	
	
	if (idEstadoSolicitud is null) then
				set @visarjuridica = 16;
				set @rechazadoSeremi = 16;
			end if;
			
			if (idEstadoSolicitud = '16') then
				set @visarjuridica = 16;
				set @rechazadoSeremi = 16;
			end if;
	
		SET @tipoSolicitud = 5;
	
	  		SELECT CABALTAHOSPADMIN_id, CABALTAHOSPADMIN_fechaActual, CABALTAHOSPADMIN_idEstadoSolicitud,
		   "Solicitud De Alta Hospitalización Administrativa" AS TIPO,
		   (SELECT LOGSOL_idUsuario FROM RAPSINET_LOGSOLICITUDES WHERE
			 CABSOL_ID = CABALTAHOSPADMIN_id AND LOGSOL_idTipoSolicitud = @tipoSolicitud
			 order by LOGSOL_id desc limit 1) AS ID_USUARIO,
		   (SELECT CONCAT(USUA_nombres," ",USUA_apellidos) FROM RAPSINET_USUARIOS
			 WHERE USUA_ID = ID_USUARIO) AS USUARIO,
			(SELECT ESTA_descripcion from rapsinet_estadossolicitudes
			 Where ESTA_id = CABALTAHOSPADMIN_idEstadoSolicitud) AS ESTADO,
			 concat(CABALTAHOSPADMIN_nombrePac, " " ,CABALTAHOSPADMIN_apaternoPac, " " ,CABALTAHOSPADMIN_amaternoPac) as PACIENTE,
			 CABALTAHOSPADMIN_rutPac,
			 CABALTAHOSPADMIN_fonoPac,
			 CABALTAHOSPADMIN_direccionPac,
			 DETALTHOSPADMIN_idCentroSalud,
			 EST_nombre
		    FROM  rapsinet_cabeceraaltahospadmin
		    INNER JOIN rapsinet_detallealtahospadmin
		    ON(CABALTAHOSPADMIN_id = CABALTHOSPADMIN_id)
		    INNER JOIN rapsinet_establecimientos
		    ON(DETALTHOSPADMIN_idCentroSalud = EST_id)
		    WHERE CABALTAHOSPADMIN_idEstadoSolicitud IN(16); 
			 	
	END IF;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistauncorreo
DROP PROCEDURE IF EXISTS `rapsinet_splistauncorreo`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistauncorreo`(IN `codigo` INT)
BEGIN

	select 
	co.CO_descripcion,
	co.CO_mail
	from rapsinet_correos co
	where co.CO_id = codigo;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistaunusuario
DROP PROCEDURE IF EXISTS `rapsinet_splistaunusuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_splistaunusuario`(IN `CODIGO` INT)
BEGIN
SELECT * FROM RAPSINET_USUARIOS WHERE USUA_id = CODIGO;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistausuarios
DROP PROCEDURE IF EXISTS `rapsinet_splistausuarios`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_splistausuarios`()
BEGIN
SELECT * FROM RAPSINET_USUARIOS USUA
LEFT JOIN RAPSINET_PERFILES PERF
ON USUA.PER_id = PERF.PER_id;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_splistavinculacion
DROP PROCEDURE IF EXISTS `rapsinet_splistavinculacion`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_splistavinculacion`()
BEGIN
	select 
	tipfam.TIPFAM_id,
	tipfam.TIPFAM_descripcion
	from rapsinet_tipofamiliar tipfam;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmisdatos
DROP PROCEDURE IF EXISTS `rapsinet_spmisdatos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmisdatos`(IN `CORREO` VARCHAR(100), IN `PASSWARE` VARCHAR(50), IN `ID_USUARIO` INT)
BEGIN
UPDATE RAPSINET_USUARIOS SET USUA_correo = CORREO , USUA_contrasena = PASSWARE
WHERE USUA_id = ID_USUARIO;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodcorreos
DROP PROCEDURE IF EXISTS `rapsinet_spmodcorreos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodcorreos`(IN `idCabecera` INT, IN `descripcion` VARCHAR(80), IN `correo` VARCHAR(80))
BEGIN
	update
	rapsinet_correos 
	set CO_descripcion = descripcion,
	CO_mail = correo
	where CO_id = idCabecera;
	
	set @mensaje = 'Correo modificado.';
	
	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodificahospital
DROP PROCEDURE IF EXISTS `rapsinet_spmodificahospital`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodificahospital`(IN `NOMBRES` VARCHAR(120), IN `DIRECCION` VARCHAR(200), IN `FONO` VARCHAR(10), IN `CORREO` VARCHAR(80), IN `REGION` INT, IN `PROVINCIA` INT, IN `COMUNA` INT, IN `CONDICION` INT, IN `ID_HOSPITAL` INT, IN `SALUD` INT, IN `ASISTENTE` VARCHAR(200))
BEGIN
UPDATE rapsinet_establecimientos SET
EST_nombre = NOMBRES,EST_ubicacion = DIRECCION,EST_fono = FONO,EST_correo = CORREO,
EST_idRegion = REGION,EST_idProvincia = PROVINCIA,EST_idComuna = COMUNA,EST_psiquiatrico = CONDICION,
SSALUD_id = SALUD,ASSOL_id = ASISTENTE
WHERE EST_id = ID_HOSPITAL;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodificamedico
DROP PROCEDURE IF EXISTS `rapsinet_spmodificamedico`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodificamedico`(IN `RUT` VARCHAR(12), IN `NOMBRES` VARCHAR(120), IN `APELLIDOS` VARCHAR(120), IN `DIRECCION` VARCHAR(200), IN `CELULAR` VARCHAR(20), IN `FONO` VARCHAR(20), IN `CORREO` VARCHAR(80), IN `COD_ESTABLECIMIENTO` INT, IN `ID_USUARIO` INT)
BEGIN
UPDATE RAPSINET_MEDICOS
SET MED_rut = RUT, MED_nombres = NOMBRES, MED_apellidos = APELLIDOS, MED_direccion = DIRECCION, MED_celular = CELULAR,
    MED_fono = FONO, MED_correo = CORREO,EST_id = COD_ESTABLECIMIENTO
WHERE MED_id = ID_USUARIO;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodificasoltratamiento
DROP PROCEDURE IF EXISTS `rapsinet_spmodificasoltratamiento`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodificasoltratamiento`(IN `FEC_SOL` VARCHAR(10), IN `FICHA` INT, IN `FEC_VIS` VARCHAR(10), IN `REGION` INT, IN `PROVINCIA` INT, IN `COMUNA` INT, IN `HOSPITAL` INT, IN `NOMBRE_PAC` VARCHAR(120), IN `AP_PAT` VARCHAR(80), IN `AP_MAT` VARCHAR(80), IN `RUT_PAC` VARCHAR(12), IN `EDAD_PAC` INT, IN `SEXO` INT, IN `DIRECCION` VARCHAR(200), IN `FONO` VARCHAR(10), IN `RUT_DOC` VARCHAR(12), IN `NOM_DOC` VARCHAR(200), IN `PRO_DOC` VARCHAR(80), IN `FONO_DOC` VARCHAR(10), IN `MAIL_DOC` VARCHAR(100), IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(80), IN `apellidosUsuario` VARCHAR(80), IN `rutUsuario` VARCHAR(12), IN `idUsuario` INT, IN `CABECERA` INT, IN `HOSPITAL_PSI` INT)
BEGIN
/*CABECERA*/
	 	 
	  UPDATE RAPSINET_CABECERAEVATRATAMIENTO SET
	  fecha_sol = STR_TO_DATE(FEC_SOL,'%d-%m-%Y'),ficha = FICHA,fecha_visita = STR_TO_DATE(FEC_VIS,'%d-%m-%Y'),
	  region = REGION,provincia = PROVINCIA,comuna = COMUNA,hospital = HOSPITAL,
	  nombres_pac = NOMBRE_PAC,ap_pat = AP_PAT,ap_mat = AP_MAT,rut_pac = RUT_PAC,
	  edad_pac = EDAD_PAC,sexo_pac = SEXO,dire_pac = DIRECCION,
	  fono_pac = FONO
	 	WHERE cab_solevatrat = CABECERA;
 
	 
/*DETALLE*/
	 
	  UPDATE RAPSINET_DETALLEEVATRATAMIENTO SET
	  rut_doc = RUT_DOC,nom_doc = NOM_DOC,prof_doc = PRO_DOC,
	  fono_doc = FONO_DOC,mail_doc = MAIL_DOC, lugar_evaluacion = HOSPITAL_PSI
	   WHERE cab_solevatrat = CABECERA; 
	 
 /*LOG*/
	 
SET @idEstadoSolicitud = (SELECT estado_sol FROM RAPSINET_CABECERAEVATRATAMIENTO WHERE cab_solevatrat = CABECERA);

INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 CABECERA,now(),idUsuario,idDepartamento,2,@idEstadoSolicitud);
		
			 
set @idAccion = 14;
			 	 
 INSERT INTO rapsinet_historicousuarios(
 HIST_nombres,HIST_apellidos,HIST_rut,
 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)VALUES(
 nombresUsuario, apellidosUsuario, rutUsuario,
 now(), @idAccion,CABECERA, 2); 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodificausuario
DROP PROCEDURE IF EXISTS `rapsinet_spmodificausuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rapsinet_spmodificausuario`(IN `RUT` VARCHAR(12), IN `NOMBRES` VARCHAR(60), IN `APELLIDOS` VARCHAR(60), IN `DIRECCION` VARCHAR(120), IN `CELULAR` VARCHAR(10), IN `FONO` VARCHAR(10), IN `CORREO` VARCHAR(80), IN `USUARIO` VARCHAR(30), IN `CLAVE` VARCHAR(30), IN `ID_USUARIO` INT)
BEGIN
UPDATE RAPSINET_USUARIOS
SET USUA_rut = RUT, USUA_nombres = NOMBRES, USUA_apellidos = APELLIDOS, USUA_direccion = DIRECCION, USUA_celular = CELULAR,
    USUA_fono = FONO, USUA_correo = CORREO, USUA_usuario = USUARIO, USUA_contrasena = CLAVE
WHERE USUA_id = ID_USUARIO;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodsolaltaadmin
DROP PROCEDURE IF EXISTS `rapsinet_spmodsolaltaadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodsolaltaadmin`(IN `idCabecera` INT, IN `idEstablecimiento` INT, IN `fechaIngreso` vARCHAR(10), IN `fechaEgreso` vARCHAR(10), IN `nombrePaciente` VARCHAR(80), IN `apellidoPaterno` VARCHAR(50), IN `apellidoMaterno` vARCHAR(50), IN `rutPaciente` vARCHAR(50), IN `direccionPaciente` vARCHAR(100), IN `fonoPaciente` INT, IN `diagnostico` vARCHAR(250), IN `evolucionClinica` vARCHAR(250), IN `tratamiento` vARCHAR(250), IN `idCentroSlud` INT, IN `direccionReferente` vARCHAR(90), IN `idMedico` INT, IN `fechaReferente` vARCHAR(10), IN `idMedicoTratante` INT, IN `idDepartamento` INT, IN `nombresUsuario` vARCHAR(70), IN `apellidosUsuario` vARCHAR(70), IN `rutUsuario` vARCHAR(50), IN `idUsuario` INT)
BEGIN

	UPDATE rapsinet_cabeceraaltahospadmin SET 
	CABALTAHOSPADMIN_idEstablecimiento = idEstablecimiento,
	CABALTAHOSPADMIN_fechaIngreso = STR_TO_DATE(fechaIngreso,'%d-%m-%Y'),
	CABALTAHOSPADMIN_fechaEgreso = STR_TO_DATE(fechaEgreso,'%d-%m-%Y'),
	CABALTAHOSPADMIN_fechaActual = now(),
	CABALTAHOSPADMIN_nombrePac = nombrePaciente,
	CABALTAHOSPADMIN_apaternoPac = apellidoPaterno,
	CABALTAHOSPADMIN_amaternoPac = apellidoMaterno,
	CABALTAHOSPADMIN_rutPac = rutPaciente,
	CABALTAHOSPADMIN_direccionPac = direccionPaciente,
	CABALTAHOSPADMIN_fonoPac = fonoPaciente
	WHERE CABALTAHOSPADMIN_id = idCabecera;
	
	UPDATE rapsinet_detallealtahospadmin SET
	DETALTHOSPADMIN_diagnostico = diagnostico,
	DETALTHOSPADMIN_evolucionClinica = evolucionClinica,
	DETALTHOSPADMIN_tratamiento = tratamiento,
	DETALTHOSPADMIN_idCentroSalud = idCentroSlud,
	DETALTHOSPADMIN_direccionRef = direccionReferente,
	DETALTHOSPADMIN_idProfesionalMedico = idMedico,
	DETALTHOSPADMIN_fechaRef = STR_TO_DATE(fechaReferente, '%d-%m-%Y'),
	DETALTHOSPADMIN_idMedicoTrat = idMedicoTratante
	WHERE CABALTHOSPADMIN_id = idCabecera;


	SET @idEstadoSolicitud = (SELECT CABALTAHOSPADMIN_idEstadoSolicitud FROM rapsinet_cabeceraaltahospadmin WHERE CABALTAHOSPADMIN_id = idCabecera);
	
	
	INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idCabecera,now(),idUsuario,idDepartamento,5,@idEstadoSolicitud);
		
			 
	set @idAccion = 12;
			 	 
   INSERT INTO rapsinet_historicousuarios(
   HIST_nombres,HIST_apellidos,HIST_rut,
   HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)VALUES(
   nombresUsuario, apellidosUsuario, rutUsuario,
   now(), @idAccion,idCabecera, 5); 



END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodsolintadmin
DROP PROCEDURE IF EXISTS `rapsinet_spmodsolintadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodsolintadmin`(IN `idEstablecimientoSol` INT, IN `opciones` vARCHAR(50), IN `diagnostico` VARCHAR(500), IN `otroAntecedente` vARCHAR(500), IN `ultimoTratamiento` vARCHAR(500), IN `idEstablecimientoInt` INT, IN `nombreMedico` VARCHAR(100), IN `rutMedico` VARCHAR(100), IN `correoMedico` vARCHAR(100), IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(100), IN `idUsuario` INT, IN `nombres_sol` VARCHAR(50), IN `apaterno_sol` vARCHAR(50), IN `amaterno_sol` vARCHAR(50), IN `rut_sol` vARCHAR(50), IN `edad_sol` INT, IN `sexo_sol` vARCHAR(50), IN `calle_sol` vARCHAR(50), IN `numerocasa_sol` vARCHAR(50), IN `sector_sol` vARCHAR(50), IN `comuna_sol` INT, IN `ciudad_sol` INT, IN `region_sol` INT, IN `telefono_sol` INT, IN `celular_sol` INT, IN `correo_sol` vARCHAR(50), IN `nombres_pac` vARCHAR(50), IN `apaterno_pac` vARCHAR(50), IN `amaterno_pac` vARCHAR(50), IN `rut_pac` vARCHAR(50), IN `edad_pac` INT, IN `sexo_pac` vARCHAR(50), IN `calle_pac` vARCHAR(50), IN `numerocasa_pac` VARCHAR(50), IN `sector_pac` VARCHAR(50), IN `comuna_pac` INT, IN `ciudad_pac` INT, IN `region_pac` INT, IN `vinculacion_pac` VARCHAR(50), IN `idCabecera` INT)
BEGIN

   UPDATE rapsinet_cabecerainternacionadmin SET 
	CABINTADMIN_fechaEvaluacion = now(),
	CABINTADMIN_idEstablecimientoSol = idEstablecimientoSol,
	CABINTADMIN_idUsuario = idUsuario
	WHERE rapsinet_cabecerainternacionadmin.CABINTADMIN_id = idCabecera;
	
	UPDATE rapsinet_detalleinternacionadmin SET
	DETINTADMIN_opciones = opciones,
	DETINTADMIN_diagnostico = diagnostico,
	DETINTADMIN_otroAntenedente = otroAntecedente,
	DETINTADMIN_ultimoTratamiento = ultimoTratamiento,
	DETINTADMIN_idEstablecimientoInt = idEstablecimientoInt,
	DETINTADMIN_nombreMedico =nombreMedico,
	DETINTADMIN_rutMedico = rutMedico,
	DETINTADMIN_correoMedico = correoMedico,
	DETINTADMIN_aPaternoSol	= apaterno_sol,
	DETINTADMIN_aMaternoSol	= amaterno_sol,
	DETINTADMIN_nombresSol = nombres_sol,
	DETINTADMIN_rutSol = rut_sol,	
	DETINTADMIN_edadSol = edad_sol,	
	DETINTADMIN_sexoSol = sexo_sol,
	DETINTADMIN_calleSol = calle_sol,
	DETINTADMIN_nCasaSol = numerocasa_sol,
	DETINTADMIN_edadSol = edad_sol,
	DETINTADMIN_sexoSol = comuna_sol,
	DETINTADMIN_sectorSol = sector_sol,
	DETINTADMIN_idComunaSol = comuna_sol,
	DETINTADMIN_idCiudadSol = ciudad_sol,
	DETINTADMIN_idRegionSol = region_sol,
	DETINTADMIN_telefonoSol = telefono_sol,
	DETINTADMIN_celularSol = celular_sol,
	DETINTADMIN_correoSol = correo_sol,
	DETINTADMIN_aPaternoPac = apaterno_pac,
	DETINTADMIN_aMaternoPac = amaterno_pac,
	DETINTADMIN_nombresPac = nombres_pac,
	DETINTADMIN_rutPac = rut_pac,
	DETINTADMIN_edadPac = edad_pac,
	DETINTADMIN_sexoPac = sexo_pac,
	DETINTADMIN_callePac = calle_pac,
	DETINTADMIN_nCasaPac = numerocasa_pac,
	DETINTADMIN_sectorPac = sector_pac,
	DETINTADMIN_idComunaPac = comuna_pac,
	DETINTADMIN_idCiudadPac = ciudad_pac,
	DETINTADMIN_idRegionPac	= region_pac,
	DETINTADMIN_vinculacion = vinculacion_pac
	WHERE rapsinet_detalleinternacionadmin.CABINTADMIN_id = idCabecera;
	
	SET @idEstadoSolicitud = (SELECT CABINTADMIN_idEstadoSolicitud FROM rapsinet_cabecerainternacionadmin WHERE CABINTADMIN_id = idCabecera);
	
	
	INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idCabecera,now(),idUsuario,idDepartamento,3,@idEstadoSolicitud);
		
			 
	set @idAccion = 8;
			 	 
   INSERT INTO rapsinet_historicousuarios(
   HIST_nombres,HIST_apellidos,HIST_rut,
   HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)VALUES(
   nombresUsuario, apellidosUsuario, rutUsuario,
   now(), @idAccion,idCabecera, 3); 
   
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmodsolinv
DROP PROCEDURE IF EXISTS `rapsinet_spmodsolinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmodsolinv`(IN `ID_CABECERA` INT, IN `ID_MEDICO` INT, IN `RUT_PACIENTE` VARCHAR(12), IN `NOMBRES_PACIENTE` VARCHAR(120), IN `APELLIDOS_PACIENTE` VARCHAR(120), IN `DIR_PACIENTE` VARCHAR(200), IN `NAC_PACIENTE` DATE, IN `INT_PACIENTE` DATE, IN `SOL_PACIENTE` DATE, IN `ID_SERVICIO` INT, IN `ID_ESTABLECIMIENTO` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(200), IN `apellidosUsuario` VARCHAR(200), IN `rutUsuario` VARCHAR(12), IN `idUsuario` INT)
BEGIN
UPDATE RAPSINET_CABECERASOLICITUDINV SET CABSOLINV_IDMEDICO=ID_MEDICO,
	  CABSOLINV_rutPaciente=RUT_PACIENTE,CABSOLINV_nombresPaciente=NOMBRES_PACIENTE,
	  CABSOLINV_apellidosPaciente=APELLIDOS_PACIENTE,
	  CABSOLINV_direccionPaciente=DIR_PACIENTE,CABSOLINV_fechaNacPaciente=NAC_PACIENTE,
	  CABSOLINV_fechaInternacion=INT_PACIENTE,CABSOLINV_fechaSolicitud=SOL_PACIENTE,
	  CABSOLINV_idServicio=ID_SERVICIO,CABSOLINV_idEstablecimiento=ID_ESTABLECIMIENTO
WHERE CABSOLINV_ID = ID_CABECERA;

SET @idEstadoSolicitud = (SELECT CABSOLINV_ESTADOSOLICITUD FROM rapsinet_CABECERASOLICITUDINV WHERE CABSOLINV_ID = ID_CABECERA);

INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 ID_CABECERA,now(),idUsuario,idDepartamento,1,@idEstadoSolicitud);
		
			 
set @idAccion = 7;
			 	 
 INSERT INTO rapsinet_historicousuarios(
 HIST_nombres,HIST_apellidos,HIST_rut,
 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)VALUES(
 nombresUsuario, apellidosUsuario, rutUsuario,
 now(), @idAccion,ID_CABECERA, 1); 
 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarcomentarios
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarcomentarios`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarcomentarios`(IN `idCabecera` INT)
BEGIN
	select
	sia.COMSIA_id,
	sia.COMSIA_idCabeceraSol,
	sia.COMSIA_comentario,
	sia.COMSIA_fecha
	from rapsinet_comentarios_sia sia
	where sia.COMSIA_idCabeceraSol = idCabecera;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarcomentarios_alta
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarcomentarios_alta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarcomentarios_alta`(IN `idCabecera` INT)
BEGIN
	select
	alta.COMALTA_id,
	alta.COMALTA_idCabeceraSol,
	alta.COMALTA_comentario,
	alta.COMALTA_fecha
	from rapsinet_comentarios_alta alta
	where alta.COMALTA_idCabeceraSol = idCabecera;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarcomentarios_eva
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarcomentarios_eva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarcomentarios_eva`(IN `idCabecera` INT)
BEGIN
	select
	COMINV_id,
	COMINV_idCabeceraSol,
	COMINV_comentario,
	COMINV_fecha
	from rapsinet_comentarios_soleva
	where COMINV_idCabeceraSol = idCabecera;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarcomentarios_inv
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarcomentarios_inv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarcomentarios_inv`(IN `idCabecera` INT)
BEGIN
	select
	inv.COMINV_id,
	inv.COMINV_idCabeceraSol,
	inv.COMINV_comentario,
	inv.COMINV_fecha
	from rapsinet_comentarios_inv inv
	where inv.COMINV_idCabeceraSol = idCabecera;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarestadosolintadmin_contraloria
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarestadosolintadmin_contraloria`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarestadosolintadmin_contraloria`()
BEGIN
	select 
	estsol.ESTA_id,
	estsol.ESTA_descripcion
	from rapsinet_estadossolicitudes estsol
	where estsol.ESTA_id = 7;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarestadosolintadmin_dsp
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarestadosolintadmin_dsp`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarestadosolintadmin_dsp`()
BEGIN
	select 
	estsol.ESTA_id,
	estsol.ESTA_descripcion
	from rapsinet_estadossolicitudes estsol
	where estsol.ESTA_id = 1 or estsol.ESTA_id = 4;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarestadosolintadmin_juridica
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarestadosolintadmin_juridica`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarestadosolintadmin_juridica`()
BEGIN

	select 
	estsol.ESTA_id,
	estsol.ESTA_descripcion
	from rapsinet_estadossolicitudes estsol
	where estsol.ESTA_id = 3 or estsol.ESTA_id = 6;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarestadosolintadmin_seremi
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarestadosolintadmin_seremi`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarestadosolintadmin_seremi`()
BEGIN
	select 
	estsol.ESTA_id,
	estsol.ESTA_descripcion
	from rapsinet_estadossolicitudes estsol
	where estsol.ESTA_id = 5 or estsol.ESTA_id = 8;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarlistahistoricousuarios
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarlistahistoricousuarios`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarlistahistoricousuarios`()
BEGIN
	select
	HIST_id,
	HIST_nombres,
	HIST_apellidos,
	HIST_fecha,
	HIST_idAccion,
	HIST_idTipoSolicitud,
	HIST_idCabecera,
	(Select ACC_descripcion from rapsinet_acciones
	 where ACC_id = HIST_idAccion) as Accion,
	(Select TIP_descripcion from rapsinet_tiposolicitudes
	 where TIP_id = HIST_idTipoSolicitud limit 1) as Tipo
	
	from rapsinet_historicousuarios;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarlogsoladmin_filtros
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarlogsoladmin_filtros`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarlogsoladmin_filtros`(IN `FILTRO` VARCHAR(10), IN `OPCION` INT)
BEGIN

IF OPCION = 1 THEN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
WHERE LEFT(logsol.LOGSOL_fecha,10) = FILTRO;
END IF;

IF OPCION = 2 THEN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
WHERE logsol.LOGSOL_idEstadoSolicitud = FILTRO;
END IF;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarlogsolicitudes
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarlogsolicitudes`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarlogsolicitudes`()
BEGIN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud);
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarlogsolicitudeseventos
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarlogsolicitudeseventos`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarlogsolicitudeseventos`(IN `IdSolicitud` INT, IN `tipoSolicitud` INT)
BEGIN
	select
	logsol.LOGSOL_id,
	logsol.CABSOL_id,
	logsol.LOGSOL_fecha,
	logsol.LOGSOL_idUsuario,
	usua.USUA_nombres,
	usua.USUA_apellidos,
	logsol.LOGSOL_idDepartamento,
	dep.DEP_descripcion,
	logsol.LOGSOL_idTipoSolicitud,
	tipsol.TIP_descripcion,
	logsol.LOGSOL_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	logsol.LOGSOL_observaciones
	from rapsinet_logsolicitudes logsol
	inner join rapsinet_usuarios usua
	on(usua.USUA_id = logsol.LOGSOL_idUsuario)
	inner join rapsinet_departamentos dep
	on(dep.DEP_id = logsol.LOGSOL_idDepartamento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = logsol.LOGSOL_idEstadoSolicitud)
	inner join rapsinet_tiposolicitudes tipsol
	on(tipsol.TIP_id = logsol.LOGSOL_idTipoSolicitud)
	where logsol.CABSOL_id = IdSolicitud and
	logsol.LOGSOL_idTipoSolicitud = tipoSolicitud;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarsolicitudaltaporvisar
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarsolicitudaltaporvisar`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarsolicitudaltaporvisar`(IN `idCabecera` INT)
BEGIN
	select 
	cabalta.CABALTAHOSPADMIN_id,
	cabalta.CABALTAHOSPADMIN_idEstablecimiento,
	estab_1.EST_nombre,
	5 as tipoSolicitud,
	date_format(cabalta.CABALTAHOSPADMIN_fechaIngreso, '%d-%m-%Y')as CABALTAHOSPADMIN_fechaIngreso,
   date_format(cabalta.CABALTAHOSPADMIN_fechaEgreso, '%d-%m-%Y')as CABALTAHOSPADMIN_fechaEgreso,
	now() as fecha_actual,
	cabalta.CABALTAHOSPADMIN_nombrePac,
	cabalta.CABALTAHOSPADMIN_apaternoPac,
	cabalta.CABALTAHOSPADMIN_amaternoPac,
	cabalta.CABALTAHOSPADMIN_rutPac,
	cabalta.CABALTAHOSPADMIN_direccionPac,
	cabalta.CABALTAHOSPADMIN_fonoPac,
	cabalta.CABALTAHOSPADMIN_idEstadoSolicitud,
	estsol.ESTA_descripcion,
	cabalta.CABALTAHOSPADMIN_observaciones,
	detalta.DETALTHOSPADMIN_diagnostico,
	detalta.DETALTHOSPADMIN_evolucionClinica,
	detalta.DETALTHOSPADMIN_tratamiento,
	detalta.DETALTHOSPADMIN_idCentroSalud,
	detalta.DETALTHOSPADMIN_direccionRef,
	detalta.DETALTHOSPADMIN_opciones,
	detalta.DETALTHOSPADMIN_idProfesionalMedico,
	date_format(detalta.DETALTHOSPADMIN_fechaRef, '%d-%m-%Y')as DETALTHOSPADMIN_fechaRef,
	detalta.DETALTHOSPADMIN_idMedicoTrat
	from rapsinet_cabeceraaltahospadmin cabalta
	inner join rapsinet_establecimientos estab_1
	on(estab_1.EST_id = cabalta.CABALTAHOSPADMIN_idEstablecimiento)
	inner join rapsinet_estadossolicitudes estsol
	on(estsol.ESTA_id = cabalta.CABALTAHOSPADMIN_idEstadoSolicitud)
	inner join rapsinet_detallealtahospadmin detalta
	on(detalta.CABALTHOSPADMIN_id = cabalta.CABALTAHOSPADMIN_id)
	where cabalta.CABALTAHOSPADMIN_id = idCabecera;
	
	
	
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spmostrarsolicitudinternacionadminporvisar
DROP PROCEDURE IF EXISTS `rapsinet_spmostrarsolicitudinternacionadminporvisar`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spmostrarsolicitudinternacionadminporvisar`(IN `idSolicitud` INT)
BEGIN

	set @ultimoTipoSolicitud = (select LOGSOL_idEstadoSolicitud  from rapsinet_logsolicitudes
	where CABSOL_id = idSolicitud order by LOGSOL_fecha ASC  limit 1);

	select distinct
	cabintadmin.CABINTADMIN_id,
	cabintadmin.CABINTADMIN_idEstadoSolicitud,
	cabintadmin.CABINTADMIN_observaciones,
   cabintadmin.CABINTADMIN_fechaEvaluacion,
	estsol.EST_nombre as estsol,
	cabintadmin.CABINTADMIN_idEstablecimientoSol,
	detintadmin.DETINTADMIN_opciones,
	detintadmin.DETINTADMIN_diagnostico,
	detintadmin.DETINTADMIN_otroAntenedente,
	detintadmin.DETINTADMIN_ultimoTratamiento,
	estint.EST_nombre as estint,
	detintadmin.DETINTADMIN_idEstablecimientoInt,
	detintadmin.DETINTADMIN_nombreMedico,
	detintadmin.DETINTADMIN_rutMedico,
	detintadmin.DETINTADMIN_correoMedico,
	cabintadmin.CABINTADMIN_idEstadoSolicitud,
	detintadmin.DETINTADMIN_aPaternoSol,
	detintadmin.DETINTADMIN_aMaternoSol,
	detintadmin.DETINTADMIN_nombresSol,
	detintadmin.DETINTADMIN_rutSol,
	detintadmin.DETINTADMIN_edadSol,
	detintadmin.DETINTADMIN_sexoSol,
	detintadmin.DETINTADMIN_calleSol,
	detintadmin.DETINTADMIN_nCasaSol,
	detintadmin.DETINTADMIN_sectorSol,
	detintadmin.DETINTADMIN_idComunaSol,
	com_sol.COM_nombre as comuna_sol,
	detintadmin.DETINTADMIN_idCiudadSol,
	prov_sol.PROV_nombre as ciudad_sol,
	detintadmin.DETINTADMIN_idRegionSol,
	reg_sol.REG_nombre as region_sol,
	detintadmin.DETINTADMIN_telefonoSol,
	detintadmin.DETINTADMIN_celularSol,
	detintadmin.DETINTADMIN_correoSol,
	detintadmin.DETINTADMIN_aPaternoPac,
	detintadmin.DETINTADMIN_aMaternoPac,
	detintadmin.DETINTADMIN_nombresPac,
	detintadmin.DETINTADMIN_rutPac,
	detintadmin.DETINTADMIN_edadPac,
	detintadmin.DETINTADMIN_sexoPac,
	detintadmin.DETINTADMIN_callePac,
	detintadmin.DETINTADMIN_nCasaPac,
	detintadmin.DETINTADMIN_sectorPac,
	detintadmin.DETINTADMIN_idComunaPac,
	com_pac.COM_nombre as comuna_pac,
	detintadmin.DETINTADMIN_idCiudadPac,
	prov_pac.PROV_nombre as ciudad_pac,
	detintadmin.DETINTADMIN_idRegionPac,
	reg_pac.REG_nombre as region_pac,
	detintadmin.DETINTADMIN_vinculacion,
	@ultimoTipoSolicitud as ultimoTipoSolicitud 
	from rapsinet_cabecerainternacionadmin cabintadmin
	inner join rapsinet_detalleinternacionadmin detintadmin
	on(cabintadmin.CABINTADMIN_id = detintadmin.CABINTADMIN_id)
	inner join rapsinet_establecimientos estsol
	on(estsol.EST_id = cabintadmin.CABINTADMIN_idEstablecimientoSol)
	inner join rapsinet_establecimientos estint
	on(estint.EST_id = detintadmin.DETINTADMIN_idEstablecimientoInt)
	inner join rapsinet_regiones reg_sol
	on(reg_sol.REG_id = detintadmin.DETINTADMIN_idRegionSol)	
	inner join rapsinet_regiones reg_pac
	on(reg_pac.REG_id = detintadmin.DETINTADMIN_idRegionPac)
	inner join rapsinet_provincias prov_sol
	on(prov_sol.PROV_id = detintadmin.DETINTADMIN_idCiudadSol)
	inner join rapsinet_provincias prov_pac
	on(prov_pac.PROV_id = detintadmin.DETINTADMIN_idCiudadPac)
	inner join rapsinet_comunas com_sol
	on(com_sol.COM_id = detintadmin.DETINTADMIN_idComunaSol)
	inner join rapsinet_comunas com_pac
	on(com_pac.COM_id = detintadmin.DETINTADMIN_idComunaPac)
	
	where cabintadmin.CABINTADMIN_id = idSolicitud;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spperfiles
DROP PROCEDURE IF EXISTS `rapsinet_spperfiles`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spperfiles`(IN `ID_PERFIL` INT)
BEGIN
SELECT * FROM RAPSINET_PERFILES_FUNCIONALIDADES PER
LEFT JOIN RAPSINET_FUNCIONES FUN
ON PER.FUNC_id = FUN.FUNC_id
WHERE PER.PER_id = ID_PERFIL;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spprimerallamadaET
DROP PROCEDURE IF EXISTS `rapsinet_spprimerallamadaET`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spprimerallamadaET`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombreUsuario` VARCHAR(50), IN `apellidosUsuario` VARCHAR(50), IN `rutUsuario` VARCHAR(50), IN `idUsuario` INT, IN `opcion` INT)
BEGIN
IF opcion = 1 THEN

	  set @idEstadoSolicitud = 13;		
		
	  UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud
     WHERE cab_solevatrat = idCabecera;
 
 	  set @mensaje = concat("La solicitud Nº: ", idCabecera, "Enviada exitosamente");	 
 	  
 	  
 	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 13;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 5;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 


END IF;

IF opcion = 0 THEN

		set @estadoSolicitud = 12;
		set @idCabecera = idCabecera;
		
		
		set @camasDisponibles = (select est.EST_camasDisponibles from rapsinet_cabeceraevatratamiento cabeva
								inner join rapsinet_detalleevatratamiento deteva
								on(cabeva.cab_solevatrat = deteva.cab_solevatrat)
								inner join rapsinet_establecimientos est
								on est.EST_id = cabeva.hospital
								where est.EST_psiquiatrico = 1 and cabeva.cab_solevatrat = idCabecera);
								
      set @establecimiento = (select est.EST_id from rapsinet_cabeceraevatratamiento cabeva
								inner join rapsinet_detalleevatratamiento deteva
								on(cabeva.cab_solevatrat = deteva.cab_solevatrat)
								inner join rapsinet_establecimientos est
								on est.EST_id = cabeva.hospital
								where est.EST_psiquiatrico = 1 and cabeva.cab_solevatrat = idCabecera);					
		
		set @resultado = @camasDisponibles + 1;
		
	   UPDATE rapsinet_establecimientos SET EST_camasDisponibles= @resultado
      WHERE EST_id = @establecimiento;
		
		 
		UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @estadoSolicitud
	   WHERE cab_solevatrat = idCabecera;
	   
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 12;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 17;
			 	 
		   insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
	   
	   
	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "Paso a estado: En espera Segunda llamada");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spprovinciasxregion
DROP PROCEDURE IF EXISTS `rapsinet_spprovinciasxregion`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spprovinciasxregion`(IN `ID_REGION` INT)
BEGIN
SELECT * FROM RAPSINET_PROVINCIAS
WHERE rapsinet_regiones_REG_id = ID_REGION
Order By PROV_nombre;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprakim_solalta
DROP PROCEDURE IF EXISTS `rapsinet_sprakim_solalta`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprakim_solalta`(IN `ID_SOL` INT, IN `RAKIM` VARCHAR(50))
BEGIN
	SET @VERIFICO = (SELECT COUNT(1) FROM RAPSINET_CABECERAALTAHOSPADMIN WHERE CABALTAHOSPADMIN_rakim = RAKIM);
	SET @MENSAJE = concat("Rakim : ",RAKIM," asignado correctamente a la solicitud: ",ID_SOL);
	
	IF (@VERIFICO = 0) THEN
	UPDATE RAPSINET_CABECERAALTAHOSPADMIN
	SET CABALTAHOSPADMIN_rakim = RAKIM
	WHERE CABALTAHOSPADMIN_id = ID_SOL;
	END IF;
	
	IF (@VERIFICO > 0) THEN
	SET @MENSAJE = "Rakim ya existe en el sistema!!!";
	END IF;
	
	SELECT @MENSAJE;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprakim_soleva
DROP PROCEDURE IF EXISTS `rapsinet_sprakim_soleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprakim_soleva`(IN `ID_SOL` INT, IN `RAKIM` VARCHAR(50))
BEGIN
SET @VERIFICO = (SELECT COUNT(1) FROM RAPSINET_CABECERAEVATRATAMIENTO WHERE rakim_eva = RAKIM);
SET @MENSAJE = concat("Rakim : ",RAKIM," asignado correctamente a la solicitud: ",ID_SOL);

IF (@VERIFICO = 0) THEN
UPDATE RAPSINET_CABECERAEVATRATAMIENTO
SET rakim_eva = RAKIM
WHERE cab_solevatrat = ID_SOL;
END IF;

IF (@VERIFICO > 0) THEN
SET @MENSAJE = "Rakim ya existe en el sistema!!!";
END IF;

SELECT @MENSAJE;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprakim_solint
DROP PROCEDURE IF EXISTS `rapsinet_sprakim_solint`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprakim_solint`(IN `ID_SOL` INT, IN `RAKIM` VARCHAR(50))
BEGIN
SET @VERIFICO = (SELECT COUNT(1) FROM RAPSINET_CABECERAINTERNACIONADMIN WHERE CABINTADMIN_rakim = RAKIM);
SET @MENSAJE = concat("Rakim : ",RAKIM," asignado correctamente a la solicitud: ",ID_SOL);

IF (@VERIFICO = 0) THEN
UPDATE RAPSINET_CABECERAINTERNACIONADMIN
SET CABINTADMIN_rakim = RAKIM
WHERE CABINTADMIN_id = ID_SOL;
END IF;

IF (@VERIFICO > 0) THEN
SET @MENSAJE = "Rakim ya existe en el sistema!!!";
END IF;

SELECT @MENSAJE;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprakim_solinv
DROP PROCEDURE IF EXISTS `rapsinet_sprakim_solinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprakim_solinv`(IN `ID_SOL` INT, IN `RAKIM` VARCHAR(50))
BEGIN
SET @VERIFICO = (SELECT COUNT(1) FROM RAPSINET_CABECERASOLICITUDINV WHERE CABSOLINV_rakim = RAKIM);
SET @MENSAJE = concat("Rakim : ",RAKIM," asignado correctamente a la solicitud: ",ID_SOL);

IF (@VERIFICO = 0) THEN
UPDATE RAPSINET_CABECERASOLICITUDINV
SET CABSOLINV_rakim = RAKIM
WHERE CABSOLINV_id = ID_SOL;
END IF;

IF (@VERIFICO > 0) THEN
SET @MENSAJE = "Rakim ya existe en el sistema!!!";
END IF;

SELECT @MENSAJE;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprechazarsolicitudaltaadmin
DROP PROCEDURE IF EXISTS `rapsinet_sprechazarsolicitudaltaadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprechazarsolicitudaltaadmin`(IN `idSolicitud` INT, IN `observacion` vARCHAR(500), IN `idDepartamento` INT, IN `nombresUsuario` vARCHAR(120), IN `apellidosUsuario` vARCHAR(120), IN `rutUsuario` vARCHAR(50), IN `idUsuario` INT, IN `idEstadoDocumento` INT)
BEGIN
/*DSP*/

    if idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 then

       /*PASA A: RECHAZADO POR DPS*/
	    set @idEstadoSolicitud = 2;
	
		 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABALTAHOSPADMIN_observaciones = observacion
		 where CABALTAHOSPADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 5;
		 set @idEstadoSolicitud = 2;
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 11;
		 	 
		       insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
		 set @mensaje = 'Visación rechazada exitósamente';
	end if;
		
/*Juridica*/

	if idEstadoDocumento = 3 or idEstadoDocumento = 6 then	 
	
		 /*PASA A: RECHAZADO POR DPS*/
	    set @idEstadoSolicitud = 4;
	
		 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABALTAHOSPADMIN_observaciones = observacion
		 where CABALTAHOSPADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 5;
		 set @idEstadoSolicitud = 4; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 11;
		 	 
				   insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;
	
/*Seremi*/

	if idEstadoDocumento = 5 or idEstadoDocumento = 8 then	 
	
		 /*PASA A: RECHAZADO POR SEREMI*/
	    set @idEstadoSolicitud = 6;
	
		 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABALTAHOSPADMIN_observaciones = observacion
		 where CABALTAHOSPADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 5;
		 set @idEstadoSolicitud = 6; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 11;
		 	 
		      insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;

/*Contraloria*/	

	if idEstadoDocumento = 7 then	 
	
		 /*PASA A: RECHAZADO POR SEREMI*/
	    set @idEstadoSolicitud = 8;
	
		 update rapsinet_cabeceraaltahospadmin set CABALTAHOSPADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABALTAHOSPADMIN_observaciones = observacion
		 where CABALTAHOSPADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 5;
		 set @idEstadoSolicitud = 8; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 11;
		 	 
		      insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;
	
	select @mensaje;	 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprechazarsolicitudinternacionadmin
DROP PROCEDURE IF EXISTS `rapsinet_sprechazarsolicitudinternacionadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprechazarsolicitudinternacionadmin`(IN `idSolicitud` INT, IN `observacion` VARCHAR(500), IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(100), IN `idUsuario` INT, IN `idEstadoDocumento` INT)
BEGIN

/*DSP*/

    if idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 then

       /*PASA A: RECHAZADO POR DPS*/
	    set @idEstadoSolicitud = 2;
	
		 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABINTADMIN_observaciones = observacion
		 where CABINTADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 3;
		 set @idEstadoSolicitud = 2;
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 6;
		 	 
		       insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
	
		 set @mensaje = 'Visación rechazada exitósamente';
	end if;
		
/*Juridica*/

	if idEstadoDocumento = 3 or idEstadoDocumento = 6 then	 
	
		 /*PASA A: RECHAZADO POR DPS*/
	    set @idEstadoSolicitud = 4;
	
		 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABINTADMIN_observaciones = observacion
		 where CABINTADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 3;
		 set @idEstadoSolicitud = 4; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 6;
		 	 
				   insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;
	
/*Seremi*/

	if idEstadoDocumento = 5 or idEstadoDocumento = 8 then	 
	
		 /*PASA A: RECHAZADO POR SEREMI*/
	    set @idEstadoSolicitud = 6;
	
		 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABINTADMIN_observaciones = observacion
		 where CABINTADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 3;
		 set @idEstadoSolicitud = 6; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 6;
		 	 
		      insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;

/*Contraloria*/	

	if idEstadoDocumento = 7 then	 
	
		 /*PASA A: RECHAZADO POR SEREMI*/
	    set @idEstadoSolicitud = 8;
	
		 update rapsinet_cabecerainternacionadmin set CABINTADMIN_idEstadoSolicitud = @idEstadoSolicitud, 
		 CABINTADMIN_observaciones = observacion
		 where CABINTADMIN_id=idSolicitud;
	
	    /*LOG*/
		 
		 set @tipoSolicitud = 3;
		 set @idEstadoSolicitud = 8; 
		 
		 insert into rapsinet_logsolicitudes(
		 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
		 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud,LOGSOL_observaciones)values(
		 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,
		 @idEstadoSolicitud,observacion);
		 
		 /*SEGURIDAD*/
		 
		 set @idAccion = 6;
		 	 
		      insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion, idSolicitud, @tipoSolicitud); 
		 set @mensaje = 'Visación rechazada exitósamente';
	
	end if;
	
	select @mensaje;	 
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprechazasoleva
DROP PROCEDURE IF EXISTS `rapsinet_sprechazasoleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprechazasoleva`(IN `idEstadoDocumento` INT, IN `idSolicitud` INT, IN `idUsuario` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(12), IN `observacion` VARCHAR(120))
BEGIN
/*DSP*/

 IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: rechazado */
			 
			 SET @idEstadoSolicitud = 2;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud,
			 obser_soleva = observacion  
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 2;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 15; /*RECHAZADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 SET @mensaje = 'Visación rechazada exitósamente';
	
END IF;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 4;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud,
			 obser_soleva = observacion  
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 4;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 15; /*APROBADO POR JURIDICA*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;
	
/*SEREMI*/

	IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 6;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud,
			 obser_soleva = observacion  
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 6;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 15; /*APROBADO POR SEREMI*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;	


	IF idEstadoDocumento = 7 THEN
	
			 /*PASA A: Rechazado CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 8;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud,
			 obser_soleva = observacion  
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 8;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 15; /*APROBADO POR SEREMI*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;	

	 SELECT @mensaje; 
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sprechaza_solinv
DROP PROCEDURE IF EXISTS `rapsinet_sprechaza_solinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sprechaza_solinv`(IN `idSolicitud` INT, IN `observacion` VARCHAR(200), IN `idEstadoDocumento` INT, IN `nombresUsuario` VARCHAR(120), IN `apellidosUsuario` VARCHAR(120), IN `rutUsuario` VARCHAR(12), IN `idUsuario` INT, IN `idDepartamento` INT)
BEGIN
/*DSP*/

 IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: rechazado */
			 
			 SET @idEstadoSolicitud = 2;
			
		 	 UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud,
			 CABSOLINV_observacion = observacion 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 2;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 3; /*RECHAZADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 SET @mensaje = 'Visación rechazada exitósamente';
	
END IF;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 4;
			
		 	 UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud,
			 CABSOLINV_observacion = observacion  
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 4;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 3; /*APROBADO POR JURIDICA*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;
	
/*SEREMI*/

	IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 6;
			
		 	 UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud,
			 CABSOLINV_observacion = observacion 	 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 6;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 3; /*APROBADO POR SEREMI*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;	


	IF idEstadoDocumento = 7 THEN
	
			 /*PASA A: Rechazado CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 8;
			
		 	 UPDATE rapsinet_cabecerasolicitudinv SET CABSOLINV_estadoSolicitud = @idEstadoSolicitud,
			 CABSOLINV_observacion = observacion 	 
			 WHERE  CABSOLINV_id=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 1;
			 set @idEstadoSolicitud = 8;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 3; /*APROBADO POR SEREMI*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
			 set @mensaje = 'Visación rechazada exitósamente';
	
END IF;	

	 SELECT @mensaje; 
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spregiones
DROP PROCEDURE IF EXISTS `rapsinet_spregiones`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spregiones`()
BEGIN
SELECT REG_ID,REG_titulo,CONCAT(REG_ROMANO," ",REG_TITULO) AS REGION FROM RAPSINET_REGIONES;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spregistraraltaadmin
DROP PROCEDURE IF EXISTS `rapsinet_spregistraraltaadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spregistraraltaadmin`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(80), IN `apellidosUsuario` VARCHAR(80), IN `rutUsuario` VARCHAR(80), IN `idUsuario` INT, IN `opcion` INT)
BEGIN
IF opcion = 1 THEN

			set @idEstablecimiento = (select DETALTHOSPADMIN_idCentroSalud from rapsinet_detallealtahospadmin where CABALTHOSPADMIN_id = idCabecera);	
			
			set @idCabecera = idCabecera;
			set @estadoSolicitud = 16;

			INSERT INTO rapsinet_registro_aa(REGAA_idCabecera,REGAA_fecha,USUA_id,USUA_nombres,USUA_apellidos,
			DEP_id, EST_id)values(idCabecera,now(),idUsuario,nombresUsuario,apellidosUsuario,idDepartamento,
			@idEstablecimiento);
				
		   UPDATE rapsinet_cabeceraaltahospadmin SET CABALTAHOSPADMIN_idEstadoSolicitud = @estadoSolicitud
	      WHERE CABALTAHOSPADMIN_id = idCabecera;
		
			 /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 16;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 22;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
		  
		  
		  set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido registrada.");

END IF;

IF opcion = 0 THEN
 
		set @estadoSolicitud = 10;
		set @idCabecera = idCabecera;
	    
		UPDATE rapsinet_cabecerainternacionadmin SET CABINTADMIN_idEstadoSolicitud= @estadoSolicitud
	   WHERE CABINTADMIN_id = idCabecera;
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 5;
			 set @idEstadoSolicitud = 10;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 18; /*cerrado*/
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 

	   
	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido cerrada.");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spregistrarevaluacion
DROP PROCEDURE IF EXISTS `rapsinet_spregistrarevaluacion`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spregistrarevaluacion`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombresUsuario` vARCHAR(50), IN `apellidosUsuario` VARCHAR(80), IN `rutUsuario` INT, IN `idUsuario` INT, IN `opcion` INT)
BEGIN
IF opcion = 1 THEN

			set @idEstablecimiento = (select hospital from rapsinet_cabeceraevatratamiento where cab_solevatrat = idCabecera);	
			set @idCabecera = idCabecera;
			set @estadoSolicitud = 16;

			INSERT INTO rapsinet_registro_et(REGET_idCabecera,REGET_fecha,USUA_id,USUA_nombres,USUA_apellidos,
			DEP_id, EST_id)values(idCabecera,now(),idUsuario,nombresUsuario,apellidosUsuario,idDepartamento,
			@idEstablecimiento);
				
		   UPDATE rapsinet_cabeceraevatratamiento SET estado_sol= @estadoSolicitud
	      WHERE cab_solevatrat = idCabecera;
		
		
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 16;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 20;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
		  
		  
		  set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido registrada.");

END IF;

IF opcion = 0 THEN

		set @estadoSolicitud = 10;
		set @idCabecera = idCabecera;
	    
		UPDATE rapsinet_cabeceraevatratamiento SET estado_sol= @estadoSolicitud
	   WHERE cab_solevatrat = idCabecera;
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 10;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 18; /*cerrado*/
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 

	   
	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido cerrada.");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spregistrarinternacion
DROP PROCEDURE IF EXISTS `rapsinet_spregistrarinternacion`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spregistrarinternacion`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombresUsuario` vARCHAR(80), IN `apellidosUsuario` vARCHAR(50), IN `rutUsuario` vARCHAR(50), IN `idUsuario` INT, IN `opcion` INT)
BEGIN
IF opcion = 1 THEN

			set @idEstablecimiento = (select DETINTADMIN_idEstablecimientoInt from rapsinet_detalleinternacionadmin where CABINTADMIN_id = idCabecera);	
			
			
			set @idCabecera = idCabecera;
			set @estadoSolicitud = 15;

			INSERT INTO rapsinet_registro_ia(REGIA_idCabecera,REGIA_fecha,USUA_id,USUA_nombres,USUA_apellidos,
			DEP_id, EST_id)values(idCabecera,now(),idUsuario,nombresUsuario,apellidosUsuario,idDepartamento,
			@idEstablecimiento);
				
		   UPDATE rapsinet_cabecerainternacionadmin SET CABINTADMIN_idEstadoSolicitud= @estadoSolicitud
	      WHERE CABINTADMIN_id = idCabecera;
		
			 /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 15;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 21;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 
		  
		  
		  set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido registrada.");

END IF;

IF opcion = 0 THEN

		set @estadoSolicitud = 10;
		set @idCabecera = idCabecera;
	    
		UPDATE rapsinet_cabecerainternacionadmin SET CABINTADMIN_idEstadoSolicitud= @estadoSolicitud
	   WHERE CABINTADMIN_id = idCabecera;
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 10;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 18; /*cerrado*/
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombresUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 

	   
	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "a sido cerrada.");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spresaltaadministrativa
DROP PROCEDURE IF EXISTS `rapsinet_spresaltaadministrativa`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spresaltaadministrativa`(IN `idCabeceraSolicitud` INT)
BEGIN

select 
	 cabalta.CABALTAHOSPADMIN_id,
	 concat(cabalta.CABALTAHOSPADMIN_nombrePac," ",cabalta.CABALTAHOSPADMIN_apaternoPac," ",cabalta.CABALTAHOSPADMIN_amaternoPac) as nombrecompleto_pac,
	 cabalta.CABALTAHOSPADMIN_rutPac, 
	 detalta.DETALTHOSPADMIN_idProfesionalMedico,
	 detalta.DETALTHOSPADMIN_idMedicoTrat,
	 concat(med.MED_nombres," ",med.MED_apellidos) as medico_tratante,
	 cabalta.CABALTAHOSPADMIN_idEstablecimiento,
	 estsol_1.EST_nombre as establecimiento,
	 cabalta.CABALTAHOSPADMIN_fechaActual,
	 detalta.DETALTHOSPADMIN_idCentroSalud,
	 estsol_2.EST_nombre as centro_salud,
	 detalta.DETALTHOSPADMIN_opciones,
	 estsol_1.ASSOL_id
	 from rapsinet_cabeceraaltahospadmin cabalta
	 inner join rapsinet_detallealtahospadmin detalta
	 on(cabalta.CABALTAHOSPADMIN_id = detalta.CABALTHOSPADMIN_id)
	 inner join rapsinet_establecimientos estsol_1
	 on(cabalta.CABALTAHOSPADMIN_idEstablecimiento = estsol_1.EST_id)
	 inner join rapsinet_establecimientos estsol_2
	 on(detalta.DETALTHOSPADMIN_idCentroSalud = estsol_2.EST_id)
	 inner join rapsinet_medicos med
	 on(med.MED_id = detalta.DETALTHOSPADMIN_idMedicoTrat)
	 where cabalta.CABALTAHOSPADMIN_id = idCabeceraSolicitud;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spresevaluacionytratamiento
DROP PROCEDURE IF EXISTS `rapsinet_spresevaluacionytratamiento`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spresevaluacionytratamiento`(IN `idCabeceraSolicitud` INT)
BEGIN

select 
	 cabeva.cab_solevatrat, /*NUMERO RESOLUCION*/
	 concat(deteva.nom_sol," ",deteva.apel_sol) as nombrecompleto_sol, /*NOMBRE SOLICITANTE*/
	 deteva.rut_sol, /*RUT SOLICITANTE*/
	 deteva.vincula_sol,
	 fam.TIPFAM_descripcion,
	 concat(cabeva.nombres_pac," ",cabeva.ap_pat," ",cabeva.ap_mat) as nombrecompleto_pac,
	 cabeva.rut_pac, /*RUT PACIENTE*/
	 DATE_FORMAT(now(),'%d-%m-%Y') as fecha_actual,
	 DATE_FORMAT(cabeva.fecha_sol,'%d-%m-%Y') as fecha_evaluacion,
	 deteva.nom_doc, /*NOMBRE MEDICO*/
	 estsol_1.EST_nombre as estab_solicitante, /*NOMBRE ESTABLECIMIENTO SOLICITANTE*/
	 estsol_1.ASSOL_id,
	 estsol_2.EST_nombre as estab_internacion,
	 deteva.det_opciones,
	 cabeva.dire_pac
	 
	 from rapsinet_cabeceraevatratamiento cabeva
	 inner join rapsinet_detalleevatratamiento deteva
	 on(cabeva.cab_solevatrat = deteva.cab_solevatrat)
	 inner join rapsinet_establecimientos estsol_1
	 on(cabeva.hospital = estsol_1.EST_id)
	 inner join rapsinet_establecimientos estsol_2
	 on(deteva.lugar_evaluacion = estsol_2.EST_id)
	 inner join rapsinet_tipofamiliar fam
	 on(deteva.vincula_sol = fam.TIPFAM_id)
where cabeva.cab_solevatrat = idCabeceraSolicitud;
	 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spresinternacionsolicitudadmin
DROP PROCEDURE IF EXISTS `rapsinet_spresinternacionsolicitudadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spresinternacionsolicitudadmin`(IN `idCabeceraSolicitud` INT)
BEGIN

 select 
	 cabintadmin.CABINTADMIN_id, /*NUMERO RESOLUCION*/
	 concat(detintadmin.DETINTADMIN_nombresSol," ",detintadmin.DETINTADMIN_aPaternoSol," ",detintadmin.DETINTADMIN_aMaternoSol) as nombrecompleto_sol, /*NOMBRE SOLICITANTE*/
	 detintadmin.DETINTADMIN_rutSol, /*RUT SOLICITANTE*/
	 detintadmin.DETINTADMIN_vinculacion,
	 concat(detintadmin.DETINTADMIN_nombresPac," ",detintadmin.DETINTADMIN_aPaternoPac," ",detintadmin.DETINTADMIN_aMaternoPac) as nombrecompleto_pac,
	 detintadmin.DETINTADMIN_rutPac, /*RUT PACIENTE*/
	 DATE_FORMAT(now(),'%d-%m-%Y') as fecha_actual,
	 DATE_FORMAT(cabintadmin.CABINTADMIN_fechaEvaluacion,'%d-%m-%Y') as fecha_evaluacion,
	 detintadmin.DETINTADMIN_nombreMedico, /*NOMBRE MEDICO*/
	 estsol_1.EST_nombre as estab_solicitante, /*NOMBRE ESTABLECIMIENTO SOLICITANTE*/
	 estsol_1.ASSOL_id,
	 estsol_2.EST_nombre as estab_internacion,
	 detintadmin.DETINTADMIN_opciones
	 
	 from rapsinet_cabecerainternacionadmin cabintadmin
	 inner join rapsinet_detalleinternacionadmin detintadmin
	 on(cabintadmin.CABINTADMIN_id = detintadmin.CABINTADMIN_id)
	 inner join rapsinet_establecimientos estsol_1
	 on(cabintadmin.CABINTADMIN_idEstablecimientoSol = estsol_1.EST_id)
	 inner join rapsinet_establecimientos estsol_2
	 on(detintadmin.DETINTADMIN_idEstablecimientoInt = estsol_2.EST_id)
	 where cabintadmin.CABINTADMIN_id = idCabeceraSolicitud;

END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spresinv
DROP PROCEDURE IF EXISTS `rapsinet_spresinv`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spresinv`(IN `idCabeceraSolicitud` INT)
BEGIN
	 select 
	 cabsolinv.CABSOLINV_id,
	 concat(med.MED_nombres," ",med.MED_apellidos) as nombre_medico,
	 concat(cabsolinv.CABSOLINV_nombresPaciente, " ", cabsolinv.CABSOLINV_apellidosPaciente) as nombre_paciente,
	 cabsolinv.CABSOLINV_rutPaciente,
	 cabsolinv.CABSOLINV_fechaInternacion,
	 DATE_FORMAT(now(),'%d-%m-%Y') as fecha_actual,
	 detsolinv.DETSOLINV_opcion
	 
	 from rapsinet_cabecerasolicitudinv cabsolinv
	 left join rapsinet_medicos med
	 on(cabsolinv.CABSOLINV_idMedico = med.MED_id)
	 inner join rapsinet_detallesolicitudinv detsolinv
	 on(detsolinv.CABSOLINV_id = cabsolinv.CABSOLINV_id)
	 where cabsolinv.CABSOLINV_id = idCabeceraSolicitud;
	 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spsegundallamadaET
DROP PROCEDURE IF EXISTS `rapsinet_spsegundallamadaET`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spsegundallamadaET`(IN `idCabecera` INT, IN `idDepartamento` INT, IN `nombreUsuario` VARCHAR(50), IN `apellidosUsuario` VARCHAR(50), IN `rutUsuario` VARCHAR(50), IN `idUsuario` INT, IN `opcion` INT)
BEGIN
IF opcion = 1 THEN

	  set @idEstadoSolicitud = 9;		
		
	  UPDATE rapsinet_cabeceraevatratamiento SET estado_sol= @idEstadoSolicitud
     WHERE cab_solevatrat = idCabecera;
 
 	  set @mensaje = concat("La solicitud Nº: ", idCabecera, "Enviada exitosamente");	 
 	  
 	  
 	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 9;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 5;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 


END IF;

IF opcion = 0 THEN

		set @estadoSolicitud = 12;
		set @idCabecera = idCabecera;
			
		 
		UPDATE rapsinet_cabeceraevatratamiento SET estado_sol= @estadoSolicitud
	   WHERE cab_solevatrat = idCabecera;
	   
	   
	   /*LOG*/
			 
			 set @tipoSolicitud = 3;
			 set @idEstadoSolicitud = 12;
			 
			 insert into rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idCabecera,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
		  set @idAccion = 17;
			 	 
		  insert into rapsinet_historicousuarios(
		  HIST_nombres,HIST_apellidos,HIST_rut,
		  HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
		  nombreUsuario, apellidosUsuario, rutUsuario,
		  now(), @idAccion, idCabecera, @tipoSolicitud); 

	   set @mensaje = concat("La solicitud Nº: ", @idCabecera, "Mantiene estado: En espera Segunda llamada");

END IF;

	select @mensaje;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spverdetalle_solintadmin
DROP PROCEDURE IF EXISTS `rapsinet_spverdetalle_solintadmin`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spverdetalle_solintadmin`(IN `SOL_ID` INT)
BEGIN
SELECT 
rapsinet_cabecerainternacionadmin.CABINTADMIN_id,CABINTADMIN_FECHAEVALUACION,CABINTADMIN_IDESTABLECIMIENTOSOL,EST_NOMBRE,
DETINTADMIN_DIAGNOSTICO,DETINTADMIN_OTROANTENEDENTE,DETINTADMIN_ULTIMOTRATAMIENTO,
DETINTADMIN_IDESTABLECIMIENTOINT,
(SELECT EST_NOMBRE FROM rapsinet_establecimientos
 WHERE EST_ID = DETINTADMIN_IDESTABLECIMIENTOINT) AS EST_INT
, DETINTADMIN_NOMBREMEDICO,DETINTADMIN_RUTMEDICO,
DETINTADMIN_CORREOMEDICO,DETINTADMIN_OPCIONES 
FROM rapsinet_cabecerainternacionadmin
LEFT JOIN rapsinet_detalleinternacionadmin ON
rapsinet_cabecerainternacionadmin.CABINTADMIN_id = rapsinet_detalleinternacionadmin.CABINTADMIN_id
LEFT JOIN rapsinet_establecimientos ON
rapsinet_cabecerainternacionadmin.CABINTADMIN_IDESTABLECIMIENTOSOL=rapsinet_establecimientos.EST_ID
WHERE rapsinet_cabecerainternacionadmin.CABINTADMIN_id=SOL_ID;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_spvisasoleva
DROP PROCEDURE IF EXISTS `rapsinet_spvisasoleva`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_spvisasoleva`(IN `idEstadoDocumento` INT, IN `idSolicitud` INT, IN `idUsuario` INT, IN `idDepartamento` INT, IN `nombresUsuario` VARCHAR(100), IN `apellidosUsuario` VARCHAR(100), IN `rutUsuario` VARCHAR(12))
BEGIN

/*DSP*/

 IF idEstadoDocumento = 1 or idEstadoDocumento = 2 or idEstadoDocumento = 4 THEN
	
			 /*PASA A: POR VISAR JURIDICA*/
			 
			 SET @idEstadoSolicitud = 3;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud 
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 3;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)VALUES(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 13; /*APROBADO POR DSP*/
			 	 
			    insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 SET @mensaje = 'Visación aprobada exitósamente';
	
END IF;

/*JURIDICA*/

	IF idEstadoDocumento = 3 or idEstadoDocumento = 6 THEN
	
			 /*PASA A: POR VISAR SEREMI*/
			 
			 set @idEstadoSolicitud = 5;
			
		 	 UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud 
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 5;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 13; /*APROBADO POR JURIDICA*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
END IF;
	
/*SEREMI*/

	IF idEstadoDocumento = 5 or idEstadoDocumento = 8 THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 7;
			
		 	UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud 
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 7;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 13; /*APROBADO POR SEREMI*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
END IF;	


IF idEstadoDocumento = 7  THEN
	
			 /*PASA A: POR VISAR CONTRALORIA*/
			 
			 set @idEstadoSolicitud = 9;
			
		 	UPDATE rapsinet_cabeceraevatratamiento SET estado_sol = @idEstadoSolicitud 
			 WHERE  cab_solevatrat=idSolicitud;
			
			 /*LOG*/
			 
			 set @tipoSolicitud = 2;
			 set @idEstadoSolicitud = 9;
			 
			 INSERT INTO rapsinet_logsolicitudes(
			 CABSOL_id, LOGSOL_fecha, LOGSOL_idUsuario,LOGSOL_idDepartamento,
			 LOGSOL_idTipoSolicitud,LOGSOL_idEstadoSolicitud)values(
			 idSolicitud,now(),idUsuario,idDepartamento,@tipoSolicitud,@idEstadoSolicitud);
			 
			 /*SEGURIDAD*/
			 
			 set @idAccion = 13; /*APROBADO POR SEREMI*/
			 	 
			     insert into rapsinet_historicousuarios(
				 HIST_nombres,HIST_apellidos,HIST_rut,
				 HIST_fecha, HIST_idAccion, HIST_idCabecera, HIST_idTipoSolicitud)values(
				 nombresUsuario, apellidosUsuario, rutUsuario,
				 now(), @idAccion,idSolicitud, @tipoSolicitud); 
	
			 set @mensaje = 'Visación aprobada exitósamente';
	
END IF;	


SET @mensaje = "Los datos no han sido ingresados";

SELECT @mensaje;  
		 
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.rapsinet_sp_listaserviciossalud
DROP PROCEDURE IF EXISTS `rapsinet_sp_listaserviciossalud`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `rapsinet_sp_listaserviciossalud`(IN `ID_REG` INT)
BEGIN
Select * from rapsinet_servicios_de_salud;
END//
DELIMITER ;


-- Dumping structure for table rapsinet.rapsinet_tipoarchivo
DROP TABLE IF EXISTS `rapsinet_tipoarchivo`;
CREATE TABLE IF NOT EXISTS `rapsinet_tipoarchivo` (
  `TIPARCH_id` int(10) DEFAULT NULL,
  `TIPARCH_EXTENSION` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_tipoarchivo: ~7 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_tipoarchivo` DISABLE KEYS */;
INSERT INTO `rapsinet_tipoarchivo` (`TIPARCH_id`, `TIPARCH_EXTENSION`) VALUES
	(1, 'JPG'),
	(2, 'PDF'),
	(3, 'DOC'),
	(4, 'PNG'),
	(5, 'XLS'),
	(6, 'XLSX'),
	(7, 'DOCX');
/*!40000 ALTER TABLE `rapsinet_tipoarchivo` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_tipofamiliar
DROP TABLE IF EXISTS `rapsinet_tipofamiliar`;
CREATE TABLE IF NOT EXISTS `rapsinet_tipofamiliar` (
  `TIPFAM_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPFAM_descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TIPFAM_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_tipofamiliar: ~10 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_tipofamiliar` DISABLE KEYS */;
INSERT INTO `rapsinet_tipofamiliar` (`TIPFAM_id`, `TIPFAM_descripcion`) VALUES
	(1, 'Padre'),
	(2, 'Madre'),
	(3, 'Primo'),
	(4, 'Prima'),
	(5, 'Hijo'),
	(6, 'Hija'),
	(7, 'Hermano'),
	(8, 'Hermana'),
	(9, 'Vecino'),
	(10, 'Vecina');
/*!40000 ALTER TABLE `rapsinet_tipofamiliar` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_tiposolicitudes
DROP TABLE IF EXISTS `rapsinet_tiposolicitudes`;
CREATE TABLE IF NOT EXISTS `rapsinet_tiposolicitudes` (
  `TIP_id` bigint(20) NOT NULL DEFAULT '0',
  `TIP_descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TIP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_tiposolicitudes: ~5 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_tiposolicitudes` DISABLE KEYS */;
INSERT INTO `rapsinet_tiposolicitudes` (`TIP_id`, `TIP_descripcion`) VALUES
	(1, 'Solicitud Internación No Voluntaria'),
	(2, 'Solicitud Evaluación y Tratamiento'),
	(3, 'Solicitud Internación Administrativa'),
	(5, 'Solicitud De Alta Hospitalización Administrativa'),
	(6, 'Solcititud De Traslado Por Evaluación y Tratamiento');
/*!40000 ALTER TABLE `rapsinet_tiposolicitudes` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_tipo_solicitante
DROP TABLE IF EXISTS `rapsinet_tipo_solicitante`;
CREATE TABLE IF NOT EXISTS `rapsinet_tipo_solicitante` (
  `TIPSOLI_id` int(10) DEFAULT NULL,
  `TIPSOLI_descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_tipo_solicitante: ~5 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_tipo_solicitante` DISABLE KEYS */;
INSERT INTO `rapsinet_tipo_solicitante` (`TIPSOLI_id`, `TIPSOLI_descripcion`) VALUES
	(1, 'FAMILIAR'),
	(2, 'EQ SALUD MENTAL'),
	(3, 'COMUNIDAD'),
	(4, 'EQ SALUD APS '),
	(5, 'OTRO');
/*!40000 ALTER TABLE `rapsinet_tipo_solicitante` ENABLE KEYS */;


-- Dumping structure for table rapsinet.rapsinet_usuarios
DROP TABLE IF EXISTS `rapsinet_usuarios`;
CREATE TABLE IF NOT EXISTS `rapsinet_usuarios` (
  `USUA_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `USUA_rut` varchar(12) NOT NULL,
  `USUA_nombres` varchar(50) NOT NULL,
  `USUA_apellidos` varchar(50) NOT NULL,
  `USUA_direccion` varchar(120) NOT NULL,
  `USUA_celular` varchar(10) DEFAULT NULL,
  `USUA_fono` varchar(10) DEFAULT NULL,
  `USUA_correo` varchar(80) NOT NULL,
  `USUA_usuario` varchar(30) NOT NULL,
  `USUA_contrasena` varchar(30) NOT NULL,
  `EST_id` int(10) NOT NULL,
  `DEPART_id` int(10) NOT NULL,
  `PER_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`USUA_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table rapsinet.rapsinet_usuarios: ~7 rows (approximately)
/*!40000 ALTER TABLE `rapsinet_usuarios` DISABLE KEYS */;
INSERT INTO `rapsinet_usuarios` (`USUA_id`, `USUA_rut`, `USUA_nombres`, `USUA_apellidos`, `USUA_direccion`, `USUA_celular`, `USUA_fono`, `USUA_correo`, `USUA_usuario`, `USUA_contrasena`, `EST_id`, `DEPART_id`, `PER_id`) VALUES
	(1, '14542091-1', 'Robert Ant', 'Rozas Navarro', 'El Olimpo', '77777777', '44444444', 'robert.rozas.n@gmail.com', 'rrozas', '1234', 1, 5, 4),
	(2, '17948622-9', 'Cristian Eduardo', 'Perez Araya', 'Miraflores', '5555555', '2020202', 'perez@gmail.com', 'cperez', '1234', 1, 5, 4),
	(6, '1-9', 'Sergio', 'CÃ¡rdenas', 'Lirios 123', '111111', '222222', 'sergio@cardenas.cl', 'user_dsp', 'dsp', 8, 1, 2),
	(7, '2-7', 'MarÃ­a', 'Correa', 'Olmecas 234', '55555', '666666', 'maria@correa.com', 'user_secretaria', 'secretaria', 13, 6, 5),
	(8, '3-4', 'Pedro', 'Cuellar', 'Avenida 456', '77777', '888888', 'pedro@cuellar', 'user_juridica', 'juridica', 11, 2, 6),
	(9, '3-5', 'Usuario', 'Seremi', 'seremi 234', '111111', '222222', 'usuario@seremi.cl', 'user_seremi', 'seremi', 4, 3, 7),
	(10, '1-2', 'Usuario', 'Contralor', 'Contraloria 1234', '111111', '222222', 'usuario@contralor', 'user_contraloria', 'contraloria', 5, 4, 8);
/*!40000 ALTER TABLE `rapsinet_usuarios` ENABLE KEYS */;


-- Dumping structure for procedure rapsinet.raspsinet_splistaunhospital
DROP PROCEDURE IF EXISTS `raspsinet_splistaunhospital`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `raspsinet_splistaunhospital`(IN `CODIGO` INT)
BEGIN
SELECT * FROM RAPSINET_ESTABLECIMIENTOS
WHERE EST_ID = CODIGO;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.raspsinet_splistaunmedico
DROP PROCEDURE IF EXISTS `raspsinet_splistaunmedico`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `raspsinet_splistaunmedico`(IN `ID_DOC` INT)
BEGIN
SELECT * FROM RAPSINET_MEDICOS 
WHERE MED_ID = ID_DOC;
END//
DELIMITER ;


-- Dumping structure for procedure rapsinet.sp_iniciosesion
DROP PROCEDURE IF EXISTS `sp_iniciosesion`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_iniciosesion`(IN `usuario` VARCHAR(50), IN `contrasena` VARCHAR(50))
BEGIN		
		select
			usua.USUA_nombres,
			usua.USUA_apellidos,
			usua.USUA_rut,
			usua.USUA_id,
			usua.DEPART_id,
			dep.DEP_descripcion,
			usua.PER_id,
			usua.EST_id
		from rapsinet_usuarios usua
		inner join rapsinet_departamentos dep
		on(usua.DEPART_id = dep.DEP_id)
		where usua.USUA_usuario = usuario and
		usua.USUA_contrasena = contrasena;
END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
