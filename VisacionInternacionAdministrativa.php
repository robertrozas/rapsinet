<?php
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include("DAO/DAOVisacionInternacionAdministrativa.php");
   
    $xajax = new xajax(); 

    $xajax->registerFunction("aprobar_solicitud");
    $xajax->registerFunction("rechazar_solicitud");


    $xajax->processRequests(); 
    
 
    $lista = $_SESSION['arrSolicitudesPorVisar'][0]['opciones'];
    $arrOpciones = explode(',',$lista);
    $OP = "OP";
    $i = 1;
    foreach ($arrOpciones as $opcion)
    {
        switch ($opcion)
        {
            case "A":
            {
                $elige = "checked";
                break;
            }
            case "B":
            {
                $elige = "checked";
                break;
            }
            case "C":
            {
                $elige = "checked";
                break;
            }
            case "":
            {
                $elige = "";
                break;
            }
            
        }
        
        $smarty->assign($OP.$i,$elige);
        $i++;
    }   
    
    function aprobar_solicitud($idCabeceraSolicitud,$estadoDocumento)
    { 
      $xaprobarsolicitud = new xajaxResponse();
      $xaprobarsolicitud->addAlert("$idCabeceraSolicitud");
      $xaprobarsolicitud->addAlert("$estadoDocumento");
      
      
      if ($estadoDocumento == 1)
      { 
        $aprobar_solicitud = new DAOVisacionInternacionAdministrativa();
        $aprobarSolicitud = $aprobar_solicitud->aprobar($idCabeceraSolicitud,$estadoDocumento);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        return $xaprobarsolicitud;  
      }  
      
      if ($estadoDocumento == 3)
      {
        $aprobar_solicitud = new DAOVisacionInternacionAdministrativa();
        $aprobarSolicitud = $aprobar_solicitud->aprobar($idCabeceraSolicitud,$estadoDocumento);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        return $xaprobarsolicitud;  
      }  
      
      if ($estadoDocumento == 5)
      {
 
        $aprobar_solicitud = new DAOVisacionInternacionAdministrativa();
        $aprobarSolicitud = $aprobar_solicitud->aprobar($idCabeceraSolicitud,$estadoDocumento);

        $xaprobarsolicitud->addAlert($aprobarSolicitud['@mensaje']);
        return $xaprobarsolicitud;  
      } 
      return $xaprobarsolicitud; 
       
    }
  
    $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
    $smarty->display('VisacionInternacionAdministrativa.tpl');
?>

