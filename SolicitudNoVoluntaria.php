<?php

    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_SolicitudNoVoluntaria.php';
    
    $xajax = new xajax(); 

    $xajax->registerFunction("test");
    $xajax->registerFunction("llena_medicos");
    $xajax->registerFunction("llena_servicios");
    $xajax->registerFunction("validarut");
    $xajax->registerFunction("llena_establecimientos");
    $xajax->registerFunction("texto");
    $xajax->registerFunction("numero");
    $xajax->registerFunction("correo");
    $xajax->registerFunction("vacio");
  

      
    $xajax->processRequests(); 

    function correo($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}]/g,'');});");
      return $texto;                  

    } 

    function texto($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^a-zA-ZáéíóúAÉÍÓÚñÑ\s]/g,'');});");
      return $texto;
    }

   function numero($nombre_input)
    {
      $texto = new xajaxResponse();
      $texto->addScript("jQuery('#".$nombre_input."').keyup(function () { this.value = this.value.replace(/[^0-9]/g,'');});");
      return $texto;
    }

    function validarut($rut,$nombre_input)
   { 
    $valida = new xajaxResponse();

    $pos = strripos($rut,"-");

    if($pos === false)
    {}
    else
     {
     $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];

    if($digito!=""){
     $s=1;
      for($m=0;$r!=0;$r/=10)
        $s=($s+$r%10*(9-$m++%6))%11;
      $dv = chr($s?$s+47:75);

    if($dv==$digito)
    { }
    else
    { $valida->addAlert("Rut incorrecto, ingrese nuevamente"); 
      $valida->addScript("document.formSol.".$nombre_input.".focus();");}  }
    
      } 
    return $valida;
    }


    function validar($rut)
    {
   
     $lista=explode('-',$rut);
     $r=$lista[0]; 
     $digito = $lista[1];
    
    $s=1;
    for($m=0;$r!=0;$r/=10)
      $s=($s+$r%10*(9-$m++%6))%11;
    $dv = chr($s?$s+47:75);

    if($dv==$digito)
     {$validar = "si";}
    else
    {$validar = "no";}  
 
    return $validar;

    }

    
    
    function test($form)
    {
       $test = new xajaxResponse();
       $solinv = new DAO_SolicitudNoVoluntaria();
       $sw="si";

       foreach ($form as $campos) {
         if($campos == "" || empty($campos))
         {$sw="no";} //Todos los campos completados
       }
        
       $valido=validar($form['rut']); 
       
       
       if($sw == "si" && $valido=="si") 
       {$solinv->ingresa_solicitudinv($form);
        $test->addAlert("Solicitud ingresada exitosamente");}
       else
       {$test->addAlert("Complete formulario..faltan campos o bien Rut erroneo");}
        

       return $test;
    }

    function llena_medicos()
    {

        $llena = new xajaxResponse();
        $combo = new DAO_SolicitudNoVoluntaria(); 


        $arreglo = $combo->llena_medicos();

        foreach ($arreglo as $valor) 
        {
            $llena->CreaOpcion('medicos',$valor['nombre'],$valor['codigo']);
        }     

        return $llena;
    }

    function llena_servicios()
    {

        $llena = new xajaxResponse();
        $combo = new DAO_SolicitudNoVoluntaria(); 


        $arreglo = $combo->llena_servicios();

        foreach ($arreglo as $valor) 
        {
            $llena->CreaOpcion('servicios',$valor['descripcion'],$valor['codigo']);
        }     

        return $llena;
    }

    function llena_establecimientos()
    {

      $llena = new xajaxResponse();
      $combo = new DAO_SolicitudNoVoluntaria(); 


      $arreglo = $combo->llena_establecimientos();

      foreach ($arreglo as $valor) 
      {
          $llena->CreaOpcion('establecimiento',utf8_encode($valor['nombre']),$valor['codigo']);
      }     

      return $llena;
    }

   if(isset($_SESSION['USUA_nombres'])){
   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
   $smarty->display('SolicitudNoVoluntaria.tpl');
   }
   else
   {$smarty->display('404.tpl');}

?>


