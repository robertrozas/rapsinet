<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_Roles.php';
   
   	
    
    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("muestra_modifica");
	$xajax->registerFunction("lista_roles");
	$xajax->registerFunction("lista_funciones");
	$xajax->registerFunction("ingresa_funciones_perfil");
	$xajax->registerFunction("ingresa_perfil");
	$xajax->registerFunction("elimina_perfil");
	$xajax->registerFunction("limpia");
	$xajax->processRequests(); 


	function muestra_modifica($codigo,$descripcion)
	{
		

		$funcion = new xajaxResponse();
		$muestra = new DAO_Roles();
		$arrLimpia = array();

		$_SESSION['arrFuncPerfil'] = $muestra->lista_funciones_x_perfil($codigo);
		
        	$funcion->addScript('document.formUser.reset();'); //reseteo el formulario

        foreach ($_SESSION['arrFuncPerfil'] as $fila) {

        	 $checkea ="document.formUser.seleccion".$fila['codigo'].".click();";
        	 $funcion->addScript($checkea);
        }


        $funcion->addAssign("perfil","value",$descripcion);
		$funcion->addAssign("codigo","value",$codigo);
		
		
		return $funcion;
	}

	
	function lista_roles()
	{

	   global $smarty;
	   $xusuarios = new xajaxResponse();
	   $arr_usuarios = new DAO_Roles();
	   $_SESSION['arrRoles'] = $arr_usuarios->lista_roles();
	   $tabla = $smarty->fetch('grilla_roles.tpl');
	   $xusuarios->addAssign("view1","innerHTML",$tabla);
	   
	   return $xusuarios;

	}

	function lista_funciones()
	{

	   global $smarty;
	   $xusuarios = new xajaxResponse();
	   $arr_usuarios = new DAO_Roles();
	   $_SESSION['arrFunciones'] = $arr_usuarios->lista_funciones();
	   $tabla = $smarty->fetch('grilla_funciones.tpl');
	   $xusuarios->addAssign("funciones","innerHTML",$tabla);
	   return $xusuarios;

	}

    
    function ingresa_funciones_perfil($form)
    {
       $ingresa = new xajaxResponse();
       $i = 0;
       
       $funcionperfil = new DAO_Roles();
       $per_id = $form['codigo'];
       
       if(empty($per_id))
       {
       	 $ingresa->addAlert("Debe seleccionar un Perfil desde la Grilla");
       }
        else
        {
	       $funcionperfil->elimina_perfil_funciones($per_id); //elimino perfiles_funcionalidades asociados
	       foreach ($form as $elemento) {
	       	  
	       	  if ($i>1)
	           $funcionperfil->asigna_perfil_funciones($per_id,$elemento); //agrego nuevos perfiles_funcionalidades 
	        	
	          $i++;
	        } 
	        $ingresa->addAlert("Funciones asignadas OK");
        }	
       
       $ingresa->addScript('document.formUser.reset();'); //reseteo el formulario

       return $ingresa;
    }
   
   function ingresa_perfil($form)
   {
      $inperfil = new xajaxResponse();
      $perfil = new DAO_Roles();
      
      if(empty($form))
      { $inperfil->addAlert("Campo Vacio");}
      else{$perfil->ingresa_perfil($form);} 
            

      $inperfil->addScript('xajax_lista_roles();');

      return $inperfil;
   }

    function elimina_perfil($per_id)
    {
       $respuesta = new xajaxResponse();
     
       $elimina = new DAO_Roles();
       $elimina->elimina_perfil($per_id); 
       
       $respuesta->addScript('xajax_lista_roles();');
       $respuesta->addAlert("Perfil y funciones asociadas eliminados");
       $respuesta->addScript('document.formUser.reset();'); //reseteo el formulario
       return $respuesta;
    } 

    function limpia()
    {
       $limpia = new xajaxResponse();
              
        $limpia->addAssign("codigo","value","");
       return $limpia;
    }

	
    if(isset($_SESSION['USUA_nombres']))
    {    $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
         $smarty->display('MantenedorRoles.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
