<?php

/* set the cache limiter to 'private' */

session_cache_limiter('private');
/* set the cache expire to 30 minutes */
session_cache_expire(2);
/* start the session */

?>