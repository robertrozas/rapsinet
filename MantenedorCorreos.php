<?php
   
    session_start();
    include 'xajax/xajax.inc.php';
    include("include/include.php");
    include 'DAO/DAO_Correos.php';
    
   	
    
    $xajax = new xajax(); 
	
	
	$xajax->registerFunction("muestra_modifica");
	$xajax->registerFunction("elimina");
	$xajax->registerFunction("lista_correos");
        $xajax->registerFunction("modificar_correo");
	$xajax->processRequests(); 

        function modificar_correo($form)
	{  
            $id = $_SESSION['clave'];
            $xmodifica = new xajaxResponse();
            $descripcion = $form['descripcion'];
            $correo = $form['mail'];
            
            
            $modifica = new DAO_Correos();
            $modifica_correo = $modifica->modificar_correo($id, $descripcion, $correo);
            $xmodifica->addAlert($modifica_correo['@mensaje']);
            $xmodifica->addScript('xajax_lista_correos();');
            $xmodifica->addScript('document.formUser.reset();');
            return $xmodifica;
            
        }
        
        

	function muestra_modifica($codigo)
	{
		$funcion = new xajaxResponse();
		$muestra = new DAO_Correos();
		$arrMuestra = array();
		$arrMuestra = $muestra->lista_uno($codigo);
                $_SESSION['clave'] = $codigo;
		$funcion->addAssign("descripcion","value",$arrMuestra['CO_descripcion']);
		$funcion->addAssign("mail","value",$arrMuestra['CO_mail']);

		return $funcion;
	}

	function elimina($codigo)
	{
	  $elimina = new xajaxResponse();
          $borra_usuario = new DAO_Usuarios();
          $borra_usuario->elimina_usuario($codigo);
          $elimina->addScript('xajax_lista_usuarios();');
          $elimina->addAlert("Ha eliminado al Usuario id: ".$codigo); 
	  return $elimina;
	}
        
            
      

	function lista_correos()
	{

	   global $smarty;
	   $xcorreos = new xajaxResponse();
	   $arr_usuarios = new DAO_Correos();
	   $_SESSION['arrCorreos'] = $arr_usuarios->lista_correos();
	   $tabla = $smarty->fetch('grilla_correos.tpl');
	   $xcorreos->addAssign("correos","innerHTML",$tabla);
	   return $xcorreos;

	}
	  
    
    if(isset($_SESSION['USUA_nombres']))
    {   $smarty->assign('xajax_js', $xajax->getJavascript('xajax'));
        $smarty->display('MantenedorCorreos.tpl');
      }
    else
    {$smarty->display('404.tpl');}
?>
